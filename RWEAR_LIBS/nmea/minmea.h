/*
 * Copyright © 2014 Kosma Moczek <kosma@cloudyourcar.com>
 * This program is free software. It comes without any warranty, to the extent
 * permitted by applicable law. You can redistribute it and/or modify it under
 * the terms of the Do What The Fuck You Want To Public License, Version 2, as
 * published by Sam Hocevar. See the COPYING file for more details.
 */

#ifndef MINMEA_H
#define MINMEA_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <time.h>
#include <math.h>

#define MINMEA_MAX_LENGTH 200
	
#define FIXEDPOINT_MAX 20000
#define FIXEDPOINT_MIN -20000
#define FIXEDPOINT_RES 8388608
#define FIXEDPOINT_PARTITION (double) 0.0047683715820312

#define DEGRESS_FIXED_MAX 60
#define DEGRESS_FIXED_MIN 0
#define DEGRESS_FIXED_RES 8388608
#define DEGRESS_FIXED_PARTITION (double) 7.1525573730469e-6


//#define GLL
#define RMC
#define GGA
//#define GSV
#define VTG
	
enum minmea_sentence_id {
    MINMEA_INVALID = -1,
    MINMEA_UNKNOWN = 0,
		MINMEA_SENTENCE_DELTA,
    MINMEA_SENTENCE_RMC,
    MINMEA_SENTENCE_GGA,
    MINMEA_SENTENCE_GSA,
    MINMEA_SENTENCE_GLL,
    MINMEA_SENTENCE_GST,
    MINMEA_SENTENCE_GSV,
    MINMEA_SENTENCE_VTG,
		MINMEA_SENTENCE_OPTMZ,
};
#ifdef MINMEA_FIXED_POINT
__packed struct minmea_float {
	signed int 		value		:24;
};
#else
__packed struct minmea_float {
	double 		value;
};
#endif

__packed struct minmea_degress {
	signed int deg :9;
	unsigned int min : 23; 	
};

__packed struct minmea_date {
	unsigned int day 		:5;
  unsigned int month	:4;
  unsigned int year		:15;
};

__packed struct minmea_time {
	unsigned int hours		:5;
	unsigned int minutes	:6;
	unsigned int seconds	:6;
  //  uint16_t microseconds; //eliminado para ahorrar espacio
};

__packed struct minmea_sentence_rmc {
		uint8_t  type;
    struct minmea_time time;
    struct minmea_degress latitude;
    struct minmea_degress longitude;
		bool valid;
    struct minmea_float speed;
    struct minmea_float course;
    struct minmea_date date;
    struct minmea_float variation;
};

__packed struct minmea_sentence_gga {
		uint8_t  type;
    struct minmea_time time;
    struct minmea_degress latitude;
    struct minmea_degress longitude;
    uint8_t fix_quality;
    uint8_t satellites_tracked;
    struct minmea_float hdop;
    struct minmea_float altitude; char altitude_units;
    struct minmea_float height; char height_units;
    uint8_t dgps_age;
};

#ifdef SAVE_PACKET_STRUCTURE
typedef __packed struct minmea_sentence_optimized
	{
	uint8_t type	;
  uint8_t hours ;
	uint8_t minutes;
	uint8_t seconds;
  uint8_t grad_latitude; 
	int grad_lat_decimal;
	uint8_t grad_long;
  int grad_lng_decimal; 
	uint8_t velocity; 
	int16_t course; 
	uint8_t fix_quality  ; 
	uint8_t satellites_tracked ; 
	uint8_t hdop	; 
	int16_t	altitude; 
}minmea_sentence_optimized_t;
#else
typedef struct minmea_sentence_optimized
	{
	uint8_t type	;
  uint8_t hours ;
	uint8_t minutes;
	uint8_t seconds;
  uint8_t grad_latitude; 
	int grad_lat_decimal;
	uint8_t grad_long;
  int grad_lng_decimal; 
	uint8_t velocity; 
	int16_t course; 
	uint8_t fix_quality  ; 
	uint8_t satellites_tracked ; 
	uint8_t hdop	; 
	int16_t	altitude; 
}minmea_sentence_optimized_t;
#endif


enum minmea_gll_status {
    MINMEA_GLL_STATUS_DATA_VALID = 'A',
    MINMEA_GLL_STATUS_DATA_NOT_VALID = 'V',
};

// FAA mode added to some fields in NMEA 2.3.
enum minmea_faa_mode {
    MINMEA_FAA_MODE_AUTONOMOUS = 'A',
    MINMEA_FAA_MODE_DIFFERENTIAL = 'D',
    MINMEA_FAA_MODE_ESTIMATED = 'E',
    MINMEA_FAA_MODE_MANUAL = 'M',
    MINMEA_FAA_MODE_SIMULATED = 'S',
    MINMEA_FAA_MODE_NOT_VALID = 'N',
    MINMEA_FAA_MODE_PRECISE = 'P',
};

__packed struct minmea_sentence_gll {
		uint8_t  type;
		struct minmea_time time;
    struct minmea_degress latitude;
    struct minmea_degress longitude;
    char status;
    char mode;
};

__packed struct minmea_sentence_gst {
    struct minmea_time time;
    struct minmea_float rms_deviation;
    struct minmea_float semi_major_deviation;
    struct minmea_float semi_minor_deviation;
    struct minmea_float semi_major_orientation;
    struct minmea_float latitude_error_deviation;
    struct minmea_float longitude_error_deviation;
    struct minmea_float altitude_error_deviation;
};

enum minmea_gsa_mode {
    MINMEA_GPGSA_MODE_AUTO = 'A',
    MINMEA_GPGSA_MODE_FORCED = 'M',
};

enum minmea_gsa_fix_type {
    MINMEA_GPGSA_FIX_NONE = 1,
    MINMEA_GPGSA_FIX_2D = 2,
    MINMEA_GPGSA_FIX_3D = 3,
};

__packed struct minmea_sentence_gsa {
    char mode;
    uint8_t fix_type;
    uint8_t sats[12];
    struct minmea_float pdop;
    struct minmea_float hdop;
    struct minmea_float vdop;
};

__packed struct minmea_sat_info {
    uint8_t nr;
    uint8_t elevation;
    uint8_t azimuth; //deberia ser int16, lo llevo a uint8 por espacio --> modificar i por I en parser
    uint8_t snr;
};

__packed struct minmea_sentence_gsv {
		uint8_t  type;
    uint8_t total_msgs;
    uint8_t msg_nr;
    uint8_t total_sats;
    struct minmea_sat_info sats[4];
};

__packed struct minmea_sentence_vtg {
		uint8_t  type;
    struct minmea_float true_track_degrees;
    struct minmea_float magnetic_track_degrees;
    struct minmea_float speed_knots;
    struct minmea_float speed_kph;
    enum minmea_faa_mode faa_mode;
};

__packed struct minmea_sentence_delta {
	unsigned int type : 2;
	unsigned int Time	:5;
	signed int Long 	:10;
	signed int Lat		:10;
	signed int Alt  	:5;//7;
};

union minmea_frames_u {
	struct minmea_sentence_rmc RMCFrame; 
	struct minmea_sentence_gga GGAFrame; 
	struct minmea_sentence_gll GLLFrame;	
	struct minmea_sentence_gsv GSVFrame;
	struct minmea_sentence_vtg VTGFrame;
	struct minmea_sentence_optimized OPTFrame;
	struct minmea_sentence_delta Delta;
};


#define UNIONSIZE 56 

typedef union minmea_frame_t {
	union minmea_frames_u Data;
	uint8_t Union[UNIONSIZE]; 
} minmea_frame_t;

uint8_t minmea_GetFrameSize(enum minmea_sentence_id frame);
/**
 * Calculate raw sentence checksum. Does not check sentence integrity.
 */
uint8_t minmea_checksum(const char *sentence);

/**
 * Check sentence validity and checksum. Returns true for valid sentences.
 */
bool minmea_check(const char *sentence, bool strict);

/**
 * Determine talker identifier.
 */
bool minmea_talker_id(char talker[3], const char *sentence);

/**
 * Determine sentence identifier.
 */
enum minmea_sentence_id minmea_sentence_id(const char *sentence, bool strict);

/**
 * Scanf-like processor for NMEA sentences. Supports the following formats:
 * c - single character (char *)
 * d - direction, returned as 1/-1, default 0 (int *)
 * f - fractional, returned as value + scale (int *, int *)
 * I - decimal, default zero (int16_t *)
 * i - decimal, default zero (uint8_t *)  // agregado por mi para ahorrar espacio
 * s - string (char *)
 * t - talker identifier and type (char *)
 * T - date/time stamp (int *, int *, int *)
 * Returns true on success. See library source code for details.
 */
bool minmea_scan(const char *sentence, const char *format, ...);

/*
 * Parse a specific type of sentence. Return true on success.
 */
bool minmea_parse_rmc(struct minmea_sentence_rmc *frame, const char *sentence);
bool minmea_parse_gga(struct minmea_sentence_gga *frame, const char *sentence);
bool minmea_parse_gsa(struct minmea_sentence_gsa *frame, const char *sentence);
bool minmea_parse_gll(struct minmea_sentence_gll *frame, const char *sentence);
bool minmea_parse_gst(struct minmea_sentence_gst *frame, const char *sentence);
bool minmea_parse_gsv(struct minmea_sentence_gsv *frame, const char *sentence);
bool minmea_parse_vtg(struct minmea_sentence_vtg *frame, const char *sentence);

/**
 * Convert GPS UTC date/time representation to a UNIX timestamp.
 */
//int minmea_gettime(struct timespec *ts, const struct minmea_date *date, const struct minmea_time *time_);

uint8_t minmea_getSize(enum minmea_sentence_id type);

bool minnmea_lineParser(const char * line,minmea_frame_t * f);

struct minmea_float double_to_fixedpoint(double input);
double  fixedpoint_to_double(struct minmea_float input);
struct minmea_degress coord_to_fixedpoint(double input);

#ifdef SAVE_PACKET_STRUCTURE
void cord_to_appformat(struct minmea_degress input,uint8_t * deg, __packed int * min);
#else
void cord_to_appformat(struct minmea_degress input,uint8_t * deg, int * min);
#endif

bool minmea_frame_to_optimized(const minmea_frame_t * input ,struct minmea_sentence_optimized * output);
/**
 * Rescale a fixed-point value to a different scale. Rounds towards zero.
 */
 /*
static inline int_least32_t minmea_rescale(struct minmea_float *f, int_least32_t new_scale)
{
    if (f->scale == 0)
        return 0;
    if (f->scale == new_scale)
        return f->value;
    if (f->scale > new_scale)
        return (f->value + ((f->value > 0) - (f->value < 0)) * f->scale/new_scale/2) / (f->scale/new_scale);
    else
        return f->value * (new_scale/f->scale);
}
*/

/**
 * Convert a fixed-point value to a floating-point value.
 * Returns NaN for "unknown" values.
 */
 /*
static inline float minmea_tofloat(struct minmea_float *f)
{
    if (f->scale == 0)
        return NAN;
    return (float) f->value / (float) f->scale;
}
*/
/**
 * Convert a raw coordinate to a floating point DD.DDD... value.
 * Returns NaN for "unknown" values.
 */
 /*
static inline float minmea_tocoord(struct minmea_float *f)
{
    if (f->scale == 0)
        return NAN;
    int_least32_t degrees = f->value / (f->scale * 100);
    int_least32_t minutes = f->value % (f->scale * 100);
    return (float) degrees + (float) minutes / (60 * f->scale);
}
*/
#ifdef __cplusplus
}
#endif

#endif /* MINMEA_H */

/* vim: set ts=4 sw=4 et: */
