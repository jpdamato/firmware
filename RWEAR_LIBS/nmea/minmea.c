/*
 * Copyright © 2014 Kosma Moczek <kosma@cloudyourcar.com>
 * This program is free software. It comes without any warranty, to the extent
 * permitted by applicable law. You can redistribute it and/or modify it under
 * the terms of the Do What The Fuck You Want To Public License, Version 2, as
 * published by Sam Hocevar. See the COPYING file for more details.
 */

#include "minmea.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <time.h>

#define boolstr(s) ((s) ? "true" : "false")


static int hex2int(char c)
{
    if (c >= '0' && c <= '9')
        return c - '0';
    if (c >= 'A' && c <= 'F')
        return c - 'A' + 10;
    if (c >= 'a' && c <= 'f')
        return c - 'a' + 10;
    return -1;
}

struct minmea_float double_to_fixedpoint(double input)
{
	double compare = 0;
	struct minmea_float out;
	#ifdef MINMEA_FIXED_POINT

	if (input >  FIXEDPOINT_MAX)
	{
		out.value = (FIXEDPOINT_RES - 1);
		return out;
	}
	
	if (input < FIXEDPOINT_MIN)
	{
		out.value = (-1) * (FIXEDPOINT_RES - 1);
		return out;
	}
	out.value = round( (input-FIXEDPOINT_MIN) / FIXEDPOINT_PARTITION);
#else
	out.value = input;
#endif
		return out;

}

double  fixedpoint_to_double(struct minmea_float input)
{
#ifdef MINMEA_FIXED_POINT
	return (double) FIXEDPOINT_MIN + input.value * FIXEDPOINT_PARTITION;
#else
	return input.value;
#endif	
}

struct minmea_degress coord_to_fixedpoint(double input)
{
	double compare = 0;
	double min = 0;
	struct minmea_degress out;
	out.deg = input / 100;
	min = input - out.deg * 100 - DEGRESS_FIXED_MIN;
	
	if (min < 0)
		min = min * (-1);

	out.min = round(min / DEGRESS_FIXED_PARTITION);
		
	return out;
}

#ifdef SAVE_PACKET_STRUCTURE
void cord_to_appformat(struct minmea_degress input,uint8_t * deg, __packed int * min)
{
	*deg = abs(input.deg);
	if (input.deg < 0)
		*min = -1000000 * (input.min * DEGRESS_FIXED_PARTITION + DEGRESS_FIXED_MIN) / 60;
	else
	*min = 1000000 * (input.min * DEGRESS_FIXED_PARTITION + DEGRESS_FIXED_MIN) / 60;
}
#else
void cord_to_appformat(struct minmea_degress input,uint8_t * deg, int * min)
{
	*deg = abs(input.deg);
	if (input.deg < 0)
		*min = -1000000 * (input.min * DEGRESS_FIXED_PARTITION + DEGRESS_FIXED_MIN) / 60;
	else
	*min = 1000000 * (input.min * DEGRESS_FIXED_PARTITION + DEGRESS_FIXED_MIN) / 60;
}
#endif

uint8_t minmea_checksum(const char *sentence)
{
    // Support senteces with or without the starting dollar sign.
    if (*sentence == '$')
        sentence++;

    uint8_t checksum = 0x00;

    // The optional checksum is an XOR of all bytes between "$" and "*".
    while (*sentence && *sentence != '*')
        checksum ^= *sentence++;

    return checksum;
}

bool minmea_check(const char *sentence, bool strict)
{
    uint8_t checksum = 0x00;
		uint8_t expected;
    // Sequence length is limited.
    if (strlen(sentence) > MINMEA_MAX_LENGTH + 3)
        return false;

    // A valid sentence starts with "$".
    if (*sentence++ != '$')
        return false;

    // The optional checksum is an XOR of all bytes between "$" and "*".
    while (*sentence && *sentence != '*' && isprint((unsigned char) *sentence))
        checksum ^= *sentence++;

    // If checksum is present...
    if (*sentence == '*') {
        // Extract checksum.
        sentence++;
        int upper = hex2int(*sentence++);
        if (upper == -1)
            return false;
        int lower = hex2int(*sentence++);
        if (lower == -1)
            return false;
         expected = upper << 4 | lower;

        // Check for checksum mismatch.
        if (checksum != expected)
            return false;
    } else if (strict) {
        // Discard non-checksummed frames in strict mode.
        return false;
    }

    // The only stuff allowed at this point is a newline.
    if (*sentence++ != '\r' || *sentence++ != '\n')
     return false;

    return true;
}

static inline bool minmea_isfield(char c) {
    return isprint((unsigned char) c) && c != ',' && c != '*';
}

bool minmea_scan(const char *sentence, const char *format, ...)
{
    bool result = false;
    bool optional = false;
    va_list ap;
    va_start(ap, format);

    const char *field = sentence;
#define next_field() \
    do { \
        /* Progress to the next field. */ \
        while (minmea_isfield(*sentence)) \
            sentence++; \
        /* Make sure there is a field there. */ \
        if (*sentence == ',') { \
            sentence++; \
            field = sentence; \
        } else if(*sentence == '\r') \
						return result; \
				else \
					field = NULL; \
    } while (0)

    while (*format) {
        char type = *format++;

        if (type == ';') {
            // All further fields are optional.
            optional = true;
            continue;
        }

        if (!field && !optional) {
            // Field requested but we ran out if input. Bail out.
            goto parse_error;
        }

        switch (type) {
            case 'c': { // Single character field (char).
                char value = '\0';

                if (field && minmea_isfield(*field))
                    value = *field;

                *va_arg(ap, char *) = value;
            } break;

            case 'd': { // Single character direction field (int).
                int16_t value = 0;

                if (field && minmea_isfield(*field)) {
                    switch (*field) {
                        case 'N':
                        case 'E':
                            value = 1;
                            break;
                        case 'S':
                        case 'W':
                            value = -1;
                            break;
                        default:
                            goto parse_error;
                    }
                }

                *va_arg(ap, int *) = value;
            } break;

            case 'f':
						case 'F':
							{ // Fractional value with scale (struct minmea_float).
								double decimal = 0;
								float decimal_pivot = 0.1;
								int32_t value = - 1;
                int8_t sign = 0;
             
								bool isDecimal = false;

                if (field) {
                    while (minmea_isfield(*field)) {
                        if (*field == '+' && !sign && value == -1) {
                            sign = 1;
                        } else if (*field == '-' && !sign && value == -1) {
                            sign = -1;
                        } else if (isdigit((unsigned char) *field)) {
                            uint8_t digit = *field - '0';
                            if (value == -1)
                                value = 0;
                           if(!isDecimal)
															value = (10 * value) + digit;
                            else 
														{
                              decimal += decimal_pivot * digit;
															decimal_pivot /=10;
														//	decimal = decimal * 10 + digit;
														//	decimal_pivot++;
														}
                        } else if (*field == '.') {
                            isDecimal = true;
                        } else if (*field == ' ') {
                            /* Allow spaces at the start of the field. Not NMEA
                             * conformant, but some modules do this. */
                            if (sign != 0 || value != -1)
                                goto parse_error;
                        } else {
                            goto parse_error;
                        }
                        field++;
                    }
                }

                decimal += (double) value;
								
								if (sign != 0)
									decimal *= sign;
								if (type == 'F')			
									*va_arg(ap, struct minmea_degress *) = coord_to_fixedpoint(decimal);
								else
									*va_arg(ap, struct minmea_float *) = double_to_fixedpoint(decimal);
            } break;
						case 'i':	
            case 'I': { // Integer value, default 0 (int).
                int16_t value = 0;

                if (field) {
                    char *endptr;
                    value = (int16_t) strtol(field, &endptr, 10);
                    if (minmea_isfield(*endptr))
                        goto parse_error;
                }
								if (type == 'I')
											*va_arg(ap, int16_t *) = value;
								else
											*va_arg(ap, uint8_t *) = value;
            } break;

            case 's': { // String value (char *).
                char *buf = va_arg(ap, char *);

                if (field) {
                    while (minmea_isfield(*field))
                        *buf++ = *field++;
                }

                *buf = '\0';
            } break;

            case 't': { // NMEA talker+sentence identifier (char *).
                // This field is always mandatory.
                if (!field)
                    goto parse_error;

                if (field[0] != '$')
                    goto parse_error;
                for (int f=0; f<5; f++)
                    if (!minmea_isfield(field[1+f]))
                        goto parse_error;

                char *buf = va_arg(ap, char *);
                memcpy(buf, field+1, 5);
                buf[5] = '\0';
            } break;

            case 'D': { // Date (int, int, int), -1 if empty.
                struct minmea_date *date = va_arg(ap, struct minmea_date *);

                int d = -1, m = -1, y = -1;

                if (field && minmea_isfield(*field)) {
                    // Always six digits.
                    for (int f=0; f<6; f++)
                        if (!isdigit((unsigned char) field[f]))
                            goto parse_error;

                    char dArr[] = {field[0], field[1], '\0'};
                    char mArr[] = {field[2], field[3], '\0'};
                    char yArr[] = {field[4], field[5], '\0'};
                    d = strtol(dArr, NULL, 10);
                    m = strtol(mArr, NULL, 10);
                    y = strtol(yArr, NULL, 10);
                }

                date->day = d;
                date->month = m;
                date->year = y;
            } break;

            case 'T': { // Time (int, int, int, int), -1 if empty.
                struct minmea_time *time_ = va_arg(ap, struct minmea_time *);

                int h = -1, i = -1, s = -1, u = -1;

                if (field && minmea_isfield(*field)) {
                    // Minimum required: integer time.
                    for (int f=0; f<6; f++)
                        if (!isdigit((unsigned char) field[f]))
                            goto parse_error;

                    char hArr[] = {field[0], field[1], '\0'};
                    char iArr[] = {field[2], field[3], '\0'};
                    char sArr[] = {field[4], field[5], '\0'};
                    h = strtol(hArr, NULL, 10);
                    i = strtol(iArr, NULL, 10);
                    s = strtol(sArr, NULL, 10);
                    field += 6;

                    // Extra: fractional time. Saved as microseconds.
                    if (*field++ == '.') {
                        int value = 0;
                        int scale = 1000000;
                        while (isdigit((unsigned char) *field) && scale > 1) {
                            value = (value * 10) + (*field++ - '0');
                            scale /= 10;
                        }
                        u = value * scale;
                    } else {
                        u = 0;
                    }
                }

                time_->hours = h;
                time_->minutes = i;
                time_->seconds = s;
               // time_->microseconds = u; Eliminado para ahorra espacio
            } break;

            case '_': { // Ignore the field.
            } break;

            default: { // Unknown.
                goto parse_error;
            } break;
        }

        next_field();
    }

    result = true;

parse_error:
    va_end(ap);
    return result;
}

bool minmea_talker_id(char talker[3], const char *sentence)
{
    char type[6];
    if (!minmea_scan(sentence, "t", type))
        return false;

    talker[0] = type[0];
    talker[1] = type[1];
    talker[2] = '\0';

    return true;
}

enum minmea_sentence_id minmea_sentence_id(const char *sentence, bool strict)
{
    if (!minmea_check(sentence, strict))
        return MINMEA_INVALID;

    char type[6];
    if (!minmea_scan(sentence, "t", type))
        return MINMEA_INVALID;

    if (!strcmp(type+2, "RMC"))
        return MINMEA_SENTENCE_RMC;
    if (!strcmp(type+2, "GGA"))
        return MINMEA_SENTENCE_GGA;
    if (!strcmp(type+2, "GSA"))
        return MINMEA_SENTENCE_GSA;
    if (!strcmp(type+2, "GLL"))
        return MINMEA_SENTENCE_GLL;
    if (!strcmp(type+2, "GST"))
        return MINMEA_SENTENCE_GST;
    if (!strcmp(type+2, "GSV"))
        return MINMEA_SENTENCE_GSV;
    if (!strcmp(type+2, "VTG"))
        return MINMEA_SENTENCE_VTG;

    return MINMEA_UNKNOWN;
}

bool minmea_parse_rmc(struct minmea_sentence_rmc *frame, const char *sentence)
{
    // $GPRMC,081836,A,3751.65,S,14507.36,E,000.0,360.0,130998,011.3,E*62
    char type[6];
    char validity;
    int8_t latitude_direction;
    int8_t longitude_direction;
    int8_t variation_direction;
    if (!minmea_scan(sentence, "tTcFdFdffDfd",
            type,
            &frame->time,
            &validity,
            &frame->latitude, &latitude_direction,
            &frame->longitude, &longitude_direction,
            &frame->speed,
            &frame->course,
            &frame->date,
            &frame->variation, &variation_direction))
        return false;
    if (strcmp(type+2, "RMC"))
        return false;

    frame->valid = (validity == 'A');
    frame->latitude.deg *= latitude_direction;
    frame->longitude.deg *= longitude_direction;
    frame->variation.value *= variation_direction;
		frame->type = (uint8_t) MINMEA_SENTENCE_RMC;
    return true;
}

bool minmea_parse_gga(struct minmea_sentence_gga *frame, const char *sentence)
{
    // $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
    char type[6];
    int8_t latitude_direction;
    int8_t longitude_direction;
		if (!minmea_scan(sentence, "tTFdFdiiffcfci_",
            type,
            &frame->time,
            &frame->latitude, &latitude_direction,
            &frame->longitude, &longitude_direction,
            &frame->fix_quality,
            &frame->satellites_tracked,
            &frame->hdop,
            &frame->altitude, &frame->altitude_units,
            &frame->height, &frame->height_units,
            &frame->dgps_age))
        return false;
    if (strcmp(type+2, "GGA"))
        return false;
	  frame->latitude.deg *= latitude_direction;
    frame->longitude.deg *= longitude_direction;
		frame->type = (uint8_t) MINMEA_SENTENCE_GGA; 
    return true;
}

bool minmea_parse_gsa(struct minmea_sentence_gsa *frame, const char *sentence)
{
    // $GPGSA,A,3,04,05,,09,12,,,24,,,,,2.5,1.3,2.1*39
    char type[6];

    if (!minmea_scan(sentence, "tciiiiiiiiiiiiifff",
            type,
            &frame->mode,
            &frame->fix_type,
            &frame->sats[0],
            &frame->sats[1],
            &frame->sats[2],
            &frame->sats[3],
            &frame->sats[4],
            &frame->sats[5],
            &frame->sats[6],
            &frame->sats[7],
            &frame->sats[8],
            &frame->sats[9],
            &frame->sats[10],
            &frame->sats[11],
            &frame->pdop,
            &frame->hdop,
            &frame->vdop))
        return false;
    if (strcmp(type+2, "GSA"))
        return false;

    return true;
}

bool minmea_parse_gll(struct minmea_sentence_gll *frame, const char *sentence)
{
    // $GPGLL,3723.2475,N,12158.3416,W,161229.487,A,A*41$;
    char type[6];
    int8_t latitude_direction;
    int8_t longitude_direction;

    if (!minmea_scan(sentence, "tFdFdTc;c",
            type,
            &frame->latitude, &latitude_direction,
            &frame->longitude, &longitude_direction,
            &frame->time,
            &frame->status,
            &frame->mode))
        return false;
    if (strcmp(type+2, "GLL"))
        return false;

    frame->latitude.deg *= latitude_direction;
    frame->longitude.deg *= longitude_direction;
		frame->type = (uint8_t) MINMEA_SENTENCE_GLL;
    return true;
}

bool minmea_parse_gst(struct minmea_sentence_gst *frame, const char *sentence)
{
    // $GPGST,024603.00,3.2,6.6,4.7,47.3,5.8,5.6,22.0*58
    char type[6];

    if (!minmea_scan(sentence, "tTfffffff",
            type,
            &frame->time,
            &frame->rms_deviation,
            &frame->semi_major_deviation,
            &frame->semi_minor_deviation,
            &frame->semi_major_orientation,
            &frame->latitude_error_deviation,
            &frame->longitude_error_deviation,
            &frame->altitude_error_deviation))
        return false;
    if (strcmp(type+2, "GST"))
        return false;

    return true;
}

bool minmea_parse_gsv(struct minmea_sentence_gsv *frame, const char *sentence)
{
    // $GPGSV,3,1,11,03,03,111,00,04,15,270,00,06,01,010,00,13,06,292,00*74
    // $GPGSV,3,3,11,22,42,067,42,24,14,311,43,27,05,244,00,,,,*4D
    // $GPGSV,4,2,11,08,51,203,30,09,45,215,28*75
    // $GPGSV,4,4,13,39,31,170,27*40
    // $GPGSV,4,4,13*7B
    char type[6];

    if (!minmea_scan(sentence, "tiii;iiiiiiiiiiiiiiii", //modificado azimuth, max 359 limitado a 255
            type,
            &frame->total_msgs,
            &frame->msg_nr,
            &frame->total_sats,
            &frame->sats[0].nr,
            &frame->sats[0].elevation,
            &frame->sats[0].azimuth,
            &frame->sats[0].snr,
            &frame->sats[1].nr,
            &frame->sats[1].elevation,
            &frame->sats[1].azimuth,
            &frame->sats[1].snr,
            &frame->sats[2].nr,
            &frame->sats[2].elevation,
            &frame->sats[2].azimuth,
            &frame->sats[2].snr,
            &frame->sats[3].nr,
            &frame->sats[3].elevation,
            &frame->sats[3].azimuth,
            &frame->sats[3].snr
            )) {
        return false;
    }
    if (strcmp(type+2, "GSV"))
        return false;
		 frame->type = (uint8_t) MINMEA_SENTENCE_GSV;
    return true;
}

bool minmea_parse_vtg(struct minmea_sentence_vtg *frame, const char *sentence)
{
    // $GPVTG,054.7,T,034.4,M,005.5,N,010.2,K*48
    // $GPVTG,156.1,T,140.9,M,0.0,N,0.0,K*41
    // $GPVTG,096.5,T,083.5,M,0.0,N,0.0,K,D*22
    // $GPVTG,188.36,T,,M,0.820,N,1.519,K,A*3F
    char type[6];
    char c_true, c_magnetic, c_knots, c_kph, c_faa_mode;

    if (!minmea_scan(sentence, "tfcfcfcfc;c",
            type,
            &frame->true_track_degrees,
            &c_true,
            &frame->magnetic_track_degrees,
            &c_magnetic,
            &frame->speed_knots,
            &c_knots,
            &frame->speed_kph,
            &c_kph,
            &c_faa_mode))
        return false;
    if (strcmp(type+2, "VTG"))
        return false;
    // check chars
    if (c_true != 'T' ||
        c_magnetic != 'M' ||
        c_knots != 'N' ||
        c_kph != 'K')
        return false;
		frame->type = MINMEA_SENTENCE_VTG;
    frame->faa_mode = c_faa_mode;

    return true;
}



/*
int minmea_gettime(struct timespec *ts, const struct minmea_date *date, const struct minmea_time *time_)
{
    if (date->year == -1 || time_->hours == -1)
        return -1;

    struct tm tm;
    memset(&tm, 0, sizeof(tm));
    tm.tm_year = 2000 + date->year - 1900;
    tm.tm_mon = date->month - 1;
    tm.tm_mday = date->day;
    tm.tm_hour = time_->hours;
    tm.tm_min = time_->minutes;
    tm.tm_sec = time_->seconds;

//    time_t timestamp = timegm(&tm);  See README.md if your system lacks timegm(). 
    if (timestamp != -1) {
        ts->tv_sec = timestamp;
        ts->tv_nsec = time_->microseconds * 1000;
        return 0;
    } else {
        return -1;
    }
}
*/

bool minnmea_lineParser(const char * line, minmea_frame_t * f)
{
switch (minmea_sentence_id(line, false)) {
	
#ifdef RMC	
            case MINMEA_SENTENCE_RMC: {
                struct minmea_sentence_rmc frame;
								if(minmea_parse_rmc(&frame, line))
								{	
									f->Data.RMCFrame = frame;
									return true; 
								}
            } break;
#endif

#ifdef GGA						
            case MINMEA_SENTENCE_GGA: {
                struct minmea_sentence_gga frame;
								if(minmea_parse_gga(&frame, line))
								{
									f->Data.GGAFrame = frame;
									return true;
								}
             } break;
#endif
						 
#ifdef GLL						
            case MINMEA_SENTENCE_GLL: {
                struct minmea_sentence_gll frame;
								if(minmea_parse_gll(&frame, line))
								{
									f->Data.GLLFrame = frame;
									return true;
								}
             } break;
#endif
						 
#ifdef GST						 
            case MINMEA_SENTENCE_GST: {
                struct minmea_sentence_gst frame;
                return (minmea_parse_gst(&frame, line));
            } break;
#endif
						
#ifdef GSV
            case MINMEA_SENTENCE_GSV: {
                struct minmea_sentence_gsv frame;
                if (minmea_parse_gsv(&frame, line))
								{
									f->Data.GSVFrame = frame;
									return true;
								}
            } break;
#endif
						
#ifdef VTG						
            case MINMEA_SENTENCE_VTG: {
               struct minmea_sentence_vtg frame;
								if (minmea_parse_vtg(&frame, line))
								{
									f->Data.VTGFrame = frame;
									return true;
								}
            } break;
#endif
            case MINMEA_INVALID: {
               // printf(INDENT_SPACES "$xxxxx sentence is not valid\n");
            } break;

            default: {
               // printf(INDENT_SPACES "$xxxxx sentence is not parsed\n");
            } break;
        }
				return false;
			}

bool minmea_frame_to_optimized(const minmea_frame_t * input ,minmea_sentence_optimized_t * output)
{
	if((input->Union[0] == MINMEA_SENTENCE_GGA) && (input->Data.GGAFrame.fix_quality > 0))
	{	
		cord_to_appformat(input->Data.GGAFrame.latitude,&output->grad_latitude,&output->grad_lat_decimal);
		cord_to_appformat(input->Data.GGAFrame.longitude,&output->grad_long,&output->grad_lng_decimal);
		
		output->altitude = fixedpoint_to_double(input->Data.GGAFrame.altitude);
		output->hdop = fixedpoint_to_double(input->Data.GGAFrame.hdop);	
		
		output->fix_quality = input->Data.GGAFrame.fix_quality;
		output->satellites_tracked = input->Data.GGAFrame.satellites_tracked;
		output->hours = input->Data.GGAFrame.time.hours;
		output->minutes =  input->Data.GGAFrame.time.minutes;
		output->seconds =  input->Data.GGAFrame.time.seconds;
		output->type = MINMEA_SENTENCE_OPTMZ;
		return true;
	}
	else 	if( input->Union[0] == MINMEA_SENTENCE_VTG)
	{
		output->velocity = fixedpoint_to_double(input->Data.VTGFrame.speed_kph);
		output->course = fixedpoint_to_double(input->Data.VTGFrame.true_track_degrees) * 10;
		return false;
	}		
	return false;
}
/* vim: set ts=4 sw=4 et: */
