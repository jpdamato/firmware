#include "dtw_test.h"

#include "app_fifo.h"

#define W_SIZE 128
#define V_SIZE 64
#define INF 1<<15


#define min(a,b) ((a<b)?a:b)   

app_fifo_t m_fifo;
uint8_t buffer [V_SIZE * 2];

uint16_t w[W_SIZE];
uint16_t mGamma[W_SIZE][V_SIZE];

uint32_t dtw_init()
{
	uint32_t err_code = app_fifo_init(&m_fifo, buffer, (uint16_t)sizeof(buffer));
	
	for( int i = 1; i < W_SIZE; i++ ) 
	{
		mGamma[0][i] = INF;
	}
	
	for( int i = 1; i < V_SIZE; i++ ) 
	{
		mGamma[i][0] = INF;
	}
	mGamma[0][0] = 0;
	
	return err_code;
	
}

uint32_t dtw_buffer_add(uint16_t val)
{
	uint32_t data_size = 2;
	uint32_t return_val = app_fifo_write(&m_fifo, (uint8_t *) &val, &data_size);
	return return_val;
}


bool naiveDTW(uint16_t window) 
{
	uint16_t cost;
	uint16_t v; // Buffer a ser actualizado
	uint32_t data_byte = 2;
	uint32_t v_size;
	uint32_t return_val;
	
	if (window > V_SIZE)
		v_size = V_SIZE;
	else
		v_size = window;
	
	for( int i = 1; i < v_size; i++ ) 
	{
		return_val = app_fifo_read(&m_fifo, (uint8_t*)&v, &data_byte);
		for( int j = 1; j < W_SIZE; j++ ) 
		{
			if( v == w[j - 1] ) 
			{
				cost = 0;
			}
			else {
				cost = 1;
			}
			float min_aux =  min(mGamma[i-1][j], mGamma[i][j-1]);
			//mGamma[i][j] = cost + min( min_aux, mGamma[i][j-1], mGamma[i-1][j-1] );
			mGamma[i][j] = cost + min( min_aux, mGamma[i-1][j-1] );
		}
	}
	
	// Hacer el corrimiento del buffer
	
	// Retorna un valor	
	return true;
}