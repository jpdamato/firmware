#include <string.h>

#include "mlmath.h"
#include "mem_statistics.h"


#define SPRINTS_DEFAULT_VEL 22
#define ACCEL_LIMIT_DEFAULT 9
#define VEL_LIMIT_DEFAULT 40
#define MAX_ACCE_LIST 10
#define R 6378137
statistics_t last_calculation;
statistics_time_t time_last;
mem_data_t incoming;
uint16_t debug_statistics;
float acc_max[MAX_ACCE_LIST];
float dacc_max[MAX_ACCE_LIST];

uint8_t acc_hist[ACCEL_LIMIT_DEFAULT];
double d_distance = 0;

double latitude_last, longitude_last;
float vel_last=0;

static double Rad(double value)
{
	return value * M_PI / 180;
}


static void acc_hist_make(uint8_t * hist,uint8_t len, float * buffer)
{
	memset(hist,0,len);
	for (uint8_t i = 0; i < len; i++)
	{
		for(uint8_t j=0; j<MAX_ACCE_LIST;j++)
		{
			if (buffer[j] >= i && buffer[j] < i+1)
				hist[i]++;
		}
	}
}
static void acc_move(float * buffer,uint8_t index)
{
	for (uint8_t i = MAX_ACCE_LIST-2; i> index; i--)
		buffer[i+1] = buffer[i];
}
static void acc_compare(float * buffer,float acc)
{
	uint8_t index = 0;
	while (acc < buffer[index] && index < MAX_ACCE_LIST)
	{
		index++;
	}
	acc_move(buffer,index);
	buffer[index] = acc; 
}

static double coord_to_double(uint8_t deg,int min)
{
	if (min < 0)
		return (double) ((-1) * deg + (double) min / 1000000);
	else 
		return (double) (deg + (double) min / 1000000);
}

static double get_distance(minmea_sentence_optimized_t gps_data)
{
	double latitude = coord_to_double(gps_data.grad_latitude,gps_data.grad_lat_decimal);
	double longitude = coord_to_double(gps_data.grad_long,gps_data.grad_lng_decimal);
	
	double d_lat = Rad(latitude_last-latitude);
	double d_long = Rad(longitude_last-longitude);
	
	double a = sin(d_lat / 2) * sin(d_lat / 2) +
            cos(Rad(latitude)) * cos(Rad(latitude_last)) *
            sin(d_long / 2) * sin(d_long / 2);
	
  double c = 2 * atan2(sqrt(a), sqrt(1 - a));
	
	//latitude_last = latitude;
	//longitude_last = longitude;
	double d = (R * c);
	
	if (d >1000)
	{
		d = 0;
		debug_statistics++;
	}
	
  return d; // returns the distance in meter
}

static uint32_t time_to_seconds(statistics_time_t time)
{
	return time.hour*3600+time.minute*60+time.second;
}

static uint16_t get_time(statistics_time_t start,statistics_time_t stop)
{
	uint16_t aux = abs(time_to_seconds(stop)-time_to_seconds(start));
	if (aux == 0)
		aux = 1; //evito division por 0
	return aux;
}


bool statistics_calc_start(statistics_time_t time_start,statistics_time_t time_stop,uint8_t sprints_vel, uint8_t accel_limit)
{
	latitude_last = 0;
	longitude_last = 0;
	vel_last = 0;
	
	last_calculation.time_start = time_start;
	last_calculation.time_stop = time_stop;
	last_calculation.distance_total=0;
	last_calculation.sprints_total=0;
	last_calculation.sprints_distance =0;
	last_calculation.velocity_max = 0;
	last_calculation.accel_max = 0;
	last_calculation.daccel_max = 0;
	last_calculation.got_start_session = false;
	
	if (sprints_vel == NULL || sprints_vel == 0)
		last_calculation.sprints_vel = SPRINTS_DEFAULT_VEL;
	else
		last_calculation.sprints_vel = sprints_vel;
	
 if (accel_limit == NULL || accel_limit == 0)
		last_calculation.accel_limit = (uint8_t) ACCEL_LIMIT_DEFAULT;
	else
		last_calculation.accel_limit = accel_limit;
	
	
	last_calculation.complete = false;
	
	time_last.hour = 0;
	time_last.minute=0;
	time_last.second=0;
	
	last_calculation.init=false;
	debug_statistics =0;
	return true;
}


void statistics_set_start_session(bool state)
{
	last_calculation.got_start_session = state;
}
bool statistics_find_start_session(const uint8_t * input, uint8_t size)
{
	uint8_t index = 0;
	uint8_t len = 0;
	
	if (last_calculation.got_start_session)
		return true;
	
	while (index < size-1) //el ultimo es ctc
	{
		memset(&incoming,0,sizeof(incoming));
		switch (input[index])
		{
			case MINMEA_SENTENCE_OPTMZ:
			{
				len = sizeof(incoming.gps_data);
				if (index + len > size)
					return false;

				index += len;	
			}break;
			case GPS_SESSION_TYPE:
			{
				len = sizeof(incoming.gps_session);
				if (index + len >size)
						return false;
				
				memcpy(&incoming.gps_session,&input[index],len);
				//statistics_time_t time = {incoming.gps_session.hour,incoming.gps_session.minute,incoming.gps_session.second};
				index += len;

				if (incoming.gps_session.number == 0)//&& (incoming.gps_session.year <2100) && (incoming.gps_session.year >2015)) //trato de validar
				{
					last_calculation.got_start_session = true;
					return true;
				}
			}break;
			case MPU_DATA_TYPE:
			{
				len = sizeof(incoming.mpu);
				if (index + len >size)
						return false;
				index += len;
				__nop();
			}break;
			case POWER_SAMPLE_ID:
			{
				len=sizeof(incoming.power);
				if (index + len >size)
						return false;
				index += len;
			}break;
			default:
				index++;
		}
	}
	return false;
}

bool statistics_calc_iteration(const uint8_t * input, uint8_t size)
{
	uint8_t index = 0;
	uint8_t len = 0;
	
	float velocity = 0;
	float accel = 0;
	while (index < size-1) //el ultimo es ctc
	{
		memset(&incoming,0,sizeof(incoming));
		switch (input[index])
		{
			case MINMEA_SENTENCE_OPTMZ:
			{
				if(last_calculation.complete)
					return false;
				
				len = sizeof(incoming.gps_data);
				
				if (index + len > size)
				{
					return false;
					__nop();	
				}
				
				memcpy(&incoming.gps_data,&input[index],len);
				index += len;	
				
				statistics_time_t stop = {incoming.gps_data.hours,incoming.gps_data.minutes,incoming.gps_data.seconds};	
				
				if (!last_calculation.init && time_to_seconds(last_calculation.time_start) < 10) //si el tiempo de inicio no es valido tomo el primero
					last_calculation.time_start = stop;
				
				if ((incoming.gps_data.hdop > 1) || ( incoming.gps_data.velocity > VEL_LIMIT_DEFAULT))
				{
					time_last = stop;
					last_calculation.init = false;
					break;
				}
				
				if (time_to_seconds(last_calculation.time_start) > time_to_seconds(stop)) // busco el tiempo de inicio
					break;
				
				if (!last_calculation.init)
				{ //inicializo el primer valor de posicion
					time_last = stop;
					
					latitude_last = coord_to_double(incoming.gps_data.grad_latitude,incoming.gps_data.grad_lat_decimal);
					longitude_last = coord_to_double(incoming.gps_data.grad_long,incoming.gps_data.grad_lng_decimal);
					vel_last = 0;
					last_calculation.init = true;
					break;
				}
			/*	
				if (incoming.gps_data.velocity == 0 || incoming.gps_data.velocity > VEL_LIMIT_DEFAULT)
				{
					time_last = stop;
					latitude_last = coord_to_double(incoming.gps_data.grad_latitude,incoming.gps_data.grad_lat_decimal);
					longitude_last = coord_to_double(incoming.gps_data.grad_long,incoming.gps_data.grad_lng_decimal);
					break;
				}*/
				
				d_distance = get_distance(incoming.gps_data);
				float dt = (float)get_time(time_last,stop);
				
				velocity = (d_distance * 3.6)  / dt;
				
				if (velocity > VEL_LIMIT_DEFAULT)
				{
					last_calculation.init = false;
					break;
				}
		
				last_calculation.distance_total += d_distance;
				latitude_last = coord_to_double(incoming.gps_data.grad_latitude,incoming.gps_data.grad_lat_decimal);
				longitude_last = coord_to_double(incoming.gps_data.grad_long,incoming.gps_data.grad_lng_decimal);
				time_last = stop;
				
				if (vel_last == 0)
				{
					vel_last = velocity;
					break;
				}
				
				accel = (velocity-vel_last) / dt;
				vel_last = velocity;
				
				if (accel < 0)
				{
					accel *= (-1);
					if (accel >last_calculation.accel_limit)
					break;
					
					if (accel > last_calculation.daccel_max)
					last_calculation.daccel_max = accel;
					
					acc_compare(dacc_max,accel);
				}
				else
				{
				if (accel >last_calculation.accel_limit)
					break;
				
				if (accel > last_calculation.accel_max)
					last_calculation.accel_max = accel;
				
					acc_compare(acc_max,accel);
				}	
				
				
					
				if (velocity >= last_calculation.sprints_vel)
				{
					last_calculation.sprints_total++;
					last_calculation.sprints_distance += d_distance;
				}
				
				
				if (velocity > last_calculation.velocity_max)
					last_calculation.velocity_max = velocity;
				
				
				if (last_calculation.init && 
						time_to_seconds(last_calculation.time_stop) > time_to_seconds(last_calculation.time_start) &&
						time_to_seconds(stop) > time_to_seconds(last_calculation.time_stop))
						last_calculation.complete = true;
						
			} break;
			case GPS_SESSION_TYPE:
			{
				len = sizeof(incoming.gps_session);
				if (index + len >size)
				{
						return false;
								__nop();	
				}
				index += len;
				last_calculation.init = false; //salteo el ultimo punto
			}break;
			case MPU_DATA_TYPE:
			{
				len = sizeof(incoming.mpu);
				if (index + len >size)
				{
						return false;
							__nop();	
				}
				index += len;
			}break;
			case POWER_SAMPLE_ID:
			{
				len=sizeof(incoming.power);
				if (index + len >size)
						return false;
				index += len;
			}break;
			default:
				index++;
		}
	}
	return true;
}

void statistics_calc_stop(void)
{
	last_calculation.time_stop = time_last;
}

statistics_t statistics_get_results(void)
{
	return last_calculation;
}

uint8_t statistics_to_string(uint8_t *buffer)
{
	return sprintf((char*)buffer,"cmpt:%d,dist:%.2f, vel_max:%d, accel_max:%.1f,daccel_max:-%.1f, sprints:%d, sprints_dist:%d, Ti: %2d:%2d:%2d,Tf: %2d:%2d:%2d",
								last_calculation.complete,last_calculation.distance_total,
								last_calculation.velocity_max,last_calculation.accel_max,last_calculation.daccel_max,last_calculation.sprints_total,
								last_calculation.sprints_distance,last_calculation.time_start.hour,last_calculation.time_start.minute,
								last_calculation.time_start.second,last_calculation.time_stop.hour,last_calculation.time_stop.minute,last_calculation.time_stop.second);
}

uint8_t statistics_acc_detail(uint8_t *buffer)
{
	return sprintf((char*)buffer,"acc_max:%.2f,%.2f,%.2f - acc_min:-%.2f,-%.2f,-%.2f",acc_max[0],acc_max[1],acc_max[2],
																																										dacc_max[0],dacc_max[1],dacc_max[2]);
}

uint8_t statistics_acc_hist(uint8_t *buffer)
{
	uint8_t acc_hist[ACCEL_LIMIT_DEFAULT];
	
	acc_hist_make(acc_hist,ACCEL_LIMIT_DEFAULT,acc_max);
	uint8_t len = sprintf((char*)buffer,"acc_hist: ");
	for (uint8_t i = 0; i < ACCEL_LIMIT_DEFAULT; i++)
		len += sprintf((char*)buffer+len,"%d,%d/",i,acc_hist[i]);
	
	len += sprintf((char*)buffer+len,"\r\n");
	
	acc_hist_make(acc_hist,ACCEL_LIMIT_DEFAULT,dacc_max);

	len += sprintf((char*)buffer+len,"dacc_hist: ");
	for (uint8_t i = 0; i < ACCEL_LIMIT_DEFAULT; i++)
		len += sprintf((char*)buffer+len,"%d,%d/",i,acc_hist[i]);
	
	len += sprintf((char*)buffer+len,"\r\n");
	
	return len;
}

bool statistics_get_start_session(void)
{
	return last_calculation.got_start_session;
}

