#ifndef __POWER_TEST_H
#define __POWER_TEST_H

#include <stdint.h>
#include <stdbool.h>

#define POWER_SAMPLE_ID 40

__packed typedef struct power_mode_s 
{
	uint8_t bit_gps : 1;
	uint8_t bit_mpu : 1;
	uint8_t bit_dwt	: 1;
} power_mode_t;

__packed typedef struct power_reg_s
{
	uint8_t type;
	uint32_t time;
	power_mode_t mode;
	uint16_t dtw_times;
	uint8_t current;
} power_reg_t;






#endif