#ifndef __DTW_TEST_H
	#define __DTW_TEST_H

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

uint32_t dtw_init(void);
uint32_t dtw_buffer_add(uint16_t val);
bool naiveDTW(uint16_t window);





#endif