#ifndef __MEM_STATISTICS_H
#define __MEM_STATISTICS_H

#include <stdint.h>
#include <stdbool.h>

#include "gps_task.h"
#include "minmea.h"
#ifdef WISE_MPU_ENABLE
	#include "mpu_task.h"
#endif
#include "power_test.h"

#define MEMORY_PACKAGE_SIZE 128

typedef union mem_data_u
{
	minmea_sentence_optimized_t gps_data;
	session_t gps_session;
	mpu_data_t mpu;
	power_reg_t power;	
}mem_data_t;


typedef struct statistics_time_s
{
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
} statistics_time_t;

typedef struct statistic_s
{
statistics_time_t time_start;
statistics_time_t time_stop;
float distance_total;
uint8_t velocity_max;
float accel_max;
float daccel_max;
uint8_t accel_limit;
uint8_t sprints_vel;
uint16_t sprints_total;
uint16_t sprints_distance;
bool init;
bool complete;
bool got_start_session;
}statistics_t;

bool statistics_calc_start(statistics_time_t time_start,statistics_time_t time_stop, uint8_t sprints_vel, uint8_t accel_limit);
bool statistics_calc_iteration(const uint8_t * input, uint8_t size);
void statistics_calc_stop(void);
bool statistics_get_start_session(void);
bool statistics_find_start_session(const uint8_t * input, uint8_t size);
uint8_t statistics_to_string(uint8_t *buffer);
uint8_t statistics_acc_detail(uint8_t *buffer);
uint8_t statistics_acc_hist(uint8_t *buffer);


#endif