
#ifndef _SPIFLASH_EXT_H
#define _SPIFLASH_EXT_H


#include <stdint.h>

/// IMPORTANT: NAND FLASH memory requires erase before write, because
///            it can only transition from 1s to 0s and only the erase command can reset all 0s to 1s
/// See http://en.wikipedia.org/wiki/Flash_memory
/// The smallest range that can be erased is a sector (4K, 32K, 64K); there is also a chip erase command

/// Standard SPI flash commands
/// Assuming the WP pin is pulled up (to disable hardware write protection)
/// To use any write commands the WEL bit in the status register must be set to 1.
/// This is accomplished by sending a 0x06 command before any such write/erase command.
/// The WEL bit in the status register resets to the logical �0� state after a
/// device power-up or reset. In addition, the WEL bit will be reset to the logical �0� state automatically under the following conditions:
/// � Write Disable operation completes successfully
/// � Write Status Register operation completes successfully or aborts
/// � Protect Sector operation completes successfully or aborts
/// � Unprotect Sector operation completes successfully or aborts
/// � Byte/Page Program operation completes successfully or aborts
/// � Sequential Program Mode reaches highest unprotected memory location
/// � Sequential Program Mode reaches the end of the memory array
/// � Sequential Program Mode aborts
/// � Block Erase operation completes successfully or aborts
/// � Chip Erase operation completes successfully or aborts
/// � Hold condition aborts
/*
//Write Protect
#define WP_PORT GPIOB
#define WP_PIN GPIO_PIN_8

//Hold
#define HOLD_PORT GPIOB
#define HOLD_PIN GPIO_PIN_8

//CS
#define CS0_PIN GPIO_PIN_2
#define CS1_PIN GPIO_PIN_0
#define CS0_PORT GPIOE
#define CS1_PORT GPIOE
*/
/*/Macros para usar cada chip select/
#define CHIP1_SELECT 							HAL_GPIO_WritePin(CS_PORT,CS1_PIN,GPIO_PIN_RESET); //REDI
#define CHIP1_UNSELECT							HAL_GPIO_WritePin(CS_PORT,CS1_PIN,GPIO_PIN_SET);//REDI
#define CHIP0_SELECT 							HAL_GPIO_WritePin(CS_PORT,CS0_PIN,GPIO_PIN_RESET);//REDI
#define CHIP0_UNSELECT							HAL_GPIO_WritePin(CS_PORT,CS0_PIN,GPIO_PIN_SET);//REDI
*/
//Otros parametros modificables
#define MEMORY_ADRESSING_SIZE 67108864   //direccion 0x04000000
#define NUMBER_OF_MEMORIES 1
#define MAX_READ 700


//comandos de la memoria
#define SPIFLASH_WRITEENABLE      0x06        // write enable //REDI
#define SPIFLASH_WRITEDISABLE     0x04        // write disable //REDI
#define SPIFLASH_PAGEPROGRAM			0x12				//page-program/write //REDI
#define SPIFLASH_READ							0x13				//read //REDI
#define SPIFLASH_ERASESECTOR			0xDC				//sector erase //REDI
#define SPIFLASH_BULKERASE				0x60				//bulk erase //REDI
#define SPIFLASH_RDSR1						0x05				//read Status Register 1
#define SPIFLASH_READID						0x9F				//read JEDEC ids

#define SPIFLASH_STATUSREAD       0x05        // read status register
#define SPIFLASH_STATUSWRITE      0x01        // write status register
#define SPIFLASH_ARRAYREAD        0x0B        // read array (fast, need to add 1 dummy byte after 3 address bytes)
#define SPIFLASH_ARRAYREADLOWFREQ 0x03        // read array (low frequency)

#define SPIFLASH_SLEEP            0xB9        // deep power down
#define SPIFLASH_WAKE             0xAB        // deep power wake up
#define SPIFLASH_IDREAD           0x9F        // read JEDEC manufacturer and device ID (2 bytes, specific bytes for each manufacturer and device)
                                              // Example for Atmel-Adesto 4Mbit AT25DF041A: 0x1F44 (page 27: http://www.adestotech.com/sites/default/files/datasheets/doc3668.pdf)
                                              // Example for Winbond 4Mbit W25X40CL: 0xEF30 (page 14: http://www.winbond.com/NR/rdonlyres/6E25084C-0BFE-4B25-903D-AE10221A0929/0/W25X40CL.pdf)
#define SPIFLASH_MACREAD          0x4B        // read unique ID number (MAC)


#define SPIFLASH_CLSR							0x30				// Limpia el Status Register 1 (RDSR1). Solo funciona cuando el bit WIP esta seteado en RDSR1.

#define CHIP_WRITE_PROTECTED 		0
#define CHIP_WRITE_UNPROTECTED	1



  static uint8_t UNIQUEID[8];
  void R_SPIFlash_Init(void); //REDI
 // uint8_t readStatus(void);
	int8_t R_SPIFlash_readStatus(uint8_t cs, unsigned char addr );
  int8_t R_SPIFlash_writeStatus(uint8_t cs, unsigned char addr, unsigned char value );
  
	unsigned short R_SPIFlash_ReadBytes(uint32_t addr, uint8_t *data, uint16_t len); //REDI
  unsigned short R_SPIFlash_WriteBytes(uint32_t addr, uint8_t *data, uint16_t len); //REDI
  unsigned char R_SPIFlash_Busy(uint32_t addr);
	unsigned char R_SPIFlash_isBusy(uint8_t addr);
  void R_SPIFlash_SectorErase(uint32_t address); //REDI
  void R_SPIFlash_BulkErase(uint8_t cs); //REDI
  void R_SPIFlash_ReadDeviceId(void);
	void R_SPIFlash_ReadID(uint8_t cs,uint8_t * data);
  uint8_t* R_SPIFlash_ReadUniqueId(void);
	void R_SPIFlash_RefreshStatus(uint8_t cs) ;
void R_SPIFlash_Reset(unsigned char cs);
	
		void R_SPIFlash_PageErase(uint8_t cs,uint32_t addr);
		unsigned short R_SPIFlash_WritePage(uint8_t cs, uint16_t pageAddr, uint16_t offset,uint8_t *data, uint16_t len);
	unsigned short R_SPIFlash_ReadPage(uint8_t cs, uint16_t pageAddr, uint16_t offset,uint8_t *data, uint16_t len);
unsigned char R_SPIFlash_isNotOK(uint8_t addr);
void R_SPIFlash_clearStatus(uint8_t cs);
		
		
	//extern void R_SPIFlash_CS(i,CHIP_SELECT);
#define CS_ACTIVE_LOW
#define HOLD_ACTIVE_LOW
void R_SPIFlash_Sleep(uint8_t cs);


#ifdef CS_ACTIVE_LOW
	#define FLASH_SELECT 	0
	#define FLASH_UNSELECT 1
#else 
	#define FLASH_SELECT 	1
	#define FLASH_UNSELECT 0
#endif


#ifdef HOLD_ACTIVE_LOW
	#define HOLD_ON			 	0
	#define HOLD_OFF 			1
#else 
	#define HOLD_ON			 	1
	#define HOLD_OFF			0
#endif





#ifdef SPI_HAS_TRANSACTION
  SPISettings _settings;
#endif

#endif