/**
  * @file    R_SPIFlash_NRF.h
  * @author  Leandro J. Aguierre
  * @version V1.0
  * @date    Octuber 16th, 2016
  * @brief   This file provides hardware specific functions to manage and control a set of S25FL512S Spansion
	*						Flash memory www.spansion.com/Support/Datasheets/S25FL512S_00.pdf
	* 	        The functions described here are targeted to STM32 microcontrollers
	*
	* @test			nrf51xxx
	*
	*	@todo 		+Add a dynamic mechanism to define PORTS and GPIO for specific Chip Select. 
							+Add Asserts to check if device was previously defined.
	*			
	*	@copyright COPYRIGHT(c) 2016 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of Redimec SRL nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/

#include "R_SPIFlash_NRF.h"
#include "wise_board.h"
#include "nrf_gpio.h"
#include "nrf_drv_spi.h"
#include <string.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_delay.h"

#define SPI_INSTANCE  1 /**< SPI instance index. */
#define SPI_TIMEOUT 50000
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */

static uint8_t m_cs_pin;

/**
 * @brief SPI user event handler.
 * @param event
 */
void spi_event_handler(nrf_drv_spi_evt_t const * p_event,
                       void *                    p_context)
{
  spi_xfer_done = true;
  NRF_LOG_INFO("Transfer completed.");
}



void R_SPIFlash_init(uint8_t miso_pin,uint8_t mosi_pin,uint8_t ss_pin,uint8_t sck_pin)
{
  nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
		//spi_config.frequency = NRF_DRV_SPI_FREQ_1M;
    spi_config.ss_pin   = NRF_DRV_SPI_PIN_NOT_USED;
    spi_config.miso_pin = miso_pin;
    spi_config.mosi_pin = mosi_pin;
    spi_config.sck_pin  = sck_pin;
		m_cs_pin = ss_pin;
    APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, spi_event_handler, NULL));
		nrf_gpio_cfg_output(m_cs_pin);
}

void R_SPIFlash_CS(uint8_t State) 
{
	nrf_gpio_pin_write(m_cs_pin, State);
}	

void R_SPIFlash_HOLD(uint32_t HOLDPin, uint8_t State) {
	//HAL_GPIO_WritePin(HOLDPin, (GPIO_PinState)State);
}	

void R_SPIFlash_WP(uint32_t WPPin, uint8_t State) {
	//HAL_GPIO_WritePin(WPPin, (GPIO_PinState)State);
}	

void R_SPIFlash_TX(unsigned char *spiBufferTx, unsigned short iTx, unsigned int timeOut){
	uint8_t spiBufferRx[iTx];
	uint32_t time_out = SPI_TIMEOUT;
	uint32_t err_code = nrf_drv_spi_transfer(&spi, spiBufferTx, iTx, spiBufferRx, iTx);
	APP_ERROR_CHECK(err_code);
	spi_xfer_done = false;

	while (!spi_xfer_done)
  {
		if (time_out-- == 0)
		break;
	}
}
void R_SPIFlash_RX(unsigned char *spiBufferRx, unsigned short iRx, unsigned int timeOut)
{
	uint8_t spiBufferTx[iRx]; 
	uint32_t time_out = SPI_TIMEOUT;
	memset(spiBufferTx,0,iRx);
	uint32_t err_code = nrf_drv_spi_transfer(&spi, spiBufferTx, iRx, spiBufferRx, iRx);
	APP_ERROR_CHECK(err_code);
	spi_xfer_done = false;

	while (!spi_xfer_done)
  {
		if (time_out-- == 0)
			break;
	}
	
}

void R_SPIFlash_Delay(uint32_t delay)
{
	nrf_delay_ms(delay);
}

