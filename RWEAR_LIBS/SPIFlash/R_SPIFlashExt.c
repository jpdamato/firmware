/**
  * @file    SPIFlash.cpp
  * @author  Emiliano N. Trepichio
  * @version V1.0
  * @date    February 1st, 2015
  * @brief   This file provides hardware abstraction level functions to manage and control Flash memories
	* 	        The memories are controlled via SPI interface.  
	*						Functions described below allow the user to write, read, and erase the memory.
	*						Various memories can be used and treated like a single-big memory in terms of adressing space.
	*	@bug 		  R_SPIFlash_Busy: Used second LSB instead of using first LSB.
	* @date			December, 2016
	* @author		Mariano Scasso
	 
	*	@bug 		R_SPIFlash_isWE method is appended in order to be used in writting process
	* @date		December, 2016
	* @author Mariano Scasso
		
	* @bug 		R_SPIFlash_ReadBytes. This function with 512 bytes to read and low speed in SPI return for timeout.
	*	@date 	October, 2017
	*	@author Isis
	*
	*	
	*
	*	@copyright COPYRIGHT(c) 2016 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/

#include "R_SPIFlashExt.h"
#include "R_SPIFlash_NRF.h"
#include <string.h>

#define FLASH_PAGESIZE 2048

/*
extern void R_SPIFlash_WP(unsigned char device,unsigned char State) ;
		
extern void R_SPIFlash_CS(unsigned char device,unsigned char State);
extern void R_SPIFlash_HOLD(unsigned char device,unsigned char State);
extern void R_SPIFlash_WP(unsigned char device,unsigned char State);
extern void 	R_SPIFlash_TX(unsigned char device ,unsigned char *spiBuffer, unsigned short len, unsigned int timeOut);
extern void 	R_SPIFlash_RX(unsigned char device ,unsigned char *spiBuffer, unsigned short len, unsigned int timeOut);
*/

//variables
uint8_t spiBufferOut[517];
uint8_t spiBufferIn[MAX_READ];
uint16_t i; 
uint32_t address;
char R_SPIFlash_Status;	
uint8_t statusRegisters[3];

/**
 @brief
*/

void R_SPIFlash_Reset(unsigned char cs){


spiBufferOut[0] = 0xFF;
		R_SPIFlash_CS(cs,FLASH_SELECT);
	//	R_SPIFlash_TX(cs,spiBufferOut, 1, 100);
	R_SPIFlash_TX(spiBufferOut, 1, 100);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);

}







/** @brief Initializes signals HOLD, WRITE PROTECT and CHIP SELECT as 1,(they are active low)
 *  *
 *	@param	void.
 *
 *	@return void.
 ***********************************************************************************************************/

void R_SPIFlash_Init(){
	unsigned char chipProtect;
	//SET_WRITE_PROTECT(0,CHIP_WRITE_UNPROTECTED);
	//set de Write Protect a 1 (es activo bajo)
//	HAL_GPIO_WritePin(WP_PORT,WP_PIN,GPIO_PIN_SET);
//	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_4,1);
	//set de Chip Select a 1 en ambas memorias (activo bajo)
	uint8_t i;
	
	for(i = 0; i < NUMBER_OF_MEMORIES; i++){
		R_SPIFlash_CS(i,FLASH_UNSELECT);
		R_SPIFlash_HOLD(i,HOLD_OFF);
	R_SPIFlash_Reset(i);
	}
		R_SPIFlash_Reset(0);
	R_SPIFlash_writeStatus(0,0xA0,0x02);
	R_SPIFlash_writeStatus(0,0xB0,0x08);
	

}

/** @brief Reads the manufacturer and device ID bytes (as a short word)
 * 
 *	@param	uint8_t cs indicates which memory ID will be read.
 *  @param uint8_t * data the array where the information will be stored
 *	@return void.
 *	@todo Check if only 3 bytes are needed
 *	@bug 

 ***********************************************************************************************************/
void R_SPIFlash_ReadID(uint8_t cs,uint8_t * data){
		
		//uint8_t spiBufferIn[512];
		spiBufferOut[0] = SPIFLASH_READID;
			
    //CHIP_SELECT(cs);
		R_SPIFlash_CS(cs, FLASH_SELECT);
	
		R_SPIFlash_TX(spiBufferOut,1,10);//R_SPIFlash_TX(cs,spiBufferOut,1,10);
		R_SPIFlash_RX(spiBufferIn,512,100);//	R_SPIFlash_RX(cs,spiBufferIn,512,100);
		//spiBufferIn[3] contains the ID_CFI Length field. Here�s the length
		if(spiBufferIn[3] != 0)
				memcpy(data,&spiBufferIn[4],spiBufferIn[3]);
		else
				memcpy(data,&spiBufferIn[4],512);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);

} 


/// Lectura de n bytes. Direccion, lugar en el que se guarda la informacion y longitud de la misma
// MODIFICABILIDAD 26 bits de direccionamiento
/** @brief Reads n bytes from desired direction onwards.
 *	
 *				 This function can begin the lecture in one memory and finish in another one,transparent to user.
 * 
 *
 *	@param	uint32_t addr: indicates the address from which will begin the lecture
 *
 *	@param uint8_t * data: the array where the information will be stored
 *
 *	@param uint16_t len:     the number of bytes desired to be read
 *
 *
 *	@return void.
 ***********************************************************************************************************/
unsigned char R_SPIFlash_ReadBytePerByte(uint32_t addr, uint8_t *data, uint16_t len) {
		
		uint32_t leidos = 0;
		//uint8_t spiBufferIn[MAX_READ];
	int j=addr;
	unsigned char dataRead;
	for(int i=0; i<len;i++){
/*			R_SPIFlash_ReadByte(j,&dataRead);*/
		
	data[i]=dataRead;
	j++; 
		if (j==MEMORY_ADRESSING_SIZE) j=0;
	
	}
return len;
}
/********************************************************************************************************
	*@brief Reads n bytes from a selected page. 
	* 
  *
	*	@param	uint8_t cs:			The chip to be read
	*	@param  uint16_t pageAddr: Page address
	* @param  uint16_t offset: The offset of data to be read inside the page
	* @param  uint8_t *data: buffer where data will be read on	
	*	@param uint16_t len:     the number of bytes desired to be read
	*
	*	@return void.
	**************************************************************************************************/
	extern volatile unsigned char isWritting;
int csAux;
int addrAux;



unsigned short R_SPIFlash_ReadPage(uint8_t cs, uint16_t pageAddr, uint16_t offset,uint8_t *data, uint16_t len) {

				//en los primeros bytes se encuentra el chip select. Puede haber variado en la anterior iteracion
				//in the first bytes lies the chip select number. It could have changed in the last iteration.
		
			 
				while (R_SPIFlash_isBusy(cs) || (R_SPIFlash_isNotOK(cs)& 0xF7)){
					if(R_SPIFlash_isNotOK(cs)) {
						R_SPIFlash_clearStatus(cs); 
						return 0;
					}for(uint8_t i=0;i<100;i++);
				}	
			
			// Seleccion de pagina
				//alojamos en el bufer de salida el comando read page y a continuacion la pagina que queremos leer
			
 			/*PAG. 37*/
				spiBufferOut[0] = 0x13;//PAGE READ SPI;
				spiBufferOut[1] = 0; // dummy byte
				spiBufferOut[2] = ((pageAddr >> 8) & 0x00FF);
				spiBufferOut[3] = pageAddr & 0x00FF;

			  R_SPIFlash_CS(cs,FLASH_SELECT);
				R_SPIFlash_TX(spiBufferOut,4,100);//R_SPIFlash_TX(cs,spiBufferOut,4,100);
				R_SPIFlash_CS(cs,FLASH_UNSELECT);
				
				R_SPIFlash_RefreshStatus(cs);

					//	R_SPIFlash_writeStatus(0,0xB0, (statusRegisters[1]|0x04));
		//		HAL_Delay(1);			
				
				/*PAG. 38*/
				spiBufferOut[0] = 0x03;//Read data;
				spiBufferOut[1] = ((offset >> 8) & 0x000000FF);
				spiBufferOut[2] = offset & 0x000000FF;
				spiBufferOut[3] = 0; 
						
			  R_SPIFlash_CS(cs,FLASH_SELECT);
				R_SPIFlash_TX(spiBufferOut,4,100);//R_SPIFlash_TX(cs,spiBufferOut,4,100);
				R_SPIFlash_RX(spiBufferIn, len, 1000);//R_SPIFlash_RX(cs,spiBufferIn, len, 1000);
				
				R_SPIFlash_CS(cs,FLASH_UNSELECT);
			//	HAL_Delay(1);
				memcpy(data,spiBufferIn,len);
			return len;
}

unsigned char mem1[512];
unsigned char mem2[512];
	extern volatile unsigned char isWritting;

/********************************************************************************************************
	*@brief Write n bytes to a selected page. 
	* 
  *
	*	@param	uint8_t cs:		The chip to be written
	*	@param  uint16_t pageAddr: Page address
	* @param  uint16_t offset: The data offset inside the page
	* @param  uint8_t *data: data buffer to be written
	*	@param uint16_t len:     the number of bytes desired to be written
	*
	*	@return void.
	**************************************************************************************************/
int csAux;
int addrAux;

unsigned short R_SPIFlash_WritePage(uint8_t cs, uint16_t pageAddr, uint16_t offset,uint8_t *data, uint16_t len) {

				//en los primeros bytes se encuentra el chip select. Puede haber variado en la anterior iteracion
				//in the first bytes lies the chip select number. It could have changed in the last iteration.
		do{
			R_SPIFlash_RefreshStatus(cs);
		} while (statusRegisters[2]&0x01);
			// Seleccion de pagina
		//transferimos write enable para poder escribir
		spiBufferOut[0] = SPIFLASH_WRITEENABLE;
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut,1,10);//R_SPIFlash_TX(cs,spiBufferOut,1,10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);

		if((offset==0) && (pageAddr %64 ==0)) 
			R_SPIFlash_PageErase(cs,pageAddr);
			 
		do{
			R_SPIFlash_RefreshStatus(cs);
		} while (statusRegisters[2]&0x01);
		// Seleccion de pagina
		//transferimos write enable para poder escribir
		spiBufferOut[0] = SPIFLASH_WRITEENABLE;
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut,1,10);//	R_SPIFlash_TX(cs,spiBufferOut,1,10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
	
do{
	R_SPIFlash_RefreshStatus(cs);
} while (statusRegisters[2]&0x01);
			// Seleccion de pagina
				//alojamos en el bufer de salida el comando read page y a continuacion la pagina que queremos leer
			
 			/*PAG. 37*/
				spiBufferOut[0] = 0x84;//0x02;//LOAD PROGRAM DATA;
				spiBufferOut[1] = ((offset >> 8) & 0x000000FF);
				spiBufferOut[2] = offset & 0x000000FF;
				memcpy(&spiBufferOut[3],data,len);
				
			  R_SPIFlash_CS(cs,FLASH_SELECT);
				R_SPIFlash_TX(spiBufferOut,len+3,len*50);//R_SPIFlash_TX(cs,spiBufferOut,len+3,len*50);
				R_SPIFlash_CS(cs,FLASH_UNSELECT);

do{
	R_SPIFlash_RefreshStatus(cs);
} while (statusRegisters[2]&0x01);
				R_SPIFlash_Delay(1);		
				R_SPIFlash_CS(cs,FLASH_SELECT);
				spiBufferOut[0] = 0x10;//PAGE READ SPI;
				spiBufferOut[1] = 0;
				spiBufferOut[2] = ((pageAddr >> 8) & 0x000000FF);
				spiBufferOut[3] = pageAddr & 0x000000FF;
				R_SPIFlash_TX(spiBufferOut,4,50);//R_SPIFlash_TX(cs,spiBufferOut,4,50);
				R_SPIFlash_CS(cs,FLASH_UNSELECT);
				R_SPIFlash_RefreshStatus(cs);
				
				return len;
}






unsigned short R_SPIFlash_ReadBytes(uint32_t addr, uint8_t *data, uint16_t len) {
	
	

		uint32_t cs;
   	uint16_t offset;
		uint16_t pageAddr;
		uint16_t bytesToRead;
		uint16_t reads=0;;
	

		do {
	//en los primeros bytes se encuentra el chip select. Puede haber variado en la anterior iteracion
		cs = addr >> 27;
	
		//en los ultimos bytes se encuentra la direccion real que nos interesa
	
	 offset = addr & (0x000007FF) ; //addr[11:0]
	 pageAddr = (uint16_t)((addr >>11) & 0x0000FFFF);
	
	 
	
		if(offset+len > FLASH_PAGESIZE){
			bytesToRead = FLASH_PAGESIZE - offset;
			}	else 
			bytesToRead = len;
		
		R_SPIFlash_ReadPage(cs,pageAddr,offset,&data[reads],bytesToRead);
		reads += bytesToRead;
		len -= bytesToRead;
		addr+=reads;
		
		
		} while (len>0);

return len;
}


int8_t R_SPIFlash_readStatus(uint8_t cs, unsigned char addr ) {
spiBufferOut[0] = SPIFLASH_RDSR1;
	spiBufferOut[1] = addr;
uint8_t status[2];		
	
    //CHIP_SELECT(cs);
		R_SPIFlash_CS(cs, FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 2, 100);
		R_SPIFlash_RX(status,1, 100);
    R_SPIFlash_CS(cs,FLASH_UNSELECT);

	return status[0];
}
	
void R_SPIFlash_RefreshStatus(uint8_t cs ) {
spiBufferOut[0] = SPIFLASH_RDSR1;
	
	
    //CHIP_SELECT(cs);
		
		spiBufferOut[1] = 0xA0;
		R_SPIFlash_CS(cs, FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 2, 100);
		R_SPIFlash_RX(&statusRegisters[0],1, 100);
    R_SPIFlash_CS(cs,FLASH_UNSELECT);

		spiBufferOut[1] = 0xB0;
		R_SPIFlash_CS(cs, FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 2, 100);
		R_SPIFlash_RX(&statusRegisters[1],1, 100);
    R_SPIFlash_CS(cs,FLASH_UNSELECT);

  	spiBufferOut[1] = 0xC0;
		R_SPIFlash_CS(cs, FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 2, 100);
		R_SPIFlash_RX(&statusRegisters[2],1, 100);
    R_SPIFlash_CS(cs,FLASH_UNSELECT);

}



  int8_t R_SPIFlash_writeStatus(uint8_t cs, unsigned char addr, unsigned char value ){
	
	spiBufferOut[0] = SPIFLASH_STATUSWRITE;
	spiBufferOut[1]	=addr;
	spiBufferOut[2]	=value;
		
  	R_SPIFlash_CS(cs, FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 3, 100);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
		R_SPIFlash_RefreshStatus(cs);
	
	};
	
void R_SPIFlash_clearStatus(uint8_t cs ) {
		spiBufferOut[0] = SPIFLASH_CLSR;
			
  	R_SPIFlash_CS(cs, FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 1, 100);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
	
}


unsigned char R_SPIFlash_Busy(uint32_t addr){
	/*
	uint32_t cs = addr >> 26;
	uint8_t aux;
	uint8_t aux2;
	aux = SPIFLASH_RDSR1;
	CHIP_SELECT(cs);
	HAL_SPI_Transmit(&hspi3,&aux,1,10);
	HAL_SPI_Receive(&hspi3,&aux,1,10);
	aux2 = aux & (0x02);
	CHIP_UNSELECT(cs);
	
	return ((aux2) == (0x02));
	*/
	uint8_t aux;
	uint8_t aux2;
	aux= R_SPIFlash_readStatus(0,0);
	aux2 = aux & (0x01);
	//HAL_SPI_DebugSerial(&aux2,1);
	if (aux2 == 1) 
		return 1; 
	else 
		return 0;
	
}

	uint8_t aux;
	uint8_t byteRx=0xAA;
/// check if the chip is busy erasing/writing
unsigned char R_SPIFlash_isBusy(unsigned char csel){


	uint8_t aux[2];
	aux [0]= SPIFLASH_RDSR1;
	aux [1]= 0xC0;

	R_SPIFlash_CS(csel,FLASH_SELECT);
	R_SPIFlash_TX(aux,2,100);
	R_SPIFlash_RX(&byteRx,1,100);
	
	R_SPIFlash_CS(csel,FLASH_UNSELECT);
	
	return ((byteRx & 0x01) == (0x01));
	

}

// check if the chip had a prolem in busy erasing/writing
//	Return 0x00 if nothing problem detected. Otherwise return 0x01 or 0x02 or 0x03
unsigned char R_SPIFlash_isNotOK(uint8_t cs){
		uint8_t auxt[2], auxr;
	
	auxt[0] = SPIFLASH_RDSR1;
	auxt[1] = 0xC0;
	
	//addr
	auxr=0xAA;	
	R_SPIFlash_CS(cs,FLASH_SELECT);
	R_SPIFlash_TX(auxt,2,100);//R_SPIFlash_TX(cs,auxt,2,100);
	R_SPIFlash_RX(&auxr,1,100);
	R_SPIFlash_CS(cs,FLASH_UNSELECT);
	
 	return (auxr&0xFC);				//Bit 6 error de escritura
														//bit 5 error de borrado
														

}



unsigned char R_SPIFlash_isWE(uint32_t addr){
	uint8_t aux;
	uint8_t aux2;
	aux= R_SPIFlash_readStatus(0,0);
	aux2 = aux & (0x02);
	if (aux2 == 2) 
		return 1; 
	else 
		return 0;
	
}


unsigned short R_SPIFlash_WriteBytes(uint32_t addr, uint8_t *data, uint16_t len) {
  uint32_t bytes2Tx;
  uint16_t maxBytes = 512-(addr%512);  //segun la direccion en la que vamos a empezar a escribir, consideramos su lugar en la p�gina
  uint16_t escribidos = 0;
 	uint32_t cs = addr >> 27;
	uint16_t bytesToWrite;
	uint16_t pageAddr;
	uint16_t colAddr;
	uint32_t written = 0;
	do {
		
		cs = addr >>28;
		uint16_t colAddr  = (uint16_t) addr & (0x000007FF) ;
		uint16_t pageAddr = (uint16_t)((addr >>11) & 0x0000FFFF);

		if(colAddr+len > FLASH_PAGESIZE){
			bytesToWrite = FLASH_PAGESIZE - colAddr;
		//	R_SPIFlash_PageErase(cs, pageAddr+1);
		}	else 
			bytesToWrite = len;
		
		R_SPIFlash_WritePage(cs,pageAddr,colAddr,&data[written],bytesToWrite);
		written += bytesToWrite;
		len -= bytesToWrite;
		//colAddr =0;
addr+=written;
		
		
		} while (len>0);
  
return 0;
	
}

/// Borra un sector, de 256Kb
void R_SPIFlash_PageErase(uint8_t cs,uint32_t addr) {
		
					R_SPIFlash_RefreshStatus(cs);
		spiBufferOut[0] = SPIFLASH_WRITEENABLE;
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
	//WTF? ESTABA COMO SELECT...
	
			R_SPIFlash_RefreshStatus(cs);
		R_SPIFlash_Delay(12);
		spiBufferOut[0] = 0xD8;//SPIFLASH_ERASESECTOR;
		spiBufferOut[1] = 0; //Dummy byte
		spiBufferOut[2] = (addr >> 8) & 0x000000FF;
		spiBufferOut[3] = addr & 0x000000FF;
	
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 4, 10);
  	R_SPIFlash_Delay(12);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
		R_SPIFlash_Delay(102);
		//WTF? ESTABA COMO SELECT...
		
		R_SPIFlash_RefreshStatus(cs);		
		
		
		
		
		/*spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		CHIP_UNSELECT(cs);
		
		spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(cs,spiBufferOut, 1, 10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
		*/
		
}



/// Borra un sector, de 256Kb
void R_SPIFlash_SectorErase(uint32_t addr) {
		
		//el cs se encuentra en los bits mas altos
		uint8_t cs = addr >> 26;
		//usamos como direccion la pasada por parametro con una mascara que pone en cero los bits del chip select
		uint32_t address = addr & (0xFFFFFFFF >> 6);
		
		spiBufferOut[0] = SPIFLASH_WRITEENABLE;
		R_SPIFlash_CS(cs,FLASH_SELECT);
		//HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
	//WTF? ESTABA COMO SELECT...
		
		spiBufferOut[0] = SPIFLASH_ERASESECTOR;
		spiBufferOut[1] = address >> 24 & 0x000000FF;
		spiBufferOut[2] = address >> 16 & 0x000000FF;
		spiBufferOut[3] = address >> 8 & 0x000000FF;
		spiBufferOut[4] = address & 0x000000FF;
	
	
	
		//a�adimos la informacion a transmitir al bufer de salida
		
    /*CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 5, 10);
		CHIP_UNSELECT(cs);
		*/
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 5, 10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
		//WTF? ESTABA COMO SELECT...
				
		/*spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		CHIP_UNSELECT(cs);
		
		spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(cs,spiBufferOut, 1, 10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
		*/
		
}


void R_SPIFlash_BulkErase(uint8_t cs) {
		
		spiBufferOut[0] = SPIFLASH_WRITEENABLE;
		/*CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		CHIP_UNSELECT(cs);
		
		//spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(cs,spiBufferOut, 1, 10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
		*/
	
		//a�adimos la informacion a transmitir al bufer de salida
			
    /*CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		CHIP_UNSELECT(cs);*/
		
//	spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
	
	
		spiBufferOut[0] = SPIFLASH_BULKERASE;
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);
	
	//	spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		/*CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		CHIP_UNSELECT(cs);*/
		/*
		spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(cs,spiBufferOut, 1, 10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);*/
}

void R_SPIFlash_Sleep(uint8_t cs) {
		
		spiBufferOut[0] = SPIFLASH_SLEEP;
		R_SPIFlash_CS(cs,FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_CS(cs,FLASH_UNSELECT);

}