/**
  * @file    SPIFlash.cpp
  * @author  Emiliano N. Trepichio
  * @version V1.0
  * @date    February 1st, 2015
  * @brief   This file provides hardware abstraction level functions to manage and control Flash memories
	* 	        The memories are controlled via SPI interface.  
	*						Functions described below allow the user to write, read, and erase the memory.
	*						Various memories can be used and treated like a single-big memory in terms of adressing space.
	*
	*	@copyright COPYRIGHT(c) 2016 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/
#include <stdio.h>
#include "R_SPIFlash.h"
#include "R_SPIFlash_NRF.h"

#include <string.h>


//variables
uint8_t spiBufferOut[512];
uint16_t i;
uint32_t address;

/// IMPORTANT: NAND FLASH memory requires erase before write, because
///            it can only transition from 1s to 0s and only the erase command can reset all 0s to 1s
/// See http://en.wikipedia.org/wiki/Flash_memory
/// The smallest range that can be erased is a sector (256K); there is also a chip erase command


/** @brief Reads the manufacturer and device ID bytes (as a short word)
 * 
 *
 *	@param	uint8_t cs indicates which memory ID will be read.
 *
 *  @param uint8_t * data the array where the information will be stored
 *
 *	@return void.
 ***********************************************************************************************************/
void R_SPIFlash_ReadID(uint8_t * data){
		uint8_t spiBufferIn[512];
		spiBufferOut[0] = SPIFLASH_READID;
		
    //CHIP_SELECT(cs);
		R_SPIFlash_CS(FLASH_SELECT);
	
	  R_SPIFlash_TX(spiBufferOut, 1, 10);
		//HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
	  R_SPIFlash_RX(spiBufferIn, 100, 10);
	  //HAL_SPI_Transfer(spiBufferOut,1,spiBufferIn,100,10);
	

		//spiBufferIn[3] contains the ID_CFI Length field. Here�s the length
		if (spiBufferIn[4] != 0)
				memcpy(data, &spiBufferIn[5],spiBufferIn[4]);
		else
				memcpy(data, &spiBufferIn[5],100);
	
		R_SPIFlash_CS(FLASH_UNSELECT);
	
		//HAL_SPI_Transfer(spiBufferOut, 1, data, 100, 10);
	 // while((SPIbusy)&& (j<0xfffff)) j++;
		//	R_SPIFlash_CS(cs, FLASH_UNSELECT);
			

} 

uint8_t R_SPIFlash_readStatus( void) {
spiBufferOut[0] = SPIFLASH_RDSR1;
uint8_t status[2];		
	
    //CHIP_SELECT(cs);
		R_SPIFlash_CS(FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_RX(status, 2, 10);
    R_SPIFlash_CS(FLASH_UNSELECT);

	return status[1];
}
/// Lectura de n bytes. Direccion, lugar en el que se guarda la informacion y longitud de la misma
// MODIFICABILIDAD 26 bits de direccionamiento
/** @brief Reads n bytes from desired direction onwards.
 *	
 *				 This function can begin the lecture in one memory and finish in another one,transparent to user.
 * 
 *
 *	@param	uint32_t addr: indicates the address from which will begin the lecture
 *
 *	@param uint8_t * data: the array where the information will be stored
 *
 *	@param uint16_t len:     the number of bytes desired to be read
 *
 *
 *	@return void.
 ***********************************************************************************************************/
unsigned char R_SPIFlash_ReadBytes(uint32_t addr, uint8_t *data, uint16_t len) {
		
		uint32_t leidos = 0;
		uint8_t spiBufferIn[MAX_READ];
		
		
		
		while(len>0){
				
				//en los primeros bytes se encuentra el chip select. Puede haber variado en la anterior iteracion
				//in the first bytes lies the chip select number. It could have changed in the last iteration.
				//en los ultimos bytes se encuentra la direccion real que nos interesa
				 address = addr & (0x03FFFFFF);
				while(R_SPIFlash_Busy());
				//alojamos en el bufer de salida el comando read y a continuacion la direccion que queremos leer
				spiBufferOut[0] = SPIFLASH_READ;
				spiBufferOut[1] = (address >> 24) & 0x000000FF;
				spiBufferOut[2] = (address >> 16) & 0x000000FF;
				spiBufferOut[3] = (address >> 8) & 0x000000FF;
				spiBufferOut[4] = address & 0x000000FF;
				uint32_t bytes2Rx = len;
				if((address + len) > (MEMORY_ADRESSING_SIZE)){//si lo que vamos a leer es mas grande que lo disponible en memoria
					bytes2Rx = MEMORY_ADRESSING_SIZE - address;//forzamos a leer una cantidad acotada por el maximo de direcciones en memoria
				}
				
				
				  R_SPIFlash_CS(FLASH_SELECT);
					R_SPIFlash_TX(spiBufferOut,5,10);
					R_SPIFlash_RX(&spiBufferIn[leidos], bytes2Rx, 10);
					R_SPIFlash_CS(FLASH_UNSELECT);
				
				addr += bytes2Rx;//se aumenta la direccion en consecuencia. Esto podria cambiar el chip select.
				leidos += bytes2Rx;//lleva la cuenta de lo leido. indice de escritura de bufferIn.
				len -= bytes2Rx;//en la prox iteracion quedan menos bytes por leer
				
		}
		//pasamos la informacion almacenada en el buffer de entrada al parametro de la funcion
		
		memcpy(data,spiBufferIn,leidos);
		if(leidos > 255)
				leidos = 255;
		return leidos;
}


/// check if the chip is busy erasing/writing
unsigned char R_SPIFlash_Busy(){
	/*
	uint32_t cs = addr >> 26;
	uint8_t aux;
	uint8_t aux2;
	aux = SPIFLASH_RDSR1;
	CHIP_SELECT(cs);
	HAL_SPI_Transmit(&hspi3,&aux,1,10);
	HAL_SPI_Receive(&hspi3,&aux,1,10);
	aux2 = aux & (0x02);
	CHIP_UNSELECT(cs);
	
	return ((aux2) == (0x02));
	*/
	uint8_t aux;
	uint8_t aux2;
	aux= R_SPIFlash_readStatus();
	aux2 = aux & (0x01);
	//HAL_SPI_DebugSerial(&aux2,1);
	if (aux2 == 1) 
		return 1; 
	else 
		return 0;
	
}


unsigned char R_SPIFlash_isWE(void){
	/*
	uint32_t cs = addr >> 26;
	uint8_t aux;
	uint8_t aux2;
	aux = SPIFLASH_RDSR1;
	CHIP_SELECT(cs);
	HAL_SPI_Transmit(&hspi3,&aux,1,10);
	HAL_SPI_Receive(&hspi3,&aux,1,10);
	aux2 = aux & (0x02);
	CHIP_UNSELECT(cs);
	
	return ((aux2) == (0x02));
	*/
	uint8_t aux;
	uint8_t aux2;
	aux= R_SPIFlash_readStatus();
	aux2 = aux & (0x02);
	//HAL_SPI_DebugSerial(&aux2,1);
	if (aux2 == 2) 
		return 1; 
	else 
		return 0;
	
}






///Escritura de n bytes
// MODIFICABILIDAD: cambiar 512 de p�gina y 26 bits de direccionamiento
/*void writeBytes(uint32_t addr, uint8_t *data, uint16_t len) {
  uint32_t bytes2Tx;
  uint16_t maxBytes = 512-(addr%512);  //segun la direccion en la que vamos a empezar a escribir, consideramos su lugar en la p�gina
  uint16_t escribidos = 0;
	
  while (len>0)
  {
		
		//en los primeros bytes se encuentra el chip select. Puede haber variado en la anterior iteracion
		uint32_t cs = addr >> 26;
		//en los ultimos bytes se encuentra la direccion real que nos interesa
		uint32_t address = addr & (0xFFFFFFFF >> 6);
			
		if(addr%(262144) == 0){
			sectorErase(addr);
			while (busy(addr));
		}
		//transferimos write enable para poder escribir
		spiBufferOut[0] = SPIFLASH_WRITEENABLE;
		
		CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		CHIP_UNSELECT(cs);
		
		//alojamos en el bufer de salida el comando write y a continuacion la direccion que queremos leer
		spiBufferOut[0] = SPIFLASH_PAGEPROGRAM;
		spiBufferOut[1] = (address >> 24) & 0x000000FF;
		spiBufferOut[2] = (address >> 16) & 0x000000FF;
		spiBufferOut[3] = (address >> 8) & 0x000000FF;
		spiBufferOut[4] = address & 0x000000FF;
		
		
		//n: cantidad de bytes a enviar en esta iteracion, como mucho una pagina entera.
    bytes2Tx = (len<=maxBytes) ? len : maxBytes;
		
		if((address + bytes2Tx) >= (MEMORY_ADRESSING_SIZE)){//si lo que vamos a escribir es mas grande que lo disponible en memoria
					bytes2Tx = MEMORY_ADRESSING_SIZE - address;//forzamos a escribir una cantidad acotada por el maximo de direcciones en memoria
				}
		
		//a�adimos la informacion a transmitir al bufer de salida
		memcpy(&spiBufferOut[5],&data[escribidos],bytes2Tx);
		
		
    CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 5 + bytes2Tx, 10);
		CHIP_UNSELECT(cs);
		
    
    addr+=bytes2Tx;  //ajusta la direccion segun la cantidad de bytes transmitidos para continuar en la siguiente iteracion. podria modificar chip select.
    escribidos +=bytes2Tx;//para posicionarse en el arreglo del dato a escribir
    len -= bytes2Tx;
    maxBytes = 512;   // ahora se pueden escribir 512 bytes por p�gina
		
		//transferimos write disable para evitar la escritura
		spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		CHIP_UNSELECT(cs);
		
  }

	
}*/


unsigned char R_SPIFlash_WriteBytes(uint32_t addr, uint8_t *data, uint16_t len) {
  uint32_t bytes2Tx;
  uint16_t maxBytes = 512-(addr%512);  //segun la direccion en la que vamos a empezar a escribir, consideramos su lugar en la p�gina
  uint16_t escribidos = 0;
	
  while (len>0)
  {
		//en los ultimos bytes se encuentra la direccion real que nos interesa
		uint32_t address = addr & (0xFFFFFFFF >> 6);

		//aux= R_SPIFlash_readStatus(0);
	
		if (addr % MEMORY_SECTOR_SIZE==0){
	//	if(addr%(262144) == 0){ //cambio por sector de 64k
			R_SPIFlash_SectorErase(addr);
			while (R_SPIFlash_Busy()){};
		}

		//transferimos write enable para poder escribir
		spiBufferOut[0] = SPIFLASH_WRITEENABLE;
		R_SPIFlash_CS(FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut,1,1);
		R_SPIFlash_CS(FLASH_UNSELECT);
	  while(!R_SPIFlash_isWE());
		///////////7
	//	aux = R_SPIFlash_readStatus(0);
	//	HAL_SPI_DebugSerial(&aux,1);
/////////////
		
		//alojamos en el bufer de salida el comando write y a continuacion la direccion que queremos leer
		spiBufferOut[0] = SPIFLASH_PAGEPROGRAM;
		spiBufferOut[1] = (address >> 24) & 0x000000FF;
		spiBufferOut[2] = (address >> 16) & 0x000000FF;
		spiBufferOut[3] = (address >> 8) & 0x000000FF;
		spiBufferOut[4] = address & 0x000000FF;
		
		
		//n: cantidad de bytes a enviar en esta iteracion, como mucho una pagina entera.
    bytes2Tx = (len<=maxBytes) ? len : maxBytes;
		
		if((address + bytes2Tx) >= (MEMORY_ADRESSING_SIZE)){//si lo que vamos a escribir es mas grande que lo disponible en memoria
					bytes2Tx = MEMORY_ADRESSING_SIZE - address;//forzamos a escribir una cantidad acotada por el maximo de direcciones en memoria
				}
		
		//a�adimos la informacion a transmitir al bufer de salida
		memcpy(&spiBufferOut[5],&data[escribidos],bytes2Tx);
		

   	R_SPIFlash_CS(FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut,5+bytes2Tx,10);
		R_SPIFlash_CS(FLASH_UNSELECT);
				
		while(R_SPIFlash_Busy()){} ;
		
		
    
    addr+=bytes2Tx;  //ajusta la direccion segun la cantidad de bytes transmitidos para continuar en la siguiente iteracion. podria modificar chip select.
    escribidos +=bytes2Tx;//para posicionarse en el arreglo del dato a escribir
    len -= bytes2Tx;
    maxBytes = 512;   // ahora se pueden escribir 512 bytes por p�gina
		
		//transferimos write disable para evitar la escritura
		spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		R_SPIFlash_CS(FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_CS(FLASH_UNSELECT);
		
	//	aux = R_SPIFlash_readStatus(0);
	//	HAL_SPI_DebugSerial(&aux,1);

  }
return 0;
	
}






/// Borra un sector, de 256Kb //o 64??
void R_SPIFlash_SectorErase(uint32_t addr) {
		
		//el cs se encuentra en los bits mas altos
	//	uint8_t cs = addr >> 26;
		
		//usamos como direccion la pasada por parametro con una mascara que pone en cero los bits del chip select
		uint32_t address = addr & (0xFFFFFFFF >> 6);
		
		spiBufferOut[0] = SPIFLASH_WRITEENABLE;
		R_SPIFlash_CS(FLASH_SELECT);
		//HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_CS(FLASH_UNSELECT);
	
	  while(!R_SPIFlash_isWE());

		
		spiBufferOut[0] = SPIFLASH_ERASESECTOR;
		spiBufferOut[1] = address >> 24 & 0x000000FF;
		spiBufferOut[2] = address >> 16 & 0x000000FF;
		spiBufferOut[3] = address >> 8 & 0x000000FF;
		spiBufferOut[4] = address & 0x000000FF;
		//a�adimos la informacion a transmitir al bufer de salida
		
    /*CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 5, 10);
		CHIP_UNSELECT(cs);
		*/
		R_SPIFlash_CS(FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 5, 10);
		R_SPIFlash_CS(FLASH_UNSELECT);
		
				
	  while(R_SPIFlash_Busy());
		
		spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		R_SPIFlash_CS(FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_CS(FLASH_UNSELECT);
		
		
}

void R_SPIFlash_BulkErase(void) {
		
		spiBufferOut[0] = SPIFLASH_WRITEENABLE;
		/*CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		CHIP_UNSELECT(cs);*/
		
		//spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		R_SPIFlash_CS(FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_CS(FLASH_UNSELECT);
		
		spiBufferOut[0] = SPIFLASH_BULKERASE;
		//a�adimos la informacion a transmitir al bufer de salida
			
    /*CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		CHIP_UNSELECT(cs);*/
		
//	spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		R_SPIFlash_CS(FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_CS(FLASH_UNSELECT);

	//	spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		/*CHIP_SELECT(cs);
		HAL_SPI_Transmit(&hspi3, spiBufferOut, 1, 10);
		CHIP_UNSELECT(cs);*/
	  while(R_SPIFlash_Busy());

		spiBufferOut[0] = SPIFLASH_WRITEDISABLE;
		R_SPIFlash_CS(FLASH_SELECT);
		R_SPIFlash_TX(spiBufferOut, 1, 10);
		R_SPIFlash_CS(FLASH_UNSELECT);
}

/*void SPIFlash::sleep() {
  command(SPIFLASH_SLEEP);
  unselect();
}

void SPIFlash::wakeup() {
  command(SPIFLASH_WAKE);
  unselect();
}

/// cleanup
void SPIFlash::end() {
  SPI.end();
}  */
