 /**
  * @file    R_SPIFlash_STM.h
  * @author  Emiliano Trepichio - Ported by Oscar E. Go�i
  *	@autor   Leandro J. Aguierre (nrf51xxx)
  * @version V1.0
  * @date    Octuber 16th, 2016
  * @brief   This file provides hardware specific functions to manage and control a set of S25FL512S Spansion
	*						Flash memory www.spansion.com/Support/Datasheets/S25FL512S_00.pdf
	* 	        The functions described here are targeted to STM32 microcontrollers
	*
	* @test			nrf51xxx
	*
	*	@todo 		+Add a dynamic mechanism to define PORTS and GPIO for specific Chip Select. 
							+Add Asserts to check if device was previously defined.
	*			
	*	@copyright COPYRIGHT(c) 2016 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *  
  *********************************************************************************************************/


#ifndef _SPIFLASH_NRF
#define _SPIFLASH_NRF
#include <stdint.h>

#define CS_ACTIVE_LOW
#define HOLD_ACTIVE_LOW


unsigned char R_SPIFlash_Attach(uint32_t CSPin, uint32_t WPPin, uint32_t HOLDPin);
void R_SPIFlash_init(uint8_t miso_pin,uint8_t mosi_pin,uint8_t ss_pin,uint8_t sck_pin);
void R_SPIFlash_CS(uint8_t State);
void R_SPIFlash_HOLD(uint32_t HOLDPin, uint8_t State);
void R_SPIFlash_WP(uint32_t WPPin, uint8_t State);
void R_SPIFlash_TX(unsigned char *spiBuffer, unsigned short len, unsigned int timeOut);
void R_SPIFlash_RX(unsigned char *spiBuffer, unsigned short len, unsigned int timeOut);
void R_SPIFlash_Delay(uint32_t delay);
#endif
