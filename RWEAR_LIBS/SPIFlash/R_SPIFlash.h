// Copyright (c) 2013-2015 by Felix Rusu, LowPowerLab.com
// SPI Flash memory library for arduino/moteino.
// This works with 256byte/page SPI flash memory
// For instance a 4MBit (512Kbyte) flash chip will have 2048 pages: 256*2048 = 524288 bytes (512Kbytes)
// Minimal modifications should allow chips that have different page size but modifications
// DEPENDS ON: Arduino SPI library
// > Updated Jan. 5, 2015, TomWS1, modified writeBytes to allow blocks > 256 bytes and handle page misalignment.
// > Updated Feb. 26, 2015 TomWS1, added support for SPI Transactions (Arduino 1.5.8 and above)
// > Selective merge by Felix after testing in IDE 1.0.6, 1.6.4
// **********************************************************************************
// License
// **********************************************************************************
// This program is free software; you can redistribute it 
// and/or modify it under the terms of the GNU General    
// Public License as published by the Free Software       
// Foundation; either version 3 of the License, or        
// (at your option) any later version.                    
//                                                        
// This program is distributed in the hope that it will   
// be useful, but WITHOUT ANY WARRANTY; without even the  
// implied warranty of MERCHANTABILITY or FITNESS FOR A   
// PARTICULAR PURPOSE. See the GNU General Public        
// License for more details.                              
//                                                        
// You should have received a copy of the GNU General    
// Public License along with this program.
// If not, see <http://www.gnu.org/licenses/>.
//                                                        
// Licence can be viewed at                               
// http://www.gnu.org/licenses/gpl-3.0.txt
//
// Please maintain this license information along with authorship
// and copyright notices in any redistribution of this code


#ifndef _SPIFLASH_H
#define _SPIFLASH_H


#include <stdint.h>

/// IMPORTANT: NAND FLASH memory requires erase before write, because
///            it can only transition from 1s to 0s and only the erase command can reset all 0s to 1s
/// See http://en.wikipedia.org/wiki/Flash_memory
/// The smallest range that can be erased is a sector (4K, 32K, 64K); there is also a chip erase command

/// Standard SPI flash commands
/// Assuming the WP pin is pulled up (to disable hardware write protection)
/// To use any write commands the WEL bit in the status register must be set to 1.
/// This is accomplished by sending a 0x06 command before any such write/erase command.
/// The WEL bit in the status register resets to the logical �0� state after a
/// device power-up or reset. In addition, the WEL bit will be reset to the logical �0� state automatically under the following conditions:
/// � Write Disable operation completes successfully
/// � Write Status Register operation completes successfully or aborts
/// � Protect Sector operation completes successfully or aborts
/// � Unprotect Sector operation completes successfully or aborts
/// � Byte/Page Program operation completes successfully or aborts
/// � Sequential Program Mode reaches highest unprotected memory location
/// � Sequential Program Mode reaches the end of the memory array
/// � Sequential Program Mode aborts
/// � Block Erase operation completes successfully or aborts
/// � Chip Erase operation completes successfully or aborts
/// � Hold condition aborts

//Write Protect
#define WP_PORT GPIOB
#define WP_PIN GPIO_PIN_8

//Hold
#define HOLD_PORT GPIOC
#define HOLD_PIN GPIO_PIN_1

//CS
#define CS0_PIN GPIO_PIN_2
#define CS1_PIN GPIO_PIN_0
#define CS0_PORT GPIOE
#define CS1_PORT GPIOE

/*/Macros para usar cada chip select/
#define CHIP1_SELECT 							HAL_GPIO_WritePin(CS_PORT,CS1_PIN,GPIO_PIN_RESET); //REDI
#define CHIP1_UNSELECT							HAL_GPIO_WritePin(CS_PORT,CS1_PIN,GPIO_PIN_SET);//REDI
#define CHIP0_SELECT 							HAL_GPIO_WritePin(CS_PORT,CS0_PIN,GPIO_PIN_RESET);//REDI
#define CHIP0_UNSELECT							HAL_GPIO_WritePin(CS_PORT,CS0_PIN,GPIO_PIN_SET);//REDI
*/
//Otros parametros modificables
#define MEMORY_SECTOR_SIZE 		0x00010000 //(64k)
//#define MEMORY_SECTOR_SIZE 		0x00040000 //(256k)
#define MEMORY_ADRESSING_SIZE 67108864   //direccion 0x04000000
#define NUMBER_OF_MEMORIES 1
#define MAX_READ 700


//comandos de la memoria
#define SPIFLASH_WRITEENABLE      0x06        // write enable //REDI
#define SPIFLASH_WRITEDISABLE     0x04        // write disable //REDI
#define SPIFLASH_PAGEPROGRAM			0x12				//page-program/write //REDI
#define SPIFLASH_READ							0x13				//read //REDI
#define SPIFLASH_ERASESECTOR			0xDC				//sector erase //REDI
#define SPIFLASH_BULKERASE				0x60				//bulk erase //REDI
#define SPIFLASH_RDSR1						0x05				//read Status Register 1
#define SPIFLASH_READID						0x9F				//read JEDEC ids

#define SPIFLASH_STATUSREAD       0x05        // read status register
#define SPIFLASH_STATUSWRITE      0x01        // write status register
#define SPIFLASH_ARRAYREAD        0x0B        // read array (fast, need to add 1 dummy byte after 3 address bytes)
#define SPIFLASH_ARRAYREADLOWFREQ 0x03        // read array (low frequency)

#define SPIFLASH_SLEEP            0xB9        // deep power down
#define SPIFLASH_WAKE             0xAB        // deep power wake up
#define SPIFLASH_IDREAD           0x9F        // read JEDEC manufacturer and device ID (2 bytes, specific bytes for each manufacturer and device)
                                              // Example for Atmel-Adesto 4Mbit AT25DF041A: 0x1F44 (page 27: http://www.adestotech.com/sites/default/files/datasheets/doc3668.pdf)
                                              // Example for Winbond 4Mbit W25X40CL: 0xEF30 (page 14: http://www.winbond.com/NR/rdonlyres/6E25084C-0BFE-4B25-903D-AE10221A0929/0/W25X40CL.pdf)
#define SPIFLASH_MACREAD          0x4B        // read unique ID number (MAC)

#define SPIFLASH_SIMBOL_NEWSECTOR		'�'			// Simbol to indicate the beginig of new sector (64Kb) en memory
#define SPIFLASH_SIMBOL_CONTINUE		'^'			// Simbol to indicate more data in memory.


#define CHIP_WRITE_PROTECTED 		0
#define CHIP_WRITE_UNPROTECTED	1



  static uint8_t UNIQUEID[8];
  uint8_t R_SPIFlash_readStatus(void);
  uint8_t R_SPIFlash_ReadBytes(uint32_t addr, uint8_t *data, uint16_t len); //REDI
  uint8_t R_SPIFlash_WriteBytes(uint32_t addr, uint8_t *data, uint16_t len); //REDI
  unsigned char R_SPIFlash_Busy(void);
	unsigned char R_SPIFlash_isWE(void);
  void R_SPIFlash_SectorErase(uint32_t addr); //REDI
  void R_SPIFlash_BulkErase(void); //REDI
	void R_SPIFlash_ReadID(uint8_t * data);


	//extern void R_SPIFlash_CS(i,CHIP_SELECT);
#define CS_ACTIVE_LOW
#define HOLD_ACTIVE_LOW

  void sleep(void);
  void wakeup(void);
  void end(void);

#ifdef CS_ACTIVE_LOW
	#define FLASH_SELECT 	0
	#define FLASH_UNSELECT 1
#else 
	#define FLASH_SELECT 	1
	#define FLASH_UNSELECT 0
#endif


#ifdef HOLD_ACTIVE_LOW
	#define HOLD_ON			 	0
	#define HOLD_OFF 			1
#else 
	#define HOLD_ON			 	1
	#define HOLD_OFF			0
#endif





#ifdef SPI_HAS_TRANSACTION
  SPISettings _settings;
#endif

#endif
