/*
 * File: interp1.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef INTERP1_H
#define INTERP1_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void interp1(const float varargin_1[5], const float varargin_2[5], const
                    float varargin_3[5], float Vq[5]);

#endif

/*
 * File trailer for interp1.h
 *
 * [EOF]
 */
