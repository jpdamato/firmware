/*
 * File: mldivide.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 21-Oct-2019 12:58:03
 */

/* Include Files */
#include "mldivide.h"
#include "calcularAceleracion.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"
#include "xzgeqp3.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : const double A[40000]
 *                const double B[10000]
 *                double Y[4]
 * Return Type  : void
 */
void mldivide(const double A[40000], const double B[10000], double Y[4])
{
  int jpvt[4];
  static double b_A[40000];
  double tau[4];
  int rankR;
  double tol;
  double b_B[10000];
  int i;
  int j;
  int Y_tmp;
  int b_Y_tmp;
  jpvt[0] = 1;
  jpvt[1] = 2;
  jpvt[2] = 3;
  jpvt[3] = 4;
  memcpy(&b_A[0], &A[0], 40000U * sizeof(double));
  tau[0] = 0.0;
  tau[1] = 0.0;
  tau[2] = 0.0;
  tau[3] = 0.0;
  qrpf(b_A, tau, jpvt);
  rankR = 0;
  tol = 2.2204460492503131E-11 * fabs(b_A[0]);
  while ((rankR < 4) && (!(fabs(b_A[rankR + 10000 * rankR]) <= tol))) {
    rankR++;
  }

  memcpy(&b_B[0], &B[0], 10000U * sizeof(double));
  Y[0] = 0.0;
  if (tau[0] != 0.0) {
    tol = b_B[0];
    for (i = 2; i < 10001; i++) {
      tol += b_A[i - 1] * b_B[i - 1];
    }

    tol *= tau[0];
    if (tol != 0.0) {
      b_B[0] -= tol;
      for (i = 2; i < 10001; i++) {
        b_B[i - 1] -= b_A[i - 1] * tol;
      }
    }
  }

  Y[1] = 0.0;
  if (tau[1] != 0.0) {
    tol = b_B[1];
    for (i = 3; i < 10001; i++) {
      tol += b_A[i + 9999] * b_B[i - 1];
    }

    tol *= tau[1];
    if (tol != 0.0) {
      b_B[1] -= tol;
      for (i = 3; i < 10001; i++) {
        b_B[i - 1] -= b_A[i + 9999] * tol;
      }
    }
  }

  Y[2] = 0.0;
  if (tau[2] != 0.0) {
    tol = b_B[2];
    for (i = 4; i < 10001; i++) {
      tol += b_A[i + 19999] * b_B[i - 1];
    }

    tol *= tau[2];
    if (tol != 0.0) {
      b_B[2] -= tol;
      for (i = 4; i < 10001; i++) {
        b_B[i - 1] -= b_A[i + 19999] * tol;
      }
    }
  }

  Y[3] = 0.0;
  if (tau[3] != 0.0) {
    tol = b_B[3];
    for (i = 5; i < 10001; i++) {
      tol += b_A[i + 29999] * b_B[i - 1];
    }

    tol *= tau[3];
    if (tol != 0.0) {
      b_B[3] -= tol;
      for (i = 5; i < 10001; i++) {
        b_B[i - 1] -= b_A[i + 29999] * tol;
      }
    }
  }

  for (i = 0; i < rankR; i++) {
    Y[jpvt[i] - 1] = b_B[i];
  }

  for (j = rankR; j >= 1; j--) {
    Y_tmp = jpvt[j - 1] - 1;
    b_Y_tmp = 10000 * (j - 1);
    Y[Y_tmp] /= b_A[(j + b_Y_tmp) - 1];
    for (i = 0; i <= j - 2; i++) {
      Y[jpvt[i] - 1] -= Y[Y_tmp] * b_A[i + b_Y_tmp];
    }
  }
}

/*
 * File trailer for mldivide.c
 *
 * [EOF]
 */
