/*
 * File: IMUBasicEKF.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "IMUBasicEKF.h"
#include "calcularAceleracion.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : float x[28]
 *                float P[784]
 *                const float h[6]
 *                const double H[168]
 *                const float z[6]
 *                const float R[36]
 * Return Type  : void
 */
void IMUBasicEKF_correctEqn(float x[28], float P[784], const float h[6], const
  double H[168], const float z[6], const float R[36])
{
  int i;
  int jp1j;
  int iy;
  double b_tmp[168];
  float smax;
  int jA;
  int j;
  signed char ipiv[6];
  float b_H[168];
  int mmj_tmp;
  float S[36];
  int b;
  float W[168];
  int jj;
  int k;
  int kBcol;
  int ix;
  float b_z[6];
  float b_P[784];
  float s;
  float b_W[784];
  for (i = 0; i < 6; i++) {
    for (jp1j = 0; jp1j < 28; jp1j++) {
      iy = i + 6 * jp1j;
      b_tmp[jp1j + 28 * i] = H[iy];
      smax = 0.0F;
      for (jA = 0; jA < 28; jA++) {
        smax += (float)H[i + 6 * jA] * P[jA + 28 * jp1j];
      }

      b_H[iy] = smax;
    }
  }

  for (i = 0; i < 6; i++) {
    for (jp1j = 0; jp1j < 6; jp1j++) {
      smax = 0.0F;
      for (jA = 0; jA < 28; jA++) {
        smax += b_H[i + 6 * jA] * (float)b_tmp[jA + 28 * jp1j];
      }

      iy = i + 6 * jp1j;
      S[iy] = smax + R[iy];
    }
  }

  for (i = 0; i < 28; i++) {
    for (jp1j = 0; jp1j < 6; jp1j++) {
      smax = 0.0F;
      for (jA = 0; jA < 28; jA++) {
        smax += P[i + 28 * jA] * (float)b_tmp[jA + 28 * jp1j];
      }

      W[i + 28 * jp1j] = smax;
    }
  }

  for (i = 0; i < 6; i++) {
    ipiv[i] = (signed char)(i + 1);
  }

  for (j = 0; j < 5; j++) {
    mmj_tmp = 4 - j;
    b = j * 7;
    jj = j * 7;
    jp1j = b + 2;
    jA = 6 - j;
    kBcol = 0;
    ix = b;
    smax = fabsf(S[jj]);
    for (k = 2; k <= jA; k++) {
      ix++;
      s = fabsf(S[ix]);
      if (s > smax) {
        kBcol = k - 1;
        smax = s;
      }
    }

    if (S[jj + kBcol] != 0.0F) {
      if (kBcol != 0) {
        iy = j + kBcol;
        ipiv[j] = (signed char)(iy + 1);
        ix = j;
        for (k = 0; k < 6; k++) {
          smax = S[ix];
          S[ix] = S[iy];
          S[iy] = smax;
          ix += 6;
          iy += 6;
        }
      }

      i = (jj - j) + 6;
      for (ix = jp1j; ix <= i; ix++) {
        S[ix - 1] /= S[jj];
      }
    }

    iy = b + 6;
    jA = jj;
    for (kBcol = 0; kBcol <= mmj_tmp; kBcol++) {
      smax = S[iy];
      if (S[iy] != 0.0F) {
        ix = jj + 1;
        i = jA + 8;
        jp1j = (jA - j) + 12;
        for (b = i; b <= jp1j; b++) {
          S[b - 1] += S[ix] * -smax;
          ix++;
        }
      }

      iy += 6;
      jA += 6;
    }
  }

  for (j = 0; j < 6; j++) {
    iy = 28 * j - 1;
    jA = 6 * j;
    for (k = 0; k < j; k++) {
      kBcol = 28 * k;
      i = k + jA;
      if (S[i] != 0.0F) {
        for (ix = 0; ix < 28; ix++) {
          b = (ix + iy) + 1;
          W[b] -= S[i] * W[ix + kBcol];
        }
      }
    }

    smax = 1.0F / S[j + jA];
    for (ix = 0; ix < 28; ix++) {
      b = (ix + iy) + 1;
      W[b] *= smax;
    }
  }

  for (j = 5; j >= 0; j--) {
    iy = 28 * j - 1;
    jA = 6 * j - 1;
    i = j + 2;
    for (k = i; k < 7; k++) {
      kBcol = 28 * (k - 1);
      jp1j = k + jA;
      if (S[jp1j] != 0.0F) {
        for (ix = 0; ix < 28; ix++) {
          b = (ix + iy) + 1;
          W[b] -= S[jp1j] * W[ix + kBcol];
        }
      }
    }
  }

  for (iy = 4; iy >= 0; iy--) {
    if (ipiv[iy] != iy + 1) {
      for (jA = 0; jA < 28; jA++) {
        kBcol = jA + 28 * iy;
        smax = W[kBcol];
        b = jA + 28 * (ipiv[iy] - 1);
        W[kBcol] = W[b];
        W[b] = smax;
      }
    }
  }

  for (i = 0; i < 6; i++) {
    b_z[i] = z[i] - h[i];
  }

  for (i = 0; i < 28; i++) {
    smax = 0.0F;
    for (jp1j = 0; jp1j < 6; jp1j++) {
      smax += W[i + 28 * jp1j] * b_z[jp1j];
    }

    x[i] += smax;
    for (jp1j = 0; jp1j < 28; jp1j++) {
      smax = 0.0F;
      for (jA = 0; jA < 6; jA++) {
        smax += W[i + 28 * jA] * (float)H[jA + 6 * jp1j];
      }

      b_W[i + 28 * jp1j] = smax;
    }

    for (jp1j = 0; jp1j < 28; jp1j++) {
      smax = 0.0F;
      for (jA = 0; jA < 28; jA++) {
        smax += b_W[i + 28 * jA] * P[jA + 28 * jp1j];
      }

      iy = i + 28 * jp1j;
      b_P[iy] = P[iy] - smax;
    }
  }

  memcpy(&P[0], &b_P[0], 784U * sizeof(float));
  IMUBasicEKF_repairQuaternion(x);
}

/*
 * Arguments    : float x[28]
 * Return Type  : void
 */
void IMUBasicEKF_repairQuaternion(float x[28])
{
  float qparts_idx_3;
  float qparts_idx_0;
  float qparts_idx_1;
  float qparts_idx_2;
  qparts_idx_3 = sqrtf(((x[0] * x[0] + x[1] * x[1]) + x[2] * x[2]) + x[3] * x[3]);
  qparts_idx_0 = x[0] / qparts_idx_3;
  qparts_idx_1 = x[1] / qparts_idx_3;
  qparts_idx_2 = x[2] / qparts_idx_3;
  qparts_idx_3 = x[3] / qparts_idx_3;
  if (qparts_idx_0 < 0.0F) {
    x[0] = -qparts_idx_0;
    x[1] = -qparts_idx_1;
    x[2] = -qparts_idx_2;
    x[3] = -qparts_idx_3;
  } else {
    x[0] = qparts_idx_0;
    x[1] = qparts_idx_1;
    x[2] = qparts_idx_2;
    x[3] = qparts_idx_3;
  }
}

/*
 * Arguments    : float x[28]
 *                float P[784]
 *                const float h[3]
 *                const float H[84]
 *                const float z[3]
 *                const float R[9]
 * Return Type  : void
 */
void b_IMUBasicEKF_correctEqn(float x[28], float P[784], const float h[3], const
  float H[84], const float z[3], const float R[9])
{
  int W_tmp;
  int b_W_tmp;
  int c_W_tmp;
  float W[84];
  int r1;
  float maxval;
  int r2;
  int rtemp;
  int r3;
  float S[9];
  float b_H[84];
  float a21;
  float y[84];
  float z_idx_1;
  float z_idx_2;
  float b_P[784];
  float f;
  float b_W[784];
  for (W_tmp = 0; W_tmp < 3; W_tmp++) {
    for (b_W_tmp = 0; b_W_tmp < 28; b_W_tmp++) {
      c_W_tmp = W_tmp + 3 * b_W_tmp;
      W[b_W_tmp + 28 * W_tmp] = H[c_W_tmp];
      maxval = 0.0F;
      for (rtemp = 0; rtemp < 28; rtemp++) {
        maxval += H[W_tmp + 3 * rtemp] * P[rtemp + 28 * b_W_tmp];
      }

      b_H[c_W_tmp] = maxval;
    }
  }

  for (W_tmp = 0; W_tmp < 3; W_tmp++) {
    for (b_W_tmp = 0; b_W_tmp < 3; b_W_tmp++) {
      maxval = 0.0F;
      for (rtemp = 0; rtemp < 28; rtemp++) {
        maxval += b_H[W_tmp + 3 * rtemp] * W[rtemp + 28 * b_W_tmp];
      }

      rtemp = W_tmp + 3 * b_W_tmp;
      S[rtemp] = maxval + R[rtemp];
    }
  }

  for (W_tmp = 0; W_tmp < 28; W_tmp++) {
    for (b_W_tmp = 0; b_W_tmp < 3; b_W_tmp++) {
      maxval = 0.0F;
      for (rtemp = 0; rtemp < 28; rtemp++) {
        maxval += P[W_tmp + 28 * rtemp] * W[rtemp + 28 * b_W_tmp];
      }

      y[W_tmp + 28 * b_W_tmp] = maxval;
    }
  }

  r1 = 0;
  r2 = 1;
  r3 = 2;
  maxval = fabsf(S[0]);
  a21 = fabsf(S[1]);
  if (a21 > maxval) {
    maxval = a21;
    r1 = 1;
    r2 = 0;
  }

  if (fabsf(S[2]) > maxval) {
    r1 = 2;
    r2 = 1;
    r3 = 0;
  }

  S[r2] /= S[r1];
  S[r3] /= S[r1];
  S[r2 + 3] -= S[r2] * S[r1 + 3];
  S[r3 + 3] -= S[r3] * S[r1 + 3];
  S[r2 + 6] -= S[r2] * S[r1 + 6];
  S[r3 + 6] -= S[r3] * S[r1 + 6];
  if (fabsf(S[r3 + 3]) > fabsf(S[r2 + 3])) {
    rtemp = r2;
    r2 = r3;
    r3 = rtemp;
  }

  S[r3 + 3] /= S[r2 + 3];
  S[r3 + 6] -= S[r3 + 3] * S[r2 + 6];
  for (rtemp = 0; rtemp < 28; rtemp++) {
    c_W_tmp = rtemp + 28 * r1;
    W[c_W_tmp] = y[rtemp] / S[r1];
    W_tmp = rtemp + 28 * r2;
    W[W_tmp] = y[rtemp + 28] - W[c_W_tmp] * S[r1 + 3];
    b_W_tmp = rtemp + 28 * r3;
    W[b_W_tmp] = y[rtemp + 56] - W[c_W_tmp] * S[r1 + 6];
    W[W_tmp] /= S[r2 + 3];
    W[b_W_tmp] -= W[W_tmp] * S[r2 + 6];
    W[b_W_tmp] /= S[r3 + 6];
    W[W_tmp] -= W[b_W_tmp] * S[r3 + 3];
    W[c_W_tmp] -= W[b_W_tmp] * S[r3];
    W[c_W_tmp] -= W[W_tmp] * S[r2];
  }

  a21 = z[0] - h[0];
  z_idx_1 = z[1] - h[1];
  z_idx_2 = z[2] - h[2];
  for (W_tmp = 0; W_tmp < 28; W_tmp++) {
    maxval = W[W_tmp + 28];
    f = W[W_tmp + 56];
    x[W_tmp] += (W[W_tmp] * a21 + maxval * z_idx_1) + f * z_idx_2;
    for (b_W_tmp = 0; b_W_tmp < 28; b_W_tmp++) {
      b_W[W_tmp + 28 * b_W_tmp] = (W[W_tmp] * H[3 * b_W_tmp] + maxval * H[3 *
        b_W_tmp + 1]) + f * H[3 * b_W_tmp + 2];
    }

    for (b_W_tmp = 0; b_W_tmp < 28; b_W_tmp++) {
      maxval = 0.0F;
      for (rtemp = 0; rtemp < 28; rtemp++) {
        maxval += b_W[W_tmp + 28 * rtemp] * P[rtemp + 28 * b_W_tmp];
      }

      rtemp = W_tmp + 28 * b_W_tmp;
      b_P[rtemp] = P[rtemp] - maxval;
    }
  }

  memcpy(&P[0], &b_P[0], 784U * sizeof(float));
  IMUBasicEKF_repairQuaternion(x);
}

/*
 * Arguments    : float x[28]
 *                float P[784]
 *                const float h[3]
 *                const double H[84]
 *                const float z[3]
 *                const float R[9]
 * Return Type  : void
 */
void c_IMUBasicEKF_correctEqn(float x[28], float P[784], const float h[3], const
  double H[84], const float z[3], const float R[9])
{
  int W_tmp;
  int b_W_tmp;
  int rtemp;
  double b_tmp[84];
  int r1;
  float maxval;
  int r2;
  int k;
  int r3;
  float S[9];
  float W[84];
  float a21;
  float y[84];
  float z_idx_1;
  float z_idx_2;
  float b_P[784];
  float f;
  float b_W[784];
  for (W_tmp = 0; W_tmp < 3; W_tmp++) {
    for (b_W_tmp = 0; b_W_tmp < 28; b_W_tmp++) {
      rtemp = W_tmp + 3 * b_W_tmp;
      b_tmp[b_W_tmp + 28 * W_tmp] = H[rtemp];
      maxval = 0.0F;
      for (k = 0; k < 28; k++) {
        maxval += (float)H[W_tmp + 3 * k] * P[k + 28 * b_W_tmp];
      }

      W[rtemp] = maxval;
    }
  }

  for (W_tmp = 0; W_tmp < 3; W_tmp++) {
    for (b_W_tmp = 0; b_W_tmp < 3; b_W_tmp++) {
      maxval = 0.0F;
      for (k = 0; k < 28; k++) {
        maxval += W[W_tmp + 3 * k] * (float)b_tmp[k + 28 * b_W_tmp];
      }

      rtemp = W_tmp + 3 * b_W_tmp;
      S[rtemp] = maxval + R[rtemp];
    }
  }

  for (W_tmp = 0; W_tmp < 28; W_tmp++) {
    for (b_W_tmp = 0; b_W_tmp < 3; b_W_tmp++) {
      maxval = 0.0F;
      for (k = 0; k < 28; k++) {
        maxval += P[W_tmp + 28 * k] * (float)b_tmp[k + 28 * b_W_tmp];
      }

      y[W_tmp + 28 * b_W_tmp] = maxval;
    }
  }

  r1 = 0;
  r2 = 1;
  r3 = 2;
  maxval = fabsf(S[0]);
  a21 = fabsf(S[1]);
  if (a21 > maxval) {
    maxval = a21;
    r1 = 1;
    r2 = 0;
  }

  if (fabsf(S[2]) > maxval) {
    r1 = 2;
    r2 = 1;
    r3 = 0;
  }

  S[r2] /= S[r1];
  S[r3] /= S[r1];
  S[r2 + 3] -= S[r2] * S[r1 + 3];
  S[r3 + 3] -= S[r3] * S[r1 + 3];
  S[r2 + 6] -= S[r2] * S[r1 + 6];
  S[r3 + 6] -= S[r3] * S[r1 + 6];
  if (fabsf(S[r3 + 3]) > fabsf(S[r2 + 3])) {
    rtemp = r2;
    r2 = r3;
    r3 = rtemp;
  }

  S[r3 + 3] /= S[r2 + 3];
  S[r3 + 6] -= S[r3 + 3] * S[r2 + 6];
  for (k = 0; k < 28; k++) {
    rtemp = k + 28 * r1;
    W[rtemp] = y[k] / S[r1];
    W_tmp = k + 28 * r2;
    W[W_tmp] = y[k + 28] - W[rtemp] * S[r1 + 3];
    b_W_tmp = k + 28 * r3;
    W[b_W_tmp] = y[k + 56] - W[rtemp] * S[r1 + 6];
    W[W_tmp] /= S[r2 + 3];
    W[b_W_tmp] -= W[W_tmp] * S[r2 + 6];
    W[b_W_tmp] /= S[r3 + 6];
    W[W_tmp] -= W[b_W_tmp] * S[r3 + 3];
    W[rtemp] -= W[b_W_tmp] * S[r3];
    W[rtemp] -= W[W_tmp] * S[r2];
  }

  a21 = z[0] - h[0];
  z_idx_1 = z[1] - h[1];
  z_idx_2 = z[2] - h[2];
  for (W_tmp = 0; W_tmp < 28; W_tmp++) {
    maxval = W[W_tmp + 28];
    f = W[W_tmp + 56];
    x[W_tmp] += (W[W_tmp] * a21 + maxval * z_idx_1) + f * z_idx_2;
    for (b_W_tmp = 0; b_W_tmp < 28; b_W_tmp++) {
      b_W[W_tmp + 28 * b_W_tmp] = (W[W_tmp] * (float)H[3 * b_W_tmp] + maxval *
        (float)H[3 * b_W_tmp + 1]) + f * (float)H[3 * b_W_tmp + 2];
    }

    for (b_W_tmp = 0; b_W_tmp < 28; b_W_tmp++) {
      maxval = 0.0F;
      for (k = 0; k < 28; k++) {
        maxval += b_W[W_tmp + 28 * k] * P[k + 28 * b_W_tmp];
      }

      rtemp = W_tmp + 28 * b_W_tmp;
      b_P[rtemp] = P[rtemp] - maxval;
    }
  }

  memcpy(&P[0], &b_P[0], 784U * sizeof(float));
  IMUBasicEKF_repairQuaternion(x);
}

/*
 * File trailer for IMUBasicEKF.c
 *
 * [EOF]
 */
