/*
 * File: INSFilterEKF.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 21-Oct-2019 12:58:03
 */

#ifndef INSFILTEREKF_H
#define INSFILTEREKF_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void INSFilterEKF_pose(const c_fusion_internal_coder_insfilt *obj, double
  pos[3], double orient_a_data[], int orient_a_size[2], double orient_b_data[],
  int orient_b_size[2], double orient_c_data[], int orient_c_size[2], double
  orient_d_data[], int orient_d_size[2]);

#endif

/*
 * File trailer for INSFilterEKF.h
 *
 * [EOF]
 */
