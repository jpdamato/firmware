/*
 * File: interp1.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "interp1.h"
#include "calcularAceleracion.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "pchip.h"
#include "rt_nonfinite.h"

/* Function Definitions */

/*
 * Arguments    : const float varargin_1[5]
 *                const float varargin_2[5]
 *                const float varargin_3[5]
 *                float Vq[5]
 * Return Type  : void
 */
void interp1(const float varargin_1[5], const float varargin_2[5], const float
             varargin_3[5], float Vq[5])
{
  int i;
  float y[5];
  float x[5];
  int exitg1;
  float xtmp;
  float h_idx_0;
  float h_idx_1;
  float h_idx_2;
  float h_idx_3;
  float del_idx_0;
  float del_idx_1;
  float del_idx_2;
  float del_idx_3;
  float hs3;
  float dzzdx;
  float slopes_idx_1;
  float slopes_idx_2;
  float slopes_idx_3;
  float pp_coefs[16];
  int low_i;
  int low_ip1;
  int high_i;
  int mid_i;
  for (i = 0; i < 5; i++) {
    y[i] = varargin_2[i];
    x[i] = varargin_1[i];
  }

  i = 0;
  do {
    exitg1 = 0;
    if (i < 5) {
      if (rtIsNaNF(varargin_1[i])) {
        exitg1 = 1;
      } else {
        i++;
      }
    } else {
      if (varargin_1[1] < varargin_1[0]) {
        xtmp = x[0];
        x[0] = x[4];
        x[4] = xtmp;
        xtmp = y[0];
        y[0] = y[4];
        y[4] = xtmp;
        xtmp = x[1];
        x[1] = x[3];
        x[3] = xtmp;
        xtmp = y[1];
        y[1] = y[3];
        y[3] = xtmp;
      }

      h_idx_0 = x[1] - x[0];
      h_idx_1 = x[2] - x[1];
      h_idx_2 = x[3] - x[2];
      h_idx_3 = x[4] - x[3];
      del_idx_0 = (y[1] - y[0]) / h_idx_0;
      del_idx_1 = (y[2] - y[1]) / h_idx_1;
      del_idx_2 = (y[3] - y[2]) / h_idx_2;
      del_idx_3 = (y[4] - y[3]) / h_idx_3;
      xtmp = h_idx_0 + h_idx_1;
      hs3 = 3.0F * xtmp;
      dzzdx = (h_idx_0 + xtmp) / hs3;
      xtmp = (h_idx_1 + xtmp) / hs3;
      slopes_idx_1 = 0.0F;
      if (del_idx_0 < 0.0F) {
        if (del_idx_1 <= del_idx_0) {
          slopes_idx_1 = del_idx_0 / (dzzdx * (del_idx_0 / del_idx_1) + xtmp);
        } else {
          if (del_idx_1 < 0.0F) {
            slopes_idx_1 = del_idx_1 / (dzzdx + xtmp * (del_idx_1 / del_idx_0));
          }
        }
      } else {
        if (del_idx_0 > 0.0F) {
          if (del_idx_1 >= del_idx_0) {
            slopes_idx_1 = del_idx_0 / (dzzdx * (del_idx_0 / del_idx_1) + xtmp);
          } else {
            if (del_idx_1 > 0.0F) {
              slopes_idx_1 = del_idx_1 / (dzzdx + xtmp * (del_idx_1 / del_idx_0));
            }
          }
        }
      }

      xtmp = h_idx_1 + h_idx_2;
      hs3 = 3.0F * xtmp;
      dzzdx = (h_idx_1 + xtmp) / hs3;
      xtmp = (h_idx_2 + xtmp) / hs3;
      slopes_idx_2 = 0.0F;
      if (del_idx_1 < 0.0F) {
        if (del_idx_2 <= del_idx_1) {
          slopes_idx_2 = del_idx_1 / (dzzdx * (del_idx_1 / del_idx_2) + xtmp);
        } else {
          if (del_idx_2 < 0.0F) {
            slopes_idx_2 = del_idx_2 / (dzzdx + xtmp * (del_idx_2 / del_idx_1));
          }
        }
      } else {
        if (del_idx_1 > 0.0F) {
          if (del_idx_2 >= del_idx_1) {
            slopes_idx_2 = del_idx_1 / (dzzdx * (del_idx_1 / del_idx_2) + xtmp);
          } else {
            if (del_idx_2 > 0.0F) {
              slopes_idx_2 = del_idx_2 / (dzzdx + xtmp * (del_idx_2 / del_idx_1));
            }
          }
        }
      }

      xtmp = h_idx_2 + h_idx_3;
      hs3 = 3.0F * xtmp;
      dzzdx = (h_idx_2 + xtmp) / hs3;
      xtmp = (h_idx_3 + xtmp) / hs3;
      slopes_idx_3 = 0.0F;
      if (del_idx_2 < 0.0F) {
        if (del_idx_3 <= del_idx_2) {
          slopes_idx_3 = del_idx_2 / (dzzdx * (del_idx_2 / del_idx_3) + xtmp);
        } else {
          if (del_idx_3 < 0.0F) {
            slopes_idx_3 = del_idx_3 / (dzzdx + xtmp * (del_idx_3 / del_idx_2));
          }
        }
      } else {
        if (del_idx_2 > 0.0F) {
          if (del_idx_3 >= del_idx_2) {
            slopes_idx_3 = del_idx_2 / (dzzdx * (del_idx_2 / del_idx_3) + xtmp);
          } else {
            if (del_idx_3 > 0.0F) {
              slopes_idx_3 = del_idx_3 / (dzzdx + xtmp * (del_idx_3 / del_idx_2));
            }
          }
        }
      }

      hs3 = exteriorSlope(del_idx_0, del_idx_1, h_idx_0, h_idx_1);
      dzzdx = (del_idx_0 - hs3) / h_idx_0;
      xtmp = (slopes_idx_1 - del_idx_0) / h_idx_0;
      pp_coefs[0] = (xtmp - dzzdx) / h_idx_0;
      pp_coefs[4] = 2.0F * dzzdx - xtmp;
      pp_coefs[8] = hs3;
      pp_coefs[12] = y[0];
      dzzdx = (del_idx_1 - slopes_idx_1) / h_idx_1;
      xtmp = (slopes_idx_2 - del_idx_1) / h_idx_1;
      pp_coefs[1] = (xtmp - dzzdx) / h_idx_1;
      pp_coefs[5] = 2.0F * dzzdx - xtmp;
      pp_coefs[9] = slopes_idx_1;
      pp_coefs[13] = y[1];
      dzzdx = (del_idx_2 - slopes_idx_2) / h_idx_2;
      xtmp = (slopes_idx_3 - del_idx_2) / h_idx_2;
      pp_coefs[2] = (xtmp - dzzdx) / h_idx_2;
      pp_coefs[6] = 2.0F * dzzdx - xtmp;
      pp_coefs[10] = slopes_idx_2;
      pp_coefs[14] = y[2];
      dzzdx = (del_idx_3 - slopes_idx_3) / h_idx_3;
      xtmp = (exteriorSlope(del_idx_3, del_idx_2, h_idx_3, h_idx_2) - del_idx_3)
        / h_idx_3;
      pp_coefs[3] = (xtmp - dzzdx) / h_idx_3;
      pp_coefs[7] = 2.0F * dzzdx - xtmp;
      pp_coefs[11] = slopes_idx_3;
      pp_coefs[15] = y[3];
      for (i = 0; i < 5; i++) {
        if (rtIsNaNF(varargin_3[i])) {
          Vq[i] = rtNaNF;
        } else {
          if (rtIsNaNF(varargin_3[i])) {
            xtmp = varargin_3[i];
          } else {
            low_i = 0;
            low_ip1 = 2;
            high_i = 5;
            while (high_i > low_ip1) {
              mid_i = ((low_i + high_i) + 1) >> 1;
              if (varargin_3[i] >= x[mid_i - 1]) {
                low_i = mid_i - 1;
                low_ip1 = mid_i + 1;
              } else {
                high_i = mid_i;
              }
            }

            xtmp = varargin_3[i] - x[low_i];
            xtmp = xtmp * (xtmp * (xtmp * pp_coefs[low_i] + pp_coefs[low_i + 4])
                           + pp_coefs[low_i + 8]) + pp_coefs[low_i + 12];
          }

          Vq[i] = xtmp;
        }
      }

      exitg1 = 1;
    }
  } while (exitg1 == 0);
}

/*
 * File trailer for interp1.c
 *
 * [EOF]
 */
