/*
 * File: calcularAceleracion.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "calcularAceleracion.h"
#include "calcularAceleracion_data.h"
#include "calcularAceleracion_initialize.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"

/* Function Definitions */

/*
 * El calculo de la aceleracion se hace a partir de un stencil de 5 puntos,
 * dos vecinos hacia adelante y dos vecinos hacia atras. Se recomiendo
 * hacerlo en diferido. O esperar a que termine la coleccion de datos.
 * Arguments    : const float v[15]
 *                float dt
 *                float acel[15]
 * Return Type  : void
 */
void calcularAceleracion(const float v[15], float dt, float acel[15])
{
  float y;
  int i;
  int acel_tmp;
  int b_acel_tmp;
  int c_acel_tmp;
  int d_acel_tmp;
  if (isInitialized_calcularAceleracion == false) {
    calcularAceleracion_initialize();
  }

  y = 16.0F * dt;
  for (i = 0; i < 3; i++) {
    acel_tmp = 5 * i + 2;
    b_acel_tmp = 5 * i + 3;
    c_acel_tmp = 5 * i + 1;
    d_acel_tmp = 5 * i + 4;
    acel[acel_tmp] = ((((-v[d_acel_tmp] + 16.0F * v[b_acel_tmp]) - 30.0F *
                        v[acel_tmp]) + 16.0F * v[c_acel_tmp]) - v[5 * i]) / y;
    acel[5 * i] = 0.0F;
    acel[b_acel_tmp] = 0.0F;
    acel[c_acel_tmp] = 0.0F;
    acel[d_acel_tmp] = 0.0F;
  }
}

/*
 * File trailer for calcularAceleracion.c
 *
 * [EOF]
 */
