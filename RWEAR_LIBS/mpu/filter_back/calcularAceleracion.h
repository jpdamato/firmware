/*
 * File: calcularAceleracion.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef CALCULARACELERACION_H
#define CALCULARACELERACION_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void calcularAceleracion(const float v[15], float dt, float acel[15]);

#endif

/*
 * File trailer for calcularAceleracion.h
 *
 * [EOF]
 */
