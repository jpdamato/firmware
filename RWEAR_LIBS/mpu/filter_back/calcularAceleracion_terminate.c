/*
 * File: calcularAceleracion_terminate.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "calcularAceleracion_terminate.h"
#include "calcularAceleracion.h"
#include "calcularAceleracion_data.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void calcularAceleracion_terminate(void)
{
  /* (no terminate code required) */
  isInitialized_calcularAceleracion = false;
}

/*
 * File trailer for calcularAceleracion_terminate.c
 *
 * [EOF]
 */
