/*
 * File: cosd.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef COSD_H
#define COSD_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void b_cosd(float *x);

#endif

/*
 * File trailer for cosd.h
 *
 * [EOF]
 */
