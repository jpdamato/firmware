/*
 * File: calibrarIMUcoder.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef CALIBRARIMUCODER_H
#define CALIBRARIMUCODER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
void calibrarIMUcoder(struct1_T *TIMU, unsigned char window, const float A[9],
                      const float b[3], float accel[15], float gyro[15], float
                      mag[15]);

#endif

/*
 * File trailer for calibrarIMUcoder.h
 *
 * [EOF]
 */
