/*
 * File: main.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/

/* Include Files */
#include "main.h"
#include "calcularAceleracion.h"
#include "calcularAceleracion_terminate.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"

/* Function Declarations */
static void argInit_1x3_real32_T(float result[3]);
static void argInit_28x1_real32_T(float result[28]);
static void argInit_28x28_real32_T(float result[784]);
static void argInit_5x1_real32_T(float result[5]);
static void argInit_5x3_real32_T(float result[15]);
static bool argInit_boolean_T(void);
static float argInit_real32_T(void);
static void argInit_struct0_T(struct0_T *result);
static void argInit_struct1_T(struct1_T *result);
static void main_calcularAceleracion(void);
static void main_calibrarGPScoder(void);
static void main_calibrarIMUcoder(void);
static void main_calibrarMagnetometro(void);
static void main_filtroCoder(void);

/* Function Definitions */

/*
 * Arguments    : float result[3]
 * Return Type  : void
 */
static void argInit_1x3_real32_T(float result[3])
{
  float result_tmp_tmp;

  /* Loop over the array to initialize each element. */
  /* Set the value of the array element.
     Change this value to the value that the application requires. */
  result_tmp_tmp = argInit_real32_T();
  result[0] = result_tmp_tmp;

  /* Set the value of the array element.
     Change this value to the value that the application requires. */
  result[1] = result_tmp_tmp;

  /* Set the value of the array element.
     Change this value to the value that the application requires. */
  result[2] = result_tmp_tmp;
}

/*
 * Arguments    : float result[28]
 * Return Type  : void
 */
static void argInit_28x1_real32_T(float result[28])
{
  int idx0;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 28; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0] = argInit_real32_T();
  }
}

/*
 * Arguments    : float result[784]
 * Return Type  : void
 */
static void argInit_28x28_real32_T(float result[784])
{
  int idx0;
  int idx1;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 28; idx0++) {
    for (idx1 = 0; idx1 < 28; idx1++) {
      /* Set the value of the array element.
         Change this value to the value that the application requires. */
      result[idx0 + 28 * idx1] = argInit_real32_T();
    }
  }
}

/*
 * Arguments    : float result[5]
 * Return Type  : void
 */
static void argInit_5x1_real32_T(float result[5])
{
  int idx0;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 5; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0] = argInit_real32_T();
  }
}

/*
 * Arguments    : float result[15]
 * Return Type  : void
 */
static void argInit_5x3_real32_T(float result[15])
{
  int idx0;
  float result_tmp_tmp;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 5; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result_tmp_tmp = argInit_real32_T();
    result[idx0] = result_tmp_tmp;

    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0 + 5] = result_tmp_tmp;

    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0 + 10] = result_tmp_tmp;
  }
}

/*
 * Arguments    : void
 * Return Type  : bool
 */
static bool argInit_boolean_T(void)
{
  return false;
}

/*
 * Arguments    : void
 * Return Type  : float
 */
static float argInit_real32_T(void)
{
  return 0.0F;
}

/*
 * Arguments    : struct0_T *result
 * Return Type  : void
 */
static void argInit_struct0_T(struct0_T *result)
{
  float result_tmp_tmp[5];
  int i;

  /* Set the value of each structure field.
     Change this value to the value that the application requires. */
  argInit_5x1_real32_T(result_tmp_tmp);
  for (i = 0; i < 5; i++) {
    result->HoraGps[i] = result_tmp_tmp[i];
    result->IndiceGPS[i] = result_tmp_tmp[i];
    result->Longitud[i] = result_tmp_tmp[i];
  }

  argInit_5x1_real32_T(result->Latitud);
  argInit_5x1_real32_T(result->Altitud);
  argInit_5x1_real32_T(result->VGPS);
}

/*
 * Arguments    : struct1_T *result
 * Return Type  : void
 */
static void argInit_struct1_T(struct1_T *result)
{
  float result_tmp_tmp[5];
  int i;

  /* Set the value of each structure field.
     Change this value to the value that the application requires. */
  argInit_5x1_real32_T(result_tmp_tmp);
  for (i = 0; i < 5; i++) {
    result->tid[i] = result_tmp_tmp[i];
    result->ax[i] = result_tmp_tmp[i];
    result->ay[i] = result_tmp_tmp[i];
  }

  argInit_5x1_real32_T(result->az);
  argInit_5x1_real32_T(result->gx);
  argInit_5x1_real32_T(result->gy);
  argInit_5x1_real32_T(result->gz);
  argInit_5x1_real32_T(result->mx);
  argInit_5x1_real32_T(result->my);
  argInit_5x1_real32_T(result->mz);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_calcularAceleracion(void)
{
  float b_fv[15];
  float acel[15];

  /* Initialize function 'calcularAceleracion' input arguments. */
  /* Initialize function input argument 'v'. */
  /* Call the entry-point 'calcularAceleracion'. */
  argInit_5x3_real32_T(b_fv);
  calcularAceleracion(b_fv, argInit_real32_T(), acel);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_calibrarGPScoder(void)
{
  struct0_T TGPS;
  float dt_tmp;
  float hora[5];
  float lat[5];
  float lon[5];
  float alt[5];
  float v[5];

  /* Initialize function 'calibrarGPScoder' input arguments. */
  /* Initialize function input argument 'TGPS'. */
  argInit_struct0_T(&TGPS);
  dt_tmp = argInit_real32_T();

  /* Call the entry-point 'calibrarGPScoder'. */
  calibrarGPScoder(&TGPS, dt_tmp, dt_tmp, hora, lat, lon, alt, v);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_calibrarIMUcoder(void)
{
  struct1_T TIMU;
  float dt_tmp;
  float horaIMU[5];
  float accel[15];
  float gyro[15];
  float mag[15];

  /* Initialize function 'calibrarIMUcoder' input arguments. */
  /* Initialize function input argument 'TIMU'. */
  argInit_struct1_T(&TIMU);
  dt_tmp = argInit_real32_T();

  /* Call the entry-point 'calibrarIMUcoder'. */
  calibrarIMUcoder(&TIMU, dt_tmp, dt_tmp, horaIMU, accel, gyro, mag);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_calibrarMagnetometro(void)
{
  float b_fv[15];
  float m[15];
  float A[9];
  float b[3];

  /* Initialize function 'calibrarMagnetometro' input arguments. */
  /* Initialize function input argument 'mag'. */
  /* Call the entry-point 'calibrarMagnetometro'. */
  argInit_5x3_real32_T(b_fv);
  calibrarMagnetometro(b_fv, m, A, b);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_filtroCoder(void)
{
  float accel_tmp_tmp[3];
  float lla[3];
  float estados[28];
  float covarianzaEstados[784];
  float VGPS;
  float ubicacionReferencia[3];
  float posAnterior[3];
  bool evaluarGPS_tmp;
  float p_data[9];
  int p_size[2];
  c_matlabshared_rotations_intern cuaternion;
  float posicion[3];
  float velocidad[3];
  float estado[28];

  /* Initialize function 'filtroCoder' input arguments. */
  /* Initialize function input argument 'accel'. */
  argInit_1x3_real32_T(accel_tmp_tmp);

  /* Initialize function input argument 'gyro'. */
  /* Initialize function input argument 'mag'. */
  /* Initialize function input argument 'lla'. */
  argInit_1x3_real32_T(lla);

  /* Initialize function input argument 'estados'. */
  argInit_28x1_real32_T(estados);

  /* Initialize function input argument 'covarianzaEstados'. */
  argInit_28x28_real32_T(covarianzaEstados);
  VGPS = argInit_real32_T();

  /* Initialize function input argument 'C'. */
  /* Initialize function input argument 'ubicacionReferencia'. */
  argInit_1x3_real32_T(ubicacionReferencia);

  /* Initialize function input argument 'posAnterior'. */
  argInit_1x3_real32_T(posAnterior);
  evaluarGPS_tmp = argInit_boolean_T();

  /* Call the entry-point 'filtroCoder'. */
  filtroCoder(accel_tmp_tmp, accel_tmp_tmp, accel_tmp_tmp, lla, estados,
              covarianzaEstados, VGPS, ubicacionReferencia, posAnterior,
              evaluarGPS_tmp, evaluarGPS_tmp, p_data, p_size, &cuaternion,
              posicion, velocidad, estado);
}

/*
 * Arguments    : int argc
 *                const char * const argv[]
 * Return Type  : int
 */
int main(int argc, const char * const argv[])
{
  (void)argc;
  (void)argv;

  /* The initialize function is being called automatically from your entry-point function. So, a call to initialize is not included here. */
  /* Invoke the entry-point functions.
     You can call entry-point functions multiple times. */
  main_calcularAceleracion();
  main_calibrarGPScoder();
  main_calibrarIMUcoder();
  main_calibrarMagnetometro();
  main_filtroCoder();

  /* Terminate the application.
     You do not need to do this more than one time. */
  calcularAceleracion_terminate();
  return 0;
}

/*
 * File trailer for main.c
 *
 * [EOF]
 */
