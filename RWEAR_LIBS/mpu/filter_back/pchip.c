/*
 * File: pchip.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "pchip.h"
#include "calcularAceleracion.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : float d1
 *                float d2
 *                float h1
 *                float h2
 * Return Type  : float
 */
float exteriorSlope(float d1, float d2, float h1, float h2)
{
  float s;
  float signd1;
  float signs;
  s = ((2.0F * h1 + h2) * d1 - h1 * d2) / (h1 + h2);
  signd1 = d1;
  if (d1 < 0.0F) {
    signd1 = -1.0F;
  } else if (d1 > 0.0F) {
    signd1 = 1.0F;
  } else {
    if (d1 == 0.0F) {
      signd1 = 0.0F;
    }
  }

  signs = s;
  if (s < 0.0F) {
    signs = -1.0F;
  } else if (s > 0.0F) {
    signs = 1.0F;
  } else {
    if (s == 0.0F) {
      signs = 0.0F;
    }
  }

  if (signs != signd1) {
    s = 0.0F;
  } else {
    signs = d2;
    if (d2 < 0.0F) {
      signs = -1.0F;
    } else if (d2 > 0.0F) {
      signs = 1.0F;
    } else {
      if (d2 == 0.0F) {
        signs = 0.0F;
      }
    }

    if (signd1 != signs) {
      signs = 3.0F * d1;
      if (fabsf(s) > fabsf(signs)) {
        s = signs;
      }
    }
  }

  return s;
}

/*
 * File trailer for pchip.c
 *
 * [EOF]
 */
