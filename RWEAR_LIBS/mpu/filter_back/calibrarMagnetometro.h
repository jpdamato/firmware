/*
 * File: calibrarMagnetometro.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef CALIBRARMAGNETOMETRO_H
#define CALIBRARMAGNETOMETRO_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
void calibrarMagnetometro(const float mag_sinCalibrar[60], float A[9], float b[3]);

#endif

/*
 * File trailer for calibrarMagnetometro.h
 *
 * [EOF]
 */
