/*
 * File: calcularAceleracion_types.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef CALCULARACELERACION_TYPES_H
#define CALCULARACELERACION_TYPES_H

/* Include Files */
#include "rtwtypes.h"
#include <stdint.h>
/* Type Definitions */
#ifndef typedef_c_fusion_internal_coder_insfilt
#define typedef_c_fusion_internal_coder_insfilt

typedef struct {
  float StateCovariance[784];
  float ReferenceLocation[3];
  double QuaternionNoise[4];
  double AngularVelocityNoise[3];
  double PositionNoise[3];
  double VelocityNoise[3];
  double AccelerationNoise[3];
  double GyroscopeBiasNoise[3];
  double AccelerometerBiasNoise[3];
  double GeomagneticVectorNoise[3];
  double MagnetometerBiasNoise[3];
  float pState[28];
} c_fusion_internal_coder_insfilt;

#endif                                 /*typedef_c_fusion_internal_coder_insfilt*/

#ifndef struct_emxArray_real32_T_1x1
#define struct_emxArray_real32_T_1x1

struct emxArray_real32_T_1x1
{
  float data[1];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_1x1*/

#ifndef typedef_emxArray_real32_T_1x1
#define typedef_emxArray_real32_T_1x1

typedef struct emxArray_real32_T_1x1 emxArray_real32_T_1x1;

#endif                                 /*typedef_emxArray_real32_T_1x1*/

#ifndef struct_s1vhBKqHRuzkVV7ODpSn2N_tag
#define struct_s1vhBKqHRuzkVV7ODpSn2N_tag

struct s1vhBKqHRuzkVV7ODpSn2N_tag
{
  emxArray_real32_T_1x1 a;
  emxArray_real32_T_1x1 b;
  emxArray_real32_T_1x1 c;
  emxArray_real32_T_1x1 d;
};

#endif                                 /*struct_s1vhBKqHRuzkVV7ODpSn2N_tag*/

#ifndef typedef_c_matlabshared_rotations_intern
#define typedef_c_matlabshared_rotations_intern

typedef struct s1vhBKqHRuzkVV7ODpSn2N_tag c_matlabshared_rotations_intern;

#endif                                 /*typedef_c_matlabshared_rotations_intern*/

#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
  float IndiceGPS[5];
  float Longitud[5];
  float Latitud[5];
  float Altitud[5];
  float VGPS[5];
	uint8_t index;
} struct0_T;

#endif                                 /*typedef_struct0_T*/

#ifndef typedef_struct1_T
#define typedef_struct1_T

typedef struct {
  float ax[5];
  float ay[5];
  float az[5];
  float gx[5];
  float gy[5];
  float gz[5];
  float mx[5];
  float my[5];
  float mz[5];
	uint8_t index;
} struct1_T;

#endif                                 /*typedef_struct1_T*/
#endif

/*
 * File trailer for calcularAceleracion_types.h
 *
 * [EOF]
 */
