/*
 * File: calcularAceleracion_rtwutil.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef CALCULARACELERACION_RTWUTIL_H
#define CALCULARACELERACION_RTWUTIL_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern float rt_remf_snf(float u0, float u1);

#endif

/*
 * File trailer for calcularAceleracion_rtwutil.h
 *
 * [EOF]
 */
