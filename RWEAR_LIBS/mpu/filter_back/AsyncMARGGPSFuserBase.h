/*
 * File: AsyncMARGGPSFuserBase.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef ASYNCMARGGPSFUSERBASE_H
#define ASYNCMARGGPSFUSERBASE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void AsyncMARGGPSFuserBase_fuseaccel(c_fusion_internal_coder_insfilt *obj,
  const float accel[3]);
extern void AsyncMARGGPSFuserBase_fusegyro(c_fusion_internal_coder_insfilt *obj,
  const float gyro[3]);
extern void AsyncMARGGPSFuserBase_fusemag(c_fusion_internal_coder_insfilt *obj,
  const float mag[3]);
extern void AsyncMARGGPSFuserBase_predict(c_fusion_internal_coder_insfilt *obj);
extern void c_AsyncMARGGPSFuserBase_basicCo(c_fusion_internal_coder_insfilt *obj,
  const float z[6], const float measNoise[36], float innov[6]);

#endif

/*
 * File trailer for AsyncMARGGPSFuserBase.h
 *
 * [EOF]
 */
