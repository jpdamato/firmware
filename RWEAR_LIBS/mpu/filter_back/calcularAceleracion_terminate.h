/*
 * File: calcularAceleracion_terminate.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef CALCULARACELERACION_TERMINATE_H
#define CALCULARACELERACION_TERMINATE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void calcularAceleracion_terminate(void);

#endif

/*
 * File trailer for calcularAceleracion_terminate.h
 *
 * [EOF]
 */
