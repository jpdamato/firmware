/*
 * File: calcularAceleracion_data.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "calcularAceleracion_data.h"
#include "calcularAceleracion.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
boolean_T isInitialized_calcularAceleracion = false;

/*
 * File trailer for calcularAceleracion_data.c
 *
 * [EOF]
 */
