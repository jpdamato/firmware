/*
 * File: calibrarIMUcoder.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "calibrarIMUcoder.h"
#include "calcularAceleracion.h"
#include "calcularAceleracion_data.h"
#include "calcularAceleracion_initialize.h"
#include "calibrarGPScoder.h"
#include "calibrarMagnetometro.h"
#include "fastsmooth.h"
#include "filtroCoder.h"
#include "interp1.h"
#include "rt_nonfinite.h"

/* Function Definitions */

/* Function Definitions */

/*
 * TIMU contiene el buffer con las medidas de la IMU, el tama�o del buffer
 * depende del input. Window es la ventana de suavizado, debe ser menor que
 * el tama�o del buffer.
 * Arguments    : struct0_T *TIMU
 *                unsigned char window
 *                const float A[9]
 *                const float b[3]
 *                float accel[15]
 *                float gyro[15]
 *                float mag[15]
 * Return Type  : void
 */
void calibrarIMUcoder(struct1_T *TIMU, unsigned char window, const float A[9],
                      const float b[3], float accel[15], float gyro[15], float
                      mag[15])
{
  int i;
  float b_TIMU[5];
	/*
  if (isInitialized_calibracionIMUMag == false) {
    calibracionIMUMag_initialize();
  }
*/
  /* TIMU debe contener las siguientes variables: */
  /* TIMU */
  /*    tid     timestep */
  /*    ax      aceleracion en x */
  /*    ay      aceleracion en y */
  /*    az      aceleracion en z */
  /*    gx      giro en x */
  /*    gy      giro en y */
  /*    gz      giro en z */
  /*    mx      mag en x */
  /*    my      mag en y     */
  /*    mz      mag en z */
  /*  if nargin < 2 */
  /*      window = 5; */
  /*  end */
  /* Se genera el vector de tiempos */
  /* timestepInicial = TIMU.tid(1); */
  /* timestepFinal = TIMU.tid(end); */
  /* xq = zeros(5,1,'single'); */
  /* xq(:,1) = transpose(timestepInicial:dt:timestepFinal); */
  /* xq(:,1) = single(dt) * single([0;1;2;3;4]) + single(timestepInicial); */
  /* Reservamos espacio para las estructuras */
  for (i = 0; i < 15; i++) {
    gyro[i] = 0.0F;
    accel[i] = 0.0F;
    mag[i] = 0.0F;
  }

  /* Interpolamos sobre los puntos faltantes */
  /*  nombres = fieldnames(TIMU); */
  /*  for i = 2:10 */
  /*      TIMU.(nombres{i}) = interp1(TIMU.tid, TIMU.(nombres{i}), xq, 'pchip'); */
  /*  end */
  /* Tras la interpolacion, hacemos un suavizado para bajar la intensidad del */
  /* ruido */
  /* Convierte grados a radianes. La entrada es un vector en grados. */
  for (i = 0; i < 5; i++) {
    b_TIMU[i] = TIMU->gx[i] * 3.14159274F / 180.0F;
  }

  fastsmooth(b_TIMU, window, *(float (*)[5])&gyro[0]);

  /* Convierte grados a radianes. La entrada es un vector en grados. */
  for (i = 0; i < 5; i++) {
    b_TIMU[i] = TIMU->gy[i] * 3.14159274F / 180.0F;
  }

  fastsmooth(b_TIMU, window, *(float (*)[5])&gyro[5]);

  /* Convierte grados a radianes. La entrada es un vector en grados. */
  for (i = 0; i < 5; i++) {
    b_TIMU[i] = TIMU->gz[i] * 3.14159274F / 180.0F;
    TIMU->mx[i] = A[0] * (TIMU->mx[i] - b[0]);
    TIMU->my[i] = A[4] * (TIMU->my[i] - b[1]);
    TIMU->mz[i] = A[8] * (TIMU->mz[i] - b[2]);
  }

  fastsmooth(b_TIMU, window, *(float (*)[5])&gyro[10]);
  fastsmooth(TIMU->ax, window, *(float (*)[5])&accel[0]);
  fastsmooth(TIMU->ay, window, *(float (*)[5])&accel[5]);
  fastsmooth(TIMU->az, window, *(float (*)[5])&accel[10]);
  fastsmooth(TIMU->mx, window, *(float (*)[5])&mag[0]);
  fastsmooth(TIMU->my, window, *(float (*)[5])&mag[5]);
  fastsmooth(TIMU->mz, window, *(float (*)[5])&mag[10]);

  /* horaIMU = xq; */
}

/*
 * File trailer for calibrarIMUcoder.c
 *
 * [EOF]
 */
