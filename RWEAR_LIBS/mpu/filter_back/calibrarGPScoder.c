/*
 * File: calibrarGPScoder.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "calibrarGPScoder.h"
#include "calcularAceleracion.h"
#include "calcularAceleracion_data.h"
#include "calcularAceleracion_initialize.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "fastsmooth.h"
#include "filtroCoder.h"
#include "interp1.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Definitions */

/*
 * TGPS contiene el buffer con las medidas de la GPS, el tama�o del buffer
 * depende del input (deberia ser mas grande que el de la IMU).
 * Window es la ventana de suavizado, debe ser menor que
 * el tama�o del buffer. dt es el tamano del step
 * Arguments    : struct0_T *TGPS
 *                float dt
 *                float window
 *                float hora[5]
 *                double lat[5]
 *                double lon[5]
 *                float alt[5]
 *                float v[5]
 * Return Type  : void
 */
void calibrarGPScoder(struct0_T *TGPS, float dt, float window,
                      double lat[5], double lon[5], float alt[5], float v[5])
{
  int k;
  int i;
  float b_TGPS[5];
  int i1;
  int i2;
  int i3;
  if (isInitialized_calcularAceleracion == false) {
    calcularAceleracion_initialize();
  }

  /* TGPS */
  /*    HoraGps */
  /*    IndiceGps */
  /*    Latitud */
  /*    Longitud */
  /*    Altitud */
  /*    VGPS */
  /* Establecemos el vector de tiempos */
  /* timestepFinal = TGPS.HoraGps(end); */
  /* xq(:,1) = transpose(timestepInicial:dt:timestepFinal); */
//  for (k = 0; k < 5; k++) {
//    hora[k] = dt * (float)k + TGPS->HoraGps[0];
//  }

//  /* Interpolamos para completar los espacios vacios */
//  for (i = 0; i < 5; i++) {
//    b_TGPS[i] = TGPS->Longitud[i];
//  }

//  interp1(TGPS->HoraGps, b_TGPS, hora, TGPS->Longitud);
//  for (i1 = 0; i1 < 5; i1++) {
//    b_TGPS[i1] = TGPS->Latitud[i1];
//  }

//  interp1(TGPS->HoraGps, b_TGPS, hora, TGPS->Latitud);
//  for (i2 = 0; i2 < 5; i2++) {
//    b_TGPS[i2] = TGPS->Altitud[i2];
//  }

//  interp1(TGPS->HoraGps, b_TGPS, hora, TGPS->Altitud);
//  for (i3 = 0; i3 < 5; i3++) {
//    b_TGPS[i3] = TGPS->VGPS[i3];
//  }

//  interp1(TGPS->HoraGps, b_TGPS, hora, TGPS->VGPS);

  /* Reducimos un poco la intensidad del ruido */
  fastsmooth_double(TGPS->Latitud, window, lat);
  fastsmooth_double(TGPS->Longitud, window, lon);
  fastsmooth(TGPS->Altitud, window, alt);
  fastsmooth(TGPS->VGPS, window, b_TGPS);
  for (k = 0; k < 5; k++) {
    v[k] = fabsf(b_TGPS[k]);
  }
}

/*
 * File trailer for calibrarGPScoder.c
 *
 * [EOF]
 */
