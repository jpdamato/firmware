/*
 * File: fastsmooth.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef FASTSMOOTH_H
#define FASTSMOOTH_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void fastsmooth(const float Y[5], float w, float SmoothY[5]);
extern void fastsmooth_double(const float Y[5], float w, double SmoothY[5]);

#endif

/*
 * File trailer for fastsmooth.h
 *
 * [EOF]
 */
