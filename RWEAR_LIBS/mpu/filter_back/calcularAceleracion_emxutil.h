/*
 * File: calcularAceleracion_emxutil.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 21-Oct-2019 12:58:03
 */

#ifndef CALCULARACELERACION_EMXUTIL_H
#define CALCULARACELERACION_EMXUTIL_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void emxEnsureCapacity_real_T(emxArray_real_T *emxArray, int oldNumel);
extern void emxFree_real_T(emxArray_real_T **pEmxArray);
extern void emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions);

#endif

/*
 * File trailer for calcularAceleracion_emxutil.h
 *
 * [EOF]
 */
