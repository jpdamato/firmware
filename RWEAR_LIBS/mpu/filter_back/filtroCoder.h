/*
 * File: filtroCoder.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef FILTROCODER_H
#define FILTROCODER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
void filtroCoder(c_fusion_internal_coder_insfilt * f,const float accel[3], const float gyro[3], const float mag[3],
                 const float lla[3], float VGPS, const double ubicacionReferencia[3], float posAnterior[3], 
								bool evaluarGPS, bool evaluarIMU,c_matlabshared_rotations_intern *cuaternion, 
								float posicion[3], float velocidad[3]);

#endif

/*
 * File trailer for filtroCoder.h
 *
 * [EOF]
 */
