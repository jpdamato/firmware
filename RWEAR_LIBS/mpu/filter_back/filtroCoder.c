/*
 * File: filtroCoder.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "filtroCoder.h"
#include "AsyncMARGGPSFuserBase.h"
#include "calcularAceleracion.h"
#include "calcularAceleracion_data.h"
#include "calcularAceleracion_initialize.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "cosd.h"
#include "rt_nonfinite.h"
#include "sind.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * Inicializamos el filtro con los parametros cargados
 * Arguments    : const float accel[3]
 *                const float gyro[3]
 *                const float mag[3]
 *                const float lla[3]
 *                const float estados[28]
 *                float covarianzaEstados[784]
 *                float VGPS
 *                const float ubicacionReferencia[3]
 *                float posAnterior[3]
 *                bool evaluarGPS
 *                bool evaluarIMU
 *                float p_data[]
 *                int p_size[2]
 *                c_matlabshared_rotations_intern *cuaternion
 *                float posicion[3]
 *                float velocidad[3]
 *                float estado[28]
 * Return Type  : void
 */
void filtroCoder(c_fusion_internal_coder_insfilt * f,const float accel[3], const float gyro[3], const float mag[3],
                 const float lla[3], float VGPS, const double
                 ubicacionReferencia[3], float posAnterior[3], bool evaluarGPS,
                 bool evaluarIMU, c_matlabshared_rotations_intern *cuaternion, float posicion[3],
                 float velocidad[3])
{

  int i;
  static const float b_fv[28] = { 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 27.555F, -2.4169F, -16.0849F, 0.0F, 0.0F, 0.0F };

  float aux[13];
  static const float val[784] = { 0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.001F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.001F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.001F };

  float b_val[28];
  float ecefPosWithENUOrigin_idx_1;
  float norma;
  float enuPos_idx_0;
  float xRN;
  float yRE;
  float gpsvel_idx_0;
  float gpsvel_idx_1;
  float gpsvel_idx_2;
  float sinphi;
  float N;
  float cosphi;
  float b_sinphi;
  float coslambda;
  float sinlambda;
  float c_sinphi;
  float b_N;
  float b_f;
  float ecefPosWithENUOrigin_idx_0;
  float pos[6];
  static const float fv1[36] = { 1.98911323E-8F, 1.39063E-10F, 0.0F, 0.0F, 0.0F,
    0.0F, 1.39063E-10F, 2.14289E-9F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    1.0E-10F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0E-7F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 1.0E-7F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0E-7F };

  float unusedExpr[6];
  if (isInitialized_calcularAceleracion == false) {
    calcularAceleracion_initialize();
  }

  f->QuaternionNoise[0] = 1.0E-6;
  f->QuaternionNoise[1] = 1.0E-6;
  f->QuaternionNoise[2] = 1.0E-6;
  f->QuaternionNoise[3] = 1.0E-6;
  f->PositionNoise[0] = 1.0E-6;
  f->VelocityNoise[0] = 1.0E-6;
  f->ReferenceLocation[0] = ubicacionReferencia[0];
  f->GyroscopeBiasNoise[0] = 1.6965748727670871E-5;
  f->AccelerometerBiasNoise[0] = 0.082911938428878784;
  f->AngularVelocityNoise[0] = 0.0010436082957312465;
  f->AccelerationNoise[0] = 0.26262769103050232;
  f->MagnetometerBiasNoise[0] = 0.18115557730197906;
  f->GeomagneticVectorNoise[0] = 9.9999999747524271E-7;
  f->PositionNoise[1] = 1.0E-6;
  f->VelocityNoise[1] = 1.0E-6;
  f->ReferenceLocation[1] = ubicacionReferencia[1];
  f->GyroscopeBiasNoise[1] = 2.8926244340254925E-5;
  f->AccelerometerBiasNoise[1] = 0.054855264723300934;
  f->AngularVelocityNoise[1] = 0.00098851346410810947;
  f->AccelerationNoise[1] = 0.018492855131626129;
  f->MagnetometerBiasNoise[1] = 0.079999998211860657;
  f->GeomagneticVectorNoise[1] = 9.9999999747524271E-7;
  f->PositionNoise[2] = 1.0E-6;
  f->VelocityNoise[2] = 1.0E-6;
  f->ReferenceLocation[2] = ubicacionReferencia[2];
  f->GyroscopeBiasNoise[2] = 4.1841463826131076E-5;
  f->AccelerometerBiasNoise[2] = 0.045979466289281845;
  f->AngularVelocityNoise[2] = 0.0010617680381983519;
  f->AccelerationNoise[2] = 0.028735747560858727;
  f->MagnetometerBiasNoise[2] = 0.14000000059604645;
  f->GeomagneticVectorNoise[2] = 9.9999999747524271E-7;

  /* Cargamos el estado que enviamos hacia afuera y la matriz de covarianza */
  /* asociada */
	/*
  memcpy(&f->pState[0], &estados[0], 28U * sizeof(float));*/
 // memcpy(&f->StateCovariance[0], &covarianzaEstados[0], 784U * sizeof(float));
 // p_size[0] = 3;
  //p_size[1] = 1;
  cuaternion->a.size[0] = 0;
  cuaternion->a.size[1] = 0;
  cuaternion->b.size[0] = 0;
  cuaternion->b.size[1] = 0;
  cuaternion->c.size[0] = 0;
  cuaternion->c.size[1] = 0;
  cuaternion->d.size[0] = 0;
  cuaternion->d.size[1] = 0;
  //p_data[0] = 0.0F;
  posicion[0] = 0.0F;
  velocidad[0] = 0.0F;
  //p_data[1] = 0.0F;
  posicion[1] = 0.0F;
  velocidad[1] = 0.0F;
//  p_data[2] = 0.0F;
  posicion[2] = 0.0F;
  velocidad[2] = 0.0F;
  if (evaluarGPS) {
    /*  Fusiono el GPS una vez por segundo */
    /* Antes de evaluar el GPS, reseteo el filtro */
    for (i = 0; i < 13; i++) {
      aux[i] = f->pState[i];
    }

    memcpy(&f->pState[0], &b_fv[0], 28U * sizeof(float));
    f->pState[24] = -16.0849F;
    memcpy(&f->StateCovariance[0], &val[0], 784U * sizeof(float));
    memcpy(&b_val[0], &f->pState[0], 28U * sizeof(float));
    b_val[24] = -16.0849F;
    b_val[23] = f->pState[23];
    b_val[22] = f->pState[22];
    for (i = 0; i < 13; i++) {
      b_val[i] = aux[i];
    }

    memcpy(&f->pState[0], &b_val[0], 28U * sizeof(float));

    /* Calculamos la velocidad a mano como redundancia para la estimacion del */
    /* GPS */
    /*  ELL2XYZ  Convierte coordinadas elipsoideas a cartesianas */
    /*    Vectorized. */
    /*  Uso:     [x,y,z]=ell2xyz(lat,lon,h,a,e2) */
    /*           [x,y,z]=ell2xyz(lat,lon,h) */
    /*  Entradas: lat - latitudes (radianes) */
    /*            lon - longitudes (radianes) */
    /*            h   - alturas (m) */
    /*            a   - semi-eje mayor del elipsoide (m); default wgs84 */
    /*            e2  - excentricidad cuadratica del elipsoide; default wgs84 */
    /*  Salida:   x \ */
    /*            y  > vectores en coordenadas cartesianas (m) */
    /*            z / */
    ecefPosWithENUOrigin_idx_1 = sinf(lla[0]);

    /*  ELL2XYZ  Convierte coordinadas elipsoideas a cartesianas */
    /*    Vectorized. */
    /*  Uso:     [x,y,z]=ell2xyz(lat,lon,h,a,e2) */
    /*           [x,y,z]=ell2xyz(lat,lon,h) */
    /*  Entradas: lat - latitudes (radianes) */
    /*            lon - longitudes (radianes) */
    /*            h   - alturas (m) */
    /*            a   - semi-eje mayor del elipsoide (m); default wgs84 */
    /*            e2  - excentricidad cuadratica del elipsoide; default wgs84 */
    /*  Salida:   x \ */
    /*            y  > vectores en coordenadas cartesianas (m) */
    /*            z / */
    norma = sinf(posAnterior[0]);

    /*  ELL2XYZ  Convierte coordinadas elipsoideas a cartesianas */
    /*    Vectorized. */
    /*  Uso:     [x,y,z]=ell2xyz(lat,lon,h,a,e2) */
    /*           [x,y,z]=ell2xyz(lat,lon,h) */
    /*  Entradas: lat - latitudes (radianes) */
    /*            lon - longitudes (radianes) */
    /*            h   - alturas (m) */
    /*            a   - semi-eje mayor del elipsoide (m); default wgs84 */
    /*            e2  - excentricidad cuadratica del elipsoide; default wgs84 */
    /*  Salida:   x \ */
    /*            y  > vectores en coordenadas cartesianas (m) */
    /*            z / */
    enuPos_idx_0 = sinf(ubicacionReferencia[0]);
    enuPos_idx_0 = (6.378137E+6F / sqrtf(1.0F - 0.00669438F * enuPos_idx_0 *
      enuPos_idx_0) + ubicacionReferencia[2]) * cosf(ubicacionReferencia[0]);
    xRN = enuPos_idx_0 * cosf(ubicacionReferencia[1]);
    yRE = enuPos_idx_0 * sinf(ubicacionReferencia[1]);

    /* Por ahora, voy a hacer que se quede con el minimo entre la */
    /* velocidad dada por el sistema y la que calculamos aqui. El */
    /* vector resultante es el vector diferencia. */
    norma = (6.378137E+6F / sqrtf(1.0F - 0.00669438F * norma * norma) +
             posAnterior[2]) * cosf(posAnterior[0]);
    enuPos_idx_0 = (6.378137E+6F / sqrtf(1.0F - 0.00669438F *
      ecefPosWithENUOrigin_idx_1 * ecefPosWithENUOrigin_idx_1) + lla[2]) * cosf
      (lla[0]);
    gpsvel_idx_0 = (norma * cosf(posAnterior[1]) - xRN) - (enuPos_idx_0 * cosf
      (lla[1]) - xRN);
    gpsvel_idx_1 = (norma * sinf(posAnterior[1]) - yRE) - (enuPos_idx_0 * sinf
      (lla[1]) - yRE);
    gpsvel_idx_2 = 0.0F;
    norma = fabsf(gpsvel_idx_0);
    enuPos_idx_0 = norma * norma;
    norma = fabsf(gpsvel_idx_1);
    norma = sqrtf(enuPos_idx_0 + norma * norma);
    if (norma > 0.0F) {
      if (0.0F < VGPS) {
        enuPos_idx_0 = VGPS;
      } else {
        enuPos_idx_0 = 0.0F;
      }

      enuPos_idx_0 = fminf(norma, enuPos_idx_0);
      gpsvel_idx_0 = gpsvel_idx_0 / norma * enuPos_idx_0;
      gpsvel_idx_1 = gpsvel_idx_1 / norma * enuPos_idx_0;
      gpsvel_idx_2 = 0.0F * enuPos_idx_0;
    }

    /* Fusionamos el GPS */
    sinphi = lla[0];
    b_sind(&sinphi);
    N = 6.378137E+6F / sqrtf(1.0F - 0.00669438F * (sinphi * sinphi));
    norma = lla[0];
    b_cosd(&norma);
    xRN = (N + lla[2]) * norma;
    cosphi = ubicacionReferencia[0];
    b_cosd(&cosphi);
    b_sinphi = ubicacionReferencia[0];
    b_sind(&b_sinphi);
    coslambda = ubicacionReferencia[1];
    b_cosd(&coslambda);
    sinlambda = ubicacionReferencia[1];
    b_sind(&sinlambda);
    c_sinphi = ubicacionReferencia[0];
    b_sind(&c_sinphi);
    b_N = 6.378137E+6F / sqrtf(1.0F - 0.00669438F * (c_sinphi * c_sinphi));
    norma = ubicacionReferencia[0];
    b_cosd(&norma);
    yRE = (b_N + ubicacionReferencia[2]) * norma;
    norma = lla[1];
    b_cosd(&norma);
    ecefPosWithENUOrigin_idx_1 = lla[1];
    b_sind(&ecefPosWithENUOrigin_idx_1);
    enuPos_idx_0 = ubicacionReferencia[1];
    b_cosd(&enuPos_idx_0);
    b_f = ubicacionReferencia[1];
    b_sind(&b_f);
    ecefPosWithENUOrigin_idx_0 = xRN * norma - yRE * enuPos_idx_0;
    ecefPosWithENUOrigin_idx_1 = xRN * ecefPosWithENUOrigin_idx_1 - yRE * b_f;
    enuPos_idx_0 = (N * 0.993305624F + lla[2]) * sinphi - (b_N * 0.993305624F +
      ubicacionReferencia[2]) * c_sinphi;
    norma = coslambda * ecefPosWithENUOrigin_idx_0 + sinlambda *
      ecefPosWithENUOrigin_idx_1;
    pos[0] = -b_sinphi * norma + cosphi * enuPos_idx_0;
    pos[3] = gpsvel_idx_0;
    pos[1] = -sinlambda * ecefPosWithENUOrigin_idx_0 + coslambda *
      ecefPosWithENUOrigin_idx_1;
    pos[4] = gpsvel_idx_1;
    pos[2] = -(cosphi * norma + b_sinphi * enuPos_idx_0);
    pos[5] = gpsvel_idx_2;
    c_AsyncMARGGPSFuserBase_basicCo(f, pos, fv1, unusedExpr);

    /* Almacenamos algunos datos para diagnostico */
    memcpy(&b_val[0], &f->pState[0], 28U * sizeof(float));
    posicion[0] = b_val[7];
    posicion[1] = b_val[8];
    posicion[2] = b_val[9];
    memcpy(&b_val[0], &f->pState[0], 28U * sizeof(float));

    /* Indicamos que el proximo step no lleva GPS */
    velocidad[0] = b_val[10];
    posAnterior[0] = lla[0];
    velocidad[1] = b_val[11];
    posAnterior[1] = lla[1];
    velocidad[2] = b_val[12];
    posAnterior[2] = lla[2];
    memcpy(&b_val[0], &f->pState[0], 28U * sizeof(float));
    cuaternion->a.size[0] = 1;
    cuaternion->a.size[1] = 1;
    cuaternion->a.data[0] = b_val[0];
    cuaternion->b.size[0] = 1;
    cuaternion->b.size[1] = 1;
    cuaternion->b.data[0] = b_val[1];
    cuaternion->c.size[0] = 1;
    cuaternion->c.size[1] = 1;
    cuaternion->c.data[0] = b_val[2];
    cuaternion->d.size[0] = 1;
    cuaternion->d.size[1] = 1;
    cuaternion->d.data[0] = b_val[3];
    memcpy(&b_val[0], &f->pState[0], 28U * sizeof(float));
  /*  p_size[0] = 1;
    p_size[1] = 3;
    p_data[0] = b_val[7];
    p_data[1] = b_val[8];
    p_data[2] = b_val[9];*/
  }

  if (evaluarIMU) {
    /* Cargamos la informacion de la IMU */
    AsyncMARGGPSFuserBase_predict(f);

    /* Integramos el giroscopo, acelerometro y magnetometro */
    AsyncMARGGPSFuserBase_fuseaccel(f, accel);
    AsyncMARGGPSFuserBase_fusegyro(f, gyro);
    AsyncMARGGPSFuserBase_fusemag(f, mag);
  }
/*
  memcpy(&estado[0], &f->pState[0], 28U * sizeof(float));
  estado[24] = f->pState[24];
  estado[23] = f->pState[23];
  estado[22] = f->pState[22];
  memcpy(&covarianzaEstados[0], &f->StateCovariance[0], 784U * sizeof(float));*/
}

/*
 * File trailer for filtroCoder.c
 *
 * [EOF]
 */
