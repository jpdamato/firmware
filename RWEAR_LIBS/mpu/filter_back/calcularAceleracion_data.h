/*
 * File: calcularAceleracion_data.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef CALCULARACELERACION_DATA_H
#define CALCULARACELERACION_DATA_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Variable Declarations */
extern boolean_T isInitialized_calcularAceleracion;

#endif

/*
 * File trailer for calcularAceleracion_data.h
 *
 * [EOF]
 */
