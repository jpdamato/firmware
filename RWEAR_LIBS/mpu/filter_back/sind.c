/*
 * File: sind.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "sind.h"
#include "calcularAceleracion.h"
#include "calcularAceleracion_rtwutil.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : float *x
 * Return Type  : void
 */
void b_sind(float *x)
{
  float absx;
  signed char n;
  if (rtIsInfF(*x) || rtIsNaNF(*x)) {
    *x = rtNaNF;
  } else {
    *x = rt_remf_snf(*x, 360.0F);
    absx = fabsf(*x);
    if (absx > 180.0F) {
      if (*x > 0.0F) {
        *x -= 360.0F;
      } else {
        *x += 360.0F;
      }

      absx = fabsf(*x);
    }

    if (absx <= 45.0F) {
      *x *= 0.0174532924F;
      n = 0;
    } else if (absx <= 135.0F) {
      if (*x > 0.0F) {
        *x = 0.0174532924F * (*x - 90.0F);
        n = 1;
      } else {
        *x = 0.0174532924F * (*x + 90.0F);
        n = -1;
      }
    } else if (*x > 0.0F) {
      *x = 0.0174532924F * (*x - 180.0F);
      n = 2;
    } else {
      *x = 0.0174532924F * (*x + 180.0F);
      n = -2;
    }

    if (n == 0) {
      *x = sinf(*x);
    } else if (n == 1) {
      *x = cosf(*x);
    } else if (n == -1) {
      *x = -cosf(*x);
    } else {
      *x = -sinf(*x);
    }
  }
}

/*
 * File trailer for sind.c
 *
 * [EOF]
 */
