/*
 * File: pchip.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef PCHIP_H
#define PCHIP_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern float exteriorSlope(float d1, float d2, float h1, float h2);

#endif

/*
 * File trailer for pchip.h
 *
 * [EOF]
 */
