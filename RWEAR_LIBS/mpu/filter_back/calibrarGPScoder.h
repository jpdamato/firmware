/*
 * File: calibrarGPScoder.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef CALIBRARGPSCODER_H
#define CALIBRARGPSCODER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void calibrarGPScoder(struct0_T *TGPS, float dt, float window, double lat[5], double lon[5], float alt[5], float v[5]);

#endif

/*
 * File trailer for calibrarGPScoder.h
 *
 * [EOF]
 */
