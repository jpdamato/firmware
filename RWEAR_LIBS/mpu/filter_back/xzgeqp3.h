/*
 * File: xzgeqp3.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef XZGEQP3_H
#define XZGEQP3_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void qrpf(float A[20], float tau[4], int jpvt[4]);

#endif

/*
 * File trailer for xzgeqp3.h
 *
 * [EOF]
 */
