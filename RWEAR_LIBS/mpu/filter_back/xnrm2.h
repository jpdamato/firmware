/*
 * File: xnrm2.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef XNRM2_H
#define XNRM2_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern float xnrm2(int n, const float x[20], int ix0);

#endif

/*
 * File trailer for xnrm2.h
 *
 * [EOF]
 */
