/*
 * File: _coder_calcularAceleracion_api.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef _CODER_CALCULARACELERACION_API_H
#define _CODER_CALCULARACELERACION_API_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"

/* Type Definitions */
#ifndef struct_emxArray_real32_T_1x1
#define struct_emxArray_real32_T_1x1

struct emxArray_real32_T_1x1
{
  real32_T data[1];
  int32_T size[2];
};

#endif                                 /*struct_emxArray_real32_T_1x1*/

#ifndef typedef_emxArray_real32_T_1x1
#define typedef_emxArray_real32_T_1x1

typedef struct emxArray_real32_T_1x1 emxArray_real32_T_1x1;

#endif                                 /*typedef_emxArray_real32_T_1x1*/

#ifndef struct_s1vhBKqHRuzkVV7ODpSn2N_tag
#define struct_s1vhBKqHRuzkVV7ODpSn2N_tag

struct s1vhBKqHRuzkVV7ODpSn2N_tag
{
  emxArray_real32_T_1x1 a;
  emxArray_real32_T_1x1 b;
  emxArray_real32_T_1x1 c;
  emxArray_real32_T_1x1 d;
};

#endif                                 /*struct_s1vhBKqHRuzkVV7ODpSn2N_tag*/

#ifndef typedef_c_matlabshared_rotations_intern
#define typedef_c_matlabshared_rotations_intern

typedef struct s1vhBKqHRuzkVV7ODpSn2N_tag c_matlabshared_rotations_intern;

#endif                                 /*typedef_c_matlabshared_rotations_intern*/

#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
  real32_T HoraGps[5];
  real32_T IndiceGPS[5];
  real32_T Longitud[5];
  real32_T Latitud[5];
  real32_T Altitud[5];
  real32_T VGPS[5];
} struct0_T;

#endif                                 /*typedef_struct0_T*/

#ifndef typedef_struct1_T
#define typedef_struct1_T

typedef struct {
  real32_T tid[5];
  real32_T ax[5];
  real32_T ay[5];
  real32_T az[5];
  real32_T gx[5];
  real32_T gy[5];
  real32_T gz[5];
  real32_T mx[5];
  real32_T my[5];
  real32_T mz[5];
} struct1_T;

#endif                                 /*typedef_struct1_T*/

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void calcularAceleracion(real32_T v[15], real32_T dt, real32_T acel[15]);
extern void calcularAceleracion_api(const mxArray * const prhs[2], int32_T nlhs,
  const mxArray *plhs[1]);
extern void calcularAceleracion_atexit(void);
extern void calcularAceleracion_initialize(void);
extern void calcularAceleracion_terminate(void);
extern void calcularAceleracion_xil_shutdown(void);
extern void calcularAceleracion_xil_terminate(void);
extern void calibrarGPScoder(struct0_T *TGPS, real32_T dt, real32_T window,
  real32_T hora[5], real32_T lat[5], real32_T lon[5], real32_T alt[5], real32_T
  v[5]);
extern void calibrarGPScoder_api(const mxArray * const prhs[3], int32_T nlhs,
  const mxArray *plhs[5]);
extern void calibrarIMUcoder(struct1_T *TIMU, real32_T dt, real32_T window,
  real32_T horaIMU[5], real32_T accel[15], real32_T gyro[15], real32_T mag[15]);
extern void calibrarIMUcoder_api(const mxArray * const prhs[3], int32_T nlhs,
  const mxArray *plhs[4]);
extern void calibrarMagnetometro(real32_T mag[15], real32_T m[15], real32_T A[9],
  real32_T b[3]);
extern void calibrarMagnetometro_api(const mxArray * const prhs[1], int32_T nlhs,
  const mxArray *plhs[3]);
extern void filtroCoder(real32_T accel[3], real32_T gyro[3], real32_T mag[3],
  real32_T lla[3], real32_T estados[28], real32_T covarianzaEstados[784],
  real32_T VGPS, real32_T ubicacionReferencia[3], real32_T posAnterior[3],
  boolean_T evaluarGPS, boolean_T evaluarIMU, real32_T p_data[], int32_T p_size
  [2], c_matlabshared_rotations_intern *cuaternion, real32_T posicion[3],
  real32_T velocidad[3], real32_T estado[28]);
extern void filtroCoder_api(const mxArray * const prhs[12], int32_T nlhs, const
  mxArray *plhs[7]);

#endif

/*
 * File trailer for _coder_calcularAceleracion_api.h
 *
 * [EOF]
 */
