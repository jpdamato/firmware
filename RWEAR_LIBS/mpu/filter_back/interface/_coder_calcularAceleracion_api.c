/*
 * File: _coder_calcularAceleracion_api.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "_coder_calcularAceleracion_api.h"
#include "_coder_calcularAceleracion_mex.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131483U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "calcularAceleracion",               /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

/* Function Declarations */
static boolean_T ab_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId);
static real32_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[15];
static const mxArray *b_emlrt_marshallOut(const real32_T u[5]);
static real32_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *dt,
  const char_T *identifier);
static const mxArray *c_emlrt_marshallOut(const real32_T u[9]);
static real32_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static const mxArray *d_emlrt_marshallOut(const real32_T u[3]);
static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *TGPS, const
  char_T *identifier, struct0_T *y);
static const mxArray *e_emlrt_marshallOut(const real32_T u_data[], const int32_T
  u_size[2]);
static real32_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *v, const
  char_T *identifier))[15];
static const mxArray *emlrt_marshallOut(const real32_T u[15]);
static void f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct0_T *y);
static const mxArray *f_emlrt_marshallOut(const emlrtStack *sp, const
  c_matlabshared_rotations_intern u);
static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real32_T y[5]);
static const mxArray *g_emlrt_marshallOut(const real32_T u_data[], const int32_T
  u_size[2]);
static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *TIMU, const
  char_T *identifier, struct1_T *y);
static const mxArray *h_emlrt_marshallOut(const real32_T u[3]);
static void i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct1_T *y);
static const mxArray *i_emlrt_marshallOut(const real32_T u[28]);
static real32_T (*j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *accel,
  const char_T *identifier))[3];
static void j_emlrt_marshallOut(const real32_T u[784], const mxArray *y);
static real32_T (*k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[3];
static void k_emlrt_marshallOut(const real32_T u[3], const mxArray *y);
static real32_T (*l_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *estados, const char_T *identifier))[28];
static real32_T (*m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[28];
static real32_T (*n_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *covarianzaEstados, const char_T *identifier))[784];
static real32_T (*o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[784];
static void p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *C, const
  char_T *identifier);
static void q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static boolean_T r_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *evaluarGPS, const char_T *identifier);
static boolean_T s_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static real32_T (*t_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[15];
static real32_T u_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId);
static void v_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real32_T ret[5]);
static real32_T (*w_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[3];
static real32_T (*x_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[28];
static real32_T (*y_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[784];

/* Function Definitions */

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : boolean_T
 */
static boolean_T ab_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  boolean_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "logical", false, 0U, &dims);
  ret = *emlrtMxGetLogicals(src);
  emlrtDestroyArray(&src);
  return ret;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : real32_T (*)[15]
 */
static real32_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[15]
{
  real32_T (*y)[15];
  y = t_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
/*
 * Arguments    : const real32_T u[5]
 * Return Type  : const mxArray *
 */
  static const mxArray *b_emlrt_marshallOut(const real32_T u[5])
{
  const mxArray *y;
  const mxArray *m;
  static const int32_T iv[1] = { 0 };

  static const int32_T iv1[1] = { 5 };

  y = NULL;
  m = emlrtCreateNumericArray(1, iv, mxSINGLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m, iv1, 1);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *dt
 *                const char_T *identifier
 * Return Type  : real32_T
 */
static real32_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *dt,
  const char_T *identifier)
{
  real32_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = d_emlrt_marshallIn(sp, emlrtAlias(dt), &thisId);
  emlrtDestroyArray(&dt);
  return y;
}

/*
 * Arguments    : const real32_T u[9]
 * Return Type  : const mxArray *
 */
static const mxArray *c_emlrt_marshallOut(const real32_T u[9])
{
  const mxArray *y;
  const mxArray *m;
  static const int32_T iv[2] = { 0, 0 };

  static const int32_T iv1[2] = { 3, 3 };

  y = NULL;
  m = emlrtCreateNumericArray(2, iv, mxSINGLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m, iv1, 2);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : real32_T
 */
static real32_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real32_T y;
  y = u_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const real32_T u[3]
 * Return Type  : const mxArray *
 */
static const mxArray *d_emlrt_marshallOut(const real32_T u[3])
{
  const mxArray *y;
  const mxArray *m;
  static const int32_T iv[2] = { 0, 0 };

  static const int32_T iv1[2] = { 1, 3 };

  y = NULL;
  m = emlrtCreateNumericArray(2, iv, mxSINGLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m, iv1, 2);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *TGPS
 *                const char_T *identifier
 *                struct0_T *y
 * Return Type  : void
 */
static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *TGPS, const
  char_T *identifier, struct0_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  f_emlrt_marshallIn(sp, emlrtAlias(TGPS), &thisId, y);
  emlrtDestroyArray(&TGPS);
}

/*
 * Arguments    : const real32_T u_data[]
 *                const int32_T u_size[2]
 * Return Type  : const mxArray *
 */
static const mxArray *e_emlrt_marshallOut(const real32_T u_data[], const int32_T
  u_size[2])
{
  const mxArray *y;
  const mxArray *m;
  static const int32_T iv[2] = { 0, 0 };

  y = NULL;
  m = emlrtCreateNumericArray(2, iv, mxSINGLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, (void *)&u_data[0]);
  emlrtSetDimensions((mxArray *)m, u_size, 2);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *v
 *                const char_T *identifier
 * Return Type  : real32_T (*)[15]
 */
static real32_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *v, const
  char_T *identifier))[15]
{
  real32_T (*y)[15];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = b_emlrt_marshallIn(sp, emlrtAlias(v), &thisId);
  emlrtDestroyArray(&v);
  return y;
}
/*
 * Arguments    : const real32_T u[15]
 * Return Type  : const mxArray *
 */
  static const mxArray *emlrt_marshallOut(const real32_T u[15])
{
  const mxArray *y;
  const mxArray *m;
  static const int32_T iv[2] = { 0, 0 };

  static const int32_T iv1[2] = { 5, 3 };

  y = NULL;
  m = emlrtCreateNumericArray(2, iv, mxSINGLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m, iv1, 2);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                struct0_T *y
 * Return Type  : void
 */
static void f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct0_T *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[6] = { "HoraGps", "IndiceGPS", "Longitud",
    "Latitud", "Altitud", "VGPS" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 6, fieldNames, 0U, &dims);
  thisId.fIdentifier = "HoraGps";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0, "HoraGps")),
                     &thisId, y->HoraGps);
  thisId.fIdentifier = "IndiceGPS";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1, "IndiceGPS")),
                     &thisId, y->IndiceGPS);
  thisId.fIdentifier = "Longitud";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 2, "Longitud")),
                     &thisId, y->Longitud);
  thisId.fIdentifier = "Latitud";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 3, "Latitud")),
                     &thisId, y->Latitud);
  thisId.fIdentifier = "Altitud";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 4, "Altitud")),
                     &thisId, y->Altitud);
  thisId.fIdentifier = "VGPS";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 5, "VGPS")),
                     &thisId, y->VGPS);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const c_matlabshared_rotations_intern u
 * Return Type  : const mxArray *
 */
static const mxArray *f_emlrt_marshallOut(const emlrtStack *sp, const
  c_matlabshared_rotations_intern u)
{
  const mxArray *y;
  const mxArray *m;
  const mxArray *propValues[4];
  const char * propNames[4] = { "a", "b", "c", "d" };

  const char * propClasses[4] = {
    "matlabshared.rotations.internal.quaternionBase",
    "matlabshared.rotations.internal.quaternionBase",
    "matlabshared.rotations.internal.quaternionBase",
    "matlabshared.rotations.internal.quaternionBase" };

  y = NULL;
  emlrtAssign(&y, emlrtCreateClassInstance(
    "matlabshared.rotations.internal.coder.quaternioncg"));
  m = NULL;
  emlrtAssign(&m, g_emlrt_marshallOut(u.a.data, u.a.size));
  propValues[0] = m;
  m = NULL;
  emlrtAssign(&m, g_emlrt_marshallOut(u.b.data, u.b.size));
  propValues[1] = m;
  m = NULL;
  emlrtAssign(&m, g_emlrt_marshallOut(u.c.data, u.c.size));
  propValues[2] = m;
  m = NULL;
  emlrtAssign(&m, g_emlrt_marshallOut(u.d.data, u.d.size));
  propValues[3] = m;
  emlrtSetAllProperties(sp, &y, 0, 4, propNames, propClasses, propValues);
  emlrtAssign(&y, emlrtConvertInstanceToRedirectSource(sp, y, 0,
    "matlabshared.rotations.internal.coder.quaternioncg"));
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                real32_T y[5]
 * Return Type  : void
 */
static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real32_T y[5])
{
  v_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const real32_T u_data[]
 *                const int32_T u_size[2]
 * Return Type  : const mxArray *
 */
static const mxArray *g_emlrt_marshallOut(const real32_T u_data[], const int32_T
  u_size[2])
{
  const mxArray *y;
  int32_T iv[2];
  const mxArray *m;
  real32_T *pData;
  int32_T i;
  int32_T b_i;
  y = NULL;
  iv[0] = u_size[0];
  iv[1] = u_size[1];
  m = emlrtCreateNumericArray(2, &iv[0], mxSINGLE_CLASS, mxREAL);
  pData = (real32_T *)emlrtMxGetData(m);
  i = 0;
  b_i = 0;
  while (b_i < u_size[1]) {
    b_i = 0;
    while (b_i < u_size[0]) {
      pData[i] = u_data[0];
      i++;
      b_i = 1;
    }

    b_i = 1;
  }

  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *TIMU
 *                const char_T *identifier
 *                struct1_T *y
 * Return Type  : void
 */
static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *TIMU, const
  char_T *identifier, struct1_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  i_emlrt_marshallIn(sp, emlrtAlias(TIMU), &thisId, y);
  emlrtDestroyArray(&TIMU);
}

/*
 * Arguments    : const real32_T u[3]
 * Return Type  : const mxArray *
 */
static const mxArray *h_emlrt_marshallOut(const real32_T u[3])
{
  const mxArray *y;
  const mxArray *m;
  static const int32_T iv[1] = { 0 };

  static const int32_T iv1[1] = { 3 };

  y = NULL;
  m = emlrtCreateNumericArray(1, iv, mxSINGLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m, iv1, 1);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                struct1_T *y
 * Return Type  : void
 */
static void i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct1_T *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[10] = { "tid", "ax", "ay", "az", "gx", "gy",
    "gz", "mx", "my", "mz" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 10, fieldNames, 0U, &dims);
  thisId.fIdentifier = "tid";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0, "tid")),
                     &thisId, y->tid);
  thisId.fIdentifier = "ax";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1, "ax")),
                     &thisId, y->ax);
  thisId.fIdentifier = "ay";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 2, "ay")),
                     &thisId, y->ay);
  thisId.fIdentifier = "az";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 3, "az")),
                     &thisId, y->az);
  thisId.fIdentifier = "gx";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 4, "gx")),
                     &thisId, y->gx);
  thisId.fIdentifier = "gy";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 5, "gy")),
                     &thisId, y->gy);
  thisId.fIdentifier = "gz";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 6, "gz")),
                     &thisId, y->gz);
  thisId.fIdentifier = "mx";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 7, "mx")),
                     &thisId, y->mx);
  thisId.fIdentifier = "my";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 8, "my")),
                     &thisId, y->my);
  thisId.fIdentifier = "mz";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 9, "mz")),
                     &thisId, y->mz);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const real32_T u[28]
 * Return Type  : const mxArray *
 */
static const mxArray *i_emlrt_marshallOut(const real32_T u[28])
{
  const mxArray *y;
  const mxArray *m;
  static const int32_T iv[1] = { 0 };

  static const int32_T iv1[1] = { 28 };

  y = NULL;
  m = emlrtCreateNumericArray(1, iv, mxSINGLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m, iv1, 1);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *accel
 *                const char_T *identifier
 * Return Type  : real32_T (*)[3]
 */
static real32_T (*j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *accel,
  const char_T *identifier))[3]
{
  real32_T (*y)[3];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = k_emlrt_marshallIn(sp, emlrtAlias(accel), &thisId);
  emlrtDestroyArray(&accel);
  return y;
}
/*
 * Arguments    : const real32_T u[784]
 *                const mxArray *y
 * Return Type  : void
 */
  static void j_emlrt_marshallOut(const real32_T u[784], const mxArray *y)
{
  static const int32_T iv[2] = { 28, 28 };

  emlrtMxSetData((mxArray *)y, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)y, iv, 2);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : real32_T (*)[3]
 */
static real32_T (*k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[3]
{
  real32_T (*y)[3];
  y = w_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
/*
 * Arguments    : const real32_T u[3]
 *                const mxArray *y
 * Return Type  : void
 */
  static void k_emlrt_marshallOut(const real32_T u[3], const mxArray *y)
{
  static const int32_T iv[2] = { 1, 3 };

  emlrtMxSetData((mxArray *)y, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)y, iv, 2);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *estados
 *                const char_T *identifier
 * Return Type  : real32_T (*)[28]
 */
static real32_T (*l_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *estados, const char_T *identifier))[28]
{
  real32_T (*y)[28];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = m_emlrt_marshallIn(sp, emlrtAlias(estados), &thisId);
  emlrtDestroyArray(&estados);
  return y;
}
/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : real32_T (*)[28]
 */
  static real32_T (*m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[28]
{
  real32_T (*y)[28];
  y = x_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *covarianzaEstados
 *                const char_T *identifier
 * Return Type  : real32_T (*)[784]
 */
static real32_T (*n_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *covarianzaEstados, const char_T *identifier))[784]
{
  real32_T (*y)[784];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = o_emlrt_marshallIn(sp, emlrtAlias(covarianzaEstados), &thisId);
  emlrtDestroyArray(&covarianzaEstados);
  return y;
}
/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : real32_T (*)[784]
 */
  static real32_T (*o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[784]
{
  real32_T (*y)[784];
  y = y_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *C
 *                const char_T *identifier
 * Return Type  : void
 */
static void p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *C, const
  char_T *identifier)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  q_emlrt_marshallIn(sp, emlrtAlias(C), &thisId);
  emlrtDestroyArray(&C);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : void
 */
static void q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  emlrtCheckMcosClass2017a(sp, parentId, u, "constantes");
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *evaluarGPS
 *                const char_T *identifier
 * Return Type  : boolean_T
 */
static boolean_T r_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *evaluarGPS, const char_T *identifier)
{
  boolean_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = s_emlrt_marshallIn(sp, emlrtAlias(evaluarGPS), &thisId);
  emlrtDestroyArray(&evaluarGPS);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : boolean_T
 */
static boolean_T s_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  boolean_T y;
  y = ab_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : real32_T (*)[15]
 */
static real32_T (*t_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[15]
{
  real32_T (*ret)[15];
  static const int32_T dims[2] = { 5, 3 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "single", false, 2U, dims);
  ret = (real32_T (*)[15])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : real32_T
 */
  static real32_T u_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  real32_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "single", false, 0U, &dims);
  ret = *(real32_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 *                real32_T ret[5]
 * Return Type  : void
 */
static void v_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real32_T ret[5])
{
  static const int32_T dims[1] = { 5 };

  real32_T (*r)[5];
  int32_T i;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "single", false, 1U, dims);
  r = (real32_T (*)[5])emlrtMxGetData(src);
  for (i = 0; i < 5; i++) {
    ret[i] = (*r)[i];
  }

  emlrtDestroyArray(&src);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : real32_T (*)[3]
 */
static real32_T (*w_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[3]
{
  real32_T (*ret)[3];
  static const int32_T dims[2] = { 1, 3 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "single", false, 2U, dims);
  ret = (real32_T (*)[3])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : real32_T (*)[28]
 */
  static real32_T (*x_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[28]
{
  real32_T (*ret)[28];
  static const int32_T dims[1] = { 28 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "single", false, 1U, dims);
  ret = (real32_T (*)[28])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : real32_T (*)[784]
 */
static real32_T (*y_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[784]
{
  real32_T (*ret)[784];
  static const int32_T dims[2] = { 28, 28 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "single", false, 2U, dims);
  ret = (real32_T (*)[784])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
/*
 * Arguments    : const mxArray * const prhs[2]
 *                int32_T nlhs
 *                const mxArray *plhs[1]
 * Return Type  : void
 */
  void calcularAceleracion_api(const mxArray * const prhs[2], int32_T nlhs,
  const mxArray *plhs[1])
{
  real32_T (*acel)[15];
  real32_T (*v)[15];
  real32_T dt;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;
  acel = (real32_T (*)[15])mxMalloc(sizeof(real32_T [15]));

  /* Marshall function inputs */
  v = emlrt_marshallIn(&st, emlrtAlias(prhs[0]), "v");
  dt = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "dt");

  /* Invoke the target function */
  calcularAceleracion(*v, dt, *acel);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(*acel);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void calcularAceleracion_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
  calcularAceleracion_xil_terminate();
  calcularAceleracion_xil_shutdown();
  emlrtExitTimeCleanup(&emlrtContextGlobal);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void calcularAceleracion_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void calcularAceleracion_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/*
 * Arguments    : const mxArray * const prhs[3]
 *                int32_T nlhs
 *                const mxArray *plhs[5]
 * Return Type  : void
 */
void calibrarGPScoder_api(const mxArray * const prhs[3], int32_T nlhs, const
  mxArray *plhs[5])
{
  real32_T (*hora)[5];
  real32_T (*lat)[5];
  real32_T (*lon)[5];
  real32_T (*alt)[5];
  real32_T (*v)[5];
  struct0_T TGPS;
  real32_T dt;
  real32_T window;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  hora = (real32_T (*)[5])mxMalloc(sizeof(real32_T [5]));
  lat = (real32_T (*)[5])mxMalloc(sizeof(real32_T [5]));
  lon = (real32_T (*)[5])mxMalloc(sizeof(real32_T [5]));
  alt = (real32_T (*)[5])mxMalloc(sizeof(real32_T [5]));
  v = (real32_T (*)[5])mxMalloc(sizeof(real32_T [5]));

  /* Marshall function inputs */
  e_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "TGPS", &TGPS);
  dt = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "dt");
  window = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "window");

  /* Invoke the target function */
  calibrarGPScoder(&TGPS, dt, window, *hora, *lat, *lon, *alt, *v);

  /* Marshall function outputs */
  plhs[0] = b_emlrt_marshallOut(*hora);
  if (nlhs > 1) {
    plhs[1] = b_emlrt_marshallOut(*lat);
  }

  if (nlhs > 2) {
    plhs[2] = b_emlrt_marshallOut(*lon);
  }

  if (nlhs > 3) {
    plhs[3] = b_emlrt_marshallOut(*alt);
  }

  if (nlhs > 4) {
    plhs[4] = b_emlrt_marshallOut(*v);
  }
}

/*
 * Arguments    : const mxArray * const prhs[3]
 *                int32_T nlhs
 *                const mxArray *plhs[4]
 * Return Type  : void
 */
void calibrarIMUcoder_api(const mxArray * const prhs[3], int32_T nlhs, const
  mxArray *plhs[4])
{
  real32_T (*horaIMU)[5];
  real32_T (*accel)[15];
  real32_T (*gyro)[15];
  real32_T (*mag)[15];
  struct1_T TIMU;
  real32_T dt;
  real32_T window;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  horaIMU = (real32_T (*)[5])mxMalloc(sizeof(real32_T [5]));
  accel = (real32_T (*)[15])mxMalloc(sizeof(real32_T [15]));
  gyro = (real32_T (*)[15])mxMalloc(sizeof(real32_T [15]));
  mag = (real32_T (*)[15])mxMalloc(sizeof(real32_T [15]));

  /* Marshall function inputs */
  h_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "TIMU", &TIMU);
  dt = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "dt");
  window = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "window");

  /* Invoke the target function */
  calibrarIMUcoder(&TIMU, dt, window, *horaIMU, *accel, *gyro, *mag);

  /* Marshall function outputs */
  plhs[0] = b_emlrt_marshallOut(*horaIMU);
  if (nlhs > 1) {
    plhs[1] = emlrt_marshallOut(*accel);
  }

  if (nlhs > 2) {
    plhs[2] = emlrt_marshallOut(*gyro);
  }

  if (nlhs > 3) {
    plhs[3] = emlrt_marshallOut(*mag);
  }
}

/*
 * Arguments    : const mxArray * const prhs[1]
 *                int32_T nlhs
 *                const mxArray *plhs[3]
 * Return Type  : void
 */
void calibrarMagnetometro_api(const mxArray * const prhs[1], int32_T nlhs, const
  mxArray *plhs[3])
{
  real32_T (*m)[15];
  real32_T (*A)[9];
  real32_T (*b)[3];
  real32_T (*mag)[15];
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  m = (real32_T (*)[15])mxMalloc(sizeof(real32_T [15]));
  A = (real32_T (*)[9])mxMalloc(sizeof(real32_T [9]));
  b = (real32_T (*)[3])mxMalloc(sizeof(real32_T [3]));

  /* Marshall function inputs */
  mag = emlrt_marshallIn(&st, emlrtAlias(prhs[0]), "mag");

  /* Invoke the target function */
  calibrarMagnetometro(*mag, *m, *A, *b);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(*m);
  if (nlhs > 1) {
    plhs[1] = c_emlrt_marshallOut(*A);
  }

  if (nlhs > 2) {
    plhs[2] = d_emlrt_marshallOut(*b);
  }
}

/*
 * Arguments    : const mxArray * const prhs[12]
 *                int32_T nlhs
 *                const mxArray *plhs[7]
 * Return Type  : void
 */
void filtroCoder_api(const mxArray * const prhs[12], int32_T nlhs, const mxArray
                     *plhs[7])
{
  real32_T (*p_data)[9];
  real32_T (*posicion)[3];
  real32_T (*velocidad)[3];
  real32_T (*estado)[28];
  const mxArray *prhs_copy_idx_0;
  const mxArray *prhs_copy_idx_1;
  const mxArray *prhs_copy_idx_2;
  const mxArray *prhs_copy_idx_3;
  const mxArray *prhs_copy_idx_4;
  const mxArray *prhs_copy_idx_5;
  const mxArray *prhs_copy_idx_6;
  const mxArray *prhs_copy_idx_7;
  const mxArray *prhs_copy_idx_8;
  const mxArray *prhs_copy_idx_9;
  real32_T (*accel)[3];
  real32_T (*gyro)[3];
  real32_T (*mag)[3];
  real32_T (*lla)[3];
  real32_T (*estados)[28];
  real32_T (*covarianzaEstados)[784];
  real32_T VGPS;
  real32_T (*ubicacionReferencia)[3];
  real32_T (*posAnterior)[3];
  boolean_T evaluarGPS;
  boolean_T evaluarIMU;
  int32_T p_size[2];
  c_matlabshared_rotations_intern cuaternion;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  p_data = (real32_T (*)[9])mxMalloc(sizeof(real32_T [9]));
  posicion = (real32_T (*)[3])mxMalloc(sizeof(real32_T [3]));
  velocidad = (real32_T (*)[3])mxMalloc(sizeof(real32_T [3]));
  estado = (real32_T (*)[28])mxMalloc(sizeof(real32_T [28]));
  prhs_copy_idx_0 = prhs[0];
  prhs_copy_idx_1 = prhs[1];
  prhs_copy_idx_2 = prhs[2];
  prhs_copy_idx_3 = prhs[3];
  prhs_copy_idx_4 = prhs[4];
  prhs_copy_idx_5 = emlrtProtectR2012b(prhs[5], 5, true, -1);
  prhs_copy_idx_6 = prhs[6];
  prhs_copy_idx_7 = prhs[7];
  prhs_copy_idx_8 = prhs[8];
  prhs_copy_idx_9 = emlrtProtectR2012b(prhs[9], 9, true, -1);

  /* Marshall function inputs */
  accel = j_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_0), "accel");
  gyro = j_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_1), "gyro");
  mag = j_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_2), "mag");
  lla = j_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_3), "lla");
  estados = l_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_4), "estados");
  covarianzaEstados = n_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_5),
    "covarianzaEstados");
  VGPS = c_emlrt_marshallIn(&st, emlrtAliasP(prhs_copy_idx_6), "VGPS");
  p_emlrt_marshallIn(&st, emlrtAliasP(prhs_copy_idx_7), "C");
  ubicacionReferencia = j_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_8),
    "ubicacionReferencia");
  posAnterior = j_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_9),
    "posAnterior");
  evaluarGPS = r_emlrt_marshallIn(&st, emlrtAliasP(prhs[10]), "evaluarGPS");
  evaluarIMU = r_emlrt_marshallIn(&st, emlrtAliasP(prhs[11]), "evaluarIMU");

  /* Invoke the target function */
  filtroCoder(*accel, *gyro, *mag, *lla, *estados, *covarianzaEstados, VGPS,
              *ubicacionReferencia, *posAnterior, evaluarGPS, evaluarIMU,
              *p_data, p_size, &cuaternion, *posicion, *velocidad, *estado);

  /* Marshall function outputs */
  plhs[0] = e_emlrt_marshallOut(*p_data, p_size);
  if (nlhs > 1) {
    plhs[1] = f_emlrt_marshallOut(&st, cuaternion);
  }

  if (nlhs > 2) {
    plhs[2] = h_emlrt_marshallOut(*posicion);
  }

  if (nlhs > 3) {
    plhs[3] = h_emlrt_marshallOut(*velocidad);
  }

  if (nlhs > 4) {
    plhs[4] = i_emlrt_marshallOut(*estado);
  }

  if (nlhs > 5) {
    j_emlrt_marshallOut(*covarianzaEstados, prhs_copy_idx_5);
    plhs[5] = prhs_copy_idx_5;
  }

  if (nlhs > 6) {
    k_emlrt_marshallOut(*posAnterior, prhs_copy_idx_9);
    plhs[6] = prhs_copy_idx_9;
  }
}

/*
 * File trailer for _coder_calcularAceleracion_api.c
 *
 * [EOF]
 */
