/*
 * File: _coder_calcularAceleracion_info.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef _CODER_CALCULARACELERACION_INFO_H
#define _CODER_CALCULARACELERACION_INFO_H

/* Include Files */
#include "mex.h"

/* Function Declarations */
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);

#endif

/*
 * File trailer for _coder_calcularAceleracion_info.h
 *
 * [EOF]
 */
