/*
 * File: _coder_calcularAceleracion_mex.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "_coder_calcularAceleracion_mex.h"
#include "_coder_calcularAceleracion_api.h"

/* Variable Definitions */
static const char * emlrtEntryPoints[5] = { "calcularAceleracion",
  "calibrarGPScoder", "calibrarIMUcoder", "calibrarMagnetometro", "filtroCoder"
};

/* Function Declarations */
MEXFUNCTION_LINKAGE void c_calibrarMagnetometro_mexFunct(int32_T nlhs, mxArray
  *plhs[3], int32_T nrhs, const mxArray *prhs[1]);
MEXFUNCTION_LINKAGE void calcularAceleracion_mexFunction(int32_T nlhs, mxArray
  *plhs[1], int32_T nrhs, const mxArray *prhs[2]);
MEXFUNCTION_LINKAGE void calibrarGPScoder_mexFunction(int32_T nlhs, mxArray
  *plhs[5], int32_T nrhs, const mxArray *prhs[3]);
MEXFUNCTION_LINKAGE void calibrarIMUcoder_mexFunction(int32_T nlhs, mxArray
  *plhs[4], int32_T nrhs, const mxArray *prhs[3]);
MEXFUNCTION_LINKAGE void filtroCoder_mexFunction(int32_T nlhs, mxArray *plhs[7],
  int32_T nrhs, const mxArray *prhs[12]);

/* Function Definitions */

/*
 * Arguments    : int32_T nlhs
 *                mxArray *plhs[3]
 *                int32_T nrhs
 *                const mxArray *prhs[1]
 * Return Type  : void
 */
void c_calibrarMagnetometro_mexFunct(int32_T nlhs, mxArray *plhs[3], int32_T
  nrhs, const mxArray *prhs[1])
{
  const mxArray *outputs[3];
  int32_T b_nlhs;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 1) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 1, 4,
                        20, "calibrarMagnetometro");
  }

  if (nlhs > 3) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 20,
                        "calibrarMagnetometro");
  }

  /* Call the function. */
  calibrarMagnetometro_api(prhs, nlhs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);
}

/*
 * Arguments    : int32_T nlhs
 *                mxArray *plhs[1]
 *                int32_T nrhs
 *                const mxArray *prhs[2]
 * Return Type  : void
 */
void calcularAceleracion_mexFunction(int32_T nlhs, mxArray *plhs[1], int32_T
  nrhs, const mxArray *prhs[2])
{
  const mxArray *outputs[1];
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 2) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 2, 4,
                        19, "calcularAceleracion");
  }

  if (nlhs > 1) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 19,
                        "calcularAceleracion");
  }

  /* Call the function. */
  calcularAceleracion_api(prhs, nlhs, outputs);

  /* Copy over outputs to the caller. */
  emlrtReturnArrays(1, plhs, outputs);
}

/*
 * Arguments    : int32_T nlhs
 *                mxArray *plhs[5]
 *                int32_T nrhs
 *                const mxArray *prhs[3]
 * Return Type  : void
 */
void calibrarGPScoder_mexFunction(int32_T nlhs, mxArray *plhs[5], int32_T nrhs,
  const mxArray *prhs[3])
{
  const mxArray *outputs[5];
  int32_T b_nlhs;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 3) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 3, 4,
                        16, "calibrarGPScoder");
  }

  if (nlhs > 5) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 16,
                        "calibrarGPScoder");
  }

  /* Call the function. */
  calibrarGPScoder_api(prhs, nlhs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);
}

/*
 * Arguments    : int32_T nlhs
 *                mxArray *plhs[4]
 *                int32_T nrhs
 *                const mxArray *prhs[3]
 * Return Type  : void
 */
void calibrarIMUcoder_mexFunction(int32_T nlhs, mxArray *plhs[4], int32_T nrhs,
  const mxArray *prhs[3])
{
  const mxArray *outputs[4];
  int32_T b_nlhs;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 3) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 3, 4,
                        16, "calibrarIMUcoder");
  }

  if (nlhs > 4) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 16,
                        "calibrarIMUcoder");
  }

  /* Call the function. */
  calibrarIMUcoder_api(prhs, nlhs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);
}

/*
 * Arguments    : int32_T nlhs
 *                mxArray *plhs[7]
 *                int32_T nrhs
 *                const mxArray *prhs[12]
 * Return Type  : void
 */
void filtroCoder_mexFunction(int32_T nlhs, mxArray *plhs[7], int32_T nrhs, const
  mxArray *prhs[12])
{
  const mxArray *outputs[7];
  int32_T b_nlhs;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 12) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 12, 4,
                        11, "filtroCoder");
  }

  if (nlhs > 7) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 11,
                        "filtroCoder");
  }

  /* Call the function. */
  filtroCoder_api(prhs, nlhs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);
}

/*
 * Arguments    : int32_T nlhs
 *                mxArray *plhs[]
 *                int32_T nrhs
 *                const mxArray *prhs[]
 * Return Type  : void
 */
void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const mxArray
                 *prhs[])
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexAtExit(calcularAceleracion_atexit);

  /* Module initialization. */
  calcularAceleracion_initialize();
  st.tls = emlrtRootTLSGlobal;

  /* Dispatch the entry-point. */
  switch (emlrtGetEntryPointIndexR2016a(&st, nrhs, prhs, emlrtEntryPoints, 5)) {
   case 0:
    calcularAceleracion_mexFunction(nlhs, plhs, nrhs - 1, *(const mxArray *(*)[2])
      &prhs[1]);
    break;

   case 1:
    calibrarGPScoder_mexFunction(nlhs, plhs, nrhs - 1, *(const mxArray *(*)[3])&
      prhs[1]);
    break;

   case 2:
    calibrarIMUcoder_mexFunction(nlhs, plhs, nrhs - 1, *(const mxArray *(*)[3])&
      prhs[1]);
    break;

   case 3:
    c_calibrarMagnetometro_mexFunct(nlhs, plhs, nrhs - 1, *(const mxArray *(*)[1])
      &prhs[1]);
    break;

   case 4:
    filtroCoder_mexFunction(nlhs, plhs, nrhs - 1, *(const mxArray *(*)[12])&
      prhs[1]);
    break;
  }

  /* Module termination. */
  calcularAceleracion_terminate();
}

/*
 * Arguments    : void
 * Return Type  : emlrtCTX
 */
emlrtCTX mexFunctionCreateRootTLS(void)
{
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  return emlrtRootTLSGlobal;
}

/*
 * File trailer for _coder_calcularAceleracion_mex.c
 *
 * [EOF]
 */
