/*
 * File: xzgeqp3.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "xzgeqp3.h"
#include "calcularAceleracion.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"
#include "xnrm2.h"
#include <math.h>
#include <string.h>

/* Function Declarations */
static float rt_hypotf_snf(float u0, float u1);

/* Function Definitions */

/*
 * Arguments    : float u0
 *                float u1
 * Return Type  : float
 */
static float rt_hypotf_snf(float u0, float u1)
{
  float y;
  float a;
  a = fabsf(u0);
  y = fabsf(u1);
  if (a < y) {
    a /= y;
    y *= sqrtf(a * a + 1.0F);
  } else if (a > y) {
    y /= a;
    y = a * sqrtf(y * y + 1.0F);
  } else {
    if (!rtIsNaNF(y)) {
      y = a * 1.41421354F;
    }
  }

  return y;
}

/*
 * Arguments    : float A[20]
 *                float tau[4]
 *                int jpvt[4]
 * Return Type  : void
 */
void qrpf(float A[20], float tau[4], int jpvt[4])
{
  float work[4];
  float smax;
  float scale;
  int k;
  float absxk;
  float vn1[4];
  float vn2[4];
  float t;
  int i;
  int ip1;
  int ii;
  int itemp;
  int pvt;
  int ix;
  int iy;
  int b_i;
  int lastv;
  int lastc;
  bool exitg2;
  int exitg1;
  int i1;
  work[0] = 0.0F;
  smax = 0.0F;
  scale = 1.29246971E-26F;
  for (k = 1; k < 6; k++) {
    absxk = fabsf(A[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      smax = smax * t * t + 1.0F;
      scale = absxk;
    } else {
      t = absxk / scale;
      smax += t * t;
    }
  }

  smax = scale * sqrtf(smax);
  vn1[0] = smax;
  vn2[0] = smax;
  work[1] = 0.0F;
  smax = 0.0F;
  scale = 1.29246971E-26F;
  for (k = 6; k < 11; k++) {
    absxk = fabsf(A[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      smax = smax * t * t + 1.0F;
      scale = absxk;
    } else {
      t = absxk / scale;
      smax += t * t;
    }
  }

  smax = scale * sqrtf(smax);
  vn1[1] = smax;
  vn2[1] = smax;
  work[2] = 0.0F;
  smax = 0.0F;
  scale = 1.29246971E-26F;
  for (k = 11; k < 16; k++) {
    absxk = fabsf(A[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      smax = smax * t * t + 1.0F;
      scale = absxk;
    } else {
      t = absxk / scale;
      smax += t * t;
    }
  }

  smax = scale * sqrtf(smax);
  vn1[2] = smax;
  vn2[2] = smax;
  work[3] = 0.0F;
  smax = 0.0F;
  scale = 1.29246971E-26F;
  for (k = 16; k < 21; k++) {
    absxk = fabsf(A[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      smax = smax * t * t + 1.0F;
      scale = absxk;
    } else {
      t = absxk / scale;
      smax += t * t;
    }
  }

  smax = scale * sqrtf(smax);
  vn1[3] = smax;
  vn2[3] = smax;
  for (i = 0; i < 4; i++) {
    ip1 = i + 2;
    ii = i * 5 + i;
    itemp = 4 - i;
    pvt = 0;
    if (4 - i > 1) {
      ix = i;
      smax = fabsf(vn1[i]);
      for (k = 2; k <= itemp; k++) {
        ix++;
        scale = fabsf(vn1[ix]);
        if (scale > smax) {
          pvt = k - 1;
          smax = scale;
        }
      }
    }

    pvt += i;
    if (pvt != i) {
      ix = pvt * 5;
      iy = i * 5;
      for (k = 0; k < 5; k++) {
        smax = A[ix];
        A[ix] = A[iy];
        A[iy] = smax;
        ix++;
        iy++;
      }

      itemp = jpvt[pvt];
      jpvt[pvt] = jpvt[i];
      jpvt[i] = itemp;
      vn1[pvt] = vn1[i];
      vn2[pvt] = vn2[i];
    }

    absxk = A[ii];
    itemp = ii + 2;
    tau[i] = 0.0F;
    smax = xnrm2(4 - i, A, ii + 2);
    if (smax != 0.0F) {
      scale = rt_hypotf_snf(A[ii], smax);
      if (A[ii] >= 0.0F) {
        scale = -scale;
      }

      if (fabsf(scale) < 9.86076132E-32F) {
        pvt = -1;
        b_i = (ii - i) + 5;
        do {
          pvt++;
          for (k = itemp; k <= b_i; k++) {
            A[k - 1] *= 1.01412048E+31F;
          }

          scale *= 1.01412048E+31F;
          absxk *= 1.01412048E+31F;
        } while (!(fabsf(scale) >= 9.86076132E-32F));

        scale = rt_hypotf_snf(absxk, xnrm2(4 - i, A, ii + 2));
        if (absxk >= 0.0F) {
          scale = -scale;
        }

        tau[i] = (scale - absxk) / scale;
        smax = 1.0F / (absxk - scale);
        for (k = itemp; k <= b_i; k++) {
          A[k - 1] *= smax;
        }

        for (k = 0; k <= pvt; k++) {
          scale *= 9.86076132E-32F;
        }

        absxk = scale;
      } else {
        tau[i] = (scale - A[ii]) / scale;
        smax = 1.0F / (A[ii] - scale);
        b_i = (ii - i) + 5;
        for (k = itemp; k <= b_i; k++) {
          A[k - 1] *= smax;
        }

        absxk = scale;
      }
    }

    A[ii] = absxk;
    if (i + 1 < 4) {
      absxk = A[ii];
      A[ii] = 1.0F;
      k = ii + 6;
      if (tau[i] != 0.0F) {
        lastv = 5 - i;
        itemp = (ii - i) + 4;
        while ((lastv > 0) && (A[itemp] == 0.0F)) {
          lastv--;
          itemp--;
        }

        lastc = 2 - i;
        exitg2 = false;
        while ((!exitg2) && (lastc + 1 > 0)) {
          itemp = (ii + lastc * 5) + 5;
          pvt = itemp;
          do {
            exitg1 = 0;
            if (pvt + 1 <= itemp + lastv) {
              if (A[pvt] != 0.0F) {
                exitg1 = 1;
              } else {
                pvt++;
              }
            } else {
              lastc--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }
      } else {
        lastv = 0;
        lastc = -1;
      }

      if (lastv > 0) {
        if (lastc + 1 != 0) {
          if (0 <= lastc) {
            memset(&work[0], 0, (lastc + 1) * sizeof(float));
          }

          iy = 0;
          b_i = (ii + 5 * lastc) + 6;
          for (itemp = k; itemp <= b_i; itemp += 5) {
            ix = ii;
            smax = 0.0F;
            i1 = (itemp + lastv) - 1;
            for (pvt = itemp; pvt <= i1; pvt++) {
              smax += A[pvt - 1] * A[ix];
              ix++;
            }

            work[iy] += smax;
            iy++;
          }
        }

        if (!(-tau[i] == 0.0F)) {
          itemp = ii;
          pvt = 0;
          for (iy = 0; iy <= lastc; iy++) {
            if (work[pvt] != 0.0F) {
              smax = work[pvt] * -tau[i];
              ix = ii;
              b_i = itemp + 6;
              i1 = lastv + itemp;
              for (k = b_i; k <= i1 + 5; k++) {
                A[k - 1] += A[ix] * smax;
                ix++;
              }
            }

            pvt++;
            itemp += 5;
          }
        }
      }

      A[ii] = absxk;
    }

    for (iy = ip1; iy < 5; iy++) {
      itemp = i + (iy - 1) * 5;
      smax = vn1[iy - 1];
      if (smax != 0.0F) {
        scale = fabsf(A[itemp]) / smax;
        scale = 1.0F - scale * scale;
        if (scale < 0.0F) {
          scale = 0.0F;
        }

        absxk = smax / vn2[iy - 1];
        absxk = scale * (absxk * absxk);
        if (absxk <= 0.000345266977F) {
          smax = xnrm2(4 - i, A, itemp + 2);
          vn1[iy - 1] = smax;
          vn2[iy - 1] = smax;
        } else {
          vn1[iy - 1] = smax * sqrtf(scale);
        }
      }
    }
  }
}

/*
 * File trailer for xzgeqp3.c
 *
 * [EOF]
 */
