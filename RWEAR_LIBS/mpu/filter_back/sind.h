/*
 * File: sind.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef SIND_H
#define SIND_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void b_sind(float *x);

#endif

/*
 * File trailer for sind.h
 *
 * [EOF]
 */
