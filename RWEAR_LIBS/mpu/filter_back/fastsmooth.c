/*
 * File: fastsmooth.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "fastsmooth.h"
#include "calcularAceleracion.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Definitions */

/*
 * fastsmooth(Y,w,type,ends) smooths vector Y with smooth
 *   of width w.
 *  The argument "type" determines the smooth type:
 *    If type=1, rectangular (sliding-average or boxcar)
 *    If type=2, triangular (2 passes of sliding-average)
 *    If type=3, pseudo-Gaussian (3 passes of sliding-average)
 *    If type=4, pseudo-Gaussian (4 passes of same sliding-average)
 *    If type=5, multiple-width (4 passes of different sliding-average)
 *  The argument "ends" controls how the "ends" of the signal
 *  (the first w/2 points and the last w/2 points) are handled.
 *    If ends=0, the ends are zero.  (In this mode the elapsed
 *      time is independent of the smooth width). The fastest.
 *    If ends=1, the ends are smoothed with progressively
 *      smaller smooths the closer to the end. (In this mode the
 *      elapsed time increases with increasing smooth widths).
 *  fastsmooth(Y,w,type) smooths with ends=0.
 *  fastsmooth(Y,w) smooths with type=1 and ends=0.
 *  Examples:
 *  fastsmooth([1 1 1 10 10 10 1 1 1 1],3)= [0 1 4 7 10 7 4 1 1 0]
 *
 *  fastsmooth([1 1 1 10 10 10 1 1 1 1],3,1,1)= [1 1 4 7 10 7 4 1 1 1]
 *
 *  x=1:100;
 *  y=randn(size(x));
 *  plot(x,y,x,fastsmooth(y,5,3,1),'r')
 *  xlabel('Blue: white noise.    Red: smoothed white noise.')
 * Arguments    : const float Y[5]
 *                float w
 *                float SmoothY[5]
 * Return Type  : void
 */
void fastsmooth(const float Y[5], float w, float SmoothY[5])
{
  float h;
  float b_w;
  int i;
  float SumPoints;
  int k;
  float halfw;
  double s[5];
  int b_i;
  float y;
  int kk;
  int vlen_tmp_tmp;
  h = 1.0F;
  b_w = roundf(w);
  if (1.0F > b_w) {
    i = 0;
  } else {
    i = (int)b_w;
  }

  if (i == 0) {
    SumPoints = 0.0F;
  } else {
    SumPoints = Y[0];
    for (k = 2; k <= i; k++) {
      SumPoints += Y[k - 1];
    }
  }

  /* Atento a que las dimensiones coincidan */
  for (i = 0; i < 5; i++) {
    s[i] = 0.0;
  }

  halfw = roundf(b_w / 2.0F);
  b_i = (int)(5.0F - b_w);
  for (k = 0; k < b_i; k++) {
    s[(int)((((float)k + 1.0F) + halfw) - 1.0F) - 1] = SumPoints;
    SumPoints -= Y[(int)(unsigned int)(float)k];
    SumPoints += Y[(int)(((float)k + 1.0F) + b_w) - 1];
    h = (float)k + 1.0F;
  }

  if ((5.0F - b_w) + 1.0F > 5.0F) {
    b_i = -1;
    i = -1;
  } else {
    b_i = (int)((5.0F - b_w) + 1.0F) - 2;
    i = 4;
  }

  i -= b_i;
  if (i == 0) {
    y = 0.0F;
  } else {
    y = Y[b_i + 1];
    for (k = 2; k <= i; k++) {
      y += Y[b_i + k];
    }
  }

  s[(int)(h + halfw) - 1] = y;
  for (i = 0; i < 5; i++) {
    SmoothY[i] = (float)s[i] / b_w;
  }

  /*  Taper the ends of the signal if ends=1. */
  SmoothY[0] = (Y[0] + Y[1]) / 2.0F;
  b_i = (int)((w + 1.0F) / 2.0F + -1.0F);
  for (kk = 0; kk < b_i; kk++) {
    SumPoints = 2.0F * ((float)kk + 2.0F);
    i = (int)(SumPoints - 1.0F);
    if (i == 0) {
      y = 0.0F;
    } else {
      y = Y[0];
      for (k = 2; k <= i; k++) {
        y += Y[k - 1];
      }
    }

    SmoothY[(int)(unsigned int)(float)kk + 1] = y / (float)i;
    vlen_tmp_tmp = (int)((5.0F - SumPoints) + 2.0F);
    i = 6 - vlen_tmp_tmp;
    if (i == 0) {
      y = 0.0F;
    } else {
      y = Y[vlen_tmp_tmp - 1];
      for (k = 2; k <= i; k++) {
        y += Y[(vlen_tmp_tmp + k) - 2];
      }
    }

    SmoothY[(int)((5.0F - ((float)kk + 2.0F)) + 1.0F) - 1] = y / (float)i;
  }

  SmoothY[4] = (Y[4] + Y[3]) / 2.0F;
}

void fastsmooth_double(const float Y[5], float w, double SmoothY[5])
{
  float h;
  float b_w;
  int i;
  float SumPoints;
  int k;
  float halfw;
  double s[5];
  int b_i;
  float y;
  int kk;
  int vlen_tmp_tmp;
  h = 1.0F;
  b_w = roundf(w);
  if (1.0F > b_w) {
    i = 0;
  } else {
    i = (int)b_w;
  }

  if (i == 0) {
    SumPoints = 0.0F;
  } else {
    SumPoints = Y[0];
    for (k = 2; k <= i; k++) {
      SumPoints += Y[k - 1];
    }
  }

  /* Atento a que las dimensiones coincidan */
  for (i = 0; i < 5; i++) {
    s[i] = 0.0;
  }

  halfw = roundf(b_w / 2.0F);
  b_i = (int)(5.0F - b_w);
  for (k = 0; k < b_i; k++) {
    s[(int)((((float)k + 1.0F) + halfw) - 1.0F) - 1] = SumPoints;
    SumPoints -= Y[(int)(unsigned int)(float)k];
    SumPoints += Y[(int)(((float)k + 1.0F) + b_w) - 1];
    h = (float)k + 1.0F;
  }

  if ((5.0F - b_w) + 1.0F > 5.0F) {
    b_i = -1;
    i = -1;
  } else {
    b_i = (int)((5.0F - b_w) + 1.0F) - 2;
    i = 4;
  }

  i -= b_i;
  if (i == 0) {
    y = 0.0F;
  } else {
    y = Y[b_i + 1];
    for (k = 2; k <= i; k++) {
      y += Y[b_i + k];
    }
  }

  s[(int)(h + halfw) - 1] = y;
  for (i = 0; i < 5; i++) {
    SmoothY[i] = (float)s[i] / b_w;
  }

  /*  Taper the ends of the signal if ends=1. */
  SmoothY[0] = (Y[0] + Y[1]) / 2.0F;
  b_i = (int)((w + 1.0F) / 2.0F + -1.0F);
  for (kk = 0; kk < b_i; kk++) {
    SumPoints = 2.0F * ((float)kk + 2.0F);
    i = (int)(SumPoints - 1.0F);
    if (i == 0) {
      y = 0.0F;
    } else {
      y = Y[0];
      for (k = 2; k <= i; k++) {
        y += Y[k - 1];
      }
    }

    SmoothY[(int)(unsigned int)(float)kk + 1] = y / (float)i;
    vlen_tmp_tmp = (int)((5.0F - SumPoints) + 2.0F);
    i = 6 - vlen_tmp_tmp;
    if (i == 0) {
      y = 0.0F;
    } else {
      y = Y[vlen_tmp_tmp - 1];
      for (k = 2; k <= i; k++) {
        y += Y[(vlen_tmp_tmp + k) - 2];
      }
    }

    SmoothY[(int)((5.0F - ((float)kk + 2.0F)) + 1.0F) - 1] = y / (float)i;
  }

  SmoothY[4] = (Y[4] + Y[3]) / 2.0F;
}


/*
 * File trailer for fastsmooth.c
 *
 * [EOF]
 */
