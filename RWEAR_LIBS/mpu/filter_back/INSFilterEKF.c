/*
 * File: INSFilterEKF.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 21-Oct-2019 12:58:03
 */

/* Include Files */
#include "INSFilterEKF.h"
#include "calcularAceleracion.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : const c_fusion_internal_coder_insfilt *obj
 *                double pos[3]
 *                double orient_a_data[]
 *                int orient_a_size[2]
 *                double orient_b_data[]
 *                int orient_b_size[2]
 *                double orient_c_data[]
 *                int orient_c_size[2]
 *                double orient_d_data[]
 *                int orient_d_size[2]
 * Return Type  : void
 */
void INSFilterEKF_pose(const c_fusion_internal_coder_insfilt *obj, double pos[3],
  double orient_a_data[], int orient_a_size[2], double orient_b_data[], int
  orient_b_size[2], double orient_c_data[], int orient_c_size[2], double
  orient_d_data[], int orient_d_size[2])
{
  double val[28];
  memcpy(&val[0], &obj->pState[0], 28U * sizeof(double));
  orient_a_size[0] = 1;
  orient_a_size[1] = 1;
  orient_a_data[0] = val[0];
  orient_b_size[0] = 1;
  orient_b_size[1] = 1;
  orient_b_data[0] = val[1];
  orient_c_size[0] = 1;
  orient_c_size[1] = 1;
  orient_c_data[0] = val[2];
  orient_d_size[0] = 1;
  orient_d_size[1] = 1;
  orient_d_data[0] = val[3];
  pos[0] = obj->pState[7];
  pos[1] = obj->pState[8];
  pos[2] = obj->pState[9];
}

/*
 * File trailer for INSFilterEKF.c
 *
 * [EOF]
 */
