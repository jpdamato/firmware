/*
 * File: mldivide.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 21-Oct-2019 12:58:03
 */

#ifndef MLDIVIDE_H
#define MLDIVIDE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void mldivide(const double A[40000], const double B[10000], double Y[4]);

#endif

/*
 * File trailer for mldivide.h
 *
 * [EOF]
 */
