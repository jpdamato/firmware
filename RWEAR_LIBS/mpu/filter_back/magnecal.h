/*
 * File: magnecal.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 21-Oct-2019 12:58:03
 */

#ifndef MAGNECAL_H
#define MAGNECAL_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void magnecal(const double d[30000], double A[9], double b[3]);

#endif

/*
 * File trailer for magnecal.h
 *
 * [EOF]
 */
