/*
 * File: AsyncMARGGPSFuserBase.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

/* Include Files */
#include "AsyncMARGGPSFuserBase.h"
#include "IMUBasicEKF.h"
#include "calcularAceleracion.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Variable Definitions */
static const float fv[9] = { 0.0001F, 0.0F, 0.0F, 0.0F, 0.0001F, 0.0F, 0.0F,
  0.0F, 0.0001F };

/* Function Declarations */
static void c_AsyncMARGGPSFuserBase_accelMe(const float x[28], float z[3]);
static void d_AsyncMARGGPSFuserBase_accelMe(const float x[28], float dhdx[84]);

/* Function Definitions */

/*
 * Arguments    : const float x[28]
 *                float z[3]
 * Return Type  : void
 */
static void c_AsyncMARGGPSFuserBase_accelMe(const float x[28], float z[3])
{
  float z_tmp;
  float b_z_tmp;
  float c_z_tmp;
  float d_z_tmp;
  float e_z_tmp;
  float f_z_tmp;
  z_tmp = 2.0F * x[0];
  b_z_tmp = 2.0F * x[1];
  c_z_tmp = x[0] * x[0];
  d_z_tmp = x[1] * x[1];
  e_z_tmp = x[2] * x[2];
  f_z_tmp = x[3] * x[3];
  z[0] = ((x[16] - x[13] * (((c_z_tmp + d_z_tmp) - e_z_tmp) - f_z_tmp)) + (x[15]
           - 9.81F) * (z_tmp * x[2] - b_z_tmp * x[3])) - x[14] * (z_tmp * x[3] +
    b_z_tmp * x[2]);
  b_z_tmp = c_z_tmp - d_z_tmp;
  c_z_tmp = 2.0F * x[2] * x[3];
  z[1] = ((x[17] - x[14] * ((b_z_tmp + e_z_tmp) - f_z_tmp)) - (x[15] - 9.81F) *
          (z_tmp * x[1] + c_z_tmp)) + x[13] * (2.0F * x[0] * x[3] - 2.0F * x[1] *
    x[2]);
  z[2] = ((x[18] - (x[15] - 9.81F) * ((b_z_tmp - e_z_tmp) + f_z_tmp)) + x[14] *
          (2.0F * x[0] * x[1] - c_z_tmp)) - x[13] * (2.0F * x[0] * x[2] + 2.0F *
    x[1] * x[3]);
}

/*
 * Arguments    : const float x[28]
 *                float dhdx[84]
 * Return Type  : void
 */
static void d_AsyncMARGGPSFuserBase_accelMe(const float x[28], float dhdx[84])
{
  float dhdx_tmp;
  float b_dhdx_tmp;
  float c_dhdx_tmp;
  float d_dhdx_tmp;
  float e_dhdx_tmp;
  float f_dhdx_tmp;
  float g_dhdx_tmp;
  float h_dhdx_tmp;
  float i_dhdx_tmp;
  dhdx_tmp = 2.0F * x[2];
  b_dhdx_tmp = 2.0F * x[0];
  c_dhdx_tmp = 2.0F * x[1];
  d_dhdx_tmp = 2.0F * x[3];
  e_dhdx_tmp = -2.0F * x[0];
  dhdx[0] = (dhdx_tmp * (x[15] - 9.81F) - d_dhdx_tmp * x[14]) - b_dhdx_tmp * x
    [13];
  dhdx[3] = (-2.0F * x[3] * (x[15] - 9.81F) - dhdx_tmp * x[14]) - c_dhdx_tmp *
    x[13];
  dhdx[6] = (b_dhdx_tmp * (x[15] - 9.81F) - c_dhdx_tmp * x[14]) + dhdx_tmp * x
    [13];
  dhdx[9] = (d_dhdx_tmp * x[13] - b_dhdx_tmp * x[14]) - c_dhdx_tmp * (x[15] -
    9.81F);
  dhdx[12] = 0.0F;
  dhdx[15] = 0.0F;
  dhdx[18] = 0.0F;
  dhdx[21] = 0.0F;
  dhdx[24] = 0.0F;
  dhdx[27] = 0.0F;
  dhdx[30] = 0.0F;
  dhdx[33] = 0.0F;
  dhdx[36] = 0.0F;
  d_dhdx_tmp = -(x[0] * x[0]);
  f_dhdx_tmp = x[1] * x[1];
  g_dhdx_tmp = x[2] * x[2];
  h_dhdx_tmp = x[3] * x[3];
  dhdx[39] = ((d_dhdx_tmp - f_dhdx_tmp) + g_dhdx_tmp) + h_dhdx_tmp;
  dhdx[42] = e_dhdx_tmp * x[3] - c_dhdx_tmp * x[2];
  dhdx[45] = b_dhdx_tmp * x[2] - c_dhdx_tmp * x[3];
  dhdx[48] = 1.0F;
  dhdx[51] = 0.0F;
  dhdx[54] = 0.0F;
  dhdx[57] = 0.0F;
  dhdx[60] = 0.0F;
  dhdx[63] = 0.0F;
  dhdx[66] = 0.0F;
  dhdx[69] = 0.0F;
  dhdx[72] = 0.0F;
  dhdx[75] = 0.0F;
  dhdx[78] = 0.0F;
  dhdx[81] = 0.0F;
  dhdx[1] = (2.0F * x[3] * x[13] - 2.0F * x[0] * x[14]) - 2.0F * x[1] * (x[15] -
    9.81F);
  c_dhdx_tmp = (2.0F * x[1] * x[14] - 2.0F * x[0] * (x[15] - 9.81F)) - 2.0F * x
    [2] * x[13];
  dhdx[4] = c_dhdx_tmp;
  i_dhdx_tmp = (-2.0F * x[3] * (x[15] - 9.81F) - 2.0F * x[2] * x[14]) - 2.0F *
    x[1] * x[13];
  dhdx[7] = i_dhdx_tmp;
  dhdx[10] = (2.0F * x[3] * x[14] - 2.0F * x[2] * (x[15] - 9.81F)) + 2.0F * x[0]
    * x[13];
  dhdx[13] = 0.0F;
  dhdx[16] = 0.0F;
  dhdx[19] = 0.0F;
  dhdx[22] = 0.0F;
  dhdx[25] = 0.0F;
  dhdx[28] = 0.0F;
  dhdx[31] = 0.0F;
  dhdx[34] = 0.0F;
  dhdx[37] = 0.0F;
  dhdx[40] = b_dhdx_tmp * x[3] - 2.0F * x[1] * x[2];
  d_dhdx_tmp += f_dhdx_tmp;
  dhdx[43] = (d_dhdx_tmp - g_dhdx_tmp) + h_dhdx_tmp;
  dhdx[46] = e_dhdx_tmp * x[1] - dhdx_tmp * x[3];
  dhdx[49] = 0.0F;
  dhdx[52] = 1.0F;
  dhdx[55] = 0.0F;
  dhdx[58] = 0.0F;
  dhdx[61] = 0.0F;
  dhdx[64] = 0.0F;
  dhdx[67] = 0.0F;
  dhdx[70] = 0.0F;
  dhdx[73] = 0.0F;
  dhdx[76] = 0.0F;
  dhdx[79] = 0.0F;
  dhdx[82] = 0.0F;
  dhdx[2] = c_dhdx_tmp;
  dhdx[5] = (2.0F * x[1] * (x[15] - 9.81F) + 2.0F * x[0] * x[14]) - 2.0F * x[3] *
    x[13];
  dhdx[8] = (2.0F * x[2] * (x[15] - 9.81F) - 2.0F * x[3] * x[14]) - 2.0F * x[0] *
    x[13];
  dhdx[11] = i_dhdx_tmp;
  dhdx[14] = 0.0F;
  dhdx[17] = 0.0F;
  dhdx[20] = 0.0F;
  dhdx[23] = 0.0F;
  dhdx[26] = 0.0F;
  dhdx[29] = 0.0F;
  dhdx[32] = 0.0F;
  dhdx[35] = 0.0F;
  dhdx[38] = 0.0F;
  dhdx[41] = e_dhdx_tmp * x[2] - 2.0F * x[1] * x[3];
  dhdx[44] = b_dhdx_tmp * x[1] - 2.0F * x[2] * x[3];
  dhdx[47] = (d_dhdx_tmp + g_dhdx_tmp) - h_dhdx_tmp;
  dhdx[50] = 0.0F;
  dhdx[53] = 0.0F;
  dhdx[56] = 1.0F;
  dhdx[59] = 0.0F;
  dhdx[62] = 0.0F;
  dhdx[65] = 0.0F;
  dhdx[68] = 0.0F;
  dhdx[71] = 0.0F;
  dhdx[74] = 0.0F;
  dhdx[77] = 0.0F;
  dhdx[80] = 0.0F;
  dhdx[83] = 0.0F;
}

/*
 * Arguments    : c_fusion_internal_coder_insfilt *obj
 *                const float accel[3]
 * Return Type  : void
 */
void AsyncMARGGPSFuserBase_fuseaccel(c_fusion_internal_coder_insfilt *obj, const
  float accel[3])
{
  //float xk[28];
  float b_fv[3];
  float fv1[84];
  //memcpy(&xk[0], &obj->pState[0], 28U * sizeof(float));
  c_AsyncMARGGPSFuserBase_accelMe(obj->pState, b_fv);
  d_AsyncMARGGPSFuserBase_accelMe(obj->pState, fv1);
  b_IMUBasicEKF_correctEqn(obj->pState, obj->StateCovariance, b_fv, fv1, accel, fv);
  //memcpy(&obj->pState[0], & &obj->pState[0], 28U * sizeof(float));
}

/*
 * Arguments    : c_fusion_internal_coder_insfilt *obj
 *                const float gyro[3]
 * Return Type  : void
 */
void AsyncMARGGPSFuserBase_fusegyro(c_fusion_internal_coder_insfilt *obj, const
  float gyro[3])
{
  //float xk[28];
  float b_xk[3];
  static const double dv[84] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

  //memcpy(&xk[0], &obj->pState[0], 28U * sizeof(float));
  b_xk[0] = obj->pState[19] + obj->pState[4];
  b_xk[1] = obj->pState[20] + obj->pState[5];
  b_xk[2] = obj->pState[21] + obj->pState[6];
  c_IMUBasicEKF_correctEqn(obj->pState, obj->StateCovariance, b_xk, dv, gyro, fv);
 // memcpy(&obj->pState[0], &xk[0], 28U * sizeof(float));
}

/*
 * Arguments    : c_fusion_internal_coder_insfilt *obj
 *                const float mag[3]
 * Return Type  : void
 */
void AsyncMARGGPSFuserBase_fusemag(c_fusion_internal_coder_insfilt *obj, const
  float mag[3])
{
 // float xk[28];
  float mx_tmp;
  float b_mx_tmp;
  float dhdx_tmp;
  float b_dhdx_tmp;
  float c_dhdx_tmp;
  float d_dhdx_tmp;
  float e_dhdx_tmp;
  float xk_tmp;
  float b_xk_tmp;
  float c_xk_tmp;
  float d_xk_tmp;
  float e_xk_tmp;
  float b_xk[3];
  float f_xk_tmp;
  float g_xk_tmp;
  float h_xk_tmp;
  float i_xk_tmp;
  float j_xk_tmp;
  float f_dhdx_tmp[84];
  static const float measNoise[9] = { 1.06685019F, 0.0259414557F, 0.0883291587F,
    0.0259414557F, 1.04068804F, 0.0729869083F, 0.0883291587F, 0.0729869083F,
    1.07279086F };

 // memcpy(&xk[0], &obj->pState[0], 28U * sizeof(float));
  mx_tmp = 2.0F *  obj->pState[0];
  b_mx_tmp = 2.0F * obj->pState[1];
  dhdx_tmp = 2.0F *  obj->pState[24];
  b_dhdx_tmp = 2.0F *  obj->pState[23];
  c_dhdx_tmp = 2.0F *  obj->pState[22];
  d_dhdx_tmp = 2.0F *  obj->pState[1];
  e_dhdx_tmp = 2.0F *  obj->pState[0];
  xk_tmp =  obj->pState[0] *  obj->pState[0];
  b_xk_tmp =  obj->pState[1] *  obj->pState[1];
  c_xk_tmp =  obj->pState[2] *  obj->pState[2];
  d_xk_tmp =  obj->pState[3] *  obj->pState[3];
  e_xk_tmp = ((xk_tmp + b_xk_tmp) - c_xk_tmp) - d_xk_tmp;
  b_xk[0] = (( obj->pState[25] +  obj->pState[22] * e_xk_tmp) -  obj->pState[24] * (mx_tmp *  obj->pState[2] - b_mx_tmp *
               obj->pState[3])) +  obj->pState[23] * (mx_tmp *  obj->pState[3] + b_mx_tmp *  obj->pState[2]);
  xk_tmp -= b_xk_tmp;
  b_xk_tmp = 2.0F *  obj->pState[2] *  obj->pState[3];
  f_xk_tmp = 2.0F *  obj->pState[1] *  obj->pState[2];
  g_xk_tmp = 2.0F *  obj->pState[0] *  obj->pState[3];
  h_xk_tmp = (xk_tmp + c_xk_tmp) - d_xk_tmp;
  b_xk[1] = (( obj->pState[26] +  obj->pState[23] * h_xk_tmp) +  obj->pState[24] * (mx_tmp *  obj->pState[1] + b_xk_tmp))
    -  obj->pState[22] * (g_xk_tmp - f_xk_tmp);
  i_xk_tmp = 2.0F *  obj->pState[0] *  obj->pState[2] + 2.0F *  obj->pState[1] *  obj->pState[3];
  j_xk_tmp = 2.0F *  obj->pState[0] *  obj->pState[1];
  xk_tmp = (xk_tmp - c_xk_tmp) + d_xk_tmp;
  b_xk[2] = (( obj->pState[27] +  obj->pState[24] * xk_tmp) -  obj->pState[23] * (j_xk_tmp - b_xk_tmp)) + obj->pState[22] * i_xk_tmp;
  f_dhdx_tmp[0] = (b_dhdx_tmp *  obj->pState[3] - dhdx_tmp *  obj->pState[2]) + c_dhdx_tmp *  obj->pState[0];
  f_dhdx_tmp[3] = (dhdx_tmp *  obj->pState[3] + b_dhdx_tmp *  obj->pState[2]) + c_dhdx_tmp *  obj->pState[1];
  f_dhdx_tmp[6] = (b_dhdx_tmp *  obj->pState[1] - dhdx_tmp *  obj->pState[0]) - c_dhdx_tmp *  obj->pState[2];
  f_dhdx_tmp[9] = (dhdx_tmp *  obj->pState[1] + b_dhdx_tmp *  obj->pState[0]) - c_dhdx_tmp *  obj->pState[3];
  f_dhdx_tmp[12] = 0.0F;
  f_dhdx_tmp[15] = 0.0F;
  f_dhdx_tmp[18] = 0.0F;
  f_dhdx_tmp[21] = 0.0F;
  f_dhdx_tmp[24] = 0.0F;
  f_dhdx_tmp[27] = 0.0F;
  f_dhdx_tmp[30] = 0.0F;
  f_dhdx_tmp[33] = 0.0F;
  f_dhdx_tmp[36] = 0.0F;
  f_dhdx_tmp[39] = 0.0F;
  f_dhdx_tmp[42] = 0.0F;
  f_dhdx_tmp[45] = 0.0F;
  f_dhdx_tmp[48] = 0.0F;
  f_dhdx_tmp[51] = 0.0F;
  f_dhdx_tmp[54] = 0.0F;
  f_dhdx_tmp[57] = 0.0F;
  f_dhdx_tmp[60] = 0.0F;
  f_dhdx_tmp[63] = 0.0F;
  f_dhdx_tmp[66] = e_xk_tmp;
  f_dhdx_tmp[69] = e_dhdx_tmp *  obj->pState[3] + d_dhdx_tmp *  obj->pState[2];
  f_dhdx_tmp[72] = d_dhdx_tmp *  obj->pState[3] - e_dhdx_tmp *  obj->pState[2];
  f_dhdx_tmp[75] = 1.0F;
  f_dhdx_tmp[78] = 0.0F;
  f_dhdx_tmp[81] = 0.0F;
  f_dhdx_tmp[1] = (2.0F *  obj->pState[24] *  obj->pState[1] + 2.0F *  obj->pState[23] *  obj->pState[0]) - 2.0F *  obj->pState[22]
    *  obj->pState[3];
  b_mx_tmp = (2.0F *  obj->pState[24] *  obj->pState[0] - 2.0F *  obj->pState[23] *  obj->pState[1]) + 2.0F *  obj->pState[22] *
     obj->pState[2];
  f_dhdx_tmp[4] = b_mx_tmp;
  mx_tmp = (2.0F *  obj->pState[24] *  obj->pState[3] + 2.0F *  obj->pState[23] *  obj->pState[2]) + 2.0F *  obj->pState[22] * obj->pState[1];
  f_dhdx_tmp[7] = mx_tmp;
  f_dhdx_tmp[10] = (2.0F *  obj->pState[24] *  obj->pState[2] - 2.0F *  obj->pState[23] *  obj->pState[3]) - 2.0F * obj->pState[22] *  obj->pState[0];
  f_dhdx_tmp[13] = 0.0F;
  f_dhdx_tmp[16] = 0.0F;
  f_dhdx_tmp[19] = 0.0F;
  f_dhdx_tmp[22] = 0.0F;
  f_dhdx_tmp[25] = 0.0F;
  f_dhdx_tmp[28] = 0.0F;
  f_dhdx_tmp[31] = 0.0F;
  f_dhdx_tmp[34] = 0.0F;
  f_dhdx_tmp[37] = 0.0F;
  f_dhdx_tmp[40] = 0.0F;
  f_dhdx_tmp[43] = 0.0F;
  f_dhdx_tmp[46] = 0.0F;
  f_dhdx_tmp[49] = 0.0F;
  f_dhdx_tmp[52] = 0.0F;
  f_dhdx_tmp[55] = 0.0F;
  f_dhdx_tmp[58] = 0.0F;
  f_dhdx_tmp[61] = 0.0F;
  f_dhdx_tmp[64] = 0.0F;
  f_dhdx_tmp[67] = f_xk_tmp - g_xk_tmp;
  f_dhdx_tmp[70] = h_xk_tmp;
  f_dhdx_tmp[73] = e_dhdx_tmp *  obj->pState[1] + b_xk_tmp;
  f_dhdx_tmp[76] = 0.0F;
  f_dhdx_tmp[79] = 1.0F;
  f_dhdx_tmp[82] = 0.0F;
  f_dhdx_tmp[2] = b_mx_tmp;
  f_dhdx_tmp[5] = (2.0F *  obj->pState[22] *  obj->pState[3] - 2.0F *  obj->pState[23] *  obj->pState[0]) - 2.0F *  obj->pState[24]
    *  obj->pState[1];
  f_dhdx_tmp[8] = (2.0F *  obj->pState[23] *  obj->pState[3] - 2.0F *  obj->pState[24] *  obj->pState[2]) + 2.0F *  obj->pState[22]
    * obj->pState[0];
  f_dhdx_tmp[11] = mx_tmp;
  f_dhdx_tmp[14] = 0.0F;
  f_dhdx_tmp[17] = 0.0F;
  f_dhdx_tmp[20] = 0.0F;
  f_dhdx_tmp[23] = 0.0F;
  f_dhdx_tmp[26] = 0.0F;
  f_dhdx_tmp[29] = 0.0F;
  f_dhdx_tmp[32] = 0.0F;
  f_dhdx_tmp[35] = 0.0F;
  f_dhdx_tmp[38] = 0.0F;
  f_dhdx_tmp[41] = 0.0F;
  f_dhdx_tmp[44] = 0.0F;
  f_dhdx_tmp[47] = 0.0F;
  f_dhdx_tmp[50] = 0.0F;
  f_dhdx_tmp[53] = 0.0F;
  f_dhdx_tmp[56] = 0.0F;
  f_dhdx_tmp[59] = 0.0F;
  f_dhdx_tmp[62] = 0.0F;
  f_dhdx_tmp[65] = 0.0F;
  f_dhdx_tmp[68] = i_xk_tmp;
  f_dhdx_tmp[71] = b_xk_tmp - j_xk_tmp;
  f_dhdx_tmp[74] = xk_tmp;
  f_dhdx_tmp[77] = 0.0F;
  f_dhdx_tmp[80] = 0.0F;
  f_dhdx_tmp[83] = 1.0F;
  b_IMUBasicEKF_correctEqn(obj->pState, obj->StateCovariance, b_xk, f_dhdx_tmp, mag,
    measNoise);
 // memcpy(&obj->pState[0], &xk[0], 28U * sizeof(float));
}

/*
 * Arguments    : c_fusion_internal_coder_insfilt *obj
 * Return Type  : void
 */
void AsyncMARGGPSFuserBase_predict(c_fusion_internal_coder_insfilt *obj)
{
  float * xk = obj->pState;
  float v[28];
  int i;
  float dfdx_tmp;
  //float P[784];
  float b_dfdx_tmp;
  float addProcNoise[784];
  float c_dfdx_tmp;
  float d_dfdx_tmp;
  float e_dfdx_tmp;
  float f_dfdx_tmp;
  float g_dfdx_tmp;
  float h_dfdx_tmp;
  float i_dfdx_tmp;
  float j_dfdx_tmp;
  float dfdx[784];
  int j;
  int i1;
  float xnext[28];
  float Pdot[784];
 // float b_P[784];
  static const signed char iv[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  static const signed char iv1[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  static const signed char iv2[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  static const signed char iv3[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  int i2;
  static const signed char iv4[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  static const signed char iv5[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  int i3;
 // memcpy(&xk[0], &obj->pState[0], 28U * sizeof(float));
  v[0] = obj->QuaternionNoise[0];
  v[1] = obj->QuaternionNoise[1];
  v[2] = obj->QuaternionNoise[2];
  v[3] = obj->QuaternionNoise[3];
  v[4] = obj->AngularVelocityNoise[0];
  v[7] = obj->PositionNoise[0];
  v[10] = obj->VelocityNoise[0];
  v[13] = obj->AccelerationNoise[0];
  v[16] = obj->AccelerometerBiasNoise[0];
  v[19] = obj->GyroscopeBiasNoise[0];
  v[22] = obj->GeomagneticVectorNoise[0];
  v[25] = obj->MagnetometerBiasNoise[0];
  v[5] = obj->AngularVelocityNoise[1];
  v[8] = obj->PositionNoise[1];
  v[11] = obj->VelocityNoise[1];
  v[14] = obj->AccelerationNoise[1];
  v[17] = obj->AccelerometerBiasNoise[1];
  v[20] = obj->GyroscopeBiasNoise[1];
  v[23] = obj->GeomagneticVectorNoise[1];
  v[26] = obj->MagnetometerBiasNoise[1];
  v[6] = obj->AngularVelocityNoise[2];
  v[9] = obj->PositionNoise[2];
  v[12] = obj->VelocityNoise[2];
  v[15] = obj->AccelerationNoise[2];
  v[18] = obj->AccelerometerBiasNoise[2];
  v[21] = obj->GyroscopeBiasNoise[2];
  v[24] = obj->GeomagneticVectorNoise[2];
  v[27] = obj->MagnetometerBiasNoise[2];
  for (i = 0; i < 784; i++) {
//    P[i] = obj->StateCovariance[i];
    addProcNoise[i] = 0.0;
  }

  dfdx_tmp = -xk[5] / 2.0F;
  b_dfdx_tmp = -xk[3] / 2.0F;
  c_dfdx_tmp = -xk[6] / 2.0F;
  d_dfdx_tmp = xk[4] / 2.0F;
  e_dfdx_tmp = xk[0] / 2.0F;
  f_dfdx_tmp = -xk[1] / 2.0F;
  g_dfdx_tmp = xk[6] / 2.0F;
  h_dfdx_tmp = xk[5] / 2.0F;
  i_dfdx_tmp = -xk[4] / 2.0F;
  j_dfdx_tmp = -xk[2] / 2.0F;
  dfdx[0] = 0.0F;
  dfdx[28] = i_dfdx_tmp;
  dfdx[56] = dfdx_tmp;
  dfdx[84] = c_dfdx_tmp;
  dfdx[112] = f_dfdx_tmp;
  dfdx[140] = j_dfdx_tmp;
  dfdx[168] = b_dfdx_tmp;
  dfdx[196] = 0.0F;
  dfdx[224] = 0.0F;
  dfdx[252] = 0.0F;
  dfdx[280] = 0.0F;
  dfdx[308] = 0.0F;
  dfdx[336] = 0.0F;
  dfdx[364] = 0.0F;
  dfdx[392] = 0.0F;
  dfdx[420] = 0.0F;
  dfdx[448] = 0.0F;
  dfdx[476] = 0.0F;
  dfdx[504] = 0.0F;
  dfdx[532] = 0.0F;
  dfdx[560] = 0.0F;
  dfdx[588] = 0.0F;
  dfdx[616] = 0.0F;
  dfdx[644] = 0.0F;
  dfdx[672] = 0.0F;
  dfdx[700] = 0.0F;
  dfdx[728] = 0.0F;
  dfdx[756] = 0.0F;
  dfdx[1] = d_dfdx_tmp;
  dfdx[29] = 0.0F;
  dfdx[57] = g_dfdx_tmp;
  dfdx[85] = dfdx_tmp;
  dfdx[113] = e_dfdx_tmp;
  dfdx[141] = b_dfdx_tmp;
  dfdx[169] = xk[2] / 2.0F;
  dfdx[197] = 0.0F;
  dfdx[225] = 0.0F;
  dfdx[253] = 0.0F;
  dfdx[281] = 0.0F;
  dfdx[309] = 0.0F;
  dfdx[337] = 0.0F;
  dfdx[365] = 0.0F;
  dfdx[393] = 0.0F;
  dfdx[421] = 0.0F;
  dfdx[449] = 0.0F;
  dfdx[477] = 0.0F;
  dfdx[505] = 0.0F;
  dfdx[533] = 0.0F;
  dfdx[561] = 0.0F;
  dfdx[589] = 0.0F;
  dfdx[617] = 0.0F;
  dfdx[645] = 0.0F;
  dfdx[673] = 0.0F;
  dfdx[701] = 0.0F;
  dfdx[729] = 0.0F;
  dfdx[757] = 0.0F;
  dfdx[2] = h_dfdx_tmp;
  dfdx[30] = c_dfdx_tmp;
  dfdx[58] = 0.0F;
  dfdx[86] = d_dfdx_tmp;
  dfdx[114] = xk[3] / 2.0F;
  dfdx[142] = e_dfdx_tmp;
  dfdx[170] = f_dfdx_tmp;
  dfdx[198] = 0.0F;
  dfdx[226] = 0.0F;
  dfdx[254] = 0.0F;
  dfdx[282] = 0.0F;
  dfdx[310] = 0.0F;
  dfdx[338] = 0.0F;
  dfdx[366] = 0.0F;
  dfdx[394] = 0.0F;
  dfdx[422] = 0.0F;
  dfdx[450] = 0.0F;
  dfdx[478] = 0.0F;
  dfdx[506] = 0.0F;
  dfdx[534] = 0.0F;
  dfdx[562] = 0.0F;
  dfdx[590] = 0.0F;
  dfdx[618] = 0.0F;
  dfdx[646] = 0.0F;
  dfdx[674] = 0.0F;
  dfdx[702] = 0.0F;
  dfdx[730] = 0.0F;
  dfdx[758] = 0.0F;
  dfdx[3] = g_dfdx_tmp;
  dfdx[31] = h_dfdx_tmp;
  dfdx[59] = i_dfdx_tmp;
  dfdx[87] = 0.0F;
  dfdx[115] = j_dfdx_tmp;
  dfdx[143] = xk[1] / 2.0F;
  dfdx[171] = e_dfdx_tmp;
  dfdx[199] = 0.0F;
  dfdx[227] = 0.0F;
  dfdx[255] = 0.0F;
  dfdx[283] = 0.0F;
  dfdx[311] = 0.0F;
  dfdx[339] = 0.0F;
  dfdx[367] = 0.0F;
  dfdx[395] = 0.0F;
  dfdx[423] = 0.0F;
  dfdx[451] = 0.0F;
  dfdx[479] = 0.0F;
  dfdx[507] = 0.0F;
  dfdx[535] = 0.0F;
  dfdx[563] = 0.0F;
  dfdx[591] = 0.0F;
  dfdx[619] = 0.0F;
  dfdx[647] = 0.0F;
  dfdx[675] = 0.0F;
  dfdx[703] = 0.0F;
  dfdx[731] = 0.0F;
  dfdx[759] = 0.0F;
  for (j = 0; j < 28; j++) {
    addProcNoise[j + 28 * j] = v[j];
    dfdx[28 * j + 4] = 0.0F;
    dfdx[28 * j + 5] = 0.0F;
    dfdx[28 * j + 6] = 0.0F;
    dfdx[28 * j + 7] = iv[j];
    dfdx[28 * j + 8] = iv1[j];
    dfdx[28 * j + 9] = iv2[j];
    dfdx[28 * j + 10] = iv3[j];
    dfdx[28 * j + 11] = iv4[j];
    dfdx[28 * j + 12] = iv5[j];
    dfdx[28 * j + 13] = 0.0F;
    dfdx[28 * j + 14] = 0.0F;
    dfdx[28 * j + 15] = 0.0F;
    dfdx[28 * j + 16] = 0.0F;
    dfdx[28 * j + 17] = 0.0F;
    dfdx[28 * j + 18] = 0.0F;
    dfdx[28 * j + 19] = 0.0F;
    dfdx[28 * j + 20] = 0.0F;
    dfdx[28 * j + 21] = 0.0F;
    dfdx[28 * j + 22] = 0.0F;
    dfdx[28 * j + 23] = 0.0F;
    dfdx[28 * j + 24] = 0.0F;
    dfdx[28 * j + 25] = 0.0F;
    dfdx[28 * j + 26] = 0.0F;
    dfdx[28 * j + 27] = 0.0F;
  }

  for (i = 0; i < 28; i++) {
    for (i1 = 0; i1 < 28; i1++) {
      j = i + 28 * i1;
//      b_P[j] = 0.0F;
      dfdx_tmp = 0.0F;
      b_dfdx_tmp = 0.0F;
      for (i2 = 0; i2 < 28; i2++) {
        i3 = i + 28 * i2;
        dfdx_tmp += dfdx[i3] * obj->StateCovariance[i2 + 28 * i1];
        b_dfdx_tmp += obj->StateCovariance[i3] * dfdx[i1 + 28 * i2];
      }

    //  b_P[j] = b_dfdx_tmp;
      //Pdot[j] = dfdx_tmp ;
			Pdot[j] = dfdx_tmp + b_dfdx_tmp;
    }
  }

  for (i = 0; i < 784; i++) {
   // Pdot[i] = (Pdot[i] + b_P[i]) + (float)addProcNoise[i];
		 Pdot[i] = Pdot[i] + (float)addProcNoise[i];
  }

  xnext[0] = xk[0] + ((-(xk[1] * xk[4]) / 2.0F - xk[2] * xk[5] / 2.0F) - xk[3] *
                      xk[6] / 2.0F) * 0.1F;
  xnext[1] = xk[1] + ((xk[0] * xk[4] / 2.0F - xk[3] * xk[5] / 2.0F) + xk[2] *
                      xk[6] / 2.0F) * 0.1F;
  xnext[2] = xk[2] + ((xk[3] * xk[4] / 2.0F + xk[0] * xk[5] / 2.0F) - xk[1] *
                      xk[6] / 2.0F) * 0.1F;
  xnext[3] = xk[3] + ((xk[1] * xk[5] / 2.0F - xk[2] * xk[4] / 2.0F) + xk[0] *
                      xk[6] / 2.0F) * 0.1F;
  xnext[4] = xk[4];
  xnext[5] = xk[5];
  xnext[6] = xk[6];
  xnext[7] = xk[7] + xk[10] * 0.1F;
  xnext[8] = xk[8] + xk[11] * 0.1F;
  xnext[9] = xk[9] + xk[12] * 0.1F;
  xnext[10] = xk[10] + xk[13] * 0.1F;
  xnext[11] = xk[11] + xk[14] * 0.1F;
  xnext[12] = xk[12] + xk[15] * 0.1F;
  xnext[13] = xk[13];
  xnext[14] = xk[14];
  xnext[15] = xk[15];
  xnext[16] = xk[16];
  xnext[17] = xk[17];
  xnext[18] = xk[18];
  xnext[19] = xk[19];
  xnext[20] = xk[20];
  xnext[21] = xk[21];
  xnext[22] = xk[22];
  xnext[23] = xk[23];
  xnext[24] = xk[24];
  xnext[25] = xk[25];
  xnext[26] = xk[26];
  xnext[27] = xk[27];
  IMUBasicEKF_repairQuaternion(xnext);
  for (j = 0; j < 28; j++) {
    for (i = 0; i < 28; i++) {
      i1 = i + 28 * j;
      obj->StateCovariance[i1] = obj->StateCovariance[i1] + 0.5F * (Pdot[i1] + Pdot[j + 28 * i]) *
        0.1F;
    }

    obj->pState[j] = xnext[j];
  }
}

/*
 * Arguments    : c_fusion_internal_coder_insfilt *obj
 *                const float z[6]
 *                const float measNoise[36]
 *                float innov[6]
 * Return Type  : void
 */
void c_AsyncMARGGPSFuserBase_basicCo(c_fusion_internal_coder_insfilt *obj, const
  float z[6], const float measNoise[36], float innov[6])
{
  float * xk = obj->pState;
  float h[6];
  int i;
  static const double dv[168] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

 // memcpy(&xk[0], &obj->pState[0], 28U * sizeof(float));
  h[0] = xk[7];
  h[1] = xk[8];
  h[2] = xk[9];
  h[3] = xk[10];
  h[4] = xk[11];
  h[5] = xk[12];
  for (i = 0; i < 6; i++) {
    innov[i] = z[i] - h[i];
  }

  IMUBasicEKF_correctEqn(xk, obj->StateCovariance, h, dv, z, measNoise);
 // memcpy(&obj->pState[0], &xk[0], 28U * sizeof(float));
}

/*
 * File trailer for AsyncMARGGPSFuserBase.c
 *
 * [EOF]
 */
