/*
 * File: magnecal.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 21-Oct-2019 12:58:03
 */

/* Include Files */
#include "magnecal.h"
#include "calcularAceleracion.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "mldivide.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */

/*
 * Calibracion del magnetometro usando una matriz diagonal
 * Arguments    : const double d[30000]
 *                double A[9]
 *                double b[3]
 * Return Type  : void
 */
void magnecal(const double d[30000], double A[9], double b[3])
{
  int i;
  static double b_d[40000];
  double c_d[10000];
  double b_dv[4];
  double d_tmp;
  double b_d_tmp;

  /*  R is the identity */
  memset(&A[0], 0, 9U * sizeof(double));
  A[0] = 1.0;
  A[4] = 1.0;
  A[8] = 1.0;
  for (i = 0; i < 10000; i++) {
    b_d[i] = d[i];
    d_tmp = d[i + 10000];
    b_d[i + 10000] = d_tmp;
    b_d_tmp = d[i + 20000];
    b_d[i + 20000] = b_d_tmp;
    b_d[i + 30000] = 1.0;
    c_d[i] = (d[i] * d[i] + d_tmp * d_tmp) + b_d_tmp * b_d_tmp;
  }

  mldivide(b_d, c_d, b_dv);
  b[0] = 0.5 * b_dv[0];
  b[1] = 0.5 * b_dv[1];
  b[2] = 0.5 * b_dv[2];

  /*  make a row vector  */
}

/*
 * File trailer for magnecal.c
 *
 * [EOF]
 */
