/*
 * File: IMUBasicEKF.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 28-Oct-2019 20:13:05
 */

#ifndef IMUBASICEKF_H
#define IMUBASICEKF_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calcularAceleracion_types.h"

/* Function Declarations */
extern void IMUBasicEKF_correctEqn(float x[28], float P[784], const float h[6],
  const double H[168], const float z[6], const float R[36]);
extern void IMUBasicEKF_repairQuaternion(float x[28]);
extern void b_IMUBasicEKF_correctEqn(float x[28], float P[784], const float h[3],
  const float H[84], const float z[3], const float R[9]);
extern void c_IMUBasicEKF_correctEqn(float x[28], float P[784], const float h[3],
  const double H[84], const float z[3], const float R[9]);

#endif

/*
 * File trailer for IMUBasicEKF.h
 *
 * [EOF]
 */
