/*
 * File: filtroCoder.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "filtroCoder.h"
#include "AsyncMARGGPSFuserBase.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include <math.h>
#include <string.h>
#include <stdint.h>
/* Function Definitions */

/*
 * $codegen
 * Arguments    : struct2_T *info
 *                const float accel[3]
 *                const float gyro[3]
 *                const float mag[3]
 *                const double lla[3]
 *                float VGPS
 *                const double ubicacionReferencia[3]
 *                bool evaluarGPS
 *                bool evaluarIMU
 *                struct3_T *salida
 * Return Type  : void
 */
 uint8_t filter_coder_debug=0;
 
//void filtroCoder(c_fusion_internal_coder_insfilt * f, const float accel[3], const float gyro[3],
//                 const float mag[3], const double lla[3], float VGPS, const
//                 double ubicacionReferencia[3], bool evaluarGPS, bool evaluarIMU,
//                 struct3_T *salida)
//{
//   

//	
//  int i;
//  float varargin_18[28];
//  static const float b_fv[28] = { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 27.555, -2.4169, -16.0849, 0.0, 0.0, 0.0 };

//  static const float val[784] = { 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
//    0.0, 0.0, 0.0, 0.0, 0.001 };

//  double v_tmp;
//  double b_v_tmp;
//  double c_v_tmp;
//  double xRN;
//  double yRE;
//  float gpsvel[3];
//  float norma;
//  float z1_idx_0;

//  /*  function [p, cuaternion, velocidad, estados, covarianzaEstados,... */
//  /*      posAnterior] = filtroCoder(accel, gyro, mag, lla, estados, covarianzaEstados,... */
//  /*      VGPS, C, ubicacionReferencia, posAnterior, evaluarGPS, evaluarIMU)  %$codegen */
//  /*  function [salida, estados, covarianzaEstados,... */
//  /*      posAnterior] = filtroCoder(estados, covarianzaEstados, posAnterior,accel, gyro, mag, lla, ... */
//  /*      VGPS, C, ubicacionReferencia, evaluarGPS, evaluarIMU)  %$codegen */
//  /* Inicializamos el filtro con los parametros cargados */
////  memcpy(&f->pState[0], &info->estados[0], 28U * sizeof(float));
// 	/*
//  for (i = 0; i < 784; i++) {
//    f->StateCovariance[i] = info->covarianzaEstados[i];
//  }*/
//		
//filter_coder_debug++;
////  f->pState[24] = info->estados[24];
// // f->pState[23] = info->estados[23];
// // f->pState[22] = info->estados[22];
// /*
//  salida->p.size[0] = 3;
//  salida->p.size[1] = 1;
//  salida->p.data[0] = 0.0;
//  salida->p.data[1] = 0.0;
//  salida->p.data[2] = 0.0;
//  salida->cuaternion.a.size[0] = 0;
//  salida->cuaternion.a.size[1] = 0;
//  salida->cuaternion.b.size[0] = 0;
//  salida->cuaternion.b.size[1] = 0;
//  salida->cuaternion.c.size[0] = 0;
//  salida->cuaternion.c.size[1] = 0;
//  salida->cuaternion.d.size[0] = 0;
//  salida->cuaternion.d.size[1] = 0;
//*/
//  /* salida.posAnterior = info.posAnterior; */
//	//evaluarGPS=false;
//  if (evaluarGPS) {
//    /*  Fusiono el GPS una vez por segundo */
//    /* Antes de evaluar el GPS, reseteo el filtro */
//    /* aux = info.estados(1:13); */

//    memcpy(&f->StateCovariance[0], &val[0], 784U * sizeof(float));
//		
//   // memcpy(&varargin_18[0], &f->pState[0], 28U * sizeof(float));
//		memset(varargin_18,0,sizeof(varargin_18));
//    varargin_18[24] = -16.0849;
//    varargin_18[23] = f->pState[23];
//    varargin_18[22] = f->pState[22];
//		
//    for (i = 0; i < 13; i++) {
//      varargin_18[i] = f->pState[i];
//    }
//   
//		memcpy(&f->pState[0], &b_fv[0], 28U * sizeof(float));

//		
//    memcpy(&f->pState[0], &varargin_18[0], 28U * sizeof(float));

//    /* Calculamos la velocidad a mano como redundancia para la estimacion del */
//    /* GPS */
//    /*  ELL2XYZ  Convierte coordinadas elipsoideas a cartesianas */
//    /*    Vectorized. */
//    /*  Uso:     [x,y,z]=ell2xyz(lat,lon,h,a,e2) */
//    /*           [x,y,z]=ell2xyz(lat,lon,h) */
//    /*  Entradas: lat - latitudes (radianes) */
//    /*            lon - longitudes (radianes) */
//    /*            h   - alturas (m) */
//    /*            a   - semi-eje mayor del elipsoide (m); default wgs84 */
//    /*            e2  - excentricidad cuadratica del elipsoide; default wgs84 */
//    /*  Salida:   x \ */
//    /*            y  > vectores en coordenadas cartesianas (m) */
//    /*            z / */
//    /*  REFELL  Calcula parametros del elipsoide de referencia */
//    /*  Uso:     [a,b,e2,finv]=refell(type) */
//    /*  Entrada: type - tipo de elipsoide de referencia (char) */
//    /*                  CLK66 = Clarke 1866 */
//    /*                  GRS67 = Geodetic Reference System 1967 */
//    /*                  GRS80 = Geodetic Reference System 1980 */
//    /*                  WGS72 = World Geodetic System 1972 */
//    /*                  WGS84 = World Geodetic System 1984 */
//    /*                  ATS77 = Quasi-earth centred ellipsoid for ATS77 */
//    /*                  NAD27 = North American Datum 1927 (=CLK66) */
//    /*                  NAD83 = North American Datum 1927 (=GRS80) */
//    /*                  INTER = International */
//    /*                  KRASS = Krassovsky (USSR) */
//    /*                  MAIRY = Modified Airy (Ireland 1965/1975) */
//    /*                  TOPEX = TOPEX/POSEIDON ellipsoid */
//    /*  Salida:  a    - semi-eje mayor (m) */
//    /*           b    - semi-eje menor (m) */
//    /*           e2   - cuadrado de la excentricidad */
//    /*           finv - inversa del achatamiento */
//    v_tmp = sin(lla[0]);

//    /*  ELL2XYZ  Convierte coordinadas elipsoideas a cartesianas */
//    /*    Vectorized. */
//    /*  Uso:     [x,y,z]=ell2xyz(lat,lon,h,a,e2) */
//    /*           [x,y,z]=ell2xyz(lat,lon,h) */
//    /*  Entradas: lat - latitudes (radianes) */
//    /*            lon - longitudes (radianes) */
//    /*            h   - alturas (m) */
//    /*            a   - semi-eje mayor del elipsoide (m); default wgs84 */
//    /*            e2  - excentricidad cuadratica del elipsoide; default wgs84 */
//    /*  Salida:   x \ */
//    /*            y  > vectores en coordenadas cartesianas (m) */
//    /*            z / */
//    /*  REFELL  Calcula parametros del elipsoide de referencia */
//    /*  Uso:     [a,b,e2,finv]=refell(type) */
//    /*  Entrada: type - tipo de elipsoide de referencia (char) */
//    /*                  CLK66 = Clarke 1866 */
//    /*                  GRS67 = Geodetic Reference System 1967 */
//    /*                  GRS80 = Geodetic Reference System 1980 */
//    /*                  WGS72 = World Geodetic System 1972 */
//    /*                  WGS84 = World Geodetic System 1984 */
//    /*                  ATS77 = Quasi-earth centred ellipsoid for ATS77 */
//    /*                  NAD27 = North American Datum 1927 (=CLK66) */
//    /*                  NAD83 = North American Datum 1927 (=GRS80) */
//    /*                  INTER = International */
//    /*                  KRASS = Krassovsky (USSR) */
//    /*                  MAIRY = Modified Airy (Ireland 1965/1975) */
//    /*                  TOPEX = TOPEX/POSEIDON ellipsoid */
//    /*  Salida:  a    - semi-eje mayor (m) */
//    /*           b    - semi-eje menor (m) */
//    /*           e2   - cuadrado de la excentricidad */
//    /*           finv - inversa del achatamiento */
//    b_v_tmp = sin(f->posAnterior[0]);

//    /*  ELL2XYZ  Convierte coordinadas elipsoideas a cartesianas */
//    /*    Vectorized. */
//    /*  Uso:     [x,y,z]=ell2xyz(lat,lon,h,a,e2) */
//    /*           [x,y,z]=ell2xyz(lat,lon,h) */
//    /*  Entradas: lat - latitudes (radianes) */
//    /*            lon - longitudes (radianes) */
//    /*            h   - alturas (m) */
//    /*            a   - semi-eje mayor del elipsoide (m); default wgs84 */
//    /*            e2  - excentricidad cuadratica del elipsoide; default wgs84 */
//    /*  Salida:   x \ */
//    /*            y  > vectores en coordenadas cartesianas (m) */
//    /*            z / */
//    /*  REFELL  Calcula parametros del elipsoide de referencia */
//    /*  Uso:     [a,b,e2,finv]=refell(type) */
//    /*  Entrada: type - tipo de elipsoide de referencia (char) */
//    /*                  CLK66 = Clarke 1866 */
//    /*                  GRS67 = Geodetic Reference System 1967 */
//    /*                  GRS80 = Geodetic Reference System 1980 */
//    /*                  WGS72 = World Geodetic System 1972 */
//    /*                  WGS84 = World Geodetic System 1984 */
//    /*                  ATS77 = Quasi-earth centred ellipsoid for ATS77 */
//    /*                  NAD27 = North American Datum 1927 (=CLK66) */
//    /*                  NAD83 = North American Datum 1927 (=GRS80) */
//    /*                  INTER = International */
//    /*                  KRASS = Krassovsky (USSR) */
//    /*                  MAIRY = Modified Airy (Ireland 1965/1975) */
//    /*                  TOPEX = TOPEX/POSEIDON ellipsoid */
//    /*  Salida:  a    - semi-eje mayor (m) */
//    /*           b    - semi-eje menor (m) */
//    /*           e2   - cuadrado de la excentricidad */
//    /*           finv - inversa del achatamiento */
//    c_v_tmp = sin(ubicacionReferencia[0]);
//    c_v_tmp = (6.378137E+6 / sqrt(1.0 - 0.00669437999014133 * c_v_tmp * c_v_tmp)
//               + ubicacionReferencia[2]) * cos(ubicacionReferencia[0]);
//    xRN = c_v_tmp * cos(ubicacionReferencia[1]);
//    yRE = c_v_tmp * sin(ubicacionReferencia[1]);

//    /* Por ahora, voy a hacer que se quede con el minimo entre la */
//    /* velocidad dada por el sistema y la que calculamos aqui. El */
//    /* vector resultante es el vector diferencia. */
//    b_v_tmp = (6.378137E+6 / sqrt(1.0 - 0.00669437999014133 * b_v_tmp * b_v_tmp)
//               + f->posAnterior[2]) * cos(f->posAnterior[0]);
//    c_v_tmp = (6.378137E+6 / sqrt(1.0 - 0.00669437999014133 * v_tmp * v_tmp) +
//               lla[2]) * cos(lla[0]);
//    gpsvel[0] = (float)(b_v_tmp * cos(f->posAnterior[1]) - xRN) - (float)
//      (c_v_tmp * cos(lla[1]) - xRN);
//    gpsvel[1] = (float)(b_v_tmp * sin(f->posAnterior[1]) - yRE) - (float)
//      (c_v_tmp * sin(lla[1]) - yRE);
//    gpsvel[2] = 0.0;
//    norma = fabsf(gpsvel[0]);
//    z1_idx_0 = norma * norma;
//    norma = fabsf(gpsvel[1]);
//    norma = sqrtf(z1_idx_0 + norma * norma);
//    if (norma > 0.0) {
//      if (0.0 < VGPS) {
//        z1_idx_0 = VGPS;
//      } else {
//        z1_idx_0 = 0.0;
//      }

//      z1_idx_0 = fminf(norma, z1_idx_0);
//      gpsvel[0] = gpsvel[0] / norma * z1_idx_0;
//      gpsvel[1] = gpsvel[1] / norma * z1_idx_0;
//      gpsvel[2] = 0.0;
//    }

//    /* usionamos el GPS */
//    AsyncMARGGPSFuserBase_fusegps(f, lla, gpsvel);

//    /* Indicamos que el proximo step no lleva GPS */
//    /*    salida.posAnterior = lla; */
//    memcpy(&varargin_18[0], &f->pState[0], 28U * sizeof(float));
//    salida->cuaternion.a.size[0] = 1;
//    salida->cuaternion.a.size[1] = 1;
//    salida->cuaternion.a.data[0] = varargin_18[0];
//    salida->cuaternion.b.size[0] = 1;
//    salida->cuaternion.b.size[1] = 1;
//    salida->cuaternion.b.data[0] = varargin_18[1];
//    salida->cuaternion.c.size[0] = 1;
//    salida->cuaternion.c.size[1] = 1;
//    salida->cuaternion.c.data[0] = varargin_18[2];
//    salida->cuaternion.d.size[0] = 1;
//    salida->cuaternion.d.size[1] = 1;
//    salida->cuaternion.d.data[0] = varargin_18[3];
//    memcpy(&varargin_18[0], &f->pState[0], 28U * sizeof(float));
//    salida->p.size[0] = 1;
//    salida->p.size[1] = 3;
//    salida->p.data[0] = varargin_18[7];
//    salida->p.data[1] = varargin_18[8];
//    salida->p.data[2] = varargin_18[9];
//  }

//  if (evaluarIMU) {
//    /* Cargamos la informacion de la IMU */
//    AsyncMARGGPSFuserBase_predict(f);

//    /* Integramos el giroscopo, acelerometro y magnetometro */
//		AsyncMARGGPSFuserBase_fuseaccel(f, accel);
//		AsyncMARGGPSFuserBase_fusegyro(f, gyro);
//		AsyncMARGGPSFuserBase_fusemag(f, mag);
//  }

//  memcpy(&varargin_18[0], &f->pState[0], 28U * sizeof(float));
//  salida->velocidad[0] = varargin_18[10];
//  salida->velocidad[1] = varargin_18[11];
//  salida->velocidad[2] = varargin_18[12];

//  /* estados = f->State; */
//  /* covarianzaEstados = f->StateCovariance; */
// // memcpy(&info->covarianzaEstados[0], &f->StateCovariance[0], 784U * sizeof(float));
// // memcpy(&info->estados[0], &f->pState[0], 28U * sizeof(float));
//  //info->estados[24] = f->pState[24];
//  //info->estados[23] = f->pState[23];
//  //info->estados[22] = f->pState[22];
//}
void filtroCoder(c_fusion_internal_coder_insfilt * f, const float accel[3], const float gyro[3],
	const float mag[3], const double lla[3],const float VGPS[3], const	double ubicacionReferencia[3], 
		const float covarianza[3], const bool evaluarGPS, const bool evaluarIMU, float dt,	struct3_T *salida)
	/*
	Informacion nueva
	1) Velocidad
		VGPS tiene que ser una nueva variable de 3 campos. vx, vy, vz. Esas se obtienen a partir de las variables
			*velN
			*velE
			*velD
		que devuelve la estructura. (Recordar que las unidades son en mm/s, por lo que debemos escalarlas aqu� o antes de entrar.)

		VGPS = [velN, velE, velD];

	Lo mejor es devolverla en el buffer, como venimos haciendo con las otras variables, as� podemos hacerle un suavizado suave.

	2) Error de precisi�n
		Esta variable no estaba definida antes, pues yo hice una calibraci�n interna para los par�metros que regulan el error de precisi�n. La ventaja
		de esto es que permite hacer mejores estimaciones y que la incertidumbre obtenida no depende de la calibraci�n a priori, sino que el GPS nos da informaci�n
		en tiempo real de la incertidumbre (sobre la calibraci�n que se hizo antes). Esto deber�a mejorar el rendimiento general.
		Esto require un cambio m�s profundo en el filtro, porque tenemos que pasar la matriz de covarianza del gps por fuera. Pero tampoco cambia demasiado.
		Para esto creamos una nueva variable que llamaremos covarianza. En principio es un vector que debe tener la siguiente informaci�n:

			*aAcc		-Error de posicionamiento horizontal
			*vAcc		-Error de posicionamiento vertical
			*sAcc		-Error de estimaci�n de velocidad

		Con esto armamos las matrices de covarianzas necesarias para la estimaci�n. (Las unidades son de mm, por lo que la estimaci�n es para una variable uniforme
		la vamos a convertir a gaussiana, esto lo tenemos que estudiar).

		covarianza = [aAcc, vAcc, sAcc];

	3) El height que hay que usar es

			*height		-Height above ellipsoid

	   Esta variable debe venir en lla, es decir, ahora es
	   lla = [lat, lon, height];

	4) El valor de PDOP lo podemos usar para estabilizar el GPS. El valor m�ximo que deber�amos usar es de 10. Esto podr�a hacerse antes.	

	5) Ya que estamos, pod�s devolver, si quer�s, la informaci�n de la aceleraci�n y la velocidad angular. Eso ser�a equivalente a guardar en la variable salida:

		*orientacion      = f->pState[0:3]
		*posicion         = f->pState[7:9]
		*velocidadAngular = f->pState[4:6]
		*velocidad        = f->pState[10:12]
		*aceleracion      = f->pState[13:15]  
     *
     *Nota final: voy a asumir que todas las variables nuevas vienen escaladas a unidades normales. Recordemos que las unidades de trabajo son:
     *latitud y longitud en grados
     *altitud en m
     *velocidad en m/s
     *aceleracion en m/s^2
     *magnetometro en uT
     *velocidad de giro en grados/s
     
     *Con esto en mente, los valores del error deben convertirse a estas unidades.
	*/

{
	int i;


	static const float val[784] = { 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	  0.0, 0.0, 0.0, 0.0, 0.001 };

	float gpsvel[3];

	f->ReferenceLocation[0] = ubicacionReferencia[0];
	f->ReferenceLocation[1] = ubicacionReferencia[1];
	f->ReferenceLocation[2] = ubicacionReferencia[2];
	filter_coder_debug++;
	
		if (evaluarIMU) 
	{
		/* Cargamos la informacion de la IMU */
		AsyncMARGGPSFuserBase_predict(f);       /*Depende de dt, si llega a cambiar dt de la IMU, hay que cambiarlo*/

		/* Integramos el giroscopo, acelerometro y magnetometro */
		AsyncMARGGPSFuserBase_fuseaccel(f, accel);
		AsyncMARGGPSFuserBase_fusegyro(f, gyro);
		AsyncMARGGPSFuserBase_fusemag(f, mag);
	}

	if (evaluarGPS)
	{
		/*  Fusiono el GPS una vez por segundo */
		/* Antes de evaluar el GPS, reseteo el filtro */

        /*Reseteo de la matriz de covarianza*/
		memcpy(&f->StateCovariance[0], &val[0], 784U * sizeof(float));

		/*Reseteo del vector de estados*/
		for (i = 4; i < 22; i++) {
			f->pState[i] = 0;
		}
		/*El vector geomagnetico se reestablece al valor por defecto (TODO: ver si eventualmente hay que cambiar esto)*/
		f->pState[22] = 27.555;
		f->pState[23] = -2.4169;
		f->pState[24] = -16.0849;

		/*El sesgo del magnetometro se reinicia*/
		for (i = 25; i < 28; i++)
		{
			f->pState[i] = 0;
		}

		/* Fusionamos el GPS */
        /*Con este cambio, deber�amos pasar directamente la velocidad del GPS*/
		AsyncMARGGPSFuserBase_fusegps(f, lla, VGPS, covarianza);

		/* Cargamos el cuaternion en la estructura de salida*/
		salida->cuaternion.a.size[0] = 1;
		salida->cuaternion.a.size[1] = 1;
		salida->cuaternion.a.data[0] = f->pState[0];
		salida->cuaternion.b.size[0] = 1;
		salida->cuaternion.b.size[1] = 1;
		salida->cuaternion.b.data[0] = f->pState[1];
		salida->cuaternion.c.size[0] = 1;
		salida->cuaternion.c.size[1] = 1;
		salida->cuaternion.c.data[0] = f->pState[2];
		salida->cuaternion.d.size[0] = 1;
		salida->cuaternion.d.size[1] = 1;
		salida->cuaternion.d.data[0] = f->pState[3];

		/*Cargamos la posicion en la estructura salida*/


	}


	/*Cargamos la velocidad (siempre carga la velocidad, con IMU y con GPS)*/
	salida->p[0] = f->pState[7];
	salida->p[1] = f->pState[8];
	salida->p[2] = f->pState[9];
	
	salida->velocidad[0] = f->pState[10];
	salida->velocidad[1] = f->pState[11];
	salida->velocidad[2] = f->pState[12];
	
	salida->aceleracion[0] =  f->pState[13];
	salida->aceleracion[1] =  f->pState[14];
	salida->aceleracion[2] =  f->pState[15];

}
/*
 * File trailer for filtroCoder.c
 *
 * [EOF]
 */
