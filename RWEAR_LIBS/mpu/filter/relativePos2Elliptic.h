/*
 * File: relativePos2Elliptic.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 03-Dec-2019 18:21:30
 */

#ifndef RELATIVEPOS2ELLIPTIC_H
#define RELATIVEPOS2ELLIPTIC_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
//#include "relativePos2Elliptic_types.h"

/* Function Declarations */
extern void relativePos2Elliptic(double x, double y, double z, const double
  ubicacionReferencia[3], double *lat, double *lon, double *h);

#endif

/*
 * File trailer for relativePos2Elliptic.h
 *
 * [EOF]
 */
