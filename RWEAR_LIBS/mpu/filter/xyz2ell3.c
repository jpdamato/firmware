/*
 * File: xyz2ell3.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 05-Nov-2019 00:26:40
 */

/* Include Files */
#include "xyz2ell3.h"
#include "quaternion2Eul.h"
#include "rt_nonfinite.h"
#include "xyz2ell3_data.h"
#include "xyz2ell3_initialize.h"
#include "xyz2ell3_rtwutil.h"
#include <math.h>

/* Function Definitions */

/*
 * XYZ2ELL3  Converts cartesian coordinates to ellipsoidal.
 *    Uses direct algorithm in B.R. Bowring, "The accuracy of
 *    geodetic latitude and height equations", Survey
 *    Review, v28 #218, October 1985, pp.202-206.  Vectorized.
 * Arguments    : float X
 *                float Y
 *                float Z
 *                const char type[3]
 *                float *lat
 *                float *lon
 *                float *h
 * Return Type  : void
 */
void xyz2ell3(double X, double Y, double Z, const char type[3], float *lat, double
              *lon, double *h)
{
  float p;
  float u;
  bool b_bool;
  int kstr;
  int exitg1;
  static const char cv[128] = { '\x00', '\x01', '\x02', '\x03', '\x04', '\x05',
    '\x06', '\x07', '\x08', '	', '\x0a', '\x0b', '\x0c', '\x0d', '\x0e', '\x0f',
    '\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17', '\x18',
    '\x19', '\x1a', '\x1b', '\x1c', '\x1d', '\x1e', '\x1f', ' ', '!', '\"', '#',
    '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2',
    '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'a',
    'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
    'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '[', '\\', ']', '^', '_',
    '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}',
    '~', '\x7f' };

  static const char cv1[3] = { 'd', 'e', 'g' };


  /*  Usage:   [lat,lon,h]=xyz2ell3(X,Y,Z,type) */
  /*           [lat,lon,h]=xyz2ell3(X,Y,Z) */
  /*  Input:   X \ */
  /*           Y  > vectors of cartesian coordinates in CT system (m) */
  /*           Z / */
  /*           type = 'rad' or type = 'deg' */
  /*  Output:  lat - vector of ellipsoidal latitudes (radians) */
  /*           lon - vector of ellipsoidal longitudes (radians) */
  /*           h   - vector of ellipsoidal heights (m) */
  *lon = rt_atan2f_snf(Y, X);
  p = sqrtf(X * X + Y * Y);
  u = atanf(6.3567525E+6F * Z * (42841.3125F / sqrtf(p * p + Z * Z) + 1.0F) /
            (6.378137E+6F * p));
  *lat = atanf((Z + 42841.3125F * powf(sinf(u), 3.0F)) / (p - 42697.6719F * powf
    (cosf(u), 3.0F)));
  u = sinf(*lat);
  *h = (p * cosf(*lat) + Z * u) - 4.068063E+13F / (6.378137E+6F / sqrtf(1.0F -
    0.00669438F * (u * u)));

  /* Height from center of the earth (I think...) */
  b_bool = false;
  kstr = 0;
  do {
    exitg1 = 0;
    if (kstr < 3) {
      if (cv[(unsigned char)type[kstr] & 127] != cv[(int)cv1[kstr]]) {
        exitg1 = 1;
      } else {
        kstr++;
      }
    } else {
      b_bool = true;
      exitg1 = 1;
    }
  } while (exitg1 == 0);

  if (b_bool) {
    /*  RAD2DEG  Converts radians to decimal degrees. Vectorized. */
    /*  Version: 8 Mar 00 */
    /*  Usage:   deg=rad2deg(rad) */
    /*  Input:   rad - vector of angles in radians */
    /*  Output:  deg - vector of angles in decimal degrees */
    /*  Copyright (c) 2011, Michael R. Craymer */
    /*  All rights reserved. */
    /*  Email: mike@craymer.com */
    *lat = *lat * 180.0F / 3.14159274F;

    /* ind=(deg<0); */
    /* deg(ind)=deg(ind)+360; */
    /*  RAD2DEG  Converts radians to decimal degrees. Vectorized. */
    /*  Version: 8 Mar 00 */
    /*  Usage:   deg=rad2deg(rad) */
    /*  Input:   rad - vector of angles in radians */
    /*  Output:  deg - vector of angles in decimal degrees */
    /*  Copyright (c) 2011, Michael R. Craymer */
    /*  All rights reserved. */
    /*  Email: mike@craymer.com */
    *lon = *lon * 180.0F / 3.14159274F;

    /* ind=(deg<0); */
    /* deg(ind)=deg(ind)+360; */
  }
}

/*
 * File trailer for xyz2ell3.c
 *
 * [EOF]
 */
