/*
 * File: xyz2ell3.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 05-Nov-2019 00:26:40
 */

#ifndef XYZ2ELL3_H
#define XYZ2ELL3_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "xyz2ell3_types.h"

/* Function Declarations */
extern void xyz2ell3(double X, double Y, double Z, const char type[3], double *lat,
                     double *lon, float *h);

#endif

/*
 * File trailer for xyz2ell3.h
 *
 * [EOF]
 */
