/*
 * File: combineVectorElements.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef COMBINEVECTORELEMENTS_H
#define COMBINEVECTORELEMENTS_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern float combineVectorElements(const float x_data[], const int x_size[1]);

#endif

/*
 * File trailer for combineVectorElements.h
 *
 * [EOF]
 */
