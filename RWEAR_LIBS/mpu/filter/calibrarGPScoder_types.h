/*
 * File: calibrarGPScoder_types.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef CALIBRARGPSCODER_TYPES_H
#define CALIBRARGPSCODER_TYPES_H

/* Include Files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef typedef_c_fusion_internal_coder_insfilt
#define typedef_c_fusion_internal_coder_insfilt

#define GPS_BUFFER_SIZE 10

typedef struct {
  float StateCovariance[784];
  double ReferenceLocation[3];
  double QuaternionNoise[4];
  double AngularVelocityNoise[3];
  double PositionNoise[3];
  double VelocityNoise[3];
  double AccelerationNoise[3];
  double GyroscopeBiasNoise[3];
  double AccelerometerBiasNoise[3];
  double GeomagneticVectorNoise[3];
  double MagnetometerBiasNoise[3];
  float pState[28];
	double posAnterior[3];
} c_fusion_internal_coder_insfilt;

#endif                                 /*typedef_c_fusion_internal_coder_insfilt*/

#ifndef struct_emxArray_real32_T_1x1
#define struct_emxArray_real32_T_1x1

struct emxArray_real32_T_1x1
{
  float data[1];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_1x1*/

#ifndef typedef_emxArray_real32_T_1x1
#define typedef_emxArray_real32_T_1x1

typedef struct emxArray_real32_T_1x1 emxArray_real32_T_1x1;

#endif                                 /*typedef_emxArray_real32_T_1x1*/

#ifndef struct_s1vhBKqHRuzkVV7ODpSn2N_tag
#define struct_s1vhBKqHRuzkVV7ODpSn2N_tag

struct s1vhBKqHRuzkVV7ODpSn2N_tag
{
  emxArray_real32_T_1x1 a;
  emxArray_real32_T_1x1 b;
  emxArray_real32_T_1x1 c;
  emxArray_real32_T_1x1 d;
};

#endif                                 /*struct_s1vhBKqHRuzkVV7ODpSn2N_tag*/

#ifndef typedef_c_matlabshared_rotations_intern
#define typedef_c_matlabshared_rotations_intern

typedef struct s1vhBKqHRuzkVV7ODpSn2N_tag c_matlabshared_rotations_intern;

#endif                                 /*typedef_c_matlabshared_rotations_intern*/

#ifndef struct_emxArray_real32_T_3x3
#define struct_emxArray_real32_T_3x3

struct emxArray_real32_T_3x3
{
  float data[9];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_3x3*/

#ifndef typedef_emxArray_real32_T_3x3
#define typedef_emxArray_real32_T_3x3

typedef struct emxArray_real32_T_3x3 emxArray_real32_T_3x3;

#endif                                 /*typedef_emxArray_real32_T_3x3*/

#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
  double Longitud[GPS_BUFFER_SIZE];
  double Latitud[GPS_BUFFER_SIZE];
  double Altitud[GPS_BUFFER_SIZE];
  float velN[GPS_BUFFER_SIZE];
	float velE[GPS_BUFFER_SIZE];
	float velD[GPS_BUFFER_SIZE];
	uint8_T index;
} struct0_T;

#endif                                 /*typedef_struct0_T*/

#ifndef typedef_struct1_T
#define typedef_struct1_T

typedef struct {
  float ax[10];
  float ay[10];
  float az[10];
  float gx[10];
  float gy[10];
  float gz[10];
  float mx[10];
  float my[10];
  float mz[10];
	uint8_T index;
} struct1_T;

#endif                                 /*typedef_struct1_T*/

#ifndef typedef_struct2_T
#define typedef_struct2_T

typedef struct {
  float estados[28];
  float covarianzaEstados[784];
  double posAnterior[3];
} struct2_T;

#endif                                 /*typedef_struct2_T*/

#ifndef struct_sI1CIR3WNKoGjWocftHT3SE_tag
#define struct_sI1CIR3WNKoGjWocftHT3SE_tag

struct sI1CIR3WNKoGjWocftHT3SE_tag
{
  float p[3];
  c_matlabshared_rotations_intern cuaternion;
  float velocidad[3];
	float aceleracion[3];
};

#endif                                 /*struct_sI1CIR3WNKoGjWocftHT3SE_tag*/

#ifndef typedef_struct3_T
#define typedef_struct3_T

typedef struct sI1CIR3WNKoGjWocftHT3SE_tag struct3_T;

#endif                                 /*typedef_struct3_T*/
#endif

/*
 * File trailer for calibrarGPScoder_types.h
 *
 * [EOF]
 */
