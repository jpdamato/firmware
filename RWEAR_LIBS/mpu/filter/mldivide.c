/*
 * File: mldivide.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "mldivide.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include <math.h>
#include <string.h>
/* Function Definitions */

/*
 * Arguments    : const float A[9]
 *                const float B[84]
 *                float Y[84]
 * Return Type  : void
 */
void b_mldivide(const float A[9], const float B[84], float Y[84])
{
  int rtemp;
  int r1;
  float b_A[9];
  int r2;
  int r3;
  float maxval;
  float a21;
  float f;
	
	memset(b_A,0,sizeof(b_A));
	
  for (rtemp = 0; rtemp < 9; rtemp++) {
    b_A[rtemp] = A[rtemp];
  }

  r1 = 0;
  r2 = 1;
  r3 = 2;
  maxval = fabsf(A[0]);
  a21 = fabsf(A[1]);
  if (a21 > maxval) {
    maxval = a21;
    r1 = 1;
    r2 = 0;
  }

  if (fabsf(A[2]) > maxval) {
    r1 = 2;
    r2 = 1;
    r3 = 0;
  }

  b_A[r2] = A[r2] / A[r1];
  b_A[r3] /= b_A[r1];
  b_A[r2 + 3] -= b_A[r2] * b_A[r1 + 3];
  b_A[r3 + 3] -= b_A[r3] * b_A[r1 + 3];
  b_A[r2 + 6] -= b_A[r2] * b_A[r1 + 6];
  b_A[r3 + 6] -= b_A[r3] * b_A[r1 + 6];
  if (fabsf(b_A[r3 + 3]) > fabsf(b_A[r2 + 3])) {
    rtemp = r2;
    r2 = r3;
    r3 = rtemp;
  }

  b_A[r3 + 3] /= b_A[r2 + 3];
  b_A[r3 + 6] -= b_A[r3 + 3] * b_A[r2 + 6];
  for (rtemp = 0; rtemp < 28; rtemp++) {
    maxval = B[r1 + 3 * rtemp];
    a21 = B[r2 + 3 * rtemp] - maxval * b_A[r2];
    f = ((B[r3 + 3 * rtemp] - maxval * b_A[r3]) - a21 * b_A[r3 + 3]) / b_A[r3 +
      6];
    Y[3 * rtemp + 2] = f;
    maxval -= f * b_A[r1 + 6];
    a21 -= f * b_A[r2 + 6];
    a21 /= b_A[r2 + 3];
    Y[3 * rtemp + 1] = a21;
    maxval -= a21 * b_A[r1 + 3];
    maxval /= b_A[r1];
    Y[3 * rtemp] = maxval;
  }
}

/*
 * Arguments    : const float A[9]
 *                const float B[3]
 *                float Y[3]
 * Return Type  : void
 */
void mldivide(const float A[9], const float B[3], float Y[3])
{
  int r1;
  float b_A[9];
  int r2;
  int r3;
  float maxval;
  float a21;
  int rtemp;
  for (r1 = 0; r1 < 9; r1++) {
    b_A[r1] = A[r1];
  }

  r1 = 0;
  r2 = 1;
  r3 = 2;
  maxval = fabsf(A[0]);
  a21 = fabsf(A[1]);
  if (a21 > maxval) {
    maxval = a21;
    r1 = 1;
    r2 = 0;
  }

  if (fabsf(A[2]) > maxval) {
    r1 = 2;
    r2 = 1;
    r3 = 0;
  }

  b_A[r2] = A[r2] / A[r1];
  b_A[r3] /= b_A[r1];
  b_A[r2 + 3] -= b_A[r2] * b_A[r1 + 3];
  b_A[r3 + 3] -= b_A[r3] * b_A[r1 + 3];
  b_A[r2 + 6] -= b_A[r2] * b_A[r1 + 6];
  b_A[r3 + 6] -= b_A[r3] * b_A[r1 + 6];
  if (fabsf(b_A[r3 + 3]) > fabsf(b_A[r2 + 3])) {
    rtemp = r2;
    r2 = r3;
    r3 = rtemp;
  }

  b_A[r3 + 3] /= b_A[r2 + 3];
  b_A[r3 + 6] -= b_A[r3 + 3] * b_A[r2 + 6];
  Y[1] = B[r2] - B[r1] * b_A[r2];
  Y[2] = (B[r3] - B[r1] * b_A[r3]) - Y[1] * b_A[r3 + 3];
  Y[2] /= b_A[r3 + 6];
  Y[0] = B[r1] - Y[2] * b_A[r1 + 6];
  Y[1] -= Y[2] * b_A[r2 + 6];
  Y[1] /= b_A[r2 + 3];
  Y[0] -= Y[1] * b_A[r1 + 3];
  Y[0] /= b_A[r1];
}

/*
 * File trailer for mldivide.c
 *
 * [EOF]
 */
