/*
 * File: calibrarGPScoder_initialize.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef CALIBRARGPSCODER_INITIALIZE_H
#define CALIBRARGPSCODER_INITIALIZE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern void calibrarGPScoder_initialize(void);

#endif

/*
 * File trailer for calibrarGPScoder_initialize.h
 *
 * [EOF]
 */
