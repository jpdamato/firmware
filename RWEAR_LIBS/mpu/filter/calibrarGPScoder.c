/*
 * File: calibrarGPScoder.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "fastsmooth.h"
#include "filtroCoder.h"

/* Function Definitions */

/*
 * TGPS contiene el buffer con las medidas de la GPS, el tama�o del buffer
 * depende del input (deberia ser mas grande que el de la IMU).
 * Window es la ventana de suavizado, debe ser menor que
 * el tama�o del buffer. dt es el tamano del step
 * Arguments    : const struct0_T *TGPS
 *                unsigned char window
 *                double lat[10]
 *                double lon[10]
 *                double alt[10]
 *                float v[10]
 * Return Type  : void
 */
void calibrarGPScoder(const struct0_T *TGPS, unsigned char window, 
											double lat[GPS_BUFFER_SIZE],double lon[GPS_BUFFER_SIZE], double alt[GPS_BUFFER_SIZE], 
											float vN[GPS_BUFFER_SIZE], float vE[GPS_BUFFER_SIZE],float vD[GPS_BUFFER_SIZE])
{
  /* TGPS */
  /*    HoraGps */
  /*    IndiceGps */
  /*    Latitud */
  /*    Longitud */
  /*    Altitud */
  /*    VGPS */
  /*  if nargin < 2 */
  /*      window = 5; */
  /*  end */
  /* Establecemos el vector de tiempos */
  /* timestepInicial = TGPS.HoraGps(1); */
  /* timestepFinal = TGPS.HoraGps(end); */
  /* xq = zeros(5,1,'single'); */
  /* xq(:,1) = transpose(timestepInicial:dt:timestepFinal); */
  /* xq(:,1) = dt * [0;1;2;3;4] + timestepInicial; */
  /* Interpolamos para completar los espacios vacios */
  /*  nombres = fieldnames(TGPS); */
  /*  for i = 1:length(nombres) */
  /*      if strcmpi(nombres{i}, 'HoraGps') */
  /*          continue */
  /*      end */
  /*      TGPS.(nombres{i}) = interp1(TGPS.HoraGps, TGPS.(nombres{i}), xq, 'pchip'); */
  /*  end */
  /* hora = xq; */
  /* Reducimos un poco la intensidad del ruido */
  fastsmooth(TGPS->Latitud, window, lat);
  fastsmooth(TGPS->Longitud, window, lon);
  fastsmooth(TGPS->Altitud, window, alt);
  b_fastsmooth(TGPS->velN, window, vN);
	b_fastsmooth(TGPS->velE, window, vE);
	b_fastsmooth(TGPS->velD, window, vD);
}

/*
 * File trailer for calibrarGPScoder.c
 *
 * [EOF]
 */
