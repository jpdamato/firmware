/*
 * File: calibrarGPScoder_rtwutil.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef CALIBRARGPSCODER_RTWUTIL_H
#define CALIBRARGPSCODER_RTWUTIL_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern double rt_remd(double u0, double u1);

#endif

/*
 * File trailer for calibrarGPScoder_rtwutil.h
 *
 * [EOF]
 */
