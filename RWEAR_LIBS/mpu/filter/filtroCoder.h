/*
 * File: filtroCoder.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef FILTROCODER_H
#define FILTROCODER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
void filtroCoder(c_fusion_internal_coder_insfilt * f, const float accel[3], const float gyro[3],
	const float mag[3], const double lla[3], const float VGPS[3], const	double ubicacionReferencia[3], 
		const float covarianza[3], const bool evaluarGPS, const bool evaluarIMU, float dt,
	struct3_T *salida);
	

#endif

/*
 * File trailer for filtroCoder.h
 *
 * [EOF]
 */
