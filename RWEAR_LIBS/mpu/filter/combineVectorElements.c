/*
 * File: combineVectorElements.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "combineVectorElements.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"

/* Function Definitions */

/*
 * Arguments    : const float x_data[]
 *                const int x_size[1]
 * Return Type  : float
 */
float combineVectorElements(const float x_data[], const int x_size[1])
{
  float y;
  int firstBlockLength;
  int k;
  if (x_size[0] == 0) {
    y = 0.0F;
  } else {
    firstBlockLength = x_size[0];
    y = x_data[0];
    for (k = 2; k <= firstBlockLength; k++) {
      y += x_data[k - 1];
    }
  }

  return y;
}

/*
 * File trailer for combineVectorElements.c
 *
 * [EOF]
 */
