/*
 * File: calibrarGPScoder.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef CALIBRARGPSCODER_H
#define CALIBRARGPSCODER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"


/* Function Declarations */
extern void calibrarGPScoder(const struct0_T *TGPS, unsigned char window, 
														double lat[10], double lon[GPS_BUFFER_SIZE], double alt[GPS_BUFFER_SIZE], 
														float vN[GPS_BUFFER_SIZE], float vE[GPS_BUFFER_SIZE],float vD[GPS_BUFFER_SIZE]);

#endif

/*
 * File trailer for calibrarGPScoder.h
 *
 * [EOF]
 */
