/*
 * File: sind.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "sind.h"
#include "calibrarGPScoder.h"
#include "calibrarGPScoder_rtwutil.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : double *x
 * Return Type  : void
 */
void b_sind(double *x)
{
  double absx;
  signed char n;
  *x = rt_remd(*x, 360.0);
  absx = fabs(*x);
  if (absx > 180.0) {
    if (*x > 0.0) {
      *x -= 360.0;
    } else {
      *x += 360.0;
    }

    absx = fabs(*x);
  }

  if (absx <= 45.0) {
    *x *= 0.017453292519943295;
    n = 0;
  } else if (absx <= 135.0) {
    if (*x > 0.0) {
      *x = 0.017453292519943295 * (*x - 90.0);
      n = 1;
    } else {
      *x = 0.017453292519943295 * (*x + 90.0);
      n = -1;
    }
  } else if (*x > 0.0) {
    *x = 0.017453292519943295 * (*x - 180.0);
    n = 2;
  } else {
    *x = 0.017453292519943295 * (*x + 180.0);
    n = -2;
  }

  if (n == 0) {
    *x = sin(*x);
  } else if (n == 1) {
    *x = cos(*x);
  } else if (n == -1) {
    *x = -cos(*x);
  } else {
    *x = -sin(*x);
  }
}

/*
 * File trailer for sind.c
 *
 * [EOF]
 */
