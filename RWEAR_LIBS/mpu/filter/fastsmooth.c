/*
 * File: fastsmooth.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "fastsmooth.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "combineVectorElements.h"
#include "filtroCoder.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * fastsmooth(Y,w,type,ends) smooths vector Y with smooth
 *   of width w.
 *  The argument "type" determines the smooth type:
 *    If type=1, rectangular (sliding-average or boxcar)
 *    If type=2, triangular (2 passes of sliding-average)
 *    If type=3, pseudo-Gaussian (3 passes of sliding-average)
 *    If type=4, pseudo-Gaussian (4 passes of same sliding-average)
 *    If type=5, multiple-width (4 passes of different sliding-average)
 *  The argument "ends" controls how the "ends" of the signal
 *  (the first w/2 points and the last w/2 points) are handled.
 *    If ends=0, the ends are zero.  (In this mode the elapsed
 *      time is independent of the smooth width). The fastest.
 *    If ends=1, the ends are smoothed with progressively
 *      smaller smooths the closer to the end. (In this mode the
 *      elapsed time increases with increasing smooth widths).
 *  fastsmooth(Y,w,type) smooths with ends=0.
 *  fastsmooth(Y,w) smooths with type=1 and ends=0.
 *  Examples:
 *  fastsmooth([1 1 1 10 10 10 1 1 1 1],3)= [0 1 4 7 10 7 4 1 1 0]
 *
 *  fastsmooth([1 1 1 10 10 10 1 1 1 1],3,1,1)= [1 1 4 7 10 7 4 1 1 1]
 *
 *  x=1:100;
 *  y=randn(size(x));
 *  plot(x,y,x,fastsmooth(y,5,3,1),'r')
 *  xlabel('Blue: white noise.    Red: smoothed white noise.')
 * Arguments    : const float Y[10]
 *                unsigned char w
 *                float SmoothY[10]
 * Return Type  : void
 */
void b_fastsmooth(const float Y[10], unsigned char w, float SmoothY[10])
{
  int h;
  int i;
  int loop_ub;
  int Y_size[1];
  float Y_data[10];
  float SumPoints;
  int halfw;
  int b_i;
  int i1;

  /* if nargin==2, ends=0; type=1; end */
  /* if nargin==3, ends=0; end */
  /* switch type */
  /*     case 1 */
  /*     case 2    */
  /*         SmoothY=sa(sa(Y,w,ends),w,ends); */
  /*      case 3 */
  /*         SmoothY=sa(sa(sa(Y,w,ends),w,ends),w,ends); */
  /*      case 4 */
  /*         SmoothY=sa(sa(sa(sa(Y,w,ends),w,ends),w,ends),w,ends); */
  /*      case 5 */
  /*         SmoothY=sa(sa(sa(sa(Y,round(1.6*w),ends),round(1.4*w),ends),round(1.2*w),ends),w,ends); */
  /*   end */
  h = 0;
  for (i = 0; i < 10; i++) {
    SmoothY[i] = 0.0F;
  }

  if (1 > w) {
    loop_ub = 0;
  } else {
    loop_ub = w;
  }

  Y_size[0] = loop_ub;
  if (0 <= loop_ub - 1) {
    memcpy(&Y_data[0], &Y[0], loop_ub * sizeof(float));
  }

  SumPoints = combineVectorElements(Y_data, Y_size);

  /* Atento a que las dimensiones coincidan */
  halfw = (int)roundf((float)w / 2.0F);
  b_i = 9 - w;
  for (i = 0; i <= b_i; i++) {
    SmoothY[(i + halfw) - 1] = SumPoints;
    SumPoints -= Y[i];
    SumPoints += Y[i + w];
    h = i;
  }

  if (11 - w > 10) {
    b_i = 0;
    i1 = 0;
  } else {
    b_i = 10 - w;
    i1 = 10;
  }

  loop_ub = i1 - b_i;
  Y_size[0] = loop_ub;
  for (i1 = 0; i1 < loop_ub; i1++) {
    Y_data[i1] = Y[b_i + i1];
  }

  SmoothY[h + halfw] = combineVectorElements(Y_data, Y_size);
  for (b_i = 0; b_i < 10; b_i++) {
    SmoothY[b_i] /= (float)w;
  }

  /*  Taper the ends of the signal if ends=1. */
  SmoothY[0] = (Y[0] + Y[1]) / 2.0F;
  b_i = (int)(((float)w + 1.0F) / 2.0F + -1.0F);
  for (i = 0; i < b_i; i++) {
    i1 = (i + 2) << 1;
    loop_ub = i1 - 1;
    Y_size[0] = i1 - 1;
    if (0 <= loop_ub - 1) {
      memcpy(&Y_data[0], &Y[0], loop_ub * sizeof(float));
    }

    halfw = i1 - 1;
    SmoothY[i + 1] = combineVectorElements(Y_data, Y_size) / (float)halfw;
    Y_size[0] = i1 - 1;
    loop_ub = i1 - 11;
    for (h = 0; h <= loop_ub + 9; h++) {
      Y_data[h] = Y[(h - i1) + 11];
    }

    SmoothY[8 - i] = combineVectorElements(Y_data, Y_size) / (float)halfw;
  }

  SmoothY[9] = (Y[9] + Y[8]) / 2.0F;
}

/*
 * fastsmooth(Y,w,type,ends) smooths vector Y with smooth
 *   of width w.
 *  The argument "type" determines the smooth type:
 *    If type=1, rectangular (sliding-average or boxcar)
 *    If type=2, triangular (2 passes of sliding-average)
 *    If type=3, pseudo-Gaussian (3 passes of sliding-average)
 *    If type=4, pseudo-Gaussian (4 passes of same sliding-average)
 *    If type=5, multiple-width (4 passes of different sliding-average)
 *  The argument "ends" controls how the "ends" of the signal
 *  (the first w/2 points and the last w/2 points) are handled.
 *    If ends=0, the ends are zero.  (In this mode the elapsed
 *      time is independent of the smooth width). The fastest.
 *    If ends=1, the ends are smoothed with progressively
 *      smaller smooths the closer to the end. (In this mode the
 *      elapsed time increases with increasing smooth widths).
 *  fastsmooth(Y,w,type) smooths with ends=0.
 *  fastsmooth(Y,w) smooths with type=1 and ends=0.
 *  Examples:
 *  fastsmooth([1 1 1 10 10 10 1 1 1 1],3)= [0 1 4 7 10 7 4 1 1 0]
 *
 *  fastsmooth([1 1 1 10 10 10 1 1 1 1],3,1,1)= [1 1 4 7 10 7 4 1 1 1]
 *
 *  x=1:100;
 *  y=randn(size(x));
 *  plot(x,y,x,fastsmooth(y,5,3,1),'r')
 *  xlabel('Blue: white noise.    Red: smoothed white noise.')
 * Arguments    : const double Y[10]
 *                unsigned char w
 *                double SmoothY[10]
 * Return Type  : void
 */
void fastsmooth(const double Y[10], unsigned char w, double SmoothY[10])
{
  int h;
  int i;
  double SumPoints;
  int k;
  int halfw;
  int i1;
  int vlen_tmp;

  /* if nargin==2, ends=0; type=1; end */
  /* if nargin==3, ends=0; end */
  /* switch type */
  /*     case 1 */
  /*     case 2    */
  /*         SmoothY=sa(sa(Y,w,ends),w,ends); */
  /*      case 3 */
  /*         SmoothY=sa(sa(sa(Y,w,ends),w,ends),w,ends); */
  /*      case 4 */
  /*         SmoothY=sa(sa(sa(sa(Y,w,ends),w,ends),w,ends),w,ends); */
  /*      case 5 */
  /*         SmoothY=sa(sa(sa(sa(Y,round(1.6*w),ends),round(1.4*w),ends),round(1.2*w),ends),w,ends); */
  /*   end */
  h = 0;
  memset(&SmoothY[0], 0, 10U * sizeof(double));
  if (1 > w) {
    i = 0;
  } else {
    i = w;
  }

  if (i == 0) {
    SumPoints = 0.0;
  } else {
    SumPoints = Y[0];
    for (k = 2; k <= i; k++) {
      SumPoints += Y[k - 1];
    }
  }

  /* Atento a que las dimensiones coincidan */
  halfw = (int)roundf((float)w / 2.0F);
  i = 9 - w;
  for (k = 0; k <= i; k++) {
    SmoothY[(k + halfw) - 1] = SumPoints;
    SumPoints -= Y[k];
    SumPoints += Y[k + w];
    h = k;
  }

  if (11 - w > 10) {
    i = -1;
    i1 = -1;
  } else {
    i1 = 9;
  }

  vlen_tmp = i1 - i;
  if (vlen_tmp == 0) {
    SumPoints = 0.0;
  } else {
    SumPoints = Y[i + 1];
    for (k = 2; k <= vlen_tmp; k++) {
      SumPoints += Y[i + k];
    }
  }

  SmoothY[h + halfw] = SumPoints;
  for (i = 0; i < 10; i++) {
    SmoothY[i] /= (double)w;
  }

  /*  Taper the ends of the signal if ends=1. */
  SmoothY[0] = (Y[0] + Y[1]) / 2.0;
  i = (int)(((float)w + 1.0F) / 2.0F + -1.0F);
  for (halfw = 0; halfw < i; halfw++) {
    i1 = (halfw + 2) << 1;
    vlen_tmp = i1 - 1;
    SumPoints = Y[0];
    for (k = 2; k <= vlen_tmp; k++) {
      SumPoints += Y[k - 1];
    }

    vlen_tmp = i1 - 1;
    SmoothY[halfw + 1] = SumPoints / (double)vlen_tmp;
    h = i1 - 1;
    SumPoints = Y[11 - i1];
    for (k = 2; k <= h; k++) {
      SumPoints += Y[(k - i1) + 10];
    }

    SmoothY[8 - halfw] = SumPoints / (double)vlen_tmp;
  }

  SmoothY[9] = (Y[9] + Y[8]) / 2.0;
}

/*
 * File trailer for fastsmooth.c
 *
 * [EOF]
 */
