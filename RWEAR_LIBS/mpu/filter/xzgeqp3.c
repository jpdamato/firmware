/*
 * File: xzgeqp3.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "xzgeqp3.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "xnrm2.h"
#include <math.h>
#include <string.h>

/* Function Declarations */
static float rt_hypotf(float u0, float u1);

/* Function Definitions */

/*
 * Arguments    : float u0
 *                float u1
 * Return Type  : float
 */
static float rt_hypotf(float u0, float u1)
{
  float y;
  float a;
  float b;
  a = fabsf(u0);
  b = fabsf(u1);
  if (a < b) {
    a /= b;
    y = b * sqrtf(a * a + 1.0F);
  } else if (a > b) {
    b /= a;
    y = a * sqrtf(b * b + 1.0F);
  } else {
    y = a * 1.41421354F;
  }

  return y;
}

/*
 * Arguments    : float A[80]
 *                int ia0
 *                int m
 *                int n
 *                float tau[4]
 *                int jpvt[4]
 * Return Type  : void
 */
void qrpf(float A[80], int ia0, int m, int n, float tau[4], int jpvt[4])
{
  int minmn;
  float work[4];
  float vn1[4];
  float vn2[4];
  int j;
  int i;
  float smax;
  int ip1;
  int knt;
  int ii;
  int nmi;
  int mmi;
  int itemp;
  int ix;
  int pvt;
  int iy;
  float temp2;
  float s;
  int lastv;
  int lastc;
  int b_i;
  bool exitg2;
  int exitg1;
  if (m < n) {
    minmn = m;
  } else {
    minmn = n;
  }

  work[0] = 0.0F;
  vn1[0] = 0.0F;
  vn2[0] = 0.0F;
  work[1] = 0.0F;
  vn1[1] = 0.0F;
  vn2[1] = 0.0F;
  work[2] = 0.0F;
  vn1[2] = 0.0F;
  vn2[2] = 0.0F;
  work[3] = 0.0F;
  vn1[3] = 0.0F;
  vn2[3] = 0.0F;
  for (j = 0; j < n; j++) {
    smax = xnrm2(m, A, ia0 + j * 20);
    vn1[j] = smax;
    vn2[j] = smax;
  }

  for (i = 0; i < minmn; i++) {
    ip1 = i + 2;
    knt = ia0 + i * 20;
    ii = (knt + i) - 1;
    nmi = n - i;
    mmi = m - i;
    if (nmi < 1) {
      itemp = -1;
    } else {
      itemp = 0;
      if (nmi > 1) {
        ix = i;
        smax = fabsf(vn1[i]);
        for (j = 2; j <= nmi; j++) {
          ix++;
          s = fabsf(vn1[ix]);
          if (s > smax) {
            itemp = j - 1;
            smax = s;
          }
        }
      }
    }

    pvt = i + itemp;
    if (pvt + 1 != i + 1) {
      ix = (ia0 + pvt * 20) - 1;
      iy = knt - 1;
      for (j = 0; j < m; j++) {
        smax = A[ix];
        A[ix] = A[iy];
        A[iy] = smax;
        ix++;
        iy++;
      }

      itemp = jpvt[pvt];
      jpvt[pvt] = jpvt[i];
      jpvt[i] = itemp;
      vn1[pvt] = vn1[i];
      vn2[pvt] = vn2[i];
    }

    if (i + 1 < m) {
      temp2 = A[ii];
      itemp = ii + 2;
      tau[i] = 0.0F;
      if (mmi > 0) {
        smax = xnrm2(mmi - 1, A, ii + 2);
        if (smax != 0.0F) {
          s = rt_hypotf(A[ii], smax);
          if (A[ii] >= 0.0F) {
            s = -s;
          }

          if (fabsf(s) < 9.86076132E-32F) {
            knt = -1;
            b_i = ii + mmi;
            do {
              knt++;
              for (j = itemp; j <= b_i; j++) {
                A[j - 1] *= 1.01412048E+31F;
              }

              s *= 1.01412048E+31F;
              temp2 *= 1.01412048E+31F;
            } while (!(fabsf(s) >= 9.86076132E-32F));

            s = rt_hypotf(temp2, xnrm2(mmi - 1, A, ii + 2));
            if (temp2 >= 0.0F) {
              s = -s;
            }

            tau[i] = (s - temp2) / s;
            smax = 1.0F / (temp2 - s);
            for (j = itemp; j <= b_i; j++) {
              A[j - 1] *= smax;
            }

            for (j = 0; j <= knt; j++) {
              s *= 9.86076132E-32F;
            }

            temp2 = s;
          } else {
            tau[i] = (s - A[ii]) / s;
            smax = 1.0F / (A[ii] - s);
            b_i = ii + mmi;
            for (j = itemp; j <= b_i; j++) {
              A[j - 1] *= smax;
            }

            temp2 = s;
          }
        }
      }

      A[ii] = temp2;
    } else {
      tau[i] = 0.0F;
    }

    if (i + 1 < n) {
      temp2 = A[ii];
      A[ii] = 1.0F;
      pvt = ii + 21;
      if (tau[i] != 0.0F) {
        lastv = mmi - 1;
        itemp = (ii + mmi) - 1;
        while ((lastv + 1 > 0) && (A[itemp] == 0.0F)) {
          lastv--;
          itemp--;
        }

        lastc = nmi - 2;
        exitg2 = false;
        while ((!exitg2) && (lastc + 1 > 0)) {
          itemp = (ii + lastc * 20) + 21;
          knt = itemp;
          do {
            exitg1 = 0;
            if (knt <= itemp + lastv) {
              if (A[knt - 1] != 0.0F) {
                exitg1 = 1;
              } else {
                knt++;
              }
            } else {
              lastc--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }
      } else {
        lastv = -1;
        lastc = -1;
      }

      if (lastv + 1 > 0) {
        if (lastc + 1 != 0) {
          if (0 <= lastc) {
            memset(&work[0], 0, (lastc + 1) * sizeof(float));
          }

          iy = 0;
          b_i = (ii + 20 * lastc) + 21;
          for (itemp = pvt; itemp <= b_i; itemp += 20) {
            ix = ii;
            smax = 0.0F;
            nmi = itemp + lastv;
            for (knt = itemp; knt <= nmi; knt++) {
              smax += A[knt - 1] * A[ix];
              ix++;
            }

            work[iy] += smax;
            iy++;
          }
        }

        if (-tau[i] != 0.0F) {
          itemp = ii;
          knt = 0;
          for (j = 0; j <= lastc; j++) {
            if (work[knt] != 0.0F) {
              smax = work[knt] * -tau[i];
              ix = ii;
              b_i = itemp + 21;
              nmi = lastv + itemp;
              for (pvt = b_i; pvt <= nmi + 21; pvt++) {
                A[pvt - 1] += A[ix] * smax;
                ix++;
              }
            }

            knt++;
            itemp += 20;
          }
        }
      }

      A[ii] = temp2;
    }

    for (j = ip1; j <= n; j++) {
      itemp = (ia0 + i) + (j - 1) * 20;
      smax = vn1[j - 1];
      if (smax != 0.0F) {
        s = fabsf(A[itemp - 1]) / smax;
        s = 1.0F - s * s;
        if (s < 0.0F) {
          s = 0.0F;
        }

        temp2 = smax / vn2[j - 1];
        temp2 = s * (temp2 * temp2);
        if (temp2 <= 0.000345266977F) {
          if (i + 1 < m) {
            smax = xnrm2(mmi - 1, A, itemp + 1);
            vn1[j - 1] = smax;
            vn2[j - 1] = smax;
          } else {
            vn1[j - 1] = 0.0F;
            vn2[j - 1] = 0.0F;
          }
        } else {
          vn1[j - 1] = smax * sqrtf(s);
        }
      }
    }
  }
}

/*
 * File trailer for xzgeqp3.c
 *
 * [EOF]
 */
