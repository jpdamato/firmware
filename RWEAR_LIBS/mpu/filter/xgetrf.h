/*
 * File: xgetrf.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef XGETRF_H
#define XGETRF_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern void xgetrf(float A[36], int ipiv[6], int *info);

#endif

/*
 * File trailer for xgetrf.h
 *
 * [EOF]
 */
