/*
 * File: sind.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef SIND_H
#define SIND_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern void b_sind(double *x);

#endif

/*
 * File trailer for sind.h
 *
 * [EOF]
 */
