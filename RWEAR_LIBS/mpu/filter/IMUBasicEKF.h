/*
 * File: IMUBasicEKF.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef IMUBASICEKF_H
#define IMUBASICEKF_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern void IMUBasicEKF_correctEqn(float x[28], float P[784], const float h[6],
  const double H[168], const float z[6], const float R[36]);
extern void IMUBasicEKF_repairQuaternion(float x[28]);

#endif

/*
 * File trailer for IMUBasicEKF.h
 *
 * [EOF]
 */
