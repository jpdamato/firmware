/*
 * File: AsyncMARGGPSFuserBase.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "AsyncMARGGPSFuserBase.h"
#include "IMUBasicEKF.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "cosd.h"
#include "filtroCoder.h"
#include "mldivide.h"
#include "sind.h"
#include <math.h>
#include <string.h>
/* Variable Definitions */
static const float fv[9] = { 0.0001, 0.0, 0.0, 0.0, 0.0001, 0.0, 0.0,
  0.0, 0.0001 };

/* Function Declarations */
static void c_AsyncMARGGPSFuserBase_accelMe(const float x[28], float z[3]);
static void d_AsyncMARGGPSFuserBase_accelMe(float x[28], float dhdx[84]);

/* Function Definitions */

/*
 * Arguments    : const float x[28]
 *                float z[3]
 * Return Type  : void
 */
static void c_AsyncMARGGPSFuserBase_accelMe(const float x[28], float z[3])
{
  float z_tmp;
  float b_z_tmp;
  float c_z_tmp;
  float d_z_tmp;
  float e_z_tmp;
  float f_z_tmp;
  z_tmp = 2.0 * x[0];
  b_z_tmp = 2.0 * x[1];
  c_z_tmp = x[0] * x[0];
  d_z_tmp = x[1] * x[1];
  e_z_tmp = x[2] * x[2];
  f_z_tmp = x[3] * x[3];
  z[0] = ((x[16] - x[13] * (((c_z_tmp + d_z_tmp) - e_z_tmp) - f_z_tmp)) + (x[15]
           - 9.81) * (z_tmp * x[2] - b_z_tmp * x[3])) - x[14] * (z_tmp * x[3] +
    b_z_tmp * x[2]);
  b_z_tmp = c_z_tmp - d_z_tmp;
  c_z_tmp = 2.0* x[2] * x[3];
  z[1] = ((x[17] - x[14] * ((b_z_tmp + e_z_tmp) - f_z_tmp)) - (x[15] - 9.81) *
          (z_tmp * x[1] + c_z_tmp)) + x[13] * (2.0* x[0] * x[3] - 2.0* x[1] *
    x[2]);
  z[2] = ((x[18] - (x[15] - 9.81) * ((b_z_tmp - e_z_tmp) + f_z_tmp)) + x[14] *
          (2.0* x[0] * x[1] - c_z_tmp)) - x[13] * (2.0* x[0] * x[2] + 2.0*
    x[1] * x[3]);
}

/*
 * Arguments    : const float x[28]
 *                float dhdx[84]
 * Return Type  : void
 */
static void d_AsyncMARGGPSFuserBase_accelMe(float x[28], float dhdx[84])
{
  float dhdx_tmp;
  float b_dhdx_tmp;
  float c_dhdx_tmp;
  float d_dhdx_tmp;
  float e_dhdx_tmp;
  float f_dhdx_tmp;
  float g_dhdx_tmp;
  float h_dhdx_tmp;
  float i_dhdx_tmp;
  dhdx_tmp = 2.0 * x[2];
  b_dhdx_tmp = 2.0 * x[0];
  c_dhdx_tmp = 2.0 * x[1];
  d_dhdx_tmp = 2.0 * x[3];
  e_dhdx_tmp = -2.0 * x[0];
  dhdx[0] = (dhdx_tmp * (x[15] - 9.81) - d_dhdx_tmp * x[14]) - b_dhdx_tmp * x
    [13];
  dhdx[3] = (-2.0 * x[3] * (x[15] - 9.81) - dhdx_tmp * x[14]) - c_dhdx_tmp *
    x[13];
  dhdx[6] = (b_dhdx_tmp * (x[15] - 9.81) - c_dhdx_tmp * x[14]) + dhdx_tmp * x
    [13];
  dhdx[9] = (d_dhdx_tmp * x[13] - b_dhdx_tmp * x[14]) - c_dhdx_tmp * (x[15] -
    9.81);
  dhdx[12] = 0.0;
  dhdx[15] = 0.0;
  dhdx[18] = 0.0;
  dhdx[21] = 0.0;
  dhdx[24] = 0.0;
  dhdx[27] = 0.0;
  dhdx[30] = 0.0;
  dhdx[33] = 0.0;
  dhdx[36] = 0.0;

  d_dhdx_tmp = -1*(x[0] * x[0]);
  f_dhdx_tmp = x[1] * x[1];
  g_dhdx_tmp = x[2] * x[2];
  h_dhdx_tmp = x[3] * x[3];

  dhdx[39] = ((d_dhdx_tmp - f_dhdx_tmp) + g_dhdx_tmp) + h_dhdx_tmp;
  dhdx[42] = e_dhdx_tmp * x[3] - c_dhdx_tmp * x[2];
  dhdx[45] = b_dhdx_tmp * x[2] - c_dhdx_tmp * x[3];
  dhdx[48] = 1.0;
  dhdx[51] = 0.0;
  dhdx[54] = 0.0;
  dhdx[57] = 0.0;
  dhdx[60] = 0.0;
  dhdx[63] = 0.0;
  dhdx[66] = 0.0;
  dhdx[69] = 0.0;
  dhdx[72] = 0.0;
  dhdx[75] = 0.0;
  dhdx[78] = 0.0;
  dhdx[81] = 0.0;
  dhdx[1] = (2.0 * x[3] * x[13] - 2.0 * x[0] * x[14]) - 2.0 * x[1] * (x[15] -
    9.81);
  c_dhdx_tmp = (2.0 * x[1] * x[14] - 2.0 * x[0] * (x[15] - 9.81)) - 2.0 * x
    [2] * x[13];
  dhdx[4] = c_dhdx_tmp;
  i_dhdx_tmp = (-2.0 * x[3] * (x[15] - 9.81) - 2.0 * x[2] * x[14]) - 2.0 *
    x[1] * x[13];
  dhdx[7] = i_dhdx_tmp;
  dhdx[10] = (2.0 * x[3] * x[14] - 2.0 * x[2] * (x[15] - 9.81)) + 2.0 * x[0]
    * x[13];
  dhdx[13] = 0.0;
  dhdx[16] = 0.0;
  dhdx[19] = 0.0;
  dhdx[22] = 0.0;
  dhdx[25] = 0.0;
  dhdx[28] = 0.0;
  dhdx[31] = 0.0;
  dhdx[34] = 0.0;
  dhdx[37] = 0.0;
  dhdx[40] = b_dhdx_tmp * x[3] - 2.0 * x[1] * x[2];
  d_dhdx_tmp += f_dhdx_tmp;
  dhdx[43] = (d_dhdx_tmp - g_dhdx_tmp) + h_dhdx_tmp;
  dhdx[46] = e_dhdx_tmp * x[1] - dhdx_tmp * x[3];
  dhdx[49] = 0.0;
  dhdx[52] = 1.0;
  dhdx[55] = 0.0;
  dhdx[58] = 0.0;
  dhdx[61] = 0.0;
  dhdx[64] = 0.0;
  dhdx[67] = 0.0;
  dhdx[70] = 0.0;
  dhdx[73] = 0.0;
  dhdx[76] = 0.0;
  dhdx[79] = 0.0;
  dhdx[82] = 0.0;
  dhdx[2] = c_dhdx_tmp;
  dhdx[5] = (2.0 * x[1] * (x[15] - 9.81) + 2.0 * x[0] * x[14]) - 2.0 * x[3] *
    x[13];
  dhdx[8] = (2.0 * x[2] * (x[15] - 9.81) - 2.0 * x[3] * x[14]) - 2.0 * x[0] *
    x[13];
  dhdx[11] = i_dhdx_tmp;
  dhdx[14] = 0.0;
  dhdx[17] = 0.0;
  dhdx[20] = 0.0;
  dhdx[23] = 0.0;
  dhdx[26] = 0.0;
  dhdx[29] = 0.0;
  dhdx[32] = 0.0;
  dhdx[35] = 0.0;
  dhdx[38] = 0.0;
  dhdx[41] = e_dhdx_tmp * x[2] - 2.0 * x[1] * x[3];
  dhdx[44] = b_dhdx_tmp * x[1] - 2.0 * x[2] * x[3];
  dhdx[47] = (d_dhdx_tmp + g_dhdx_tmp) - h_dhdx_tmp;
  dhdx[50] = 0.0;
  dhdx[53] = 0.0;
  dhdx[56] = 1.0;
  dhdx[59] = 0.0;
  dhdx[62] = 0.0;
  dhdx[65] = 0.0;
  dhdx[68] = 0.0;
  dhdx[71] = 0.0;
  dhdx[74] = 0.0;
  dhdx[77] = 0.0;
  dhdx[80] = 0.0;
  dhdx[83] = 0.0;
	
}

/*
 * Arguments    : c_fusion_internal_coder_insfilt *obj
 *                const float accel[3]
 * Return Type  : void
 */
void AsyncMARGGPSFuserBase_fuseaccel(c_fusion_internal_coder_insfilt *obj, const
  float accel[3])
{
  float h[3];
  float dhdx[84];
  int i;
  /*float P[784];*/
  int i1;
  float b_tmp[84];
  float y_tmp[9];
  float b_y_tmp[84];
  float f;
  int y_tmp_tmp;
  float x_tmp[84];
  float f1;
  float b_accel[3];
	
	memset(dhdx,0,sizeof(dhdx));
	
  c_AsyncMARGGPSFuserBase_accelMe(obj->pState, h);
  d_AsyncMARGGPSFuserBase_accelMe(obj->pState, dhdx);
  /*for (i = 0; i < 784; i++) {
    P[i] = obj->StateCovariance[i];
  }*/

  for (i = 0; i < 3; i++) {
    for (i1 = 0; i1 < 28; i1++) {
      b_tmp[i1 + 28 * i] = dhdx[i + 3 * i1];
    }
  }

  for (i = 0; i < 28; i++) {
    for (i1 = 0; i1 < 3; i1++) {
      f = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        f += obj->StateCovariance[i + 28 * y_tmp_tmp] * b_tmp[y_tmp_tmp + 28 * i1];
      }

      x_tmp[i + 28 * i1] = f;
    }
  }

  for (i = 0; i < 3; i++) {
    for (i1 = 0; i1 < 28; i1++) {
      f = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        f += dhdx[i + 3 * y_tmp_tmp] * obj->StateCovariance[y_tmp_tmp + 28 * i1];
      }

      b_y_tmp[i + 3 * i1] = f;
    }

    for (i1 = 0; i1 < 3; i1++) {
      f = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        f += b_y_tmp[i + 3 * y_tmp_tmp] * b_tmp[y_tmp_tmp + 28 * i1];
      }

      y_tmp_tmp = i + 3 * i1;
      y_tmp[y_tmp_tmp] = f + fv[y_tmp_tmp];
    }
  }

  b_mldivide(y_tmp, b_y_tmp, dhdx);
  for (i = 0; i < 28; i++) {
    f = x_tmp[i + 28];
    f1 = x_tmp[i + 56];
    for (i1 = 0; i1 < 28; i1++) {
      y_tmp_tmp = i + 28 * i1;
	  obj->StateCovariance[y_tmp_tmp] -= (x_tmp[i] * dhdx[3 * i1] + f * dhdx[3 * i1 + 1]) + f1 *
        dhdx[3 * i1 + 2];
    }
  }

  for (i = 0; i < 3; i++) {
    for (i1 = 0; i1 < 3; i1++) {
      f = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        f += b_y_tmp[i + 3 * y_tmp_tmp] * b_tmp[y_tmp_tmp + 28 * i1];
      }

      y_tmp_tmp = i + 3 * i1;
      y_tmp[y_tmp_tmp] = f + fv[y_tmp_tmp];
    }

    b_accel[i] = accel[i] - h[i];
  }

  mldivide(y_tmp, b_accel, h);
  for (i = 0; i < 28; i++) {
    obj->pState[i] += (x_tmp[i] * h[0] + x_tmp[i + 28] * h[1]) + x_tmp[i + 56] *
      h[2];
  }

  IMUBasicEKF_repairQuaternion(obj->pState);
  /*memcpy(&obj->StateCovariance[0], &P[0], 784U * sizeof(float));*/
}

/*
	 * Arguments : c_fusion_internal_coder_insfilt *obj
	 * const double lla[3]
	 * const float vel[3]
	 * Return Type : void
	 */
	void AsyncMARGGPSFuserBase_fusegps(c_fusion_internal_coder_insfilt *obj, const
	 double lla[3], const float vel[3], const float covarianza[3])
	{
	 double enuPos_idx_0;
	 double enuPos_idx_1;
	 double enuPos_idx_2;
	 double sinphi;
	 double N;
	 double ecefPosWithENUOrigin_idx_1;
	 double rho;
	 double cosphi;
	 double b_sinphi;
	 double coslambda;
	 double sinlambda;
	 double c_sinphi;
	 double b_N;
	 double d;
	 double tmp;
	 double ecefPosWithENUOrigin_idx_0;
	 float val[28];
	 float b_val[28];
	 int i;
	 float c_val[6];
	 float d_val[784];
	 float pos[6];
	 static const double dv[168] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
	
	 float measNoise[36] = { covarianza[0], 0.0, 0.0, 0.0,0.0, 0.0, 
	 0.0, covarianza[0], 0.0, 0.0, 0.0, 0.0,
	 0.0, 0.0, covarianza[1], 0.0, 0.0, 0.0,
	 0.0, 0.0, 0.0, covarianza[2], 0.0, 0.0,
	 0.0, 0.0, 0.0, 0.0, covarianza[2], 0.0,
	 0.0, 0.0, 0.0, 0.0, 0.0, covarianza[2]};
	
	 enuPos_idx_0 = obj->ReferenceLocation[0];
	 enuPos_idx_1 = obj->ReferenceLocation[1];
	 enuPos_idx_2 = obj->ReferenceLocation[2];
	 sinphi = lla[0];
	 b_sind(&sinphi);
	 N = 6.378137E+6 / sqrt(1.0 - 0.0066943799901413165 * (sinphi * sinphi));
	 ecefPosWithENUOrigin_idx_1 = lla[0];
	 b_cosd(&ecefPosWithENUOrigin_idx_1);
	 rho = (N + lla[2]) * ecefPosWithENUOrigin_idx_1;
	 cosphi = enuPos_idx_0;
	 b_cosd(&cosphi);
	 b_sinphi = enuPos_idx_0;
	 b_sind(&b_sinphi);
	 coslambda = enuPos_idx_1;
	 b_cosd(&coslambda);
	 sinlambda = enuPos_idx_1;
	 b_sind(&sinlambda);
	 c_sinphi = enuPos_idx_0;
	 b_sind(&c_sinphi);
	 b_N = 6.378137E+6 / sqrt(1.0 - 0.0066943799901413165 * (c_sinphi * c_sinphi));
	 b_cosd(&enuPos_idx_0);
	 enuPos_idx_0 *= b_N + enuPos_idx_2;
	 ecefPosWithENUOrigin_idx_1 = lla[1];
	 b_cosd(&ecefPosWithENUOrigin_idx_1);
	 d = lla[1];
	 b_sind(&d);
	 tmp = enuPos_idx_1;
	 b_cosd(&tmp);
	 b_sind(&enuPos_idx_1);
	 ecefPosWithENUOrigin_idx_0 = rho * ecefPosWithENUOrigin_idx_1 - enuPos_idx_0 *
	 tmp;
	 ecefPosWithENUOrigin_idx_1 = rho * d - enuPos_idx_0 * enuPos_idx_1;
	 enuPos_idx_0 = (N * 0.99330562000985867 + lla[2]) * sinphi - (b_N *
	 0.99330562000985867 + enuPos_idx_2) * c_sinphi;
	 tmp = coslambda * ecefPosWithENUOrigin_idx_0 + sinlambda *
	 ecefPosWithENUOrigin_idx_1;
	
	 c_val[0] = obj->pState[7];
	 c_val[1] = obj->pState[8];
	 c_val[2] = obj->pState[9];
	 c_val[3] = obj->pState[10];
	 c_val[4] = obj->pState[11];
	 c_val[5] = obj->pState[12];
	 pos[0] = (float)(-b_sinphi * tmp + cosphi * enuPos_idx_0);
	 pos[1] = (float)(-sinlambda * ecefPosWithENUOrigin_idx_0 + coslambda *
	 ecefPosWithENUOrigin_idx_1);
		pos[2] = (float)-(cosphi * tmp + b_sinphi * enuPos_idx_0);
		
		pos[3] = vel[0];
	 pos[4] = vel[1];
	 pos[5] = vel[2];
		
	 IMUBasicEKF_correctEqn(obj->pState, obj->StateCovariance, c_val, dv, pos, measNoise);
	 /*memcpy(&obj->pState[0], &b_val[0], 28U * sizeof(float));*/
	 /*memcpy(&obj->StateCovariance[0], &d_val[0], 784U * sizeof(float));*/
	}
/*
 * Arguments    : c_fusion_internal_coder_insfilt *obj
 *                const float gyro[3]
 * Return Type  : void
 */
void AsyncMARGGPSFuserBase_fusegyro(c_fusion_internal_coder_insfilt *obj, const
  float gyro[3])
{
  float val[28];
  int i;
  /*float P[784];*/
  int i1;
  float y_tmp[9];
  float b_y_tmp[84];
  float b_fv[84];
  float f;
  int y_tmp_tmp;
  float b_val[3];
  float x_tmp[84];
  static const signed char iv[84] = { 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 };

  float f1;
  static const signed char iv1[84] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  float fv1[3];
  memcpy(&val[0], &obj->pState[0], 28U * sizeof(float));
  /*for (i = 0; i < 784; i++) {
    P[i] = obj->StateCovariance[i];
  }*/

  for (i = 0; i < 28; i++) {
    for (i1 = 0; i1 < 3; i1++) {
      f = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        f += obj->StateCovariance[i + 28 * y_tmp_tmp] * (float)iv[y_tmp_tmp + 28 * i1];
      }

      x_tmp[i + 28 * i1] = f;
    }
  }

  for (i = 0; i < 3; i++) {
    for (i1 = 0; i1 < 28; i1++) {
      f = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        f += (float)iv1[i + 3 * y_tmp_tmp] * obj->StateCovariance[y_tmp_tmp + 28 * i1];
      }

      b_y_tmp[i + 3 * i1] = f;
    }

    for (i1 = 0; i1 < 3; i1++) {
      f = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        f += b_y_tmp[i + 3 * y_tmp_tmp] * (float)iv[y_tmp_tmp + 28 * i1];
      }

      y_tmp_tmp = i + 3 * i1;
      y_tmp[y_tmp_tmp] = f + fv[y_tmp_tmp];
    }
  }

  b_mldivide(y_tmp, b_y_tmp, b_fv);
  for (i = 0; i < 28; i++) {
    f = x_tmp[i + 28];
    f1 = x_tmp[i + 56];
    for (i1 = 0; i1 < 28; i1++) {
      y_tmp_tmp = i + 28 * i1;
	  obj->StateCovariance[y_tmp_tmp] -= (x_tmp[i] * b_fv[3 * i1] + f * b_fv[3 * i1 + 1]) + f1 *
        b_fv[3 * i1 + 2];
    }
  }

  b_val[0] = val[19] + val[4];
  b_val[1] = val[20] + val[5];
  b_val[2] = val[21] + val[6];
  for (i = 0; i < 3; i++) {
    for (i1 = 0; i1 < 3; i1++) {
      f = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        f += b_y_tmp[i + 3 * y_tmp_tmp] * (float)iv[y_tmp_tmp + 28 * i1];
      }

      y_tmp_tmp = i + 3 * i1;
      y_tmp[y_tmp_tmp] = f + fv[y_tmp_tmp];
    }

    b_val[i] = gyro[i] - b_val[i];
  }

  mldivide(y_tmp, b_val, fv1);
  for (i = 0; i < 28; i++) {
    obj->pState[i] += (x_tmp[i] * fv1[0] + x_tmp[i + 28] * fv1[1]) + x_tmp[i +
      56] * fv1[2];
  }

  IMUBasicEKF_repairQuaternion(obj->pState);
  /*memcpy(&obj->StateCovariance[0], &P[0], 784U * sizeof(float));*/
}

/*
 * Arguments    : c_fusion_internal_coder_insfilt *obj
 *                const float mag[3]
 * Return Type  : void
 */
void AsyncMARGGPSFuserBase_fusemag(c_fusion_internal_coder_insfilt *obj, const
  float mag[3])
{
  float val[28];
  float b_val[28];
  float mx_tmp;
  float b_mx_tmp;
  float dhdx_tmp;
  float b_dhdx_tmp;
  float c_dhdx_tmp;
  float d_dhdx_tmp;
  float e_dhdx_tmp;
  float dhdx[84];
  float f_dhdx_tmp;
  float g_dhdx_tmp;
  int i;
  /*float P[784];*/
  int i1;
  float b_tmp[84];
  int y_tmp_tmp;
  float c_val[3];
  float x_tmp[84];
  float y_tmp[9];
  float b_fv[3];
  float b_y_tmp[84];
  static const float measNoise[9] = { 1.06685019, 0.0259414557, 0.0883291587,
    0.0259414557, 1.04068804, 0.0729869083, 0.0883291587, 0.0729869083,
    1.07279086};

  memcpy(&val[0], &obj->pState[0], 28U * sizeof(float));
  memcpy(&b_val[0], &obj->pState[0], 28U * sizeof(float));
  mx_tmp = 2.0* val[0];
  b_mx_tmp = 2.0* val[1];
  dhdx_tmp = 2.0* b_val[24];
  b_dhdx_tmp = 2.0* b_val[23];
  c_dhdx_tmp = 2.0 * b_val[22];
  d_dhdx_tmp = 2.0 * b_val[1];
  e_dhdx_tmp = 2.0 * b_val[0];
  dhdx[0] = (b_dhdx_tmp * b_val[3] - dhdx_tmp * b_val[2]) + c_dhdx_tmp * b_val[0];
  dhdx[3] = (dhdx_tmp * b_val[3] + b_dhdx_tmp * b_val[2]) + c_dhdx_tmp * b_val[1];
  dhdx[6] = (b_dhdx_tmp * b_val[1] - dhdx_tmp * b_val[0]) - c_dhdx_tmp * b_val[2];
  dhdx[9] = (dhdx_tmp * b_val[1] + b_dhdx_tmp * b_val[0]) - c_dhdx_tmp * b_val[3];
  dhdx[12] = 0.0;
  dhdx[15] = 0.0;
  dhdx[18] = 0.0;
  dhdx[21] = 0.0;
  dhdx[24] = 0.0;
  dhdx[27] = 0.0;
  dhdx[30] = 0.0;
  dhdx[33] = 0.0;
  dhdx[36] = 0.0;
  dhdx[39] = 0.0;
  dhdx[42] = 0.0;
  dhdx[45] = 0.0;
  dhdx[48] = 0.0;
  dhdx[51] = 0.0;
  dhdx[54] = 0.0;
  dhdx[57] = 0.0;
  dhdx[60] = 0.0;
  dhdx[63] = 0.0;
  dhdx_tmp = b_val[0] * b_val[0];
  b_dhdx_tmp = b_val[1] * b_val[1];
  c_dhdx_tmp = b_val[2] * b_val[2];
  f_dhdx_tmp = b_val[3] * b_val[3];
  dhdx[66] = ((dhdx_tmp + b_dhdx_tmp) - c_dhdx_tmp) - f_dhdx_tmp;
  dhdx[69] = e_dhdx_tmp * b_val[3] + d_dhdx_tmp * b_val[2];
  dhdx[72] = d_dhdx_tmp * b_val[3] - e_dhdx_tmp * b_val[2];
  dhdx[75] = 1.0;
  dhdx[78] = 0.0;
  dhdx[81] = 0.0;
  dhdx[1] = (2.0 * b_val[24] * b_val[1] + 2.0 * b_val[23] * b_val[0]) - 2.0 *
    b_val[22] * b_val[3];
  d_dhdx_tmp = (2.0 * b_val[24] * b_val[0] - 2.0 * b_val[23] * b_val[1]) +
    2.0 * b_val[22] * b_val[2];
  dhdx[4] = d_dhdx_tmp;
  g_dhdx_tmp = (2.0 * b_val[24] * b_val[3] + 2.0 * b_val[23] * b_val[2]) +
    2.0 * b_val[22] * b_val[1];
  dhdx[7] = g_dhdx_tmp;
  dhdx[10] = (2.0 * b_val[24] * b_val[2] - 2.0 * b_val[23] * b_val[3]) - 2.0 *
    b_val[22] * b_val[0];
  dhdx[13] = 0.0;
  dhdx[16] = 0.0;
  dhdx[19] = 0.0;
  dhdx[22] = 0.0;
  dhdx[25] = 0.0;
  dhdx[28] = 0.0;
  dhdx[31] = 0.0;
  dhdx[34] = 0.0;
  dhdx[37] = 0.0;
  dhdx[40] = 0.0;
  dhdx[43] = 0.0;
  dhdx[46] = 0.0;
  dhdx[49] = 0.0;
  dhdx[52] = 0.0;
  dhdx[55] = 0.0;
  dhdx[58] = 0.0;
  dhdx[61] = 0.0;
  dhdx[64] = 0.0;
  dhdx[67] = 2.0 * b_val[1] * b_val[2] - 2.0 * b_val[0] * b_val[3];
  dhdx_tmp -= b_dhdx_tmp;
  dhdx[70] = (dhdx_tmp + c_dhdx_tmp) - f_dhdx_tmp;
  b_dhdx_tmp = 2.0 * b_val[2] * b_val[3];
  dhdx[73] = e_dhdx_tmp * b_val[1] + b_dhdx_tmp;
  dhdx[76] = 0.0;
  dhdx[79] = 1.0;
  dhdx[82] = 0.0;
  dhdx[2] = d_dhdx_tmp;
  dhdx[5] = (2.0 * b_val[22] * b_val[3] - 2.0 * b_val[23] * b_val[0]) - 2.0 *
    b_val[24] * b_val[1];
  dhdx[8] = (2.0 * b_val[23] * b_val[3] - 2.0 * b_val[24] * b_val[2]) + 2.0 *
    b_val[22] * b_val[0];
  dhdx[11] = g_dhdx_tmp;
  dhdx[14] = 0.0;
  dhdx[17] = 0.0;
  dhdx[20] = 0.0;
  dhdx[23] = 0.0;
  dhdx[26] = 0.0;
  dhdx[29] = 0.0;
  dhdx[32] = 0.0;
  dhdx[35] = 0.0;
  dhdx[38] = 0.0;
  dhdx[41] = 0.0;
  dhdx[44] = 0.0;
  dhdx[47] = 0.0;
  dhdx[50] = 0.0;
  dhdx[53] = 0.0;
  dhdx[56] = 0.0;
  dhdx[59] = 0.0;
  dhdx[62] = 0.0;
  dhdx[65] = 0.0;
  dhdx[68] = 2.0 * b_val[0] * b_val[2] + 2.0 * b_val[1] * b_val[3];
  dhdx[71] = b_dhdx_tmp - 2.0 * b_val[0] * b_val[1];
  dhdx[74] = (dhdx_tmp - c_dhdx_tmp) + f_dhdx_tmp;
  dhdx[77] = 0.0;
  dhdx[80] = 0.0;
  dhdx[83] = 1.0;
  memcpy(&b_val[0], &obj->pState[0], 28U * sizeof(float));
  /*for (i = 0; i < 784; i++) {
    P[i] = obj->StateCovariance[i];
  }*/

  for (i = 0; i < 3; i++) {
    for (i1 = 0; i1 < 28; i1++) {
      b_tmp[i1 + 28 * i] = dhdx[i + 3 * i1];
    }
  }

  for (i = 0; i < 28; i++) {
    for (i1 = 0; i1 < 3; i1++) {
      b_dhdx_tmp = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        b_dhdx_tmp += obj->StateCovariance[i + 28 * y_tmp_tmp] * b_tmp[y_tmp_tmp + 28 * i1];
      }

      x_tmp[i + 28 * i1] = b_dhdx_tmp;
    }
  }

  dhdx_tmp = val[0] * val[0];
  b_dhdx_tmp = val[1] * val[1];
  c_dhdx_tmp = val[2] * val[2];
  d_dhdx_tmp = val[3] * val[3];
  c_val[0] = ((val[25] + val[22] * (((dhdx_tmp + b_dhdx_tmp) - c_dhdx_tmp) -
    d_dhdx_tmp)) - val[24] * (mx_tmp * val[2] - b_mx_tmp * val[3])) + val[23] *
    (mx_tmp * val[3] + b_mx_tmp * val[2]);
  dhdx_tmp -= b_dhdx_tmp;
  b_dhdx_tmp = 2.0 * val[2] * val[3];
  c_val[1] = ((val[26] + val[23] * ((dhdx_tmp + c_dhdx_tmp) - d_dhdx_tmp)) +
              val[24] * (mx_tmp * val[1] + b_dhdx_tmp)) - val[22] * (2.0 * val
    [0] * val[3] - 2.0 * val[1] * val[2]);
  c_val[2] = ((val[27] + val[24] * ((dhdx_tmp - c_dhdx_tmp) + d_dhdx_tmp)) -
              val[23] * (2.0 * val[0] * val[1] - b_dhdx_tmp)) + val[22] * (2.0
    * val[0] * val[2] + 2.0 * val[1] * val[3]);
  for (i = 0; i < 3; i++) {
    for (i1 = 0; i1 < 28; i1++) {
      b_dhdx_tmp = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        b_dhdx_tmp += dhdx[i + 3 * y_tmp_tmp] * obj->StateCovariance[y_tmp_tmp + 28 * i1];
      }

      b_y_tmp[i + 3 * i1] = b_dhdx_tmp;
    }

    for (i1 = 0; i1 < 3; i1++) {
      b_dhdx_tmp = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        b_dhdx_tmp += b_y_tmp[i + 3 * y_tmp_tmp] * b_tmp[y_tmp_tmp + 28 * i1];
      }

      y_tmp_tmp = i + 3 * i1;
      y_tmp[y_tmp_tmp] = b_dhdx_tmp + measNoise[y_tmp_tmp];
    }

    c_val[i] = mag[i] - c_val[i];
  }

  mldivide(y_tmp, c_val, b_fv);
  for (i = 0; i < 28; i++) {
    obj->pState[i] = b_val[i] + ((x_tmp[i] * b_fv[0] + x_tmp[i + 28] * b_fv[1])
      + x_tmp[i + 56] * b_fv[2]);
  }

  for (i = 0; i < 3; i++) {
    for (i1 = 0; i1 < 3; i1++) {
      b_dhdx_tmp = 0.0;
      for (y_tmp_tmp = 0; y_tmp_tmp < 28; y_tmp_tmp++) {
        b_dhdx_tmp += b_y_tmp[i + 3 * y_tmp_tmp] * b_tmp[y_tmp_tmp + 28 * i1];
      }

      y_tmp_tmp = i + 3 * i1;
      y_tmp[y_tmp_tmp] = b_dhdx_tmp + measNoise[y_tmp_tmp];
    }
  }

  b_mldivide(y_tmp, b_y_tmp, dhdx);
  for (i = 0; i < 28; i++) {
    b_dhdx_tmp = x_tmp[i + 28];
    dhdx_tmp = x_tmp[i + 56];
    for (i1 = 0; i1 < 28; i1++) {
      y_tmp_tmp = i + 28 * i1;
	  obj->StateCovariance[y_tmp_tmp] -= (x_tmp[i] * dhdx[3 * i1] + b_dhdx_tmp * dhdx[3 * i1 + 1])
        + dhdx_tmp * dhdx[3 * i1 + 2];
    }
  }

  IMUBasicEKF_repairQuaternion(obj->pState);
  /*memcpy(&obj->StateCovariance[0], &P[0], 784U * sizeof(float));*/
}

/*
 * Arguments    : c_fusion_internal_coder_insfilt *obj
 * Return Type  : void
 */
void AsyncMARGGPSFuserBase_predict(c_fusion_internal_coder_insfilt *obj)
{
  double addProcNoise[28];
  int j;
  float dfdx_tmp;
  float xdot[28];
  float b_dfdx_tmp;
  float val[28];
  float c_dfdx_tmp;
  float dfdx[784];
  float d_dfdx_tmp;
  float e_dfdx_tmp;
  float f_dfdx_tmp;
  static const signed char iv[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  static const signed char iv1[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  static const signed char iv2[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  static const signed char iv3[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  static const signed char iv4[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  static const signed char iv5[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  float g_dfdx_tmp;
  float h_dfdx_tmp;
  float i_dfdx_tmp;
  float j_dfdx_tmp;
  int i;
  int i1;
  float Pdot[784];
//  float b_obj[784];
  int Pdot_tmp;
  int i2;
  addProcNoise[0] = obj->QuaternionNoise[0];
  addProcNoise[1] = obj->QuaternionNoise[1];
  addProcNoise[2] = obj->QuaternionNoise[2];
  addProcNoise[3] = obj->QuaternionNoise[3];
  addProcNoise[4] = obj->AngularVelocityNoise[0];
  addProcNoise[7] = obj->PositionNoise[0];
  addProcNoise[10] = obj->VelocityNoise[0];
  addProcNoise[13] = obj->AccelerationNoise[0];
  addProcNoise[16] = obj->AccelerometerBiasNoise[0];
  addProcNoise[19] = obj->GyroscopeBiasNoise[0];
  addProcNoise[22] = obj->GeomagneticVectorNoise[0];
  addProcNoise[25] = obj->MagnetometerBiasNoise[0];
  addProcNoise[5] = obj->AngularVelocityNoise[1];
  addProcNoise[8] = obj->PositionNoise[1];
  addProcNoise[11] = obj->VelocityNoise[1];
  addProcNoise[14] = obj->AccelerationNoise[1];
  addProcNoise[17] = obj->AccelerometerBiasNoise[1];
  addProcNoise[20] = obj->GyroscopeBiasNoise[1];
  addProcNoise[23] = obj->GeomagneticVectorNoise[1];
  addProcNoise[26] = obj->MagnetometerBiasNoise[1];
  addProcNoise[6] = obj->AngularVelocityNoise[2];
  addProcNoise[9] = obj->PositionNoise[2];
  addProcNoise[12] = obj->VelocityNoise[2];
  addProcNoise[15] = obj->AccelerationNoise[2];
  addProcNoise[18] = obj->AccelerometerBiasNoise[2];
  addProcNoise[21] = obj->GyroscopeBiasNoise[2];
  addProcNoise[24] = obj->GeomagneticVectorNoise[2];
  addProcNoise[27] = obj->MagnetometerBiasNoise[2];
  for (j = 0; j < 28; j++) {
    xdot[j] = obj->pState[j];
    val[j] = obj->pState[j];
    dfdx[28 * j + 4] = 0.0;
    dfdx[28 * j + 5] = 0.0;
    dfdx[28 * j + 6] = 0.0;
    dfdx[28 * j + 7] = iv[j];
    dfdx[28 * j + 8] = iv1[j];
    dfdx[28 * j + 9] = iv2[j];
    dfdx[28 * j + 10] = iv3[j];
    dfdx[28 * j + 11] = iv4[j];
    dfdx[28 * j + 12] = iv5[j];
    dfdx[28 * j + 13] = 0.0;
    dfdx[28 * j + 14] = 0.0;
    dfdx[28 * j + 15] = 0.0;
    dfdx[28 * j + 16] = 0.0;
    dfdx[28 * j + 17] = 0.0;
    dfdx[28 * j + 18] = 0.0;
    dfdx[28 * j + 19] = 0.0;
    dfdx[28 * j + 20] = 0.0;
    dfdx[28 * j + 21] = 0.0;
    dfdx[28 * j + 22] = 0.0;
    dfdx[28 * j + 23] = 0.0;
    dfdx[28 * j + 24] = 0.0;
    dfdx[28 * j + 25] = 0.0;
    dfdx[28 * j + 26] = 0.0;
    dfdx[28 * j + 27] = 0.0;
  }

  dfdx_tmp = xdot[0];
  b_dfdx_tmp = xdot[0];
  c_dfdx_tmp = xdot[1];
  d_dfdx_tmp = xdot[1];
  e_dfdx_tmp = xdot[2];
  f_dfdx_tmp = xdot[0];
  xdot[0] = (-(xdot[1] * xdot[4]) / 2.0 - xdot[2] * xdot[5] / 2.0) - xdot[3] *
    xdot[6] / 2.0;
  xdot[1] = (dfdx_tmp * xdot[4] / 2.0 - xdot[3] * xdot[5] / 2.0) + xdot[2] *
    xdot[6] / 2.0;
  xdot[2] = (xdot[3] * xdot[4] / 2.0 + b_dfdx_tmp * xdot[5] / 2.0) -
    c_dfdx_tmp * xdot[6] / 2.0;
  xdot[3] = (d_dfdx_tmp * xdot[5] / 2.0 - e_dfdx_tmp * xdot[4] / 2.0) +
    f_dfdx_tmp * xdot[6] / 2.0;
  xdot[4] = 0.0;
  xdot[5] = 0.0;
  xdot[6] = 0.0;
  xdot[7] = xdot[10];
  xdot[8] = xdot[11];
  xdot[9] = xdot[12];
  xdot[10] = xdot[13];
  xdot[11] = xdot[14];
  xdot[12] = xdot[15];
  xdot[13] = 0.0;
  xdot[14] = 0.0;
  xdot[15] = 0.0;
  xdot[16] = 0.0;
  xdot[17] = 0.0;
  xdot[18] = 0.0;
  xdot[19] = 0.0;
  xdot[20] = 0.0;
  xdot[21] = 0.0;
  xdot[22] = 0.0;
  xdot[23] = 0.0;
  xdot[24] = 0.0;
  xdot[25] = 0.0;
  xdot[26] = 0.0;
  xdot[27] = 0.0;
  dfdx_tmp = -val[5] / 2.0;
  b_dfdx_tmp = -val[3] / 2.0;
  c_dfdx_tmp = -val[6] / 2.0;
  d_dfdx_tmp = val[4] / 2.0;
  e_dfdx_tmp = val[0] / 2.0;
  f_dfdx_tmp = -val[1] / 2.0;
  g_dfdx_tmp = val[6] / 2.0;
  h_dfdx_tmp = val[5] / 2.0;
  i_dfdx_tmp = -val[4] / 2.0;
  j_dfdx_tmp = -val[2] / 2.0;
  dfdx[0] = 0.0;
  dfdx[28] = i_dfdx_tmp;
  dfdx[56] = dfdx_tmp;
  dfdx[84] = c_dfdx_tmp;
  dfdx[112] = f_dfdx_tmp;
  dfdx[140] = j_dfdx_tmp;
  dfdx[168] = b_dfdx_tmp;
  dfdx[196] = 0.0;
  dfdx[224] = 0.0;
  dfdx[252] = 0.0;
  dfdx[280] = 0.0;
  dfdx[308] = 0.0;
  dfdx[336] = 0.0;
  dfdx[364] = 0.0;
  dfdx[392] = 0.0;
  dfdx[420] = 0.0;
  dfdx[448] = 0.0;
  dfdx[476] = 0.0;
  dfdx[504] = 0.0;
  dfdx[532] = 0.0;
  dfdx[560] = 0.0;
  dfdx[588] = 0.0;
  dfdx[616] = 0.0;
  dfdx[644] = 0.0;
  dfdx[672] = 0.0;
  dfdx[700] = 0.0;
  dfdx[728] = 0.0;
  dfdx[756] = 0.0;
  dfdx[1] = d_dfdx_tmp;
  dfdx[29] = 0.0;
  dfdx[57] = g_dfdx_tmp;
  dfdx[85] = dfdx_tmp;
  dfdx[113] = e_dfdx_tmp;
  dfdx[141] = b_dfdx_tmp;
  dfdx[169] = val[2] / 2.0;
  dfdx[197] = 0.0;
  dfdx[225] = 0.0;
  dfdx[253] = 0.0;
  dfdx[281] = 0.0;
  dfdx[309] = 0.0;
  dfdx[337] = 0.0;
  dfdx[365] = 0.0;
  dfdx[393] = 0.0;
  dfdx[421] = 0.0;
  dfdx[449] = 0.0;
  dfdx[477] = 0.0;
  dfdx[505] = 0.0;
  dfdx[533] = 0.0;
  dfdx[561] = 0.0;
  dfdx[589] = 0.0;
  dfdx[617] = 0.0;
  dfdx[645] = 0.0;
  dfdx[673] = 0.0;
  dfdx[701] = 0.0;
  dfdx[729] = 0.0;
  dfdx[757] = 0.0;
  dfdx[2] = h_dfdx_tmp;
  dfdx[30] = c_dfdx_tmp;
  dfdx[58] = 0.0;
  dfdx[86] = d_dfdx_tmp;
  dfdx[114] = val[3] / 2.0;
  dfdx[142] = e_dfdx_tmp;
  dfdx[170] = f_dfdx_tmp;
  dfdx[198] = 0.0;
  dfdx[226] = 0.0;
  dfdx[254] = 0.0;
  dfdx[282] = 0.0;
  dfdx[310] = 0.0;
  dfdx[338] = 0.0;
  dfdx[366] = 0.0;
  dfdx[394] = 0.0;
  dfdx[422] = 0.0;
  dfdx[450] = 0.0;
  dfdx[478] = 0.0;
  dfdx[506] = 0.0;
  dfdx[534] = 0.0;
  dfdx[562] = 0.0;
  dfdx[590] = 0.0;
  dfdx[618] = 0.0;
  dfdx[646] = 0.0;
  dfdx[674] = 0.0;
  dfdx[702] = 0.0;
  dfdx[730] = 0.0;
  dfdx[758] = 0.0;
  dfdx[3] = g_dfdx_tmp;
  dfdx[31] = h_dfdx_tmp;
  dfdx[59] = i_dfdx_tmp;
  dfdx[87] = 0.0;
  dfdx[115] = j_dfdx_tmp;
  dfdx[143] = val[1] / 2.0;
  dfdx[171] = e_dfdx_tmp;
  dfdx[199] = 0.0;
  dfdx[227] = 0.0;
  dfdx[255] = 0.0;
  dfdx[283] = 0.0;
  dfdx[311] = 0.0;
  dfdx[339] = 0.0;
  dfdx[367] = 0.0;
  dfdx[395] = 0.0;
  dfdx[423] = 0.0;
  dfdx[451] = 0.0;
  dfdx[479] = 0.0;
  dfdx[507] = 0.0;
  dfdx[535] = 0.0;
  dfdx[563] = 0.0;
  dfdx[591] = 0.0;
  dfdx[619] = 0.0;
  dfdx[647] = 0.0;
  dfdx[675] = 0.0;
  dfdx[703] = 0.0;
  dfdx[731] = 0.0;
  dfdx[759] = 0.0;
  for (i = 0; i < 28; i++) {
    for (i1 = 0; i1 < 28; i1++) {
      j = i + 28 * i1;
//      b_obj[j] = 0.0;
      dfdx_tmp = 0.0;
      b_dfdx_tmp = 0.0;
      for (Pdot_tmp = 0; Pdot_tmp < 28; Pdot_tmp++) {
        i2 = i + 28 * Pdot_tmp;
        dfdx_tmp += dfdx[i2] * obj->StateCovariance[Pdot_tmp + 28 * i1];
        b_dfdx_tmp += obj->StateCovariance[i2] * dfdx[i1 + 28 * Pdot_tmp];
      }

 //     b_obj[j] = b_dfdx_tmp;
   //   Pdot[j] = dfdx_tmp;
			   Pdot[j] = dfdx_tmp + b_dfdx_tmp;
    }
  }

 /* for (i = 0; i < 784; i++) {
    Pdot[i] += b_obj[i];
  }*/

  for (j = 0; j < 28; j++) {
    Pdot_tmp = j + 28 * j;
    Pdot[Pdot_tmp] += (float)addProcNoise[j];
  }

  for (i = 0; i < 28; i++) {
    for (i1 = 0; i1 < 28; i1++) {
      j = i1 + 28 * i;
      dfdx[j] = 0.5 * (Pdot[j] + Pdot[i + 28 * i1]);
    }
  }

  memcpy(&Pdot[0], &dfdx[0], 784U * sizeof(float));
  for (i = 0; i < 28; i++) {
    obj->pState[i] += xdot[i] * 0.1;
  }

  for (i = 0; i < 784; i++) {
    obj->StateCovariance[i] += Pdot[i] * 0.1;
  }

  IMUBasicEKF_repairQuaternion(obj->pState);
}

/*
 * File trailer for AsyncMARGGPSFuserBase.c
 *
 * [EOF]
 */
