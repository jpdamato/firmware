/*
 * File: IMUBasicEKF.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "IMUBasicEKF.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "xgetrf.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : float x[28]
 *                float P[784]
 *                const float h[6]
 *                const double H[168]
 *                const float z[6]
 *                const float R[36]
 * Return Type  : void
 */
void IMUBasicEKF_correctEqn(float x[28], float P[784], const float h[6], const
  double H[168], const float z[6], const float R[36])
{
  int i;
  int i1;
  int b_i;
  float b_tmp[168];
  float A[36];
  int ipiv[6];
  int A_tmp;
  float temp;
  float B[6];
  float x_tmp[168];
  int kAcol;
  float y_tmp[168];
  int temp_tmp;
  int jBcol;
  int y_tmp_tmp;
  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 28; i1++) {
      b_tmp[i1 + 28 * i] = (float)H[i + 6 * i1];
    }
  }

  for (i = 0; i < 28; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      temp = 0.0F;
      for (A_tmp = 0; A_tmp < 28; A_tmp++) {
        temp += P[i + 28 * A_tmp] * b_tmp[A_tmp + 28 * i1];
      }

      x_tmp[i + 28 * i1] = temp;
    }
  }

  for (b_i = 0; b_i < 6; b_i++) {
    for (i = 0; i < 28; i++) {
      temp = 0.0F;
      for (i1 = 0; i1 < 28; i1++) {
        temp += (float)H[b_i + 6 * i1] * P[i1 + 28 * i];
      }

      y_tmp[b_i + 6 * i] = temp;
    }

    B[b_i] = z[b_i] - h[b_i];
    for (i = 0; i < 6; i++) {
      temp = 0.0F;
      for (i1 = 0; i1 < 28; i1++) {
        temp += y_tmp[b_i + 6 * i1] * (float)b_tmp[i1 + 28 * i];
      }

      A_tmp = b_i + 6 * i;
      A[A_tmp] = temp + R[A_tmp];
    }
  }

  xgetrf(A, ipiv, &A_tmp);
  for (A_tmp = 0; A_tmp < 5; A_tmp++) {
    if (ipiv[A_tmp] != A_tmp + 1) {
      temp = B[A_tmp];
      B[A_tmp] = B[ipiv[A_tmp] - 1];
      B[ipiv[A_tmp] - 1] = temp;
    }
  }

  for (A_tmp = 0; A_tmp < 6; A_tmp++) {
    kAcol = 6 * A_tmp;
    if (B[A_tmp] != 0.0F) {
      i = A_tmp + 2;
      for (b_i = i; b_i < 7; b_i++) {
        B[b_i - 1] -= B[A_tmp] * A[(b_i + kAcol) - 1];
      }
    }
  }

  for (A_tmp = 5; A_tmp >= 0; A_tmp--) {
    kAcol = 6 * A_tmp;
    if (B[A_tmp] != 0.0F) {
      B[A_tmp] /= A[A_tmp + kAcol];
      for (b_i = 0; b_i < A_tmp; b_i++) {
        B[b_i] -= B[A_tmp] * A[b_i + kAcol];
      }
    }
  }

  for (i = 0; i < 28; i++) {
    temp = 0.0F;
    for (i1 = 0; i1 < 6; i1++) {
      temp += x_tmp[i + 28 * i1] * B[i1];
    }

    x[i] += temp;
  }

  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      temp = 0.0F;
      for (A_tmp = 0; A_tmp < 28; A_tmp++) {
        temp += y_tmp[i + 6 * A_tmp] * (float)b_tmp[A_tmp + 28 * i1];
      }

      A_tmp = i + 6 * i1;
      A[A_tmp] = temp + R[A_tmp];
    }
  }

  xgetrf(A, ipiv, &A_tmp);
  for (A_tmp = 0; A_tmp < 5; A_tmp++) {
    if (ipiv[A_tmp] != A_tmp + 1) {
      for (kAcol = 0; kAcol < 28; kAcol++) {
        temp_tmp = A_tmp + 6 * kAcol;
        temp = y_tmp[temp_tmp];
        y_tmp_tmp = (ipiv[A_tmp] + 6 * kAcol) - 1;
        y_tmp[temp_tmp] = y_tmp[y_tmp_tmp];
        y_tmp[y_tmp_tmp] = temp;
      }
    }
  }

  for (temp_tmp = 0; temp_tmp < 28; temp_tmp++) {
    jBcol = 6 * temp_tmp;
    for (A_tmp = 0; A_tmp < 6; A_tmp++) {
      kAcol = 6 * A_tmp;
      i = A_tmp + jBcol;
      if (y_tmp[i] != 0.0F) {
        i1 = A_tmp + 2;
        for (b_i = i1; b_i < 7; b_i++) {
          y_tmp_tmp = (b_i + jBcol) - 1;
          y_tmp[y_tmp_tmp] -= y_tmp[i] * A[(b_i + kAcol) - 1];
        }
      }
    }
  }

  for (temp_tmp = 0; temp_tmp < 28; temp_tmp++) {
    jBcol = 6 * temp_tmp;
    for (A_tmp = 5; A_tmp >= 0; A_tmp--) {
      kAcol = 6 * A_tmp;
      i = A_tmp + jBcol;
      if (y_tmp[i] != 0.0F) {
        y_tmp[i] /= A[A_tmp + kAcol];
        for (b_i = 0; b_i < A_tmp; b_i++) {
          y_tmp_tmp = b_i + jBcol;
          y_tmp[y_tmp_tmp] -= y_tmp[i] * A[b_i + kAcol];
        }
      }
    }
  }

  for (i = 0; i < 28; i++) {
    for (i1 = 0; i1 < 28; i1++) {
      temp = 0.0F;
      for (A_tmp = 0; A_tmp < 6; A_tmp++) {
        temp += x_tmp[i + 28 * A_tmp] * y_tmp[A_tmp + 6 * i1];
      }

      A_tmp = i + 28 * i1;
      P[A_tmp] -= temp;
    }
  }

  IMUBasicEKF_repairQuaternion(x);
}

/*
 * Arguments    : float x[28]
 * Return Type  : void
 */
void IMUBasicEKF_repairQuaternion(float x[28])
{
  float qparts_idx_3;
  float qparts_idx_0;
  float qparts_idx_1;
  float qparts_idx_2;
  qparts_idx_3 = sqrtf(((x[0] * x[0] + x[1] * x[1]) + x[2] * x[2]) + x[3] * x[3]);
  qparts_idx_0 = x[0] / qparts_idx_3;
  qparts_idx_1 = x[1] / qparts_idx_3;
  qparts_idx_2 = x[2] / qparts_idx_3;
  qparts_idx_3 = x[3] / qparts_idx_3;
  if (qparts_idx_0 < 0.0F) {
    x[0] = -qparts_idx_0;
    x[1] = -qparts_idx_1;
    x[2] = -qparts_idx_2;
    x[3] = -qparts_idx_3;
  } else {
    x[0] = qparts_idx_0;
    x[1] = qparts_idx_1;
    x[2] = qparts_idx_2;
    x[3] = qparts_idx_3;
  }
}

/*
 * File trailer for IMUBasicEKF.c
 *
 * [EOF]
 */
