/*
 * File: calibrarGPScoder_initialize.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "calibrarGPScoder_initialize.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void calibrarGPScoder_initialize(void)
{
}

/*
 * File trailer for calibrarGPScoder_initialize.c
 *
 * [EOF]
 */
