/*
 * File: quaternion2Eul.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 04-Dec-2019 01:07:12
 */

/* Include Files */
#include "quaternion2Eul.h"
#include <math.h>
#include <string.h>

/* Function Declarations */
static double rt_roundd(double u);

/* Function Definitions */

/*
 * Arguments    : double u
 * Return Type  : double
 */
static double rt_roundd(double u)
{
  double y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/*
 * QUATERNION2EUL Devuelve los angulos de Euler a partir del quaternion q
 * Arguments    : const double q1[4]
 *                short eul[3]
 * Return Type  : void
 */
void quaternion2Eul(const double q1[4], short eul[3])
{
  double b;
  double normRowMatrix_idx_3;
  double sy;
  double eulShaped_idx_0;
  double normRowMatrix_idx_1;
  double eulShaped_idx_1;
  double normRowMatrix_idx_2;
  double eulShaped_idx_2;
  double matrix_idx_0;
  double matrix_idx_1;
  double matrix_idx_2;
  double tempR[9];
  double R[9];
  int k;
  int R_tmp;
  double y_data[1];
  double r_data[1];
  b = 1.0 / sqrt(((q1[0] * q1[0] + q1[1] * q1[1]) + q1[2] * q1[2]) + q1[3] * q1
                 [3]);
  normRowMatrix_idx_3 = q1[0] * b;
  sy = normRowMatrix_idx_3;
  eulShaped_idx_0 = normRowMatrix_idx_3 * normRowMatrix_idx_3;
  normRowMatrix_idx_3 = q1[1] * b;
  normRowMatrix_idx_1 = normRowMatrix_idx_3;
  eulShaped_idx_1 = normRowMatrix_idx_3 * normRowMatrix_idx_3;
  normRowMatrix_idx_3 = q1[2] * b;
  normRowMatrix_idx_2 = normRowMatrix_idx_3;
  eulShaped_idx_2 = normRowMatrix_idx_3 * normRowMatrix_idx_3;
  normRowMatrix_idx_3 = q1[3] * b;
  b = 1.0 / sqrt(((eulShaped_idx_0 + eulShaped_idx_1) + eulShaped_idx_2) +
                 normRowMatrix_idx_3 * normRowMatrix_idx_3);
  matrix_idx_0 = sy * b;
  matrix_idx_1 = normRowMatrix_idx_1 * b;
  matrix_idx_2 = normRowMatrix_idx_2 * b;
  sy = normRowMatrix_idx_3 * b;
  eulShaped_idx_0 = sy * sy;
  eulShaped_idx_1 = matrix_idx_2 * matrix_idx_2;
  tempR[0] = 1.0 - 2.0 * (eulShaped_idx_1 + eulShaped_idx_0);
  eulShaped_idx_2 = matrix_idx_1 * matrix_idx_2;
  normRowMatrix_idx_1 = matrix_idx_0 * sy;
  tempR[1] = 2.0 * (eulShaped_idx_2 - normRowMatrix_idx_1);
  normRowMatrix_idx_2 = matrix_idx_1 * sy;
  b = matrix_idx_0 * matrix_idx_2;
  tempR[2] = 2.0 * (normRowMatrix_idx_2 + b);
  tempR[3] = 2.0 * (eulShaped_idx_2 + normRowMatrix_idx_1);
  eulShaped_idx_2 = matrix_idx_1 * matrix_idx_1;
  tempR[4] = 1.0 - 2.0 * (eulShaped_idx_2 + eulShaped_idx_0);
  eulShaped_idx_0 = matrix_idx_2 * sy;
  normRowMatrix_idx_1 = matrix_idx_0 * matrix_idx_1;
  tempR[5] = 2.0 * (eulShaped_idx_0 - normRowMatrix_idx_1);
  tempR[6] = 2.0 * (normRowMatrix_idx_2 - b);
  tempR[7] = 2.0 * (eulShaped_idx_0 + normRowMatrix_idx_1);
  tempR[8] = 1.0 - 2.0 * (eulShaped_idx_2 + eulShaped_idx_1);
  memcpy(&R[0], &tempR[0], 9U * sizeof(double));
  for (k = 0; k < 3; k++) {
    R_tmp = 3 * k;
    R[k] = tempR[R_tmp];
    R[k + 3] = tempR[R_tmp + 1];
    R[k + 6] = tempR[R_tmp + 2];
  }

  sy = sqrt(R[5] * R[5] + R[2] * R[2]);
  eulShaped_idx_0 = atan2(R[5], R[2]);
  eulShaped_idx_1 = atan2(sy, R[8]);
  eulShaped_idx_2 = atan2(R[7], -R[6]);
  if (sy < 2.2204460492503131E-15) {
    y_data[0] = -R[1];
    r_data[0] = atan2(y_data[0], R[4]);
    y_data[0] = eulShaped_idx_1;
    for (k = 0; k < 1; k++) {
      eulShaped_idx_0 = r_data[0];
    }

    for (k = 0; k < 1; k++) {
      eulShaped_idx_1 = y_data[0];
    }

    eulShaped_idx_2 = 0.0;
  }

  eulShaped_idx_0 = -eulShaped_idx_0;
  eulShaped_idx_1 = -eulShaped_idx_1;
  eulShaped_idx_2 = -eulShaped_idx_2;

  /*  RAD2DEG  Converts radians to decimal degrees. Vectorized. */
  /*  Version: 8 Mar 00 */
  /*  Usage:   deg=rad2deg(rad) */
  /*  Input:   rad - vector of angles in radians */
  /*  Output:  deg - vector of angles in decimal degrees */
  /*  Copyright (c) 2011, Michael R. Craymer */
  /*  All rights reserved. */
  /*  Email: mike@craymer.com */
  /* ind=(deg<0); */
  /* deg(ind)=deg(ind)+360; */
  eul[0] = (short)rt_roundd(eulShaped_idx_2 * 180.0 / 3.1415926535897931);
  eul[1] = (short)rt_roundd(eulShaped_idx_1 * 180.0 / 3.1415926535897931);
  eul[2] = (short)rt_roundd(eulShaped_idx_0 * 180.0 / 3.1415926535897931);
}

/*
 * File trailer for quaternion2Eul.c
 *
 * [EOF]
 */
