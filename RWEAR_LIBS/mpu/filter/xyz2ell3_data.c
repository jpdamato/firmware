/*
 * File: xyz2ell3_data.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 05-Nov-2019 00:26:40
 */

/* Include Files */
#include "xyz2ell3_data.h"
#include "quaternion2Eul.h"
#include "rt_nonfinite.h"
#include "xyz2ell3.h"

/* Variable Definitions */
boolean_T isInitialized_xyz2ell3 = false;

/*
 * File trailer for xyz2ell3_data.c
 *
 * [EOF]
 */
