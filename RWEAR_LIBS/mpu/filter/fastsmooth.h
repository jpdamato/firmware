/*
 * File: fastsmooth.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef FASTSMOOTH_H
#define FASTSMOOTH_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern void b_fastsmooth(const float Y[10], unsigned char w, float SmoothY[10]);
extern void fastsmooth(const double Y[10], unsigned char w, double SmoothY[10]);

#endif

/*
 * File trailer for fastsmooth.h
 *
 * [EOF]
 */
