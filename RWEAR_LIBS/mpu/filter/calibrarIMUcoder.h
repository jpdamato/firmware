/*
 * File: calibrarIMUcoder.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef CALIBRARIMUCODER_H
#define CALIBRARIMUCODER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern void calibrarIMUcoder(struct1_T *TIMU, unsigned char window, const float
  A[9], const float b[3], float accel[30], float gyro[30], float mag[30]);

#endif

/*
 * File trailer for calibrarIMUcoder.h
 *
 * [EOF]
 */
