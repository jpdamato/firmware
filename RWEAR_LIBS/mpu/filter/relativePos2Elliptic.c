/*
 * File: relativePos2Elliptic.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 03-Dec-2019 18:21:30
 */

/* Include Files */
#include "relativePos2Elliptic.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : double x
 *                double y
 *                double z
 *                const double ubicacionReferencia[3]
 *                double *lat
 *                double *lon
 *                double *h
 * Return Type  : void
 */
void relativePos2Elliptic(double x, double y, double z, const double
  ubicacionReferencia[3], double *lat, double *lon, double *h)
{
  double u;
  double X;
  double Y;
  double p;

  /*  ELL2XYZ  Convierte coordinadas elipsoideas a cartesianas */
  /*    Vectorized. */
  /*  Uso:     [x,y,z]=ell2xyz(lat,lon,h,a,e2) */
  /*           [x,y,z]=ell2xyz(lat,lon,h) */
  /*  Entradas: lat - latitudes (radianes) */
  /*            lon - longitudes (radianes) */
  /*            h   - alturas (m) */
  /*            a   - semi-eje mayor del elipsoide (m); default wgs84 */
  /*            e2  - excentricidad cuadratica del elipsoide; default wgs84 */
  /*  Salida:   x \ */
  /*            y  > vectores en coordenadas cartesianas (m) */
  /*            z / */
  /*  REFELL  Calcula parametros del elipsoide de referencia */
  /*  Uso:     [a,b,e2,finv]=refell(type) */
  /*  Entrada: type - tipo de elipsoide de referencia (char) */
  /*                  CLK66 = Clarke 1866 */
  /*                  GRS67 = Geodetic Reference System 1967 */
  /*                  GRS80 = Geodetic Reference System 1980 */
  /*                  WGS72 = World Geodetic System 1972 */
  /*                  WGS84 = World Geodetic System 1984 */
  /*                  ATS77 = Quasi-earth centred ellipsoid for ATS77 */
  /*                  NAD27 = North American Datum 1927 (=CLK66) */
  /*                  NAD83 = North American Datum 1927 (=GRS80) */
  /*                  INTER = International */
  /*                  KRASS = Krassovsky (USSR) */
  /*                  MAIRY = Modified Airy (Ireland 1965/1975) */
  /*                  TOPEX = TOPEX/POSEIDON ellipsoid */
  /*  Salida:  a    - semi-eje mayor (m) */
  /*           b    - semi-eje menor (m) */
  /*           e2   - cuadrado de la excentricidad */
  /*           finv - inversa del achatamiento */
  u = sin(ubicacionReferencia[0]);

  /* Si llegara a verse invertido, cambiar el signo + por -. (Creo que as� */
  /* deber�a andar bien). */
  u = (6.378137E+6 / sqrt(1.0 - 0.00669437999014133 * u * u) +
       ubicacionReferencia[2]) * cos(ubicacionReferencia[0]);
  X = x + u * cos(ubicacionReferencia[1]);
  Y = y + u * sin(ubicacionReferencia[1]);

  /*  XYZ2ELL3  Converts cartesian coordinates to ellipsoidal. */
  /*    Uses direct algorithm in B.R. Bowring, "The accuracy of */
  /*    geodetic latitude and height equations", Survey */
  /*    Review, v28 #218, October 1985, pp.202-206.  Vectorized. */
  /*     */
  /*  Usage:   [lat,lon,h]=xyz2ell3(X,Y,Z,type) */
  /*           [lat,lon,h]=xyz2ell3(X,Y,Z) */
  /*  Input:   X \ */
  /*           Y  > vectors of cartesian coordinates in CT system (m) */
  /*           Z / */
  /*           type = 'rad' or type = 'deg' */
  /*  Output:  lat - vector of ellipsoidal latitudes (radians) */
  /*           lon - vector of ellipsoidal longitudes (radians) */
  /*           h   - vector of ellipsoidal heights (m) */
  p = sqrt(X * X + Y * Y);
  u = atan(6.3567523142451793E+6 * z * (42841.311513313653 / sqrt(p * p + z * z)
            + 1.0) / (6.378137E+6 * p));
  *lat = atan((z + 42841.311513313653 * pow(sin(u), 3.0)) / (p -
    42697.672707180056 * pow(cos(u), 3.0)));
  u = sin(*lat);
  *h = (p * cos(*lat) + z * u) - 4.0680631590769E+13 / (6.378137E+6 / sqrt(1.0 -
    0.00669437999014133 * (u * u)));

  /* Height from center of the earth (I think...) */
  /*  RAD2DEG  Converts radians to decimal degrees. Vectorized. */
  /*  Version: 8 Mar 00 */
  /*  Usage:   deg=rad2deg(rad) */
  /*  Input:   rad - vector of angles in radians */
  /*  Output:  deg - vector of angles in decimal degrees */
  /*  Copyright (c) 2011, Michael R. Craymer */
  /*  All rights reserved. */
  /*  Email: mike@craymer.com */
  *lat = *lat * 180.0 / 3.1415926535897931;

  /* ind=(deg<0); */
  /* deg(ind)=deg(ind)+360; */
  /*  RAD2DEG  Converts radians to decimal degrees. Vectorized. */
  /*  Version: 8 Mar 00 */
  /*  Usage:   deg=rad2deg(rad) */
  /*  Input:   rad - vector of angles in radians */
  /*  Output:  deg - vector of angles in decimal degrees */
  /*  Copyright (c) 2011, Michael R. Craymer */
  /*  All rights reserved. */
  /*  Email: mike@craymer.com */
  *lon = atan2(Y, X) * 180.0 / 3.1415926535897931;

  /* ind=(deg<0); */
  /* deg(ind)=deg(ind)+360; */
}

/*
 * File trailer for relativePos2Elliptic.c
 *
 * [EOF]
 */
