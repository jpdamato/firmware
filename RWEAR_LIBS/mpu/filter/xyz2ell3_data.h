/*
 * File: xyz2ell3_data.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 05-Nov-2019 00:26:40
 */

#ifndef XYZ2ELL3_DATA_H
#define XYZ2ELL3_DATA_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "xyz2ell3_types.h"

/* Variable Declarations */
extern boolean_T isInitialized_xyz2ell3;

#endif

/*
 * File trailer for xyz2ell3_data.h
 *
 * [EOF]
 */
