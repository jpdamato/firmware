/*
 * File: main.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 09-Nov-2019 18:23:44
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/

/* Include Files */
#include "main.h"
#include "calibrarGPScoder.h"
#include "calibrarGPScoder_terminate.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include <string.h>

/* Function Declarations */
static void argInit_10x1_real32_T(float result[10]);
static void argInit_10x1_real_T(double result[10]);
static void argInit_1x3_real_T(double result[3]);
static void argInit_20x3_real32_T(float result[60]);
static void argInit_28x1_real32_T(float result[28]);
static void argInit_28x28_real32_T(float result[784]);
static void argInit_3x1_real32_T(float result[3]);
static void argInit_3x3_real32_T(float result[9]);
static bool argInit_boolean_T(void);
static float argInit_real32_T(void);
static double argInit_real_T(void);
static void argInit_struct0_T(struct0_T *result);
static void argInit_struct1_T(struct1_T *result);
static void argInit_struct2_T(struct2_T *result);
static unsigned char argInit_uint8_T(void);
static void main_calibrarGPScoder(void);
static void main_calibrarIMUcoder(void);
static void main_calibrarMagnetometro(void);
static void main_filtroCoder(void);

/* Function Definitions */

/*
 * Arguments    : float result[10]
 * Return Type  : void
 */
static void argInit_10x1_real32_T(float result[10])
{
  int idx0;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 10; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0] = argInit_real32_T();
  }
}

/*
 * Arguments    : double result[10]
 * Return Type  : void
 */
static void argInit_10x1_real_T(double result[10])
{
  int idx0;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 10; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0] = argInit_real_T();
  }
}

/*
 * Arguments    : double result[3]
 * Return Type  : void
 */
static void argInit_1x3_real_T(double result[3])
{
  double result_tmp_tmp;

  /* Loop over the array to initialize each element. */
  /* Set the value of the array element.
     Change this value to the value that the application requires. */
  result_tmp_tmp = argInit_real_T();
  result[0] = result_tmp_tmp;

  /* Set the value of the array element.
     Change this value to the value that the application requires. */
  result[1] = result_tmp_tmp;

  /* Set the value of the array element.
     Change this value to the value that the application requires. */
  result[2] = result_tmp_tmp;
}

/*
 * Arguments    : float result[60]
 * Return Type  : void
 */
static void argInit_20x3_real32_T(float result[60])
{
  int idx0;
  float result_tmp_tmp;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 20; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result_tmp_tmp = argInit_real32_T();
    result[idx0] = result_tmp_tmp;

    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0 + 20] = result_tmp_tmp;

    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0 + 40] = result_tmp_tmp;
  }
}

/*
 * Arguments    : float result[28]
 * Return Type  : void
 */
static void argInit_28x1_real32_T(float result[28])
{
  int idx0;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 28; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0] = argInit_real32_T();
  }
}

/*
 * Arguments    : float result[784]
 * Return Type  : void
 */
static void argInit_28x28_real32_T(float result[784])
{
  int idx0;
  int idx1;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 28; idx0++) {
    for (idx1 = 0; idx1 < 28; idx1++) {
      /* Set the value of the array element.
         Change this value to the value that the application requires. */
      result[idx0 + 28 * idx1] = argInit_real32_T();
    }
  }
}

/*
 * Arguments    : float result[3]
 * Return Type  : void
 */
static void argInit_3x1_real32_T(float result[3])
{
  float result_tmp_tmp;

  /* Loop over the array to initialize each element. */
  /* Set the value of the array element.
     Change this value to the value that the application requires. */
  result_tmp_tmp = argInit_real32_T();
  result[0] = result_tmp_tmp;

  /* Set the value of the array element.
     Change this value to the value that the application requires. */
  result[1] = result_tmp_tmp;

  /* Set the value of the array element.
     Change this value to the value that the application requires. */
  result[2] = result_tmp_tmp;
}

/*
 * Arguments    : float result[9]
 * Return Type  : void
 */
static void argInit_3x3_real32_T(float result[9])
{
  int idx0;
  float result_tmp_tmp;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 3; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result_tmp_tmp = argInit_real32_T();
    result[idx0] = result_tmp_tmp;

    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0 + 3] = result_tmp_tmp;

    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0 + 6] = result_tmp_tmp;
  }
}

/*
 * Arguments    : void
 * Return Type  : bool
 */
static bool argInit_boolean_T(void)
{
  return false;
}

/*
 * Arguments    : void
 * Return Type  : float
 */
static float argInit_real32_T(void)
{
  return 0.0F;
}

/*
 * Arguments    : void
 * Return Type  : double
 */
static double argInit_real_T(void)
{
  return 0.0;
}

/*
 * Arguments    : struct0_T *result
 * Return Type  : void
 */
static void argInit_struct0_T(struct0_T *result)
{
  double result_tmp_tmp[10];

  /* Set the value of each structure field.
     Change this value to the value that the application requires. */
  argInit_10x1_real_T(result_tmp_tmp);
  memcpy(&result->Longitud[0], &result_tmp_tmp[0], 10U * sizeof(double));
  memcpy(&result->Latitud[0], &result_tmp_tmp[0], 10U * sizeof(double));
  memcpy(&result->Altitud[0], &result_tmp_tmp[0], 10U * sizeof(double));
  argInit_10x1_real32_T(result->VGPS);
}

/*
 * Arguments    : struct1_T *result
 * Return Type  : void
 */
static void argInit_struct1_T(struct1_T *result)
{
  float result_tmp_tmp[10];
  int i;

  /* Set the value of each structure field.
     Change this value to the value that the application requires. */
  argInit_10x1_real32_T(result_tmp_tmp);
  for (i = 0; i < 10; i++) {
    result->ax[i] = result_tmp_tmp[i];
    result->ay[i] = result_tmp_tmp[i];
    result->az[i] = result_tmp_tmp[i];
  }

  argInit_10x1_real32_T(result->gx);
  argInit_10x1_real32_T(result->gy);
  argInit_10x1_real32_T(result->gz);
  argInit_10x1_real32_T(result->mx);
  argInit_10x1_real32_T(result->my);
  argInit_10x1_real32_T(result->mz);
}

/*
 * Arguments    : struct2_T *result
 * Return Type  : void
 */
static void argInit_struct2_T(struct2_T *result)
{
  /* Set the value of each structure field.
     Change this value to the value that the application requires. */
  argInit_28x1_real32_T(result->estados);
  argInit_28x28_real32_T(result->covarianzaEstados);
  argInit_1x3_real_T(result->posAnterior);
}

/*
 * Arguments    : void
 * Return Type  : unsigned char
 */
static unsigned char argInit_uint8_T(void)
{
  return 0U;
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_calibrarGPScoder(void)
{
  struct0_T r;
  double lat[10];
  double lon[10];
  double alt[10];
  float v[10];

  /* Initialize function 'calibrarGPScoder' input arguments. */
  /* Initialize function input argument 'TGPS'. */
  /* Call the entry-point 'calibrarGPScoder'. */
  
  /*Inicializo la variable*/
  argInit_struct0_T(&r);
  r.Longitud[0] = -59.083961000000002;
  r.Longitud[1] = -59.083962000000000;
  r.Longitud[2] = -59.083962999999997;
  r.Longitud[3] = -59.083962999999997;
  r.Longitud[4] = -59.083964000000002;
  r.Longitud[5] = -59.083964000000002;
  r.Longitud[6] = -59.083962999999997;
  r.Longitud[7] = -59.083962999999997;
  r.Longitud[8] = -59.083962999999997;
  r.Longitud[9] = -59.083962000000000;

  r.Latitud[0] = -37.321249000000002;
  r.Latitud[1] = -37.321249000000002;
  r.Latitud[2] = -37.321249000000002;
  r.Latitud[3] = -37.321249000000002;
  r.Latitud[4] = -37.321249000000002;
  r.Latitud[5] = -37.321247999999997;
  r.Latitud[6] = -37.321247999999997;
  r.Latitud[7] = -37.321249000000002;
  r.Latitud[8] = -37.321249000000002;
  r.Latitud[9] = -37.321247999999997;

  r.Altitud[0] = 200;
  r.Altitud[1] = 200;
  r.Altitud[2] = 200;
  r.Altitud[3] = 200;
  r.Altitud[4] = 200;
  r.Altitud[5] = 200;
  r.Altitud[6] = 200;
  r.Altitud[7] = 200;
  r.Altitud[8] = 200;
  r.Altitud[9] = 200;

  calibrarGPScoder(&r, 5, lat, lon, alt, v);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_calibrarIMUcoder(void)
{
  struct1_T r;
  float b_fv[9];
  float fv1[3];
  float accel[30];
  float gyro[30];
  float mag[30];

  /* Initialize function 'calibrarIMUcoder' input arguments. */
  /* Initialize function input argument 'TIMU'. */
  /* Initialize function input argument 'A'. */
  /* Initialize function input argument 'b'. */
  /* Call the entry-point 'calibrarIMUcoder'. */
  argInit_struct1_T(&r);
  argInit_3x3_real32_T(b_fv);
  argInit_3x1_real32_T(fv1);
  r.ax[0] = 9.3000002;
  r.ax[1] = 9.3699999;
  r.ax[2] = 9.3699999;
  r.ax[3] = 9.3446598;
  r.ax[4] = 9.3199997;
  r.ax[5] = 9.5000000;
  r.ax[6] = 9.3299999;
  r.ax[7] = 9.3199997;
  r.ax[8] = 9.2856426;
  r.ax[9] = 9.2600002;

  r.ay[0] = 0.6600000;
  r.ay[1] = 0.7000000;
  r.ay[2] = 0.7000000;
  r.ay[3] = 0.7646153;
  r.ay[4] = 0.8200000;
  r.ay[5] = 0.7500000;
  r.ay[6] = 0.7500001;
  r.ay[7] = 0.7000000;
  r.ay[8] = 0.6888235;
  r.ay[9] = 0.6800000;

  r.az[0] = -2.7700000;
  r.az[1] = - 2.7200000;
  r.az[2] = - 2.7300000;
  r.az[3] = - 2.6229210;
  r.az[4] = - 2.5200000;
  r.az[5] = - 2.5500000;
  r.az[6] = - 2.6199999;
  r.az[7] = - 2.7100000;
  r.az[8] = - 2.7156262;
  r.az[9] = - 2.6600001;

  r.gx[0] = -0.7500000;
  r.gx[1] = -1.0000005;
  r.gx[2] = -0.4400002;
  r.gx[3] = -0.0595400;
  r.gx[4] = 0.3399995;
  r.gx[5] = 1.0300000;
  r.gx[6] = 1.0900004;
  r.gx[7] = 0.6600001;
  r.gx[8] = 0.4447281;
  r.gx[9] = 0.5299997;

  r.gy[0] = 0.7800000;
  r.gy[1] = 1.9399991;
  r.gy[2] = 2.3100002;
  r.gy[3] = 1.5729350;
  r.gy[4] = 0.6600007;
  r.gy[5] = 0;
  r.gy[6] = 0.4699995;
  r.gy[7] = 0.7199998;
  r.gy[8] = 1.6577288;
  r.gy[9] = 3.0299988;

  r.gz[0] = 1.5000000;
  r.gz[1] = 1.3800001;
  r.gz[2] = 1.3100001;
  r.gz[3] = 0.7312676;
  r.gz[4] = 0.2200000;
  r.gz[5] = 0.7800000;
  r.gz[6] = 0.9099996;
  r.gz[7] = 1.3099999;
  r.gz[8] = 1.3529119;
  r.gz[9] = 1.2200001;

  r.mx[0] = -13.5000000;
  r.mx[1] = -13.5000010;
  r.mx[2] = -12.6300001;
  r.mx[3] = -12.6300001;
  r.mx[4] = -12.6300001;
  r.mx[5] = -12.6300001;
  r.mx[6] = -12.6300001;
  r.mx[7] = -13.8100004;
  r.mx[8] = -13.8100004;
  r.mx[9] = -13.8100004;

  r.my[0] = 6.7500000;
  r.my[1] = 6.7499995;
  r.my[2] = 7.3800001;
  r.my[3] = 7.3800001;
  r.my[4] = 7.3800001;
  r.my[5] = 7.3800001;
  r.my[6] = 7.3800001;
  r.my[7] = 7.2500000;
  r.my[8] = 7.2500000;
  r.my[9] = 7.2500000;

  r.mz[0] = -14.5600004;
  r.mz[1] = -14.5599985;
  r.mz[2] = -16.2500000;
  r.mz[3] = -16.2500000;
  r.mz[4] = -16.2500000;
  r.mz[5] = -16.2500000;
  r.mz[6] = -16.2500000;
  r.mz[7] = -15;
  r.mz[8] = -15;
  r.mz[9] = -15;

  b_fv[0] = 1.0F;
  b_fv[4] = 1.0F;
  b_fv[8] = 1.0F;

  fv1[0] = -10.7654858;
  fv1[1] =   7.9342480;
  fv1[2] = -16.7373066;



  calibrarIMUcoder(&r, 5, b_fv, fv1, accel, gyro, mag);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_calibrarMagnetometro(void)
{
  float b_fv[60];
  float A[9];
  float b[3];

  /* Initialize function 'calibrarMagnetometro' input arguments. */
  /* Initialize function input argument 'mag_sinCalibrar'. */
  /* Call the entry-point 'calibrarMagnetometro'. */
  argInit_20x3_real32_T(b_fv);
  b_fv[0] = -12.6300001;
  b_fv[1] = -13.3800001;
  b_fv[2] = -11.5600004;
  b_fv[3] = -12.6300001;
  b_fv[4] = -13.3800001;
  b_fv[5] = -13.5000000;
  b_fv[6] = -12.9399996;
  b_fv[7] = -12.9399996;
  b_fv[8] = -11.8800001;
  b_fv[9] = -12.6300001;
  b_fv[10] = -14.0000000;
  b_fv[11] = -12.6300001;
  b_fv[12] = -13.5000000;
  b_fv[13] = -14.2500000;
  b_fv[14] = -13.5000000;
  b_fv[15] = -12.6300001;
  b_fv[16] = -12.6300001;
  b_fv[17] = -11.0000000;
  b_fv[18] = -13.0600004;
  b_fv[19] = -12.5000000;

  b_fv[20] = 7.0599999;
  b_fv[21] = 8.4399996;
  b_fv[22] = 6.3099999;
  b_fv[23] = 7.0599999;
  b_fv[24] = 7.0599999;
  b_fv[25] = 6.1900001;
  b_fv[26] = 7.5949998;
  b_fv[27] = 7.0599999;
  b_fv[28] = 5.2500000;
  b_fv[29] = 7.3800001;
  b_fv[30] = 7.6900001;
  b_fv[31] = 8.1300001;
  b_fv[32] = 7.2500000;
  b_fv[33] = 9.0000000;
  b_fv[34] = 5.7500000;
  b_fv[35] = 7.6900001;
  b_fv[36] = 7.3800001;
  b_fv[37] = 6.7500000;
  b_fv[38] = 6.5000000;
  b_fv[39] = 7.5000000;

  b_fv[40] = -13.6899996;
  b_fv[41] = - 18.3099995;
  b_fv[42] = - 14.1300001;
  b_fv[43] = - 15.1899996;
  b_fv[44] = - 14.4399996;
  b_fv[45] = - 15.6300001;
  b_fv[46] = - 14.7500000;
  b_fv[47] = - 13.0600004;
  b_fv[48] = - 17.2500000;
  b_fv[49] = - 13.3800001;
  b_fv[50] = - 16.2500000;
  b_fv[51] = - 14.7500000;
  b_fv[52] = - 15.6300001;
  b_fv[53] = - 19.5000000;
  b_fv[54] = - 13.5000000;
  b_fv[55] = - 14.1300001;
  b_fv[56] = - 14.1300001;
  b_fv[57] = - 14.0000000;
  b_fv[58] = - 14.2500000;
  b_fv[59] = - 15.0000000;

  calibrarMagnetometro(b_fv, A, b);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_filtroCoder(void)
{
  struct2_T info;
  float accel_tmp_tmp[3];
  float gyro_tmp_tmp[3];
  float mag_tmp_tmp[3];
  double lla_tmp[3];
  float VGPS;
  double ubicacionReferencia[3];
  bool evaluarGPS_tmp;
  bool evaluarIMU_tmp;
  struct3_T salida;

  /* Initialize function 'filtroCoder' input arguments. */
  /* Initialize function input argument 'info'. */
  argInit_struct2_T(&info);

  /* Initialize function input argument 'accel'. */
  argInit_3x1_real32_T(accel_tmp_tmp);

  /* Initialize function input argument 'gyro'. */
  argInit_3x1_real32_T(gyro_tmp_tmp);
  /* Initialize function input argument 'mag'. */
  argInit_3x1_real32_T(mag_tmp_tmp);
  /* Initialize function input argument 'lla'. */
  argInit_1x3_real_T(lla_tmp);
  VGPS = argInit_real32_T();

  /* Initialize function input argument 'C'. */
  /* Initialize function input argument 'ubicacionReferencia'. */
  argInit_1x3_real_T(ubicacionReferencia);
  ubicacionReferencia[0] = -37.321249000000002;
  ubicacionReferencia[1] = -59.083961000000002;
  ubicacionReferencia[2] = 200;
  evaluarGPS_tmp = argInit_boolean_T();
   
  struct0_T r;
  double lat[10];
  double lon[10];
  double alt[10];
  float v[10];

  float b_fv[60];
  float A[9];
  float b[3];

  struct1_T rr;
  float bb_fv[9];
  float fv1[3];
  float accel[30];
  float gyro[30];
  float mag[30];

  /* Initialize function 'calibrarGPScoder' input arguments. */
  /* Initialize function input argument 'TGPS'. */
  /* Call the entry-point 'calibrarGPScoder'. */

  /*Inicializo la variable*/
  argInit_struct0_T(&r);
  r.Longitud[0] = -59.083961000000002;
  r.Longitud[1] = -59.083962000000000;
  r.Longitud[2] = -59.083962999999997;
  r.Longitud[3] = -59.083962999999997;
  r.Longitud[4] = -59.083964000000002;
  r.Longitud[5] = -59.083964000000002;
  r.Longitud[6] = -59.083962999999997;
  r.Longitud[7] = -59.083962999999997;
  r.Longitud[8] = -59.083962999999997;
  r.Longitud[9] = -59.083962000000000;

  r.Latitud[0] = -37.321249000000002;
  r.Latitud[1] = -37.321249000000002;
  r.Latitud[2] = -37.321249000000002;
  r.Latitud[3] = -37.321249000000002;
  r.Latitud[4] = -37.321249000000002;
  r.Latitud[5] = -37.321247999999997;
  r.Latitud[6] = -37.321247999999997;
  r.Latitud[7] = -37.321249000000002;
  r.Latitud[8] = -37.321249000000002;
  r.Latitud[9] = -37.321247999999997;

  r.Altitud[0] = 200;
  r.Altitud[1] = 200;
  r.Altitud[2] = 200;
  r.Altitud[3] = 200;
  r.Altitud[4] = 200;
  r.Altitud[5] = 200;
  r.Altitud[6] = 200;
  r.Altitud[7] = 200;
  r.Altitud[8] = 200;
  r.Altitud[9] = 200;

  calibrarGPScoder(&r, 5, lat, lon, alt, v);

  

  /* Initialize function 'calibrarMagnetometro' input arguments. */
  /* Initialize function input argument 'mag_sinCalibrar'. */
  /* Call the entry-point 'calibrarMagnetometro'. */
  argInit_20x3_real32_T(b_fv);
  b_fv[0] = -12.6300001;
  b_fv[1] = -13.3800001;
  b_fv[2] = -11.5600004;
  b_fv[3] = -12.6300001;
  b_fv[4] = -13.3800001;
  b_fv[5] = -13.5000000;
  b_fv[6] = -12.9399996;
  b_fv[7] = -12.9399996;
  b_fv[8] = -11.8800001;
  b_fv[9] = -12.6300001;
  b_fv[10] = -14.0000000;
  b_fv[11] = -12.6300001;
  b_fv[12] = -13.5000000;
  b_fv[13] = -14.2500000;
  b_fv[14] = -13.5000000;
  b_fv[15] = -12.6300001;
  b_fv[16] = -12.6300001;
  b_fv[17] = -11.0000000;
  b_fv[18] = -13.0600004;
  b_fv[19] = -12.5000000;

  b_fv[20] = 7.0599999;
  b_fv[21] = 8.4399996;
  b_fv[22] = 6.3099999;
  b_fv[23] = 7.0599999;
  b_fv[24] = 7.0599999;
  b_fv[25] = 6.1900001;
  b_fv[26] = 7.5949998;
  b_fv[27] = 7.0599999;
  b_fv[28] = 5.2500000;
  b_fv[29] = 7.3800001;
  b_fv[30] = 7.6900001;
  b_fv[31] = 8.1300001;
  b_fv[32] = 7.2500000;
  b_fv[33] = 9.0000000;
  b_fv[34] = 5.7500000;
  b_fv[35] = 7.6900001;
  b_fv[36] = 7.3800001;
  b_fv[37] = 6.7500000;
  b_fv[38] = 6.5000000;
  b_fv[39] = 7.5000000;

  b_fv[40] = -13.6899996;
  b_fv[41] = -18.3099995;
  b_fv[42] = -14.1300001;
  b_fv[43] = -15.1899996;
  b_fv[44] = -14.4399996;
  b_fv[45] = -15.6300001;
  b_fv[46] = -14.7500000;
  b_fv[47] = -13.0600004;
  b_fv[48] = -17.2500000;
  b_fv[49] = -13.3800001;
  b_fv[50] = -16.2500000;
  b_fv[51] = -14.7500000;
  b_fv[52] = -15.6300001;
  b_fv[53] = -19.5000000;
  b_fv[54] = -13.5000000;
  b_fv[55] = -14.1300001;
  b_fv[56] = -14.1300001;
  b_fv[57] = -14.0000000;
  b_fv[58] = -14.2500000;
  b_fv[59] = -15.0000000;

  calibrarMagnetometro(b_fv, A, b);
  
  /* Initialize function 'calibrarIMUcoder' input arguments. */
  /* Initialize function input argument 'TIMU'. */
  /* Initialize function input argument 'A'. */
  /* Initialize function input argument 'b'. */
  /* Call the entry-point 'calibrarIMUcoder'. */
  argInit_struct1_T(&rr);
  argInit_3x3_real32_T(b_fv);
  argInit_3x1_real32_T(fv1);
  rr.ax[0] = 9.3000002;
  rr.ax[1] = 9.3699999;
  rr.ax[2] = 9.3699999;
  rr.ax[3] = 9.3446598;
  rr.ax[4] = 9.3199997;
  rr.ax[5] = 9.5000000;
  rr.ax[6] = 9.3299999;
  rr.ax[7] = 9.3199997;
  rr.ax[8] = 9.2856426;
  rr.ax[9] = 9.2600002;

  rr.ay[0] = 0.6600000;
  rr.ay[1] = 0.7000000;
  rr.ay[2] = 0.7000000;
  rr.ay[3] = 0.7646153;
  rr.ay[4] = 0.8200000;
  rr.ay[5] = 0.7500000;
  rr.ay[6] = 0.7500001;
  rr.ay[7] = 0.7000000;
  rr.ay[8] = 0.6888235;
  rr.ay[9] = 0.6800000;

  rr.az[0] = -2.7700000;
  rr.az[1] = -2.7200000;
  rr.az[2] = -2.7300000;
  rr.az[3] = -2.6229210;
  rr.az[4] = -2.5200000;
  rr.az[5] = -2.5500000;
  rr.az[6] = -2.6199999;
  rr.az[7] = -2.7100000;
  rr.az[8] = -2.7156262;
  rr.az[9] = -2.6600001;

  rr.gx[0] = -0.7500000;
  rr.gx[1] = -1.0000005;
  rr.gx[2] = -0.4400002;
  rr.gx[3] = -0.0595400;
  rr.gx[4] = 0.3399995;
  rr.gx[5] = 1.0300000;
  rr.gx[6] = 1.0900004;
  rr.gx[7] = 0.6600001;
  rr.gx[8] = 0.4447281;
  rr.gx[9] = 0.5299997;

  rr.gy[0] = 0.7800000;
  rr.gy[1] = 1.9399991;
  rr.gy[2] = 2.3100002;
  rr.gy[3] = 1.5729350;
  rr.gy[4] = 0.6600007;
  rr.gy[5] = 0;
  rr.gy[6] = 0.4699995;
  rr.gy[7] = 0.7199998;
  rr.gy[8] = 1.6577288;
  rr.gy[9] = 3.0299988;

  rr.gz[0] = 1.5000000;
  rr.gz[1] = 1.3800001;
  rr.gz[2] = 1.3100001;
  rr.gz[3] = 0.7312676;
  rr.gz[4] = 0.2200000;
  rr.gz[5] = 0.7800000;
  rr.gz[6] = 0.9099996;
  rr.gz[7] = 1.3099999;
  rr.gz[8] = 1.3529119;
  rr.gz[9] = 1.2200001;

  rr.mx[0] = -13.5000000;
  rr.mx[1] = -13.5000010;
  rr.mx[2] = -12.6300001;
  rr.mx[3] = -12.6300001;
  rr.mx[4] = -12.6300001;
  rr.mx[5] = -12.6300001;
  rr.mx[6] = -12.6300001;
  rr.mx[7] = -13.8100004;
  rr.mx[8] = -13.8100004;
  rr.mx[9] = -13.8100004;

  rr.my[0] = 6.7500000;
  rr.my[1] = 6.7499995;
  rr.my[2] = 7.3800001;
  rr.my[3] = 7.3800001;
  rr.my[4] = 7.3800001;
  rr.my[5] = 7.3800001;
  rr.my[6] = 7.3800001;
  rr.my[7] = 7.2500000;
  rr.my[8] = 7.2500000;
  rr.my[9] = 7.2500000;

  rr.mz[0] = -14.5600004;
  rr.mz[1] = -14.5599985;
  rr.mz[2] = -16.2500000;
  rr.mz[3] = -16.2500000;
  rr.mz[4] = -16.2500000;
  rr.mz[5] = -16.2500000;
  rr.mz[6] = -16.2500000;
  rr.mz[7] = -15;
  rr.mz[8] = -15;
  rr.mz[9] = -15;

  bb_fv[0] = 1.0F;
  bb_fv[4] = 1.0F;
  bb_fv[8] = 1.0F;
  bb_fv[1] = 0;
  bb_fv[2] = 0;
  bb_fv[3] = 0;
  bb_fv[5] = 0;
  bb_fv[6] = 0;
  bb_fv[7] = 0;

  fv1[0] = -13.1835270;
  fv1[1] = 7.9342480;
  fv1[2] = -15.1538038;

  calibrarIMUcoder(&rr, 5, A, b, accel, gyro, mag);

  /*Establecemos las condiciones iniciales*/
  accel_tmp_tmp[0] = accel[0];
  accel_tmp_tmp[1] = accel[10];
  accel_tmp_tmp[2] = accel[20];

  gyro_tmp_tmp[0] = gyro[0];
  gyro_tmp_tmp[1] = gyro[10];
  gyro_tmp_tmp[2] = gyro[20];

  mag_tmp_tmp[0] = mag[0];
  mag_tmp_tmp[1] = mag[10];
  mag_tmp_tmp[2] = mag[20];

  lla_tmp[0] = -37.321249000000002;
  lla_tmp[1] = -59.083961000000002;
  lla_tmp[2] = 200;

  info.estados[0] = 1.0F;
  info.estados[22] = 27.5550003;
  info.estados[23] = -2.4168999;
  info.estados[24] = -16.0848999;

  info.posAnterior[0] = -37.321249000000002;
  info.posAnterior[1] = -59.083961000000002;
  info.posAnterior[2] = 200;

  for (int i = 0; i < 28; i++) {
	  info.covarianzaEstados[29*i] = 0.001F;
  }
  
  evaluarGPS_tmp = 1;
  evaluarIMU_tmp = 0;
  /* Call the entry-point 'filtroCoder'. */
  for (int i = 0; i < 14; i++) {
	  if (i < 4) {
		  lla_tmp[0] = lat[i];
		  lla_tmp[1] = lon[i];
		  lla_tmp[2] = alt[i];
		  VGPS = v[i];
		  evaluarGPS_tmp = 1;
		  evaluarIMU_tmp = 0;
	  }
	  else {
		  evaluarGPS_tmp = 0;
	  }
	  if (i >= 3) {
			  accel_tmp_tmp[0] = accel[i - 3];
			  accel_tmp_tmp[1] = accel[i + 10 - 3];
			  accel_tmp_tmp[2] = accel[i + 20 - 3];

			  gyro_tmp_tmp[0] = gyro[i - 3];
			  gyro_tmp_tmp[1] = gyro[i + 10 - 3];
			  gyro_tmp_tmp[2] = gyro[i + 20 - 3];

			  mag_tmp_tmp[0] = mag[i - 3];
			  mag_tmp_tmp[1] = mag[i + 10 - 3];
			  mag_tmp_tmp[2] = mag[i + 20 - 3];

			  VGPS = 0;
			  evaluarIMU_tmp = 1;
	  }
	  filtroCoder(&info, accel_tmp_tmp, gyro_tmp_tmp, mag_tmp_tmp, lla_tmp, VGPS,
		  ubicacionReferencia, evaluarGPS_tmp, evaluarIMU_tmp, &salida);
  }
 
}

/*
 * Arguments    : int argc
 *                const char * const argv[]
 * Return Type  : int
 */
int main(int argc, const char * const argv[])
{
  (void)argc;
  (void)argv;

  /* The initialize function is being called automatically from your entry-point function. So, a call to initialize is not included here. */
  /* Invoke the entry-point functions.
     You can call entry-point functions multiple times. */
  main_calibrarGPScoder();
  main_calibrarIMUcoder();
  main_calibrarMagnetometro();
  main_filtroCoder();

  /* Terminate the application.
     You do not need to do this more than one time. */
  calibrarGPScoder_terminate();
  return 0;
}

/*
 * File trailer for main.c
 *
 * [EOF]
 */
