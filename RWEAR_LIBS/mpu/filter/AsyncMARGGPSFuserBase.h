/*
 * File: AsyncMARGGPSFuserBase.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef ASYNCMARGGPSFUSERBASE_H
#define ASYNCMARGGPSFUSERBASE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern void AsyncMARGGPSFuserBase_fuseaccel(c_fusion_internal_coder_insfilt *obj,
  const float accel[3]);
void AsyncMARGGPSFuserBase_fusegps(c_fusion_internal_coder_insfilt *obj, const
	 double lla[3], const float vel[3], const float covarianza[3]);
		 extern void AsyncMARGGPSFuserBase_fusegyro(c_fusion_internal_coder_insfilt *obj,
  const float gyro[3]);
extern void AsyncMARGGPSFuserBase_fusemag(c_fusion_internal_coder_insfilt *obj,
  const float mag[3]);
extern void AsyncMARGGPSFuserBase_predict(c_fusion_internal_coder_insfilt *obj);

#endif

/*
 * File trailer for AsyncMARGGPSFuserBase.h
 *
 * [EOF]
 */
