/*
 * File: cosd.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef COSD_H
#define COSD_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern void b_cosd(double *x);

#endif

/*
 * File trailer for cosd.h
 *
 * [EOF]
 */
