/*
 * File: calibrarGPScoder_terminate.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "calibrarGPScoder_terminate.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void calibrarGPScoder_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for calibrarGPScoder_terminate.c
 *
 * [EOF]
 */
