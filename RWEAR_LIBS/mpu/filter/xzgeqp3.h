/*
 * File: xzgeqp3.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef XZGEQP3_H
#define XZGEQP3_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern void qrpf(float A[80], int ia0, int m, int n, float tau[4], int jpvt[4]);

#endif

/*
 * File trailer for xzgeqp3.h
 *
 * [EOF]
 */
