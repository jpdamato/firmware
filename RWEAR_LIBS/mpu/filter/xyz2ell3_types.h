/*
 * File: xyz2ell3_types.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 05-Nov-2019 00:26:40
 */

#ifndef XYZ2ELL3_TYPES_H
#define XYZ2ELL3_TYPES_H

/* Include Files */
#include "rtwtypes.h"
#endif

/*
 * File trailer for xyz2ell3_types.h
 *
 * [EOF]
 */
