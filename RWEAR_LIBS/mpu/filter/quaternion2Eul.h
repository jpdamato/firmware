/*
 * File: quaternion2Eul.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 04-Dec-2019 01:07:12
 */

#ifndef QUATERNION2EUL_H
#define QUATERNION2EUL_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
//#include "quaternion2Eul_types.h"

/* Function Declarations */
extern void quaternion2Eul(const double q1[4], short eul[3]);

#endif

/*
 * File trailer for quaternion2Eul.h
 *
 * [EOF]
 */
