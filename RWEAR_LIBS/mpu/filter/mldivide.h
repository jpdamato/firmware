/*
 * File: mldivide.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

#ifndef MLDIVIDE_H
#define MLDIVIDE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calibrarGPScoder_types.h"

/* Function Declarations */
extern void b_mldivide(const float A[9], const float B[84], float Y[84]);
extern void mldivide(const float A[9], const float B[3], float Y[3]);

#endif

/*
 * File trailer for mldivide.h
 *
 * [EOF]
 */
