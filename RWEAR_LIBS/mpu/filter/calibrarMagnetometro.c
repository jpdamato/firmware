/*
 * File: calibrarMagnetometro.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 10-Nov-2019 18:03:40
 */

/* Include Files */
#include "calibrarMagnetometro.h"
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "filtroCoder.h"
#include "xzgeqp3.h"
#include <math.h>

/* Function Definitions */

/*
 * Calibra el magnetometro para que el campo magnetico se encuentre centrado
 * en el dispositivo. Esta calibracion puede requerir de una muestra
 * representativa del campo alrededor del sensor, por lo que se recomienda
 * recorrer el espacio donde se quiere utilizar para tener una idea que este
 * metodo pueda ajustar.
 * Arguments    : const float mag_sinCalibrar[60]
 *                float A[9]
 *                float b[3]
 * Return Type  : void
 */
void calibrarMagnetometro(const float mag_sinCalibrar[60], float A[9], float b[3])
{
  int jpvt[4];
  int rankR;
  float tau[4];
  float tol;
  float bv_tmp;
  float bv[20];
  float b_A[80];
  float soln[4];
  int i;
  int j;
  int soln_tmp;
  int b_soln_tmp;

  /* Calibracion del magnetometro */
  /* Calibracion del magnetometro usando una matriz diagonal */
  /*  R is the identity */
  jpvt[0] = 1;
  jpvt[1] = 2;
  jpvt[2] = 3;
  jpvt[3] = 4;
  for (rankR = 0; rankR < 20; rankR++) {
    tol = mag_sinCalibrar[rankR + 20];
    bv_tmp = mag_sinCalibrar[rankR + 40];
    bv[rankR] = (mag_sinCalibrar[rankR] * mag_sinCalibrar[rankR] + tol * tol) +
      bv_tmp * bv_tmp;
    b_A[rankR] = mag_sinCalibrar[rankR];
    b_A[rankR + 20] = tol;
    b_A[rankR + 40] = bv_tmp;
    b_A[rankR + 60] = 1.0F;
  }

  tau[0] = 0.0F;
  tau[1] = 0.0F;
  tau[2] = 0.0F;
  tau[3] = 0.0F;
  qrpf(b_A, 1, 20, 4, tau, jpvt);
  rankR = 0;
  tol = 2.38418579E-5F * fabsf(b_A[0]);
  while ((rankR < 4) && (fabsf(b_A[rankR + 20 * rankR]) > tol)) {
    rankR++;
  }

  soln[0] = 0.0F;
  if (tau[0] != 0.0F) {
    tol = bv[0];
    for (i = 2; i < 21; i++) {
      tol += b_A[i - 1] * bv[i - 1];
    }

    tol *= tau[0];
    if (tol != 0.0F) {
      bv[0] -= tol;
      for (i = 2; i < 21; i++) {
        bv[i - 1] -= b_A[i - 1] * tol;
      }
    }
  }

  soln[1] = 0.0F;
  if (tau[1] != 0.0F) {
    tol = bv[1];
    for (i = 3; i < 21; i++) {
      tol += b_A[i + 19] * bv[i - 1];
    }

    tol *= tau[1];
    if (tol != 0.0F) {
      bv[1] -= tol;
      for (i = 3; i < 21; i++) {
        bv[i - 1] -= b_A[i + 19] * tol;
      }
    }
  }

  soln[2] = 0.0F;
  if (tau[2] != 0.0F) {
    tol = bv[2];
    for (i = 4; i < 21; i++) {
      tol += b_A[i + 39] * bv[i - 1];
    }

    tol *= tau[2];
    if (tol != 0.0F) {
      bv[2] -= tol;
      for (i = 4; i < 21; i++) {
        bv[i - 1] -= b_A[i + 39] * tol;
      }
    }
  }

  soln[3] = 0.0F;
  if (tau[3] != 0.0F) {
    tol = bv[3];
    for (i = 5; i < 21; i++) {
      tol += b_A[i + 59] * bv[i - 1];
    }

    tol *= tau[3];
    if (tol != 0.0F) {
      bv[3] -= tol;
      for (i = 5; i < 21; i++) {
        bv[i - 1] -= b_A[i + 59] * tol;
      }
    }
  }

  for (i = 0; i < rankR; i++) {
    soln[jpvt[i] - 1] = bv[i];
  }

  for (j = rankR; j >= 1; j--) {
    soln_tmp = jpvt[j - 1] - 1;
    b_soln_tmp = 20 * (j - 1);
    soln[soln_tmp] /= b_A[(j + b_soln_tmp) - 1];
    for (i = 0; i <= j - 2; i++) {
      soln[jpvt[i] - 1] -= soln[soln_tmp] * b_A[i + b_soln_tmp];
    }
  }

  for (rankR = 0; rankR < 9; rankR++) {
    A[rankR] = 0.0F;
  }

  /*  make a row vector  */
  A[0] = 1.0F;
  b[0] = 0.5F * soln[0];
  A[4] = 1.0F;
  b[1] = 0.5F * soln[1];
  A[8] = 1.0F;
  b[2] = 0.5F * soln[2];
}

/*
 * File trailer for calibrarMagnetometro.c
 *
 * [EOF]
 */
