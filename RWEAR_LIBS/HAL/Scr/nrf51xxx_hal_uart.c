/**
  ******************************************************************************
  * @file    nrf51xxx_hal_uart.c
  * @author  Leandro J. Aguierre
  * @version V0.1
  * @date    11-Octubre-2016
  * @brief   UART HAL module driver.
  *          This file provides firmware functions to manage the following 
  *          functionalities of the Universal Asynchronous Receiver Transmitter (UART) peripheral:
  *           + Initialization and de-initialization functions
  *           + IO operation functions
  *           + Peripheral Control functions  
  *           + Peripheral State and Errors functions  
  *           
  @verbatim       
  @endverbatim
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Redimec SRL</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Redimec SRL nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>
#include "nrf.h"

#include "app_error.h"
#include "nrf51xxx_hal_uart.h"
#include "nrf51xxx_hal_gpio.h"
#include "cmd_task.h"
#include "ble_task.h"
/** @defgroup UART 
  * @brief HAL UART module driver
  * @{
  */
#if (HAL_UART_MODULE_ENABLED == 1)
    
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define UART_TIMEOUT_VALUE  22000

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* static void UART_SetConfig (UART_HandleTypeDef *huart);
static HAL_StatusTypeDef UART_Transmit_IT(UART_HandleTypeDef *huart);
static HAL_StatusTypeDef UART_Receive_IT(UART_HandleTypeDef *huart);
static void UART_DMATransmitCplt(DMA_HandleTypeDef *hdma);
static void UART_DMATxHalfCplt(DMA_HandleTypeDef *hdma);
static void UART_DMAReceiveCplt(DMA_HandleTypeDef *hdma);
static void UART_DMARxHalfCplt(DMA_HandleTypeDef *hdma);
static void UART_DMAError(DMA_HandleTypeDef *hdma); 
static HAL_StatusTypeDef UART_WaitOnFlagUntilTimeout(UART_HandleTypeDef *huart, uint32_t Flag, FlagStatus Status, uint32_t Timeout); */

/* Private functions ---------------------------------------------------------*/


SemaphoreHandle_t uart_semaphore_nmea; //uart nmea receive syncro

uint8_t debug_uart;
uint8_t m_uart_rx_pin;
uint8_t m_uart_tx_pin;
uint32_t m_uart_baudrate;
app_uart_event_handler_t m_uart_evt_handler;
/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to 
 *          a string. The string will be be sent over BLE when the last character received was a 
 *          'new line' i.e '\n' (hex 0x0D) or if the string has reached a length of 
 *          @ref NUS_MAX_DATA_LENGTH.
 */
/**@snippet [Handling the data received over UART] */
uint32_t       err_code;
/*
static void uart_event_handle(app_uart_evt_t * p_event)
{
	BaseType_t yield_req = pdFALSE;
	
switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&in_buffer[in_buffer_index]));

        case APP_UART_COMMUNICATION_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_communication);
						//debug_uart = p_event->data.error_communication;
            break;

        case APP_UART_FIFO_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_code);
					//debug_uart = 2;
            break;

        default:
            break;
    }
}
*/
uint32_t HAL_UART_Init(uint8_t rx_pin, uint8_t tx_pin, uint32_t baud_rate,app_uart_event_handler_t event_handler)
{
	 uint32_t    err_code;

   const app_uart_comm_params_t comm_params =
    {
        rx_pin,
        tx_pin,		
        0,
        0,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        baud_rate
    };
		
	
    APP_UART_FIFO_INIT( &comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       event_handler,
                       APP_IRQ_PRIORITY_MID,
                       err_code);
		if (err_code == NRF_SUCCESS)
		{
			m_uart_rx_pin = rx_pin;
			m_uart_tx_pin = tx_pin;
			m_uart_baudrate = baud_rate;
			m_uart_evt_handler = event_handler;
		}
    APP_ERROR_CHECK(err_code);
		uint8_t debug_uart = err_code;
		return err_code;
}


	

void HAL_UART_Uninit(void)
{
 app_uart_close();
}

void HAL_UART_Reset(void)
{
  nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_TXDRDY);
  nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_RXTO);
  nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_ERROR);
  app_uart_close();
	HAL_UART_Init(m_uart_rx_pin,m_uart_tx_pin,m_uart_baudrate,m_uart_evt_handler);
}

void HAL_UART_SerialSend(uint8_t *out,uint8_t len)
{
	for (uint16_t i=0; i<len;i++)
	{
		while(app_uart_put(out[i]) != NRF_SUCCESS);
	}
}

#endif /* HAL_UART_MODULE_ENABLED */
/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT Redimec SRL *****END OF FILE****/
