/**
  ******************************************************************************
  * @file    nrf51xxx_hal_bletransference.c
  * @author  Mariano G. Scasso
  * @version V0.1
  * @date    10-Octubre-2016
  * @brief   BLE HAL NUS transference modu�e.
  *          This file provides firmware functions to manage the following 
  *          functionalities of the General Purpose Input/Output (GPIO) peripheral:
  *           + Initialization and de-initialization functions
  *           + IO operation functions
  *
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Redimec SRL</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>

#include "nordic_ble_functions.h"
#include "nrf51xxx_hal_ble.h"
#include "RWeAR.h"

ble_stream_t Stream;

void HAL_BLE_Init(hal_ble_init_t init) 
{
		
}


//env�a un paquete por BLE_NUS
uint32_t HAL_BLE_PackageSend(uint8_t *package)
{
	uint32_t err_code;
	uint16_t time_out = 10;

	do
	{
		err_code = ble_nus_string_send(&m_nus,package, HAL_BLE_PACKAGE_SIZE);
		time_out--;
	} while (err_code == NRF_ERROR_BUSY && time_out);

	return err_code;
}

uint32_t HAL_BLE_StringSend(uint8_t *data, uint8_t len)
{
	uint32_t err_code;
	uint16_t time_out = 10;
	do
	{
		err_code = ble_nus_string_send(&m_nus,data, len);
		time_out--;
	} while (err_code == NRF_ERROR_BUSY && time_out);

	return err_code;
}

#define POLYNOMIAL 0xA6 

typedef uint8_t crc;

#define WIDTH  	8
#define TOPBIT (1 << (WIDTH - 1))

uint8_t HAL_BLE_crc8(uint8_t const message[], uint8_t nBytes)
{
    uint8_t  remainder = 0;	

    /*
     * Perform modulo-2 division, a byte at a time.
     */
    for (uint8_t byte = 0; byte < nBytes; ++byte)
    {
        /*
         * Bring the next byte into the remainder.
         */
        remainder ^= (message[byte] << (WIDTH - 8));

        /*
         * Perform modulo-2 division, a bit at a time.
         */
        for (uint8_t bit = 8; bit > 0; --bit)
        {
            /*
             * Try to divide the current data bit.
             */
            if (remainder & TOPBIT)
            {
                remainder = (remainder << 1) ^ POLYNOMIAL;
            }
            else
            {
                remainder = (remainder << 1);
            }
        }
    }

    /*
     * The final remainder is the CRC result.
     */
    return (remainder);

}  


//prepara el stream para su env�o
void HAL_BLE_StreamSet(uint8_t * data, uint32_t len, bool binary, bool SEpackage)
{
	uint8_t aux[HAL_BLE_PACKAGE_SIZE];
	
	Stream.SendStartEndPackage = SEpackage;
	Stream.rawData = data;
	Stream.rawDataLen = len;
	Stream.BinaryPackage = binary;
	
	Stream.PackageSize = HAL_BLE_PACKAGE_SIZE;
	
	Stream.PackageQty = len / Stream.PackageSize;
	if (len % Stream.PackageSize)
		Stream.PackageQty++;
	
	Stream.PackageSend=0;
	memcpy(aux,"SOT-PKGQTY:",11);
	//num2Str(S_INT,0, aux+12, Stream.PackageQty);
	//HAL_BLE_PackageNew(aux,Stream.StartPackage,sizeof(aux));
	memcpy(aux,"EOT-CRC16:",10);
	//num2Str(S_INT,0, aux+11, Stream.PackageQty);
	//HAL_BLE_PackageNew(aux,Stream.EndPackage,sizeof(aux));
}

struct ble_binary_package HAL_BLE_NewPackage(uint8_t * data, uint8_t pcknumber)
{
	struct ble_binary_package NewPack;
	
	memcpy(NewPack.data,data,HAL_BLE_PACKAGE_DATA);
	NewPack.number = pcknumber;
	NewPack.crc = HAL_BLE_crc8(data,HAL_BLE_PACKAGE_DATA);
	NewPack.id = WISE_DEVICE_PUBLIC_ID;
	if (pcknumber == Stream.PackageQty -1)
		NewPack.reserved = '*';
	else
		NewPack.reserved = 0;
	return NewPack;
}

uint32_t HAL_BLE_StreamSend( )
{
	uint32_t err_code;
	ble_binary_package_t NewPackage;
	uint16_t j =0;
	

	Stream.TrasmitionInProgress = true;

	while(Stream.PackageSend < Stream.PackageQty)
	{
   		j = Stream.PackageSize * Stream.PackageSend;
			memset(NewPackage.Union,0,HAL_BLE_PACKAGE_SIZE);
		
			if (j + Stream.PackageSize <= Stream.rawDataLen)
			{
				if (Stream.BinaryPackage)
					NewPackage.package = HAL_BLE_NewPackage(Stream.rawData+j,Stream.PackageSend);
			}
	

		if (NRF_SUCCESS != HAL_BLE_PackageSend(NewPackage.Union))
			return err_code;
		else
				Stream.PackageSend++;
	}			
	if (Stream.PackageSend >= Stream.PackageQty)
		{
			if (Stream.SendStartEndPackage)
			{
			err_code = HAL_BLE_PackageSend(Stream.EndPackage);
			if (err_code == NRF_ERROR_BUSY)
				return err_code;
			}
			return HAL_BLE_ERROR_STREAM_COMPLETE;
		}
	return HAL_BLE_ERROR; 	
		
}

uint32_t HAL_BLE_SendPacket(uint8_t number)
{
	ble_binary_package_t NewPackage;
	if (number < Stream.PackageQty)
	{
		NewPackage.package = HAL_BLE_NewPackage(Stream.rawData + Stream.PackageSize * number ,number);
		return HAL_BLE_PackageSend(NewPackage.Union);
	}
	else
		return 1; //error
}




