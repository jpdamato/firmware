/**
  ******************************************************************************
  * @file    nrf51xxx_hal_gpio.c
  * @author  Leandro J. Aguierre
  * @version V0.1
  * @date    10-Octubre-2016
  * @brief   GPIO HAL module driver.
  *          This file provides firmware functions to manage the following 
  *          functionalities of the General Purpose Input/Output (GPIO) peripheral:
  *           + Initialization and de-initialization functions
  *           + IO operation functions
  *
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Redimec SRL</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "nrf51xxx_hal_gpio.h"
#include "app_util_platform.h"
#include "RWeAr.h"
#include "nrf51xxx_hal_delay.h"

/** @addtogroup nRF51xxx_HAL_Driver
  * @{
  */

/** @defgroup GPIO GPIO
  * @brief GPIO HAL module driver
  * @{
  */



/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/** @addtogroup GPIO_Private_Constants GPIO Private Constants
  * @{
  */

#define GPIO_NUMBER           ((uint32_t)31
/**
  * @}
  */
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
/** @defgroup GPIO_Exported_Functions GPIO Exported Functions
  * @{
  */

/** @defgroup GPIO_Exported_Functions_Group1 Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
@verbatim    
 ===============================================================================
              ##### Initialization and de-initialization functions #####
 ===============================================================================
  [..]
    This section provides functions allowing to initialize and de-initialize the GPIOs
    to be ready for use.
 
@endverbatim
  * @{
  */


/**
  * @brief  Initializes the GPIOx peripheral according to the specified parameters in the GPIO_Init.
  * @param  GPIOx: where x can be (A..K) to select the GPIO peripheral for STM32F429X device or
  *                      x can be (A..I) to select the GPIO peripheral for STM32F40XX and STM32F427X devices.
  * @param  GPIO_Init: pointer to a GPIO_InitTypeDef structure that contains
  *         the configuration information for the specified GPIO peripheral.
  * @retval None
  */
	
#if (HAL_GPIO_MODULE_ENABLED == 1)

void HAL_GPIO_Init(GPIO_InitTypeDef *GPIO_Init)
{
  nrf_gpio_cfg(GPIO_Init->Pin, (nrf_gpio_pin_dir_t) GPIO_Init->Dir, (nrf_gpio_pin_input_t)GPIO_Init->Input, (nrf_gpio_pin_pull_t)GPIO_Init->Pull, (nrf_gpio_pin_drive_t)GPIO_Init->Drive, (nrf_gpio_pin_sense_t)GPIO_Init->Sense);
	
}

void HAL_GPIO_InterruptsInit( )
{
	nrf_drv_gpiote_init();
}



/**
  * @brief  De-initializes the GPIOx peripheral registers to their default reset values.
  * @param  GPIO_Pin: specifies the port bit to be written.
  *          This parameter can be one of GPIO_PIN_x where x can be (0..15).
  * @retval None
  */
void HAL_GPIO_DeInit(uint32_t GPIO_Pin)
{
	if(GPIO_Pin < NUMBER_OF_PINS)
	{
		nrf_gpio_cfg_default(GPIO_Pin);
	}
}

/**
  * @}
  */

/** @defgroup GPIO_Exported_Functions_Group2 IO operation functions 
 *  @brief   GPIO Read and Write
 *
@verbatim
 ===============================================================================
                       ##### IO operation functions #####
 ===============================================================================

@endverbatim
  * @{
  */

/**
  * @brief  Reads the specified input port pin.
  * @param  GPIOx: where x can be (A..K) to select the GPIO peripheral for STM32F429X device or
  *                      x can be (A..I) to select the GPIO peripheral for STM32F40XX and STM32F427X devices.
  * @param  GPIO_Pin: specifies the port bit to read.
  *         This parameter can be GPIO_PIN_x where x can be (0..15).
  * @retval The input port pin value.
  */
GPIO_PinState HAL_GPIO_ReadPin(uint32_t GPIO_Pin)
{
		return  (GPIO_PinState)((NRF_GPIO->IN >> GPIO_Pin) & 1UL);
}

/**
  * @brief  Sets or clears the selected data port bit.
  *
  * @note   This function uses GPIOx_BSRR register to allow atomic read/modify
  *         accesses. In this way, there is no risk of an IRQ occurring between
  *         the read and the modify access.
  *
  * @param  GPIO_Pin: specifies the port bit to be written.
  *          This parameter can be one of GPIO_PIN_x where x can be (0..15).
  * @param  PinState: specifies the value to be written to the selected bit.
  *          This parameter can be one of the GPIO_PinState enum values:
  *            @arg GPIO_PIN_RESET: to clear the port pin
  *            @arg GPIO_PIN_SET: to set the port pin
  * @retval None
  */
void HAL_GPIO_OutPin(uint32_t GPIO_Pin, GPIO_PinState PinState)
{
	if(GPIO_Pin < NUMBER_OF_PINS)
	{
		if(PinState != GPIO_PIN_RESET)
			nrf_gpio_pin_set(GPIO_Pin);
		else
			nrf_gpio_pin_clear(GPIO_Pin);
	}
}

/**
  * @brief  Toggles the specified GPIO pins.
  * @param  GPIO_Pin: Specifies the pins to be toggled.
  * @retval None
  */
void HAL_GPIO_TogglePin(uint32_t GPIO_Pin)
{
  /* Check the parameters */
  if(GPIO_Pin < NUMBER_OF_PINS)
	{
		nrf_gpio_pin_toggle(GPIO_Pin);
	}
}
/**
  * @brief  Function for reseting pin configuration to its default state.
  * @param  GPIO_Pin Specifies the pin number (allowed values 0-31).
  * @retval None
  */
void HAL_GPIO_Default(uint32_t GPIO_Pin)
{
	if(GPIO_Pin < NUMBER_OF_PINS)
	{
		nrf_gpio_cfg_default(GPIO_Pin);
	}
}
/**
 * @brief Function for writing a value to a GPIO pin.
 *
 * Note that the pin must be configured as an output for this
 * function to have any effect.
 *
 * @param GPIO_Pin specifies the pin number (0-31) to
 * write.
 *
 * @param Value specifies the value to be written to the pin.
 * @arg 0 clears the pin
 * @arg >=1 sets the pin.
 * @retval None
*/
void HAL_GPIO_WritePin(uint32_t GPIO_Pin, uint32_t Value)
{
	if(GPIO_Pin < NUMBER_OF_PINS)
	{
		nrf_gpio_pin_write(GPIO_Pin, Value);
	}
}
/**
 * @brief Function for reading the sense configuration of a GPIO pin.
 * @param pin_number specifies the pin number (0-31) to read.
 * @retval Sense configuration
 */
uint32_t HAL_GPIO_GetSensePin(uint32_t GPIO_Pin)
{
		return (uint32_t)nrf_gpio_pin_sense_get(GPIO_Pin);
}
/**
  * @brief  Function for configuring the given GPIO pin number as a watcher. Only input is connected.
  * @param  GPIO_Pin Specifies the pin number (allowed values 0-31).
  * @retval None
  */
void HAL_GPIO_Watcher(uint32_t GPIO_Pin)
{
	if(GPIO_Pin < NUMBER_OF_PINS)
	{
		nrf_gpio_cfg_watcher(GPIO_Pin);
	}
}
/**
  * @brief  Function for disconnecting input for the given GPIO.
  * @param  GPIO_Pin Specifies the pin number (allowed values 0-31).
  * @retval None
  */
void HAL_GPIO_InputDisconnect(uint32_t GPIO_Pin)
{
	if(GPIO_Pin < NUMBER_OF_PINS)
	{
		nrf_gpio_input_disconnect(GPIO_Pin);
	}
}
/**
  * @brief  Function for setting the direction for a GPIO pin.
  * @param  GPIO_Pin Specifies the pin number (allowed values 0-31).
	* @param 	Dir specifies the direction
  * @retval None
  */
void HAL_GPIO_SetDir(uint32_t GPIO_Pin, GPIO_PinInput Dir)
{
	if(GPIO_Pin < NUMBER_OF_PINS)
	{
		nrf_gpio_pin_dir_set(GPIO_Pin, (nrf_gpio_pin_dir_t)Dir);
	}
}
/**
 * @brief Function for setting a GPIO pin.
 *
 * Note that the pin must be configured as an output for this
 * function to have any effect.
 *
 * @param pin_number Specifies the pin number (0-31) to set.
 * @retval None
 */
void HAL_GPIO_SetPin(uint32_t GPIO_Pin)
{
	if(GPIO_Pin < NUMBER_OF_PINS)
	{
		nrf_gpio_pin_set(GPIO_Pin);
	}
}
/**
 * @brief Function for clearing a GPIO pin.
 *
 * Note that the pin must be configured as an output for this
 * function to have any effect.
 *
 * @param pin_number Specifies the pin number (0-31) to set.
 * @retval None
 */
void HAL_GPIO_ResetPin(uint32_t GPIO_Pin)
{
	if(GPIO_Pin < NUMBER_OF_PINS)
	{
		nrf_gpio_pin_clear(GPIO_Pin);
	}
}
/**
  * @brief  This function handles EXTI interrupt request.
  * @param  GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
void HAL_GPIO_EXTI_IRQHandler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
  //HAL_GPIO_SetPin(LED4_NUMBER);
}


/**
  * @brief  This function handles EXTI interrupt request.
  * @param  GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */


 void Go2Sleep(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
		ret_code_t err_code;
				
				nrf_drv_gpiote_in_uninit(BUTTON1_PIN);
				nrf_drv_gpiote_in_uninit(INT9DOF_PIN_NUMBER);
				//Disable power-down button to prevent System-off wakeup
				//nrf_drv_gpiote_in_uninit(BUTTON2_PIN);           
       // nrf_drv_gpiote_in_event_disable(BUTTON2_PIN);  
	
				LEDsOff();
				for (int i=0;i<3;i++)
				{
					HAL_GPIO_DelayMS(30);
					HAL_GPIO_TogglePin(LED_RED);
					HAL_GPIO_DelayMS(50);
				}
	
				//Configure wake-up button
				nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(false);     //Configure to generate interrupt and wakeup on pin signal low. "false" means that gpiote will use the PORT event, which is low power, i.e. does not add any noticable current consumption (<<1uA). Setting this to "true" will make the gpiote module use GPIOTE->IN events which add ~8uA for nRF52 and ~1mA for nRF51.
				in_config.pull = NRF_GPIO_PIN_PULLUP;                                            //Configure pullup for input pin to prevent it from floting. Pin is pulled down when button is pressed on nRF5x-DK boards, see figure two in http://infocenter.nordicsemi.com/topic/com.nordic.infocenter.nrf52/dita/nrf52/development/dev_kit_v1.1.0/hw_btns_leds.html?cp=2_0_0_1_4
				err_code = nrf_drv_gpiote_in_init(BUTTON1_PIN, &in_config, NULL);             //Initialize the wake-up pin
				//APP_ERROR_CHECK(err_code);                                                       //Check error code returned
				nrf_drv_gpiote_in_event_enable(BUTTON1_PIN, true);                            //Enable event and interrupt for the wakeup pin
				
				LEDsOff();	
				//Enter System-off
				  err_code = sd_power_system_off();		
}

void HAL_GPIO_Go2Sleep()
{
	Go2Sleep(NULL, NULL); 
}


void HAL_GPIO_SleepConfig(void)
{

//Configure sense input pin to enable wakeup and interrupt on button press.
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(false);     //Configure to generate interrupt and wakeup on pin signal low. "false" means that gpiote will use the PORT event, which is low power, i.e. does not add any noticable current consumption (<<1uA). Setting this to "true" will make the gpiote module use GPIOTE->IN events which add ~8uA for nRF52 and ~1mA for nRF51.
    in_config.pull = NRF_GPIO_PIN_PULLUP;                                            //Configure pullup for input pin to prevent it from floting. Pin is pulled down when button is pressed on nRF5x-DK boards, see figure two in http://infocenter.nordicsemi.com/topic/com.nordic.infocenter.nrf52/dita/nrf52/development/dev_kit_v1.1.0/hw_btns_leds.html?cp=2_0_0_1_4		
    nrf_drv_gpiote_in_init(BUTTON1_PIN, &in_config, Go2Sleep);   //Initialize the pin with interrupt handler in_pin_handler
		nrf_drv_gpiote_in_event_enable(BUTTON1_PIN, true);                            //Enable event and interrupt for the wakeup pin
}

uint32_t HAL_GPIO_EXTI_Init(uint16_t GPIO_Pin,uint16_t Pull, GPIO_SenseMode mode, nrf_drv_gpiote_evt_handler_t IRQHandler)
{
     
	ret_code_t err_code;
	nrf_drv_gpiote_in_config_t in_config;
	in_config.is_watcher = false;
  in_config.hi_accuracy = true;
	in_config.pull = Pull;
	in_config.sense = (nrf_gpiote_polarity_t) mode;

  err_code = nrf_drv_gpiote_in_init(GPIO_Pin, &in_config,  IRQHandler);
  nrf_drv_gpiote_in_event_enable(GPIO_Pin, true);
	
	return err_code;

}
/**
  * @brief  EXTI line detection callbacks.
  * @param  GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
__weak void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  
}

/**
  * @}
  */


/**
  * @}
  */

#endif /* HAL_GPIO_MODULE_ENABLED */
/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
