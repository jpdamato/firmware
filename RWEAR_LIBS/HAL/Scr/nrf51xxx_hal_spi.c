/**
  ******************************************************************************
  * @file    nrf51xxx_hal_spi.c
  * @author  Leandro J. Aguierre
  * @version V0.1
  * @date    21-Octubre-2016
  * @brief   UART HAL module driver.
  *          This file provides firmware functions to manage the following 
  *          functionalities of the Universal Asynchronous Receiver Transmitter (UART) peripheral:
  *           + Initialization and de-initialization functions
  *           + IO operation functions
  *           + Peripheral Control functions  
  *           + Peripheral State and Errors functions  
  *           
  @verbatim       
  @endverbatim
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Redimec SRL</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Redimec SRL nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "RWeAr.h"
#include "nrf51xxx_hal_spi.h"
#include "app_uart.h"
#include "app_error.h"

/** @defgroup SPI
  * @brief HAL SPI module driver
  * @{
  */

#if (HAL_SPI_MODULE_ENABLED == 1)
    
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define SPI_TIMEOUT_VALUE  	5000						/**< SPI Time out config*/
#define SPI_INSTANCE  			1 						 /**< SPI instance index. */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
volatile bool SPIbusy;
/* Private function prototypes -----------------------------------------------*/
uint32_t spi_debug;
/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Manejo de Interrupciones del SPI. 
  * @retval None
  */
void SPI1_IRQHandler(nrf_drv_spi_evt_t const * p_event, void * p_context)
{
	SPIbusy	= false;
}
/**
  * @brief  Initializes the SPI peripheral. 
  * @retval HAL status
  */
uint32_t HAL_SPI_Init(SPI_InitTypeDef *SPI)
{
	uint32_t err_code;
	nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
	
	
	
	spi_config.ss_pin = SPI->SS_Pin;
	spi_config.mosi_pin = SPI->MOSI_Pin;
	spi_config.miso_pin = SPI->MISO_Pin;
	spi_config.sck_pin = SPI->SCK_Pin;
	spi_config.irq_priority = SPI->IRQ_Priority;
	//spi_config.orc = SPI->ORC;
	spi_config.mode = (nrf_drv_spi_mode_t)SPI->Mode;  
	spi_config.frequency = (nrf_drv_spi_frequency_t)SPI->Frequency;
	spi_config.bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST;
	err_code =  nrf_drv_spi_init(&spi, &spi_config, SPI1_IRQHandler,NULL); //modo evento, con bandera SPIBusy (ver para implementar timeout)
	//err_code =  nrf_drv_spi_init(&spi, &spi_config, NULL); //Secuencial
	SPIbusy = false;
	spi_debug = err_code;
	return err_code;
}
/**
  * @brief  Initializes the SPI Transmit. 
  * @retval HAL status
  */
uint32_t HAL_SPI_Transmit(uint8_t *spiBuffer, uint32_t len, uint32_t timeOut)
{
	uint32_t err_code;
	uint8_t Buffer[4];
	
	err_code = nrf_drv_spi_transfer(&spi, spiBuffer, len, Buffer, 4);
	
	return err_code;
}
/**
  * @brief  Initializes the SPI Receive. 
  * @retval HAL status
  */
uint32_t HAL_SPI_Receive(uint8_t *spiBuffer, uint32_t len, uint32_t timeOut)
{
	uint32_t err_code;
	uint8_t Buffer;
	
	err_code = nrf_drv_spi_transfer(&spi, &Buffer, 1, spiBuffer, len);
	
	return err_code;
}
/**
  * @brief  Initializes the SPI Transmit/Receive. 
  * @retval HAL status
  */
uint32_t HAL_SPI_Transfer(uint8_t *spiBufferTx, uint32_t iTx, uint8_t *spiBufferRx, uint32_t iRx, uint32_t timeOut)
{
	uint32_t err_code;
	uint32_t trys=0;
	SPIbusy = true;
	err_code = nrf_drv_spi_transfer(&spi, spiBufferTx, iTx, spiBufferRx, iRx);

	while ((SPIbusy) && (trys < timeOut * SPI_TIMEOUT_VALUE))
		trys++;
spi_debug =err_code;	
if (trys >= SPI_TIMEOUT_VALUE)
	return HAL_SPI_ERROR_TIMEOUT;
else
		return err_code;

}

void HAL_SPI_DebugSerial(uint8_t *out,uint8_t len)
{
#ifdef DEBUG_SERIAL
	for (uint16_t i=0; i<len;i++)
	{
		while(app_uart_put(out[i]) != NRF_SUCCESS);
	}
#endif
}

#endif /* HAL_SPI_MODULE_ENABLED */
/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT Redimec SRL *****END OF FILE****/
