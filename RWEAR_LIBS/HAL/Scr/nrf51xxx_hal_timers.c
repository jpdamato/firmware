#include "nrf51xxx_hal_timers.h"

const nrf_drv_timer_t TIMER_0 = NRF_DRV_TIMER_INSTANCE(0);

void HAL_TIMERS_Timer0Init(nrf_timer_event_handler_t TimerIRQ, uint16_t period)
{
	 uint32_t err_code = NRF_SUCCESS;
   uint32_t time_ticks;

//Configure TIMER_LED for generating simple light effect - leds on board will invert his state one after the other.
    err_code = nrf_drv_timer_init(&TIMER_0, NULL, TimerIRQ);
    APP_ERROR_CHECK(err_code);
    
    time_ticks = nrf_drv_timer_ms_to_ticks(&TIMER_0, period);
    
    nrf_drv_timer_extended_compare(
         &TIMER_0, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);
    
    nrf_drv_timer_enable(&TIMER_0);
}

