/**
  ******************************************************************************
  * @file    nrf51xxx_hal_spi.c
  * @author  Leandro J. Aguierre
  * @version V0.1
  * @date    21-Octubre-2016
  * @brief   UART HAL module driver.
  *          This file provides firmware functions to manage the following 
  *          functionalities of the Universal Asynchronous Receiver Transmitter (UART) peripheral:
  *           + Initialization and de-initialization functions
  *           + IO operation functions
  *           + Peripheral Control functions  
  *           + Peripheral State and Errors functions  
  *           
  @verbatim       
  @endverbatim
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Redimec SRL</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Redimec SRL nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "nrf_drv_twi.h"
#include "nrf51xxx_hal_i2c.h"
#include "app_uart.h"
#include "app_error.h"
#include "RWeAR.h"
/** @defgroup SPI
  * @brief HAL SPI module driver
  * @{
  */
#if (HAL_I2C_MODULE_ENABLED == 1)
    
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define I2C_TIMEOUT_VALUE  	22000
#define I2C_INSTANCE  			0 						 /**< SPI instance index. */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static const nrf_drv_twi_t I2C = NRF_DRV_TWI_INSTANCE(I2C_INSTANCE);
volatile bool I2C_TXbusy;
volatile bool I2C_RXbusy;
volatile uint8_t I2Crty;
/* Private function prototypes -----------------------------------------------*/
	//static uint32_t err_code;
/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Manejo de Interrupciones del SPI. 
  * @retval None
  */
void I2C_IRQHandler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{

	switch (p_event->type)
  {
	case NRF_DRV_TWI_EVT_DONE:	
		 switch(p_event->xfer_desc.type)
            {
                case NRF_DRV_TWI_XFER_TX:
                    I2C_TXbusy = false;
                    break;
                case NRF_DRV_TWI_XFER_TXTX:
                    I2C_TXbusy = false;
                    break;
                case NRF_DRV_TWI_XFER_RX:
                    I2C_TXbusy = false;
                    break;
                case NRF_DRV_TWI_XFER_TXRX:
                    I2C_TXbusy = false;
                    break;
                default:
                    break;
            }
      break;

		case NRF_DRV_TWI_EVT_ADDRESS_NACK:
				//I2Crty++;
				break;
    default:
      //  I2Cbusy=1;// No implementation needed.
        break;
    }
}
/**
  * @brief  Initializes the SPI peripheral. 
  * @retval HAL status
  */
uint32_t HAL_I2C_Init(I2C_InitTypeDef *I2C_init)
{
	uint32_t err_code;
	nrf_drv_twi_config_t i2c_config;
	
	i2c_config.frequency = I2C_init->Frequency;
	i2c_config.interrupt_priority = I2C_init->IRQ_Priority;
	i2c_config.scl = I2C_init->SCL_Pin;
	i2c_config.sda = I2C_init->SDA_Pin;
	
	err_code =  nrf_drv_twi_init(&I2C,&i2c_config,NULL,NULL);
	nrf_drv_twi_enable(&I2C);
	I2C_TXbusy = false;
	I2C_RXbusy = false;

	return err_code;
}
/**
  * @brief  Initializes the I2C Transmit. 
  * @retval HAL status
  */
uint32_t HAL_I2C_Transmit(uint8_t addr, uint8_t *data,uint8_t size,bool no_stop, uint32_t timeOut)
{
	uint32_t err_code;
	
	I2C_TXbusy=1;
	I2Crty=1;
	err_code = nrf_drv_twi_tx(&I2C,addr,data,size,no_stop);
	/*
	while (I2C_TXbusy)
	{
		if (I2Crty % 2 == 0)
		{
			I2Crty++;
			err_code = nrf_drv_twi_tx(&I2C,addr,data,size,no_stop);
		}
		else
			break;
	}
	*/
	return err_code;
}
/**
  * @brief  Initializes the I2C Receive. 
  * @retval HAL status
  */
uint32_t HAL_I2C_Receive(uint8_t addr, uint8_t *data,uint8_t size, uint32_t timeOut)
{
	uint32_t err_code;
	uint8_t Buffer;

	I2C_RXbusy = true;
	I2Crty=1;
	err_code = nrf_drv_twi_rx(&I2C,addr,data,size);
	/*
	while (I2C_RXbusy)
	{
		if (I2Crty % 2 == 0)
		{
			I2Crty++;
			err_code = nrf_drv_twi_rx(&I2C,addr,data,size);
		}
		else
			break;
	}
	*/
	return err_code;
}


/**
  * @brief  Initializes the SPI Transmit/Receive. 
  * @retval HAL status
  */
uint32_t HAL_I2C_Transfer(uint8_t *spiBufferTx, uint32_t iTx, uint8_t *spiBufferRx, uint32_t iRx, uint32_t timeOut)
{
	uint32_t err_code;
	I2C_TXbusy = true;
	//err_code = nrf_drv_twi_xfer(&I2C,
	//err_code = nrf_drv_spi_transfer(&spi, spiBufferTx, iTx, spiBufferRx, iRx);

	while (I2C_TXbusy); // OR TIME OUT

	return err_code;
}



#endif /* HAL_SPI_MODULE_ENABLED */
/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT Redimec SRL *****END OF FILE****/
