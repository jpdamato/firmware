/**
  ******************************************************************************
  * @file    nrf51xxx_hal_spi.h
  * @author  Leandro J. Aguierre
  * @version V0.1
  * @date    21-Octubre-2016
  * @brief   Header file of UART HAL module.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Redimec SRL</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NRF51XXX_HAL_I2C_H
#define __NRF51XXX_HAL_I2C_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "app_error.h"


/** @addtogroup NRF_HAL_Driver
  * @{
  */

/** @addtogroup SPI
  * @{
  */ 
/**
 * @brief SPI data rates.
 */
typedef enum
{
    I2C_FREQ_100K = TWI_FREQUENCY_FREQUENCY_K100, 	///< 125 kbps.
    I2C_FREQ_250K = TWI_FREQUENCY_FREQUENCY_K250, 	///< 250 kbps.
    I2C_FREQ_400K = TWI_FREQUENCY_FREQUENCY_K400, 	///< 500 kbps.
}   I2C_Frequency_t;
	
	

/* Exported types ------------------------------------------------------------*/ 
/** 
  * @brief I2C Init Structure definition  
  */ 
typedef struct
{
    uint8_t SDA_Pin;      ///< SDA pin number.
    uint8_t SCL_Pin;     ///< SCL pin number (optional).
    uint8_t IRQ_Priority; ///< Interrupt priority.
    I2C_Frequency_t Frequency; ///< SPI frequency.
} I2C_InitTypeDef;


extern volatile bool I2C_Txbusy;
extern volatile bool I2C_Rxbusy;
//Prototipo de funciones
uint32_t HAL_I2C_Init(I2C_InitTypeDef *I2C);
uint32_t HAL_I2C_Transmit(uint8_t addr, uint8_t *data,uint8_t size,bool no_stop, uint32_t timeOut);
uint32_t HAL_I2C_Receive(uint8_t addr, uint8_t *data,uint8_t size, uint32_t timeOut);
//uint32_t HAL_I2C_Transfer(uint8_t *spiBufferTx, uint32_t iTx, uint8_t *spiBufferRx, uint32_t iRx, uint32_t timeOut);


#endif /* __NRF51XXX_HAL_UART_H */

/************************ (C) COPYRIGHT Redimec SRL *****END OF FILE****/
