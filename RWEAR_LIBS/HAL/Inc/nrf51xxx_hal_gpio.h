/**
  ******************************************************************************
  * @file    NRF51XXX_hal_gpio.h
  * @author  Leandro J. Aguierre
  * @version V0.1
  * @date    10-Octubre-2016
  * @brief   Header file of GPIO HAL module.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Redimec SRL</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NRF51XXX_HAL_GPIO_H
#define __NRF51XXX_HAL_GPIO_H

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"

#ifdef __cplusplus
 extern "C" {
#endif

	 
/* Includes ------------------------------------------------------------------*/

	 
	 
/** @addtogroup NRF51XXX_HAL_Driver
  * @{
  */

/** @addtogroup GPIO
  * @{
  */ 

/* Exported types ------------------------------------------------------------*/
/** @defgroup GPIO_Exported_Types GPIO Exported Types
  * @{
  */

/** 
  * @brief GPIO Init structure definition  
  */ 
typedef struct
{
  uint32_t Pin;       /*!< Specifies the GPIO pins to be configured.
                           This parameter can be any value of @ref GPIO_pins_define */
  uint32_t Dir;      	/*!< Specifies the operating mode for the selected pins.
											     This parameter can be a value of @ref GPIO_mode_define */
  uint32_t Input;        
  uint32_t Pull;      /*!< Specifies the Pull-up or Pull-Down activation for the selected pins.
                           This parameter can be a value of @ref GPIO_pull_define */
  uint32_t Drive;                               
  uint32_t Sense;  
}GPIO_InitTypeDef;
/** 
  * @brief  GPIO Bit SET and Bit RESET enumeration 
  */
typedef enum
{
  GPIO_PIN_RESET = 0,
  GPIO_PIN_SET
}GPIO_PinState;
/** 
  * @brief  GPIO Bit INPUT and Bit OUTPUT enumeration 
  */
typedef enum
{
  GPIO_PIN_INPUT = 0,
  GPIO_PIN_OUTPUT
}GPIO_PinInput;

typedef enum
{
	GPIO_SENSE_HITOLO = NRF_GPIOTE_POLARITY_HITOLO,
	GPIO_SENSE_LOTOHI = NRF_GPIOTE_POLARITY_LOTOHI,
	GPIO_SENSE_CHANGE = NRF_GPIOTE_POLARITY_TOGGLE
}GPIO_SenseMode;
/**
  * @}
  */

/* Exported constants --------------------------------------------------------*/

/** @defgroup GPIO_Exported_Constants GPIO Exported Constants
  * @{
  */ 

/** @defgroup GPIO_pins_define GPIO pins define
  * @{
  */
#define GPIO_PIN_0                 ((uint16_t)0)  /* Pin 0 selected    */
#define GPIO_PIN_1                 ((uint16_t)1)  /* Pin 1 selected    */
#define GPIO_PIN_2                 ((uint16_t)2)  /* Pin 2 selected    */
#define GPIO_PIN_3                 ((uint16_t)3)  /* Pin 3 selected    */
#define GPIO_PIN_4                 ((uint16_t)4)  /* Pin 4 selected    */
#define GPIO_PIN_5                 ((uint16_t)5)  /* Pin 5 selected    */
#define GPIO_PIN_6                 ((uint16_t)6)  /* Pin 6 selected    */
#define GPIO_PIN_7                 ((uint16_t)7)  /* Pin 7 selected    */
#define GPIO_PIN_8                 ((uint16_t)8)  /* Pin 8 selected    */
#define GPIO_PIN_9                 ((uint16_t)9)  /* Pin 9 selected    */
#define GPIO_PIN_10                ((uint16_t)10)  /* Pin 10 selected   */
#define GPIO_PIN_11                ((uint16_t)11)  /* Pin 11 selected   */
#define GPIO_PIN_12                ((uint16_t)12)  /* Pin 12 selected   */
#define GPIO_PIN_13                ((uint16_t)13)  /* Pin 13 selected   */
#define GPIO_PIN_14                ((uint16_t)14)  /* Pin 14 selected   */
#define GPIO_PIN_15                ((uint16_t)15)  /* Pin 15 selected   */
#define GPIO_PIN_All               ((uint16_t)0xFFFF)  /* All pins selected */

#define GPIO_PIN_MASK              ((uint32_t)0x0000FFFF) /* PIN mask for assert test */
/**
  * @}
  */

/** @defgroup GPIO_mode_define GPIO mode define
  * @brief GPIO Configuration Mode 
  *        Elements values convention: 0xX0yz00YZ
  *           - X  : GPIO mode or EXTI Mode
  *           - y  : External IT or Event trigger detection 
  *           - z  : IO configuration on External IT or Event
  *           - Y  : Output type (Push Pull or Open Drain)
  *           - Z  : IO Direction mode (Input, Output, Alternate or Analog)
  * @{
  */ 
/**
  * @}
  */
/**
  * @}
  */

 /** @defgroup GPIO_pull_define GPIO pull define
   * @brief GPIO Pull-Up or Pull-Down Activation
   * @{
   */  
#define  GPIO_NOPULL        ((uint32_t)0x00000000)   /*!< No Pull-up or Pull-down activation  */
#define  GPIO_PULLUP        ((uint32_t)0x00000001)   /*!< Pull-up activation                  */
#define  GPIO_PULLDOWN      ((uint32_t)0x00000002)   /*!< Pull-down activation                */
/**
  * @}
  */
  
/**
  * @}
  */

/* Exported functions --------------------------------------------------------*/
/** @addtogroup GPIO_Exported_Functions
  * @{
  */

/** @addtogroup GPIO_Exported_Functions_Group1
  * @{
  */
/* Initialization and de-initialization functions *****************************/
void HAL_GPIO_Init(GPIO_InitTypeDef *GPIO_Init);
void HAL_GPIO_InterruptsInit(void);
void HAL_GPIO_DeInit(uint32_t GPIO_Pin);
uint32_t HAL_GPIO_EXTI_Init(uint16_t GPIO_Pin,uint16_t Pull, GPIO_SenseMode mode, nrf_drv_gpiote_evt_handler_t IRQHandler);
void HAL_GPIO_SleepConfig(void);
void HAL_GPIO_Go2Sleep(void);


/**
  * @}
  */

/** @addtogroup GPIO_Exported_Functions_Group2
  * @{
  */
/* IO operation functions *****************************************************/
GPIO_PinState HAL_GPIO_ReadPin(uint32_t GPIO_Pin);
void HAL_GPIO_OutPin(uint32_t GPIO_Pin, GPIO_PinState PinState);
void HAL_GPIO_TogglePin(uint32_t GPIO_Pin);
void HAL_GPIO_Default(uint32_t GPIO_Pin);
void HAL_GPIO_WritePin(uint32_t GPIO_Pin, uint32_t Value);
uint32_t HAL_GPIO_GetSensePin(uint32_t GPIO_Pin);
void HAL_GPIO_Watcher(uint32_t GPIO_Pin);
void HAL_GPIO_InputDisconnect(uint32_t GPIO_Pin);
void HAL_GPIO_SetDir(uint32_t GPIO_Pin, GPIO_PinInput Dir);
void HAL_GPIO_SetPin(uint32_t GPIO_Pin);
void HAL_GPIO_ResetPin(uint32_t GPIO_Pin);
void HAL_GPIO_EXTI_IRQHandler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
__weak void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
/**
  * @}
  */ 

/**
  * @}
  */ 
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/** @defgroup GPIO_Private_Constants GPIO Private Constants
  * @{
  */

/**
  * @}
  */

/* Private macros ------------------------------------------------------------*/


/* Private functions ---------------------------------------------------------*/
/** @defgroup GPIO_Private_Functions GPIO Private Functions
  * @{
  */

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __STM32F4xx_HAL_GPIO_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
