/**
  ******************************************************************************
  * @file    nrf51xxx_hal_uart.h
  * @author  Leandro J. Aguierre
  * @version V0.1
  * @date    11-Octubre-2016
  * @brief   Header file of UART HAL module.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Redimec SRL</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NRF51XXX_HAL_UART_H
#define __NRF51XXX_HAL_UART_H

#include <stdint.h>
#include "nrf_uart.h"
#include "app_uart.h"
#include "RWeAR.h"

//RTOS
#include "FreeRTOS.h"
#include "semphr.h"

#define FlowCtrlDIS					APP_UART_FLOW_CONTROL_DISABLED				
#define FlowCtrolENA				APP_UART_FLOW_CONTROL_ENABLED
#define FlowCtrlLowPower		APP_UART_FLOW_CONTROL_LOW_POWER

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */

#define UART_NMEA_BUFFER_SIZE						128
/* Includes ------------------------------------------------------------------*/


/** @addtogroup NRF_HAL_Driver
  * @{
  */

/** @addtogroup UART
  * @{
  */ 

/* Exported types ------------------------------------------------------------*/ 
/** 
  * @brief USART Init Structure definition  
  */ 
typedef struct
{
    uint8_t                 Rx_Pin;    	/**< RX pin number. */
    uint8_t                 Tx_Pin;    	/**< TX pin number. */
    uint8_t                 RTS_Pin;  	/**< RTS pin number, only used if flow control is enabled. */
    uint8_t                 CTS_Pin;   	/**< CTS pin number, only used if flow control is enabled. */
    uint8_t									FlowCtrl; 	/**< Flow control setting, if flow control is used, the system will use low power UART mode, based on CTS signal. */
    bool                    Parity;   	/**< Even parity if TRUE, no parity if FALSE. */
    uint32_t                BaudRate;   /**< Baud rate configuration. */
} UART_InitTypeDef;

/** 
  * @brief HAL State structures definition  
  */ 
typedef enum
{
  HAL_USART_STATE_RESET             = 0x00,    /*!< Peripheral is not yet Initialized   */
  HAL_USART_STATE_READY             = 0x01,    /*!< Peripheral Initialized and ready for use */
  HAL_USART_STATE_BUSY              = 0x02,    /*!< an internal process is ongoing */   
  HAL_USART_STATE_BUSY_TX           = 0x12,    /*!< Data Transmission process is ongoing */ 
  HAL_USART_STATE_BUSY_RX           = 0x22,    /*!< Data Reception process is ongoing */
  HAL_USART_STATE_BUSY_TX_RX        = 0x32,    /*!< Data Transmission Reception process is ongoing */
  HAL_USART_STATE_TIMEOUT           = 0x03,    /*!< Timeout state */
  HAL_USART_STATE_ERROR             = 0x04     /*!< Error */      
}HAL_USART_StateTypeDef;

/** 
  * @brief  HAL USART Error Code structure definition  
  */ 
typedef enum
{
  HAL_USART_ERROR_NONE      = 0x00,    /*!< No error            */
  HAL_USART_ERROR_PE        = 0x01,    /*!< Parity error        */
  HAL_USART_ERROR_NE        = 0x02,    /*!< Noise error         */
  HAL_USART_ERROR_FE        = 0x04,    /*!< frame error         */
  HAL_USART_ERROR_ORE       = 0x08,    /*!< Overrun error       */
  HAL_USART_ERROR_DMA       = 0x10     /*!< DMA transfer error  */
}HAL_USART_ErrorTypeDef;


extern SemaphoreHandle_t uart_semaphore_nmea; //uart nmea receive syncro
extern uint8_t uart_nmea_buffer[UART_NMEA_BUFFER_SIZE];
extern uint8_t uart_nmea_buffer_index;
uint32_t HAL_UART_Init(uint8_t rx_pin, uint8_t tx_pin, uint32_t baud_rate,app_uart_event_handler_t event_handler);
void HAL_UART_Reset(void);
void HAL_UART_Uninit(void);
void HAL_UART_SerialSend(uint8_t *out,uint8_t len);
#endif /* __NRF51XXX_HAL_UART_H */

/************************ (C) COPYRIGHT Redimec SRL *****END OF FILE****/
