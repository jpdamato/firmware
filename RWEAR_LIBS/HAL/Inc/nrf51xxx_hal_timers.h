#include "nrf_drv_timer.h"

#ifndef __NRF51XXX_HAL_TIMERS_H
#define __NRF51XXX_HAL_TIMERS_H


void HAL_TIMERS_Timer0Init(nrf_timer_event_handler_t TimerIRQ, uint16_t period);

#endif //  __NRF51XXX_HAL_TIMERS_H