/**
  ******************************************************************************
  * @file    nrf51xxx_hal_spi.h
  * @author  Leandro J. Aguierre
  * @version V0.1
  * @date    21-Octubre-2016
  * @brief   Header file of UART HAL module.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Redimec SRL</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NRF51XXX_HAL_SPI_H
#define __NRF51XXX_HAL_SPI_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "app_error.h"
#include "nrf_drv_spi.h"


#define HAL_SPI_SUCCESS				NRF_SUCCESS
#define HAL_SPI_ERROR_TIMEOUT	NRF_ERROR_TIMEOUT
/** @addtogroup NRF_HAL_Driver
  * @{
  */

/** @addtogroup SPI
  * @{
  */ 
/**
 * @brief SPI data rates.
 */
typedef enum
{
    SPI_FREQ_125K = NRF_DRV_SPI_FREQ_125K, 	///< 125 kbps.
    SPI_FREQ_250K = NRF_DRV_SPI_FREQ_250K, 	///< 250 kbps.
    SPI_FREQ_500K = NRF_DRV_SPI_FREQ_500K, 	///< 500 kbps.
    SPI_FREQ_1M = NRF_DRV_SPI_FREQ_1M,	   	///< 1 Mbps.
    SPI_FREQ_2M = NRF_DRV_SPI_FREQ_2M,  	 	///< 2 Mbps.
    SPI_FREQ_4M = NRF_DRV_SPI_FREQ_4M,   		///< 4 Mbps.
		SPI_FREQ_8M = NRF_DRV_SPI_FREQ_8M    		///< 8 Mbps.
} SPI_Frequency_t;	
	
/**
 * @brief SPI modes.
 */
typedef enum
{
    SPI_MODE0 = NRF_DRV_SPI_MODE_0, ///< SCK active high, sample on leading edge of clock.
    SPI_MODE1 = NRF_DRV_SPI_MODE_1, ///< SCK active high, sample on trailing edge of clock.
    SPI_MODE2 = NRF_DRV_SPI_MODE_2, ///< SCK active low, sample on leading edge of clock.
    SPI_MODE3 = NRF_DRV_SPI_MODE_3  ///< SCK active low, sample on trailing edge of clock.
} SPI_Mode_t;

/**
 * @brief SPI bit orders.
 */
typedef enum
{
    SPI_BIT_ORDER_MSB_FIRST = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST, ///< Most significant bit shifted out first.
    SPI_BIT_ORDER_LSB_FIRST = NRF_DRV_SPI_BIT_ORDER_LSB_FIRST  ///< Least significant bit shifted out first.
} SPI_Bit_Order_t;

/* Exported types ------------------------------------------------------------*/ 
/** 
  * @brief SPI Init Structure definition  
  */ 
typedef struct
{
    uint8_t SCK_Pin;      ///< SCK pin number.
    uint8_t MOSI_Pin;     ///< MOSI pin number (optional).
                          /**< Set to @ref NRF_DRV_SPI_PIN_NOT_USED
                           *   if this signal is not needed. */
    uint8_t MISO_Pin;     ///< MISO pin number (optional).
                          /**< Set to @ref NRF_DRV_SPI_PIN_NOT_USED
                           *   if this signal is not needed. */
    uint8_t SS_Pin;       ///< Slave Select pin number (optional).
                          /**< Set to @ref NRF_DRV_SPI_PIN_NOT_USED
                           *   if this signal is not needed. The driver 
                           *   supports only active low for this signal. 
                           *   If the signal should be active high,
                           *   it must be controlled externally. */
    uint8_t IRQ_Priority; ///< Interrupt priority.
    uint8_t ORC;          ///< Over-run character.
                          /**< This character is used when all bytes from the TX buffer are sent,
                               but the transfer continues due to RX. */
    SPI_Frequency_t 				Frequency; ///< SPI frequency.
    SPI_Mode_t      				Mode;      ///< SPI mode.
    nrf_drv_spi_bit_order_t Bit_Order; ///< SPI bit order.
} SPI_InitTypeDef;

extern volatile bool SPIbusy;

//Prototipo de funciones
uint32_t HAL_SPI_Init(SPI_InitTypeDef *SPI);
uint32_t HAL_SPI_Transmit(uint8_t *spiBuffer, uint32_t len, uint32_t timeOut);
uint32_t HAL_SPI_Receive(uint8_t *spiBuffer, uint32_t len, uint32_t timeOut);
uint32_t HAL_SPI_Transfer(uint8_t *spiBufferTx, uint32_t iTx, uint8_t *spiBufferRx, uint32_t iRx, uint32_t timeOut);
void HAL_SPI_DebugSerial(uint8_t *out, uint8_t len);

#endif /* __NRF51XXX_HAL_UART_H */

/************************ (C) COPYRIGHT Redimec SRL *****END OF FILE****/
