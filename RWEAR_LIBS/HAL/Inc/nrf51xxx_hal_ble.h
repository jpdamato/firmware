/**
  ******************************************************************************
  * @file    nrf51xxx_hal_bletransference.h
  * @author  Mariano G. Scasso
  * @version V0.1
  * @date    23-Octubre-2016
  * @brief   BLE HAL file transference driver.
  *          This file provides firmware functions to manage the following 
  *          functionalities of the files transferences over BLE based on nordic sofdevice s130 functions.
  *           + Packets simbols definitios
  *           + Packaging functions
	*						+ Send/recive functions				
  *
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Redimec SRL</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 
	
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NRF51XXX_HAL_BLE_H
#define __NRF51XXX_HAL_BLE_H

#ifdef __cplusplus
 extern "C" {
#endif
	 
#include "ble_nus.h"
/* Protocol simbols definitios */

#define HAL_BLE_PACKAGE_SIZE							20
#define HAL_BLE_PACKAGE_DATA							16
	 
#define HAL_BLE_ERROR							56000
#define HAL_BLE_ERROR_STREAM_COMPLETE		    	HAL_BLE_ERROR
#define HAL_BLE_ERROR_STREAM_INPROGRESS				HAL_BLE_ERROR +1
#define HAL_BLE_ERROR_STREAM_INVALID_PACKAGE 	HAL_BLE_ERROR +2
	 

/* types definition */
	 
typedef struct
{
	ble_nus_t  * Nus;
	
}	hal_ble_init_t;

typedef uint8_t ble_package_t[HAL_BLE_PACKAGE_SIZE];

typedef struct
{
	uint8_t * rawData;
	uint32_t rawDataLen;
	uint8_t  StartPackage[HAL_BLE_PACKAGE_SIZE];
	uint8_t  EndPackage[HAL_BLE_PACKAGE_SIZE];
	uint8_t PackageSize;
	uint32_t PackageQty,PackageSend;
	bool TrasmitionInProgress;
	bool SendStartEndPackage;
	bool BinaryPackage;
} ble_stream_t;

struct ble_binary_package
{
	uint8_t id;
	uint8_t number;
	uint8_t data [16];
	uint8_t reserved;
	uint8_t crc;
};

typedef union ble_binary_package_u
{
	struct ble_binary_package package;
	uint8_t Union[HAL_BLE_PACKAGE_SIZE];
} ble_binary_package_t;
	 
extern ble_stream_t Stream;
extern ble_nus_t    m_nus;
	 /* Functions definitions */
void 		 				HAL_BLE_Init(hal_ble_init_t init);	 
void 		 				HAL_BLE_StreamSet(uint8_t * data,uint32_t len,bool binary, bool SEpackage);
uint32_t 				HAL_BLE_StreamSend(void);
uint32_t HAL_BLE_StringSend(uint8_t *data, uint8_t len);
uint32_t 				HAL_BLE_SendPacket(uint8_t number);
uint32_t 				HAL_BLE_StremPackageQty(ble_stream_t stream);
struct ble_binary_package HAL_BLE_NewPackage(uint8_t * data, uint8_t pcknumber);
uint32_t 				HAL_BLE_PackageSend(uint8_t *package);
uint8_t HAL_BLE_crc8(uint8_t const message[], uint8_t nBytes);
#ifdef __cplusplus
}
#endif

#endif // HAL_BLETRANSFERENCE
