
#ifndef BLE_RGPS_H__
#define BLE_RGPS_H__

#include "ble.h"
#include "ble_srv_common.h"
#include "app_util_platform.h"
#include <stdint.h>
#include <stdbool.h>

#include "minmea.h"

#define BLE_UUID_RGPS_SERVICE 0x0600                      /**< The UUID of the Motion Service. */
#define BLE_RGPS_MAX_DATA_LEN (30) /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Motion service module. */

#ifdef __GNUC__
    #ifdef PACKED
        #undef PACKED
    #endif

    #define PACKED(TYPE) TYPE __attribute__ ((packed))
#endif

typedef struct minmea_sentence_gga ble_rgps_data_t;

typedef PACKED( struct
{
   bool on_off;
   uint8_t refresh_t;
}) ble_rgps_config_t;


typedef enum
{
    BLE_RGPS_EVT_CONFIG_RECEIVED,
    BLE_RGPS_EVT_NOTIF_DATA
}ble_rgps_evt_type_t;

/* Forward declaration of the ble_rgps_t type. */
typedef struct ble_rgps_s ble_rgps_t;

/**@brief Motion Service event handler type. */
typedef void (*ble_rgps_evt_handler_t) (ble_rgps_t        * p_rgps,
                                       ble_rgps_evt_type_t evt_type,
                                       uint8_t          * p_data,
                                       uint16_t           length);

/**@brief Motion Service initialization structure.
 *
 * @details This structure contains the initialization information for the service. The application
 * must fill this structure and pass it to the service using the @ref ble_rgps_init function.
 */
typedef struct
{
    ble_rgps_config_t      * p_init_config;
    ble_rgps_evt_handler_t   evt_handler; /**< Event handler to be called for handling received data. */
} ble_rgps_init_t;

/**@brief Motion Service structure.
 *
 * @details This structure contains status information related to the service.
 */
struct ble_rgps_s
{
    uint8_t                  uuid_type;                    /**< UUID type for Motion Service Base UUID. */
    uint16_t                 service_handle;               /**< Handle of Motion Service (as provided by the S110 SoftDevice). */
    ble_gatts_char_handles_t config_handles;               /**< Handles related to the config characteristic (as provided by the S132 SoftDevice). */
    ble_gatts_char_handles_t data_handles;                  /**< Handles related to the tap characteristic (as provided by the S132 SoftDevice). */
	//add more charact here//
	
    uint16_t                 conn_handle;                  /**< Handle of the current connection (as provided by the S110 SoftDevice). BLE_CONN_HANDLE_INVALID if not in a connection. */
    bool                     is_data_notif_enabled;         /**< Variable to indicate if the peer has enabled notification of the */
    ble_rgps_evt_handler_t    evt_handler;                  /**< Event handler to be called for handling received data. */
};

/**@brief Function for initializing the Motion Service.
 *
 * @param[out] p_wss      Motion Service structure. This structure must be supplied
 *                        by the application. It is initialized by this function and will
 *                        later be used to identify this particular service instance.
 * @param[in] p_rgps_init  Information needed to initialize the service.
 *
 * @retval NRF_SUCCESS If the service was successfully initialized. Otherwise, an error code is returned.
 * @retval NRF_ERROR_NULL If either of the pointers p_wss or p_rgps_init is NULL.
 */
uint32_t ble_rgps_init(ble_rgps_t * p_wss, const ble_rgps_init_t * p_rgps_init);

/**@brief Function for handling the Motion Service's BLE events.
 *
 * @details The Motion Service expects the application to call this function each time an
 * event is received from the S110 SoftDevice. This function processes the event if it
 * is relevant and calls the Motion Service event handler of the
 * application if necessary.
 *
 * @param[in] p_wss       Motion Service structure.
 * @param[in] p_ble_evt   Event received from the S110 SoftDevice.
 */
void ble_rgps_on_ble_evt(ble_rgps_t * p_wss, ble_evt_t * p_ble_evt);

/**@brief Function for sending tap data.
 *
 * @details This function sends the input tap as an tap characteristic notification to the peer.
 *
 * @param[in] p_rgps       Pointer to the Motion Service structure.
 * @param[in] p_data      Pointer to the tap data.
 *
 * @retval NRF_SUCCESS If the string was sent successfully. Otherwise, an error code is returned.
 */
uint32_t ble_rgps_char_data_set(ble_rgps_t * p_rgps, ble_rgps_data_t * p_data);


#endif // BLE_RGPS_H__

/** @} */
