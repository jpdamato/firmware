#ifndef __WISE_BOARD_H
	#define __WISE_BOARD_H

#define WISE_BOARD_4_2
#define WISE_WDT_ENABLE
#define WISE_DFU_ENABLE

#ifdef WISE_DFU_ENABLE
	#define WISE_SORFTWARE_VERSION "Firmware V5.1 dfu "  __DATE__
#else
	#define WISE_SORFTWARE_VERSION "Firmware V5.1 " __DATE__
#endif


#ifdef WISE_BOARD_4_1
#include "nrf_saadc.h"
#include "nrf_lpcomp.h"
/* LEDs definitions */
#define LEDS_NUMBER    					2
#define LED_GREEN_PIN_NUMBER							30
#define LED_RED_PIN_NUMBER								31
#define LEDS_LIST 							{LED_1, LED_2}
/* GPS pins */
#define RX_PIN_NUMBER  					6
#define TX_PIN_NUMBER  					1
#define RST_GPS_PIN_NUMBER			11
#define PPS_PIN_NUMBER					0
#define INTGPS_PIN_NUMBER				19
#define GPSON_PIN_NUMBER				25
#define ANTON_PIN_NUMBER				16
#define HWFC           					false
/* SPI */
#define MISO_PIN_NUMBER  				27    // SPI MISO signal. 
#define CSN_PIN_NUMBER					26    // SPI CSN signal. 
#define MOSI_PIN_NUMBER 				28    // SPI MOSI signal. 
#define SCK_PIN_NUMBER   				29    // SPI SCK signal. 

//Sensado Analógico
#define VSENSE1_PIN_NUMBER				4
#define VSENSE1_PIN_ADC						NRF_SAADC_INPUT_AIN2
#define VSENSE2_PIN_NUMBER				2
#define VSENSE2_PIN_ADC						NRF_SAADC_INPUT_AIN0
#define VSENSE2_PIN_LPCOMP				NRF_LPCOMP_INPUT_0
#define CHARGER_FLT_PIN_NUMBER		23
#define CHARGER_CHG_PIN_NUMBER		7

//MPU9250
#define SCL_PIN_NUMBER					8
#define	SDA_PIN_NUMBER					17		
#define INT9DOF_PIN_NUMBER				5 //#define MPU_INT   INT9DOF_PIN_NUMBER


//BUTTONS
#define BUTTON1_PIN							18
#define POWER_OFF_PIN						12

//COMPATIBILITY PINS DEFINE
#define MPU_CS_PIN_NUMBER	14
#define MEM_HOLD_PIN_NUMBER 14
#define MEM_WP_PIN_NUMBER 14
#endif

#ifdef WISE_BOARD_4_2
#include "nrf_saadc.h"
#include "nrf_lpcomp.h"
/* LEDs definitions */
#define LEDS_NUMBER    					2
#define LED_GREEN_PIN_NUMBER							30
#define LED_RED_PIN_NUMBER								31
#define LEDS_LIST 							{LED_1, LED_2}
/* GPS pins */
#define RX_PIN_NUMBER  					6
#define TX_PIN_NUMBER  					1
#define RST_GPS_PIN_NUMBER			11
#define PPS_PIN_NUMBER					0
#define INTGPS_PIN_NUMBER				12
#define GPSON_PIN_NUMBER				25 
#define ANTON_PIN_NUMBER				25 //la antena no se apaga por separado
#define HWFC           					false
/* SPI */
#define MISO_PIN_NUMBER  				27    // SPI MISO signal. 
#define CSN_PIN_NUMBER					26    // SPI CSN signal. 
#define MOSI_PIN_NUMBER 				28    // SPI MOSI signal. 
#define SCK_PIN_NUMBER   				29    // SPI SCK signal. 
#define MEM_WP_PIN_NUMBER 			26 //WRITE PROTECT
#define MEM_HOLD_PIN_NUMBER			13 //HOLD
//Sensado Analógico
#define VSENSE1_PIN_NUMBER				4
#define VSENSE1_PIN_ADC						NRF_SAADC_INPUT_AIN2
#define VSENSE2_PIN_NUMBER				2
#define VSENSE2_PIN_ADC						NRF_SAADC_INPUT_AIN0
#define VSENSE2_PIN_LPCOMP				NRF_LPCOMP_INPUT_0
//#define CHARGER_FLT_PIN_NUMBER		23
#define CHARGER_CHG_PIN_NUMBER		7

//MPU9250
#define SCL_PIN_NUMBER					8
#define	SDA_PIN_NUMBER					17		
#define INT9DOF_PIN_NUMBER				5 //#define MPU_INT   INT9DOF_PIN_NUMBER
#define MPU_CS_PIN_NUMBER	3

//BUTTONS
#define BUTTON1_PIN							18
#define POWER_OFF_PIN						24


#endif



#endif
