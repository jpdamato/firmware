/**
  * @file    hmi.h
  * @author  Mariano G. Scasso
  * @version V1.0
  * @date    May 1st, 2017
  * @brief   	This file contain the function definitios for the gps task. 
	* 	      
	*
	*	@copyright COPYRIGHT(c) 2017 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/


#ifndef __GPS_TASK_H
	#define __GPS_TASK_H
#include <stdbool.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "timers.h"

extern QueueHandle_t xGPS_Commands_Queue; /*!< gps commands queue, public to the other tasks */
extern TaskHandle_t xGPS_task_handle;


typedef __packed struct ublox_pvt
{
	uint32_t t_ms; //ms - gps time of week
	uint16_t year; //year UTC
	uint8_t month; //1..12 utc
	uint8_t day;	//day of moth, 1..31
	uint8_t hour; //hour of day, 0..23
	uint8_t min; //minute per hour, 0..59
	uint8_t sec; //seg per minute, 0..60
	uint8_t valid; //b0 = validDate - b1 = validTime - b2 = fullResolved - b3 validMag
	uint32_t tAcc;//ns - time acuracy
	int32_t nano; //ns fraction of second
	uint8_t fixType;//0:no fix - 1:dead reckoning - 2: 2d fix - 3: 3d fix - 4: GNSS +  dead reckoning - 5: time only fix
	uint8_t flags;// b0 = gnss fix ok - b1 = diffSoln - b2 = X - b3 = X - b4= psmState - b5 = headVehValid - b6/7 = carrSoln 
	uint8_t flags2;// X
	uint8_t numSV; // num of satellites
	int32_t lon; // deg, 1e-7 scaling
	int32_t lat;// deg, 1e-7 scaling
	int32_t height; //mm, Height above ellipsoid
	int32_t hMSL;  //mm, Height above mean sea level 
	uint32_t aAcc; //mm, Horizontal accuracy estimate
	uint32_t vAcc; //mm, Vertical accuracy estimate
	int32_t velN; //mm/s, NED north velocity
	int32_t velE; //mm/s, NED east velocity
	int32_t velD; //mm/s, NED down velocity
	int32_t gSpeed; //mm/s, Ground Speed (2-D)
	int32_t headMot; //deg,1e-5 scaling, He//deg,1e-5ading of motion (2-D)
	uint32_t sAcc; //mm/s Speed accuracy estimate
	uint32_t headAcc;//deg,1e-5, Heading accuracy estimate (both motionand vehicle)
	uint16_t pDOP;//0.01 scaling, Position DOP
	uint8_t reserved[6]; //reserved
	int32_t headVeh; //deg, 1e-5,Heading of vehicle (2-D) 
	int16_t magDec;//deg, 1e-2, Magnetic declination
	uint16_t magAcc; //deg, 1e-2, Magnetic declination accuracy
}gps_binary_t;

#define GPS_BIN_TYPE 35
typedef __packed struct pvt_to_save
{
	uint8_t type;
	uint32_t t_ms; //ms - gps time of week
	uint8_t valid;
	uint8_t fixType;
	int32_t lon; // deg, 1e-7 scaling
	int32_t lat;// deg, 1e-7 scaling
	int32_t height; //mm, Height above ellipsoid
	int32_t hMSL;  //mm, Height above mean sea level 
	uint32_t aAcc; //mm, Horizontal accuracy estimate
	uint32_t vAcc; //mm, Vertical accuracy estimate
	int32_t velN; //mm/s, NED north velocity
	int32_t velE; //mm/s, NED east velocity
	int32_t velD; //mm/s, NED down velocity
	int32_t gSpeed; //mm/s, Ground Speed (2-D)
}gps_bin_save_t;

/**
  * @brief  GPS commads. Used to make a state transition on the GPS smachine.
*/
typedef enum
{
	GPS_COMMAND_NULL = 0,
	GPS_COMMAND_START,
	GPS_COMMAND_STOP,
	GPS_COMMAND_MONITOR,
	GPS_COMMAND_RATE_1HZ,
	GPS_COMMAND_RATE_4HZ,
	GPS_COMMAND_RATE_10HZ
}gps_command_t;


/**
  * @brief  GPS smachine states
*/
typedef enum
{
	GPS_STATE_IDLE = 0,
	GPS_STATE_START,
	GPS_STATE_FIXED,
} gps_state_t;

/**
  * @brief  GPS smachine definition
*/
typedef struct gps_smachine_s
{
	gps_state_t state;
	gps_command_t command;
	uint8_t start_time;
} gps_smachine_t;

#define GPS_SESSION_TYPE 20
typedef struct session_s
{
	uint8_t type;
	uint8_t number;
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t aux[23];
} session_t;

/**
  * @brief  GPS task function, called from the scheduler
  * @param  FreeRTOS task parameter, non used.
  * @retval None
  */

void xGPS_task(void *pvParameters);


/**
  * @brief  get the date obtained from RMC frame
  * @param  year, month and day pointer to the destination variable.
  * @retval None
  */
void gps_task_init(void);
bool gps_get_date(uint16_t * year,uint8_t * month, uint8_t * day);
void gps_get_time(uint8_t * hour,uint8_t * minute, uint8_t * second);
void gps_power(bool state);
uint8_t gps_get_state(void);
void gps_fill_session_aux(uint8_t*buffer,uint8_t len);
uint32_t gps_get_ms(void);

#endif //__GPS_TASK_H
