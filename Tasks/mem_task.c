/**
  * @file    mem_task.c
  * @author  Mariano G. Scasso
  * @version V1.0
  * @date    May 1st, 2017
  * @brief   	This file implements the functions of the mem task. A mem smachine with init, buffering,save and other states is defined. 
	*						The states transition is achived throught a commands queue. Functions for memory index recovery after fail conditions are
	*						also implemented.
	* 	      
	*
	*	@copyright COPYRIGHT(c) 2017 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/


#include "mem_task.h"
#include "ble_task.h"
#include "hmi_task.h"
#include <string.h>
#include "wise_board.h"
#include "R_SPIFlash_NRF.h"
#ifndef WISE_MEM_EXT
	#include "R_SPIFlash.h"
#else
	#include "R_SPIFlashExt.h"
#endif

#ifdef WISE_STATISTICS_ENABLE
#include "mem_statistics.h"
#endif

TaskHandle_t xMEM_task_handle;
QueueHandle_t xMEM_Commands_Queue; 				/*!< mem commands queue */
SemaphoreHandle_t mem_semaphore_buffer;		/*!< databuffer is full */

static uint8_t mem_buffer[MEM_BUFFER_SIZE]; /*!< Keep data until save */
volatile static uint8_t mem_buffer_index;		/*!< Position at the buffer */
static bool mem_buffer_enable;							/*!< mem buffer enable */

static uint32_t mem_index_save_user;				/*!< This index keep the memory address where the user will save the data_index
																									when stop of saving*/
																	
static uint32_t mem_index_save_backup;			/*!< This index keep the memory address used for automatic backup of memory data_index */
static uint32_t mem_data_index;							/*!< Actual data memory address for saving operations */
static uint32_t mem_read_index;							/*!< Actual data memory address for reading operations */

static uint8_t byte4[4];										/*!< Used to cast an 32 int to a 4 uint8_t array */

static bool MemoryIndexRecovery(void);			/*!< Function to search index at memory_init */
static bool MemorySearchIndex(uint32_t StarAddress);

static 	mem_smachine_t mem_smachine;						/*!< Memory smachine */
static mem_state_t state_previous;
uint8_t mem_read_timeout;


#define POLYNOMIAL 0xA6 

typedef uint8_t crc;

#define WIDTH  	8
#define TOPBIT (1 << (WIDTH - 1))

static uint8_t mem_crc(uint8_t const message[], uint8_t nBytes)
{
    uint8_t  remainder = 0;	

    /*
     * Perform modulo-2 division, a byte at a time.
     */
    for (uint8_t byte = 0; byte < nBytes; ++byte)
    {
        /*
         * Bring the next byte into the remainder.
         */
        remainder ^= (message[byte] << (WIDTH - 8));

        /*
         * Perform modulo-2 division, a bit at a time.
         */
        for (uint8_t bit = 8; bit > 0; --bit)
        {
            /*
             * Try to divide the current data bit.
             */
            if (remainder & TOPBIT)
            {
                remainder = (remainder << 1) ^ POLYNOMIAL;
            }
            else
            {
                remainder = (remainder << 1);
            }
        }
    }

    /*
     * The final remainder is the CRC result.
     */
    return (remainder);
}  


/**
  * @brief  Add data to memory buffer, to be saved when the buffer is full
  * @param  data: pointer to uint8_t array with data wich will be saved
	* @param	len: size of bytes to be saved
  * @retval bool, succed or not.
  */
bool xMEM_add_to_buffer(uint8_t * data, uint8_t len)
{
	
	if (!mem_buffer_enable) //si el buffer no esta habilitado salgo
		return false;
	
	if (mem_buffer_index + len < MEM_BUFFER_SIZE-1)
	{
		memcpy(mem_buffer + mem_buffer_index, data, len);
		mem_buffer_index += len;
		return true;
	}
	else
	{
		mem_buffer[MEM_BUFFER_SIZE-1] = mem_crc(mem_buffer,MEM_BUFFER_SIZE-1); //calclo hasta el anteultimo lugar
		xSemaphoreGive(mem_semaphore_buffer); //aviso que se lleno el buffer
		return false;
	}
}

/**
  * @brief  Send a start packet for synchr when a memory transference start. 
  * @retval none.
  */
static void ble_transference_start(void)
{
	uint8_t buffer[20];
	uint32_t stream_qty = (mem_data_index - MEMORY_START)/m_ble_nus_max_data_len;
	
	memset(buffer,0,20);
	buffer[0] = '*';
	memcpy(buffer+1,&stream_qty,sizeof(stream_qty));
	memcpy(buffer+17, "***",3);
	buffer[16] = 'U'; //raw transference
	
	ble_string_send(buffer,sizeof(buffer));
}

/**
  * @brief  Send a BLE packet with memory status
  * @retval none.
  */
static void mem_status(uint8_t status)
{
	uint8_t status_pack[20];
	memset(status_pack,0,20);
	sprintf((char*)status_pack,"$MEMST,%d  ,%d\r\n",status,(mem_data_index-MEMORY_START)/ MEM_BUFFER_SIZE);

	ble_string_send(status_pack,sizeof(status_pack));
}

/**
  * @brief  MEM task function, called from the scheduler
  * @param  FreeRTOS task parameter, non used.
  * @retval None
  */

void xMEM_task(void *pvParameters)
{
	const TickType_t xDelay100ms = pdMS_TO_TICKS( 100UL );
	hmi_command_t command_to_hmi;
	BaseType_t xStatus;
	mem_semaphore_buffer = xSemaphoreCreateBinary(); /*!< create buffer semaphore*/
	
  if(NULL == mem_semaphore_buffer)
  {
       APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }
	
	mem_read_index = MEMORY_START;
	mem_smachine.state = MEM_STATE_INIT;
	mem_read_timeout = 0xff;
	
	R_SPIFlash_init(MISO_PIN_NUMBER,MOSI_PIN_NUMBER,CSN_PIN_NUMBER,SCK_PIN_NUMBER);
	R_SPIFlash_ReadID(mem_buffer);
	for(;;)
	{
		xStatus = xQueueReceive( xMEM_Commands_Queue, &mem_smachine.command, 1); /*!<receive a command from queue*/
		if (pdPASS == xStatus)
			switch(mem_smachine.command)
			{
				case MEM_COMMAND_SAVE_START:
					if (mem_smachine.state == MEM_STATE_IDLE)
						mem_smachine.state = MEM_STATE_SAVE_START; //else error					
					break;
				case MEM_COMMAND_SAVE_STOP:
						if(mem_smachine.state == MEM_STATE_SAVE || mem_smachine.state == MEM_STATE_BUFFERING)
							mem_smachine.state = MEM_STATE_SAVE_STOP;
						else if ((mem_smachine.state == MEM_STATE_READ) || (mem_smachine.state == MEM_STATE_READ_DEBUG))//lo uso como reading stop tambien
						{	
							mem_smachine.state = MEM_STATE_IDLE;
						}
					break;
				case MEM_COMMAND_READ_START: /**< Start memory read and ble transference. */
					mem_read_index = MEMORY_START;
					if (mem_smachine.state == MEM_STATE_READ)
					{
						mem_smachine.state = MEM_STATE_IDLE; //cancelo lectura
					}
					else
					{
						state_previous = mem_smachine.state; //vuelvo al estado anterior
						mem_smachine.state = MEM_STATE_READ;
						ble_transference_start();
					}
					break;
				
				case MEM_COMMAND_READ_DEBUG:
					mem_read_index = MEMORY_START;
					mem_smachine.state = MEM_STATE_READ_DEBUG;
					break;
				case MEM_COMMAND_ERASE:
					mem_smachine.state = MEM_STATE_ERASE;
					break;
				case MEM_COMMAND_STATISTICS_CALC:
					mem_read_index = mem_data_index - MEM_BUFFER_SIZE;
					state_previous = mem_smachine.state;
					mem_smachine.state = MEM_STATE_STATISTICS_CALC;
					break;
#ifdef WISE_STATISTICS_ENABLE
				case MEM_COMMAND_STATISTICS_GET:
					memset(mem_buffer,0,sizeof(mem_buffer));
					mem_buffer_index = statistics_to_string(mem_buffer);
					ble_string_send(mem_buffer,mem_buffer_index);
					
					mem_buffer_index = statistics_acc_detail(mem_buffer);
					ble_string_send(mem_buffer,mem_buffer_index);
					
					mem_buffer_index = statistics_acc_hist(mem_buffer);
					ble_string_send(mem_buffer,mem_buffer_index);
					break;
#endif
				case MEM_COMMAND_GET_STATE:
					mem_status(mem_smachine.state);
					break;
				default:
					break;
			}
		
		switch (mem_smachine.state)  /*!<Memory smachine*/
		{
			case MEM_STATE_INIT:
			{
				if (MemoryIndexRecovery ())	 					/*!<Find memory index*/
					mem_smachine.state = MEM_STATE_IDLE;	  /*!<Succed go to idle*/
				else
				 mem_smachine.state = MEM_STATE_FAIL;
			 
				mem_buffer_enable = false;
				break;
			}
			case MEM_STATE_ERASE:
			{
				R_SPIFlash_SectorErase(MEMORY_START);  /*!<Clean memory data start sector*/
				R_SPIFlash_SectorErase(MEMORY_INDEX_ADDRESS); /*!<Clean memory index backup sector*/
				
				mem_data_index = MEMORY_START;
				memcpy(byte4,&mem_data_index,4);
				R_SPIFlash_WriteBytes(MEMORY_INDEX_ADDRESS,byte4,4);	/*!<Save actual index in memory*/
				
				mem_index_save_user = MEMORY_INDEX_ADDRESS;
				mem_index_save_backup = MEMORY_BACKUP_ADDRESS;
				mem_smachine.state = MEM_STATE_IDLE;											/*!<Go to idle state*/
				break;
			}
			case MEM_STATE_SAVE_START:
			{
				memset(byte4,0,4); 																		/*!<When save session start, clean last user address in memory*/
				R_SPIFlash_WriteBytes(mem_index_save_user,byte4,4);
				mem_index_save_user += 4;
				if (mem_index_save_user >= MEMORY_BACKUP_ADDRESS)
				{
					mem_smachine.state = MEM_STATE_FAIL;
					return;
				}
				memset(mem_buffer,0, sizeof(mem_buffer));
				mem_buffer_index = 0;
				mem_buffer_enable = true;
				mem_smachine.state = MEM_STATE_BUFFERING; 								/*!<Buffering start*/
				break;
			}			
			case MEM_STATE_BUFFERING:
				if(xSemaphoreTake(mem_semaphore_buffer, xDelay100ms * 20) == pdPASS) /*!<Save if the buffer is full*/
				{
					mem_buffer_enable = false;
					mem_smachine.state = MEM_STATE_SAVE;
					command_to_hmi.type = HMI_COMMAND_LED_RED;
					command_to_hmi.value[0] = 800;
					command_to_hmi.value[1] = 100;
					command_to_hmi.value[2] = 2;
					xQueueSendToBack( xHMI_Commands_Queue, &command_to_hmi, xDelay100ms );
				}
				else
				{
					mem_buffer_enable = true;
				}
				
				break;
			case MEM_STATE_SAVE:
			{
				if (mem_data_index + MEM_BUFFER_SIZE >= MEMORY_MAX_ADDRESS) /*!<Address out of range*/
				{
					mem_smachine.state = MEM_STATE_FAIL;	
					break;
				}
				
					R_SPIFlash_WriteBytes(mem_data_index,mem_buffer,MEM_BUFFER_SIZE); /*!<Write data in memory*/
					mem_data_index += MEM_BUFFER_SIZE;
				
				if (((mem_data_index % MEMORY_BACKUP_PERIOD) == 0) && (mem_data_index > MEMORY_START)) /*!<Automatic index backup*/
					{
						byte4[0] = MEMORY_BACKUP_SYMBOL; /*!<Save a backup symbol each after other, counting this symbol the data index is recovered*/
						R_SPIFlash_WriteBytes(mem_index_save_backup,byte4,1);
						mem_index_save_backup++;
					}
					mem_smachine.state = MEM_STATE_BUFFERING;	 /*!<Continue*/
					memset(mem_buffer,0,MEM_BUFFER_SIZE);
					mem_buffer_index = 0;
					mem_buffer_enable = true;
					break;
			}
			case MEM_STATE_SAVE_STOP:
			{
				memcpy(byte4,&mem_data_index,4);
				R_SPIFlash_WriteBytes(mem_index_save_user,byte4,4); /*!<Save data index at the user address*/
				mem_smachine.state = MEM_STATE_IDLE;
				mem_buffer_enable = false;
				break;
			}
			case MEM_STATE_READ:

				if ((mem_read_index >= mem_data_index) || (mem_read_timeout == 0))  /*!<Exit if its done*/
				{
					mem_read_index = MEMORY_START;
					mem_smachine.state = state_previous; //vuelvo al estado anterior
					mem_buffer_enable = true;
					mem_read_timeout = 0xff;
					
					command_to_hmi.type = HMI_COMMAND_LED_GREEN;
					command_to_hmi.value[0] = 1;
					command_to_hmi.value[1] = 0;
					command_to_hmi.value[2] = 2;
					xQueueSendToBack( xHMI_Commands_Queue, &command_to_hmi, 10 );
					break;
				}
				//SIN BREAK
			case MEM_STATE_READ_DEBUG:/*!<To read al memory from start*/
					R_SPIFlash_ReadBytes(mem_read_index,mem_buffer,MEM_BUFFER_SIZE);  /*!<Read data and put at the buffer*/
					if(ble_string_send(mem_buffer,MEM_BUFFER_SIZE) == MEM_BUFFER_SIZE)
					{
						mem_read_index += MEM_BUFFER_SIZE;
						mem_read_timeout = 0xff;
						
						command_to_hmi.type = HMI_COMMAND_LED_GREEN;
						command_to_hmi.value[0] = 70;
						command_to_hmi.value[1] = 50;
						command_to_hmi.value[2] = 2;
						xQueueSendToBack( xHMI_Commands_Queue, &command_to_hmi, 10 );
					}
					else
					{
						mem_read_timeout--;
						vTaskDelay(xDelay100ms/5);
					}
				
				break;
#ifdef WISE_STATISTICS_ENABLE
			case MEM_STATE_STATISTICS_CALC:
			{
				mem_buffer_enable = false;

				memset(mem_buffer,0,MEM_BUFFER_SIZE);
				R_SPIFlash_ReadBytes(mem_read_index,mem_buffer,MEM_BUFFER_SIZE);  /*!<Read data and put at the buffer*/
				
				if (!statistics_find_start_session(mem_buffer,MEM_BUFFER_SIZE)) //busco la ultima sesion
				{
					if (mem_read_index <= MEMORY_START)
					{
						mem_smachine.state = state_previous;
						
						mem_buffer_index= sprintf((char*) mem_buffer,"STST error\r\n");
						ble_string_send(mem_buffer,mem_buffer_index);
						memset(mem_buffer,0,sizeof(mem_buffer));
						mem_buffer_enable = true;
					}
					mem_read_index -= MEM_BUFFER_SIZE;
					break;
				}
				
				mem_read_index += MEM_BUFFER_SIZE;
				
				if ((mem_read_index > mem_data_index) || statistics_calc_iteration(mem_buffer,MEM_BUFFER_SIZE) == false)  /*!<Exit if its done*/
				{
					statistics_calc_stop();
					
					memset(mem_buffer,0,sizeof(mem_buffer));
					mem_buffer_index = statistics_to_string(mem_buffer);
					ble_string_send(mem_buffer,mem_buffer_index);
					
					mem_read_index = MEMORY_START;
					mem_smachine.state = state_previous;
					mem_buffer_enable = true;
					break;
				}
				//vTaskDelay(xDelay100ms/50);
			} break;
#endif
			case MEM_STATE_IDLE:
				//sin break	
			case MEM_STATE_FAIL:
								//sin break	

			default:
				vTaskDelay(xDelay100ms*5);
					break;
		}
		mem_smachine.command = MEM_COMMAND_NULL;
	}
}

/**
  * @brief 	Walk the memory until is empty
  * @param  Start address.
* @retval true: sucssed
  */
static bool MemorySearchIndex(uint32_t StarAddress)
{
	uint32_t Address = StarAddress;
	uint8_t Last=0;
	
	do
	{
		Last = byte4[0];	
		R_SPIFlash_ReadBytes(Address,byte4,4);	
		Address += 64;
	}
		while((byte4[0] != 0xff) && (Last != 0xff) && (Address < MEMORY_MAX_ADDRESS));
		if (Address < (MEMORY_MAX_ADDRESS - MEM_BUFFER_SIZE))
		{
			mem_data_index = Address - 64;
			return true;
		}
		else
			return false;
}

/**
  * @brief Test an uint32_t
  * @param  adress.
	* @retval return true if its a valid address
  */
static bool valid_mem_address(uint32_t input)
{
	if ((input >= MEMORY_START) && (input < 0x01ff0000))
		return true;
	else
		return false;
}

/**
  * @brief Use backup symbols to find the last data address, also
	*				complete user and back index.
  * @param  none.
	* @retval return true if sussed
  */
static bool MemoryIndexRecovery () 
{
	uint32_t a;
	uint8_t memRead[16];
	uint32_t address = MEMORY_INDEX_ADDRESS;
	int8_t i=0;
	address = MEMORY_INDEX_ADDRESS;
	
		while (address < MEMORY_BACKUP_ADDRESS)
		{
			memset(memRead,0,12);
			R_SPIFlash_ReadBytes(address,memRead,16);//recupero los 4 primeros indices
			memcpy(&a,memRead+12,4); // miro el ultimo
			address +=16;
			if (a == 0) //si es 0 continuo, sino miro en detalle
				continue;
			for (i = 0; i < 4; i++)
			{
				memcpy(&a,memRead + 12 - (i*4),4); // miro en orden inverso, si encuentro una direcci�n valida, es el ultimo indice guardado
				if (valid_mem_address(a))
				{
					mem_index_save_user = address - 4 - i*4;
					mem_index_save_backup = MEMORY_BACKUP_ADDRESS + ((a - MEMORY_START) / MEMORY_BACKUP_PERIOD);
					mem_data_index = a;
					
					return true; //encontre indice valido
				}
				
			}
			//si no encontre indice valido (era todo 0, o valores invalidos) seteo user index y busco en backup. Esta condicion se produce
			//porque la placa se apag� antes de que el usuario ejecute el commando "memory stop"
			for (i=0; i<16; i++)
			{
				if (memRead[i] == 0xff)
					break;
			}
			mem_index_save_user = address - 16 + i;
			break;
		}
		//AHORA CUENTO * para recuperar el indice por indirecci�n
		address = MEMORY_BACKUP_ADDRESS;
		while (address < MEMORY_START)
		{
			memset(memRead,0,12);
			R_SPIFlash_ReadBytes(address,memRead,16);
			if (memRead[15] == MEMORY_BACKUP_SYMBOL)
			{
				address +=16;
				continue;
			}
			
			for ( i = 0; i < 16; i++)
			{
				if (memRead[i] == MEMORY_BACKUP_SYMBOL)
					address++;
				else
					break;
			}
			break;
		}
		mem_index_save_backup = address + 1; //seteo el backup index
		return MemorySearchIndex(MEMORY_START + (address - MEMORY_BACKUP_ADDRESS) * MEMORY_BACKUP_PERIOD); //busco a partir del ultimo backup
}

void mem_task_init(void)
{
	xMEM_Commands_Queue = xQueueCreate( 2,sizeof(mem_command_t));

	if (xMEM_Commands_Queue != NULL)
	{
		if(pdPASS != xTaskCreate(xMEM_task, "MEM", 256, NULL, 3, &xMEM_task_handle))
		{
      APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
		}
	}
}

uint8_t xMEM_get_state()
{
	return mem_smachine.state;
}
uint32_t xMEM_get_packages(void)
{
	return (mem_data_index-MEMORY_START)/ MEM_BUFFER_SIZE;
}

void mem_write_manufacturing_data(const uint8_t * data, uint16_t len)
{
	R_SPIFlash_SectorErase(MEMORY_RESERVED);  /*!<Clean memory data start sector*/
	R_SPIFlash_WriteBytes(MEMORY_RESERVED, (uint8_t*)data,len); /*!<Write data in memory*/
}

uint8_t mem_read_manufacturing_data( uint8_t * data)
{
	memset(mem_buffer,0,MEM_BUFFER_SIZE);
	R_SPIFlash_ReadBytes(MEMORY_RESERVED,mem_buffer,MEM_BUFFER_SIZE);  /*!<Read data and put at the buffer*/
	uint8_t l = strlen((char*)mem_buffer);
	memcpy(data,mem_buffer,128);
	memset(mem_buffer,0,MEM_BUFFER_SIZE);
	return l;
}
