/**
  * @file    hmi.h
  * @author  Mariano G. Scasso
  * @version V1.0
  * @date    May 1st, 2017
  * @brief   	This file contain the function definitios for the mem task. 
	* 	      
	*
	*	@copyright COPYRIGHT(c) 2017 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/


#ifndef __MEM_TASK_H
	#define __MEM_TASK_H
	
#include <stdbool.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

#define MEM_BUFFER_SIZE 128
#define MEMORY_RESERVED 			0x00 // 1th SECTOR FOR CONFIG
#define MEMORY_INDEX_ADDRESS 	0x10000 // 2nd SECTOR FOR memory index (optimizar)
#define MEMORY_BACKUP_ADDRESS		0x18000 //backup
#define MEMORY_START					0x20000 // 3nd SECTOR FOR DATA (1th BUFFER SIZE FOR INDEX)
#define MEMORY_MAX_ADDRESS		0x01ff0000 //maximo indice de datos
#define MEMORY_BACKUP_PERIOD	0x00800
#define MEMORY_BACKUP_SYMBOL	'*'	 

extern TaskHandle_t xMEM_task_handle;
extern QueueHandle_t xMEM_Commands_Queue;

typedef enum
{
	MEM_COMMAND_NULL = 0,
	MEM_COMMAND_SAVE_START,
	MEM_COMMAND_SAVE_STOP,
	MEM_COMMAND_READ_START,
	MEM_COMMAND_READ_DEBUG,
	MEM_COMMAND_READ_STOP,
	MEM_COMMAND_ERASE,
	MEM_COMMAND_GET_STATE,
	MEM_COMMAND_STATISTICS_CALC,
	MEM_COMMAND_STATISTICS_GET
}mem_command_t;

typedef enum
{
	MEM_STATE_INIT = 0,
	MEM_STATE_IDLE,
	MEM_STATE_SAVE_START,
	MEM_STATE_BUFFERING,
	MEM_STATE_SAVE,
	MEM_STATE_SAVE_STOP,
	MEM_STATE_READ,
	MEM_STATE_READ_DEBUG,
	MEM_STATE_ERASE,
	MEM_STATE_STATISTICS_CALC,
	MEM_STATE_FAIL
} mem_state_t;

typedef struct mem_smachine_s
{
	mem_state_t state;
	mem_command_t command;
	bool buffer_full;
} mem_smachine_t;

	
void xMEM_task(void *pvParameters);
bool xMEM_add_to_buffer(uint8_t * data, uint8_t len);
void mem_task_init(void);
void mem_write_manufacturing_data(const uint8_t * data, uint16_t len);
uint8_t mem_read_manufacturing_data(uint8_t * data);
uint8_t xMEM_get_state(void);
uint32_t xMEM_get_packages(void);
#endif
