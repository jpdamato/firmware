/**
  * @file    cmd_task.h
  * @author  Mariano G. Scasso
  * @version V1.0
  * @date    May 1st, 2017
  * @brief   	This file contain the function definitios for the hmi task. 
	* 	      
	*
	*	@copyright COPYRIGHT(c) 2017 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/


#ifndef __CMD_TASK_H
	#define __CMD_TASK_H

#include <stdint.h>
#include <stdbool.h>
#include "nrf51xxx_hal_gpio.h"

#include "FreeRTOS.h"
#include "queue.h"
#include "timers.h"

/**
  * @brief  External ble commands definitions - Commands received throught ble nus.
*/
#define EXT_COMMAND_SIMBOL 					'@'
#define EXT_COMMAND_DATAIN					'S' //RECIBE DATOS 20 BYTES MAX
#define EXT_COMMAND_STOP						'0'
#define EXT_COMMAND_GPS_START				'1'
#define EXT_COMMAND_GPS_STOP				'2'
#define EXT_COMMAND_MONITORMODE			'3'
#define EXT_COMMAND_TRANSFERMODE 		'4'
#define EXT_COMMAND_SAVE_START			'5'
#define EXT_COMMAND_SAVE_STOP				'6'
#define EXT_COMMAND_MEM_CLEAR				'7'

#define EXT_COMMAND_MPU_MONITOR			'8'
#define EXT_COMMAND_MPU_START				'a'
#define EXT_COMMAND_MPU_STOP				'b'
#define EXT_COMMAND_MPU_QUATERN_MONITOR 'q'

#define EXT_COMMAND_BULKERASE				'9'

#define EXT_COMMAND_GPS_STATUS			'G'
#define EXT_COMMAND_MEM_STATUS			'M'
#define EXT_COMMAND_BAT_STATUS			'B'
#define EXT_COMMAND_MEM_DEBUG				'D'
#define EXT_COMMAND_SW_VERSION 			'V'
#define EXT_COMMAND_ID							'I'

#define EXT_COMMAND_UART_ENABLE			'U'
#define EXT_COMMAND_UART_DISABLE		'u'

#define EXT_COMMAND_STATISTICS_CALC 'e'
#define EXT_COMMAND_STATISTICS_GET 'f'


extern TaskHandle_t xCMD_task_handle;
/**
  * @brief  HMI Task function called from the scheduler
  * @param  pvParameters definition for FreeRTOS not used
  * @retval None
  */
void xCMD_task(void *pvParameters);

/**
  * @brief Enqueue the command "CMD_reject" on the HMI commands queue. 
	* 				This funtion is used to indicated a fail to execute a command by any task
  * @param none
  * @retval PdPass / pdFail
  */

#endif //__HMI_H
