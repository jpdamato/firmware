/**
  * @file    hmi.h
  * @author  Mariano G. Scasso
  * @version V1.0
  * @date    May 1st, 2017
  * @brief   	This file contain the function definitios for the hmi task. 
	* 	      
	*
	*	@copyright COPYRIGHT(c) 2017 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/


#ifndef __HMI_H
	#define __HMI_H

#include <stdint.h>
#include <stdbool.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "timers.h"

#include "app_timer.h"



extern QueueHandle_t xHMI_Commands_Queue; /*!< hmi commands queue, public*/
extern TaskHandle_t xHMI_task_handle;


/**
  * @brief  HMI commads. Used to make a state transition on the HMI smachine.
*/

typedef enum
{
	HMI_COMMAND_SLEEP =0, /*!< Go to sleep command */
	HMI_COMMAND_BLE_CONNECTED,
	HMI_COMMAND_BLE_DISCONNECTED,
	HMI_COMMAND_LED_RED,
	HMI_COMMAND_LED_GREEN,
}hmi_command_type_t;

/**
* @brief  HMI command type definition. Type: the receptor of the command, "Value" the action defined by each task.
*/
typedef struct hmi_command_s
{
	hmi_command_type_t type;
	uint16_t value[3]; //para compatibilizar con leds
} hmi_command_t;


/**
  * @brief  Led type definition.
*/
typedef struct hmi_led_s
{
	app_timer_id_t const * led_timer_id;
	uint8_t pin; 		/*!< led pin */
	uint16_t ton;  	/*!< time the led  is on */
	uint16_t toff;  	/*!< time the led  is off */
	uint8_t reps;
} hmi_led_t;

/**
  * @brief  Led command type, used to set the led behavior
*/
typedef struct led_cmd_s
{
 	uint16_t ton;
	uint16_t toff;
	uint16_t reps;
} led_cmd_t;

/**
  * @brief  HMI smachine states
*/
typedef enum
{
	HMI_STATE_COMMAND_RECEIVE = 0,
	HMI_STATE_SLEEP,
	HMI_STATE_FAIL,
	HMI_STATE_IDLE,
	HMI_STATE_CONNECTED,
}hmi_state_t;

/**
  * @brief  HMI Task function called from the scheduler
  * @param  pvParameters definition for FreeRTOS not used
  * @retval None
  */
void xHMI_task(void *pvParameters);
/**
  * @brief  Update LED status according to its config
  * @param  led to be updated
  * @retval None
  */

void hmi_led_init(hmi_led_t * led,uint8_t pin_number, app_timer_id_t const * timer_id,app_timer_timeout_handler_t timer_handler);
void hmi_led_update(hmi_led_t * led);
/**
  * @brief  Config a device's LED behavior
  * @param  led to be configurated
	* @param  command to the configurator. Setup a blink, on, off  state.
  * @retval None
  */
void hmi_led_setup(hmi_led_t * led, led_cmd_t command);

/**
  * @brief Enqueue the command "CMD_reject" on the HMI commands queue. 
	* 				This funtion is used to indicated a fail to execute a command by any task
  * @param none
  * @retval PdPass / pdFail
  */
BaseType_t xHMI_command_reject(void);
uint16_t hmi_battery_level(bool percent);
#endif //__HMI_H
