/**
  * @file    hmi.h
  * @author  Mariano G. Scasso
  * @version V1.0
  * @date    May 1st, 2017
  * @brief   	This file contain the function definitios for the mpu task. 
	* 	      
	*
	*	@copyright COPYRIGHT(c) 2017 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/

#ifndef __MPU_TASK_H
	#define __MPU_TASK_H
#include <stdbool.h>
#include <stdint.h>

#include "nrf_drv_mpu.h"

#include "FreeRTOS.h"
#include "queue.h"
#include "timers.h"

#include "ble_nus.h"
#include "app_mpu.h"
#include "calibrarGPScoder_types.h"

extern QueueHandle_t xMPU_Commands_Queue; /*!< mpu commands queue, public to the other tasks */
extern TaskHandle_t xMPU_task_handle;
/**
  * @brief  MPU commads. Used to make a state transition on the MPU smachine.
*/

#define MPU_DATA_TYPE 30
typedef enum
{
	MPU_COMMAND_NULL = 0,
	MPU_COMMAND_DEBUG_ON,
	MPU_COMMAND_DEBUG_OFF,
	MPU_COMMAND_GESTURE_ON,
	MPU_COMMAND_GESTURE_OFF,
	MPU_COMMAND_ROT_ON,
	MPU_COMMAND_ROT_OFF,
	MPU_COMMAND_EULER_ON,
	MPU_COMMAND_EULER_OFF,
	MPU_COMMAND_RAW,
	MPU_COMMAND_QUATERN,
	MPU_COMMAND_DTW_ON,
	MPU_COMMAND_DTW_OFF,
	MPU_COMMAND_ON,
	MPU_COMMAND_OFF
}mpu_command_t;


/**
  * @brief  MPU smachine states
*/
typedef enum
{
	MPU_STATE_OFF = 0,
	MPU_STATE_INIT,
	MPU_STATE_RAW,
	MPU_STATE_RAW_STRING,
	MPU_STATE_QUATERNION,
	MPU_STATE_IDLE,
	MPU_STATE_FAIL
} mpu_state_t;

typedef struct mpu_smachine_s
{
	mpu_state_t state;
	mpu_command_t command;
} mpu_smachine_t;

typedef __packed struct mpu_data_s
{
	uint8_t type;
	uint32_t time;
	accel_values_t accel;
	gyro_values_t gyro;
	magn_values_t magn;
} mpu_data_t;

typedef __packed struct filter_data_s
{
	uint8_t type;
	uint32_t time;
	float pos[2];
	int16_t quat[4];// ve
	int16_t vel[3]; 
	int8_t acc[3]; // aceleracion por 10
} filter_data_t;

void mpu_task_init(void);
void xMPU_task(void *pvParameters);
uint8_t xMPU_get_state(void);
bool xMPU_get_dwt_state(void);
bool xMPU_get_power(void);
uint16_t xMPU_get_dwt_times(void);
bool gps_buffer_add(uint32_t time, double lat, double lon, float alt,float vN, float vE, float vD, float aAcc, float vAcc, float sAcc );
void filter_set_gps_dt(float dt);
bool filter_save(void);
#endif // __MPU_TASK_H
