/**
  * @file    hmi_task.c
  * @author  Mariano G. Scasso
  * @version V1.0
  * @date    May 1st, 2017
  * @brief   	This file implements the functions of the hmi task. A command received from BLE is 
  *						processed here as well one from a button. Also the task is able to receive a command from
	*						other task. The commands are enqueued on the xHMI_Commands_Queue.
	* 	      
	*
	*	@copyright COPYRIGHT(c) 2016 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/

#include <string.h>
#include "hmi_task.h"

#include "nordic_common.h"
#include "nrf_drv_gpiote.h"
#include "nrf_gpio.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_lpcomp.h"

#include "wise_board.h"

#include "ble_task.h"

#ifdef WISE_BAS_ENABLE
	#include "ble_bas.h"
	extern ble_bas_t m_bas;
#endif
#ifdef WISE_GPS_ENABLE
	#include "gps_task.h"
#endif
#ifdef WISE_MEM_ENABLE
	#include "mem_task.h"
#endif

#ifdef WISE_MPU_ENABLE
	#include "mpu_task.h"
#endif

#ifdef WISE_POWERTEST_ENABLE
	#include "power_test.h"
	power_reg_t power_register;
	uint16_t current_prom;
	uint8_t current_prom_index;

#endif

#include "nrf_drv_wdt.h"
#include "nrf_gpio.h"
#include "app_button.h"

#define HMI_SLEEP_TIME_OUT 2400 //20 minutos inactivo para dormir
#define HMI_BATTERY_UPDATE_TIME_OUT 20
TaskHandle_t xHMI_task_handle;
QueueHandle_t xHMI_Commands_Queue; /*!< hmi commands queue */
static hmi_state_t hmi_smachine;
static hmi_command_t hmi_cmd_rcv;
static uint16_t hmi_time_out;
static nrf_saadc_value_t adc_buf[2];

#define ADC_REF_VOLTAGE_IN_MILLIVOLTS   600                                     /**< Reference voltage (in milli volts) used by ADC while doing conversion. */
#define ADC_PRE_SCALING_COMPENSATION    8                                       /**< The ADC is configured to use VDD with 1/3 prescaling as input. And hence the result of conversion is to be multiplied by 3 to get the actual value of the battery voltage.*/
#define DIODE_FWD_VOLT_DROP_MILLIVOLTS  0                                     /**< Typical forward voltage drop of the diode . */
#define ADC_RES_10BIT                   1024                                    /**< Maximum digital value for 10-bit ADC conversion. */


nrf_drv_wdt_channel_id m_channel_id;

/**@brief Macro to convert the result of ADC conversion in millivolts.
 *
 * @param[in]  ADC_VALUE   ADC result.
 *
 * @retval     Result converted to millivolts.
 */

#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE)\
        ((((ADC_VALUE) * ADC_REF_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_PRE_SCALING_COMPENSATION)


#define FPU_EXCEPTION_MASK 0x0000009F 

#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50)
#define BUTTON_DETECTION_HOLD						APP_TIMER_TICKS(5000)
#define BUTTON_DETECTION_RELEASE				APP_TIMER_TICKS(500)

#define POWER_SAMPLE_TIME 100
#define BATTERY_LEVEL_HIGH 3900
#define BATTERY_LEVEL_MID 3300
#define BATTERY_LEVEL_LOW	3100

uint32_t battery_mv = 3500;
	nrf_saadc_value_t adc_result;
static uint32_t button_start_ticks = 0;
static uint32_t button_tick_release = 0;
static uint32_t button_ticks = 0;
static bool button_pressing = false;

static uint32_t battery_update_iteration;

APP_TIMER_DEF(hmi_button_tick_timer);
APP_TIMER_DEF(hmi_led_green_timer);
APP_TIMER_DEF(hmi_led_red_timer);

hmi_led_t hmi_led_red, hmi_led_green;

//debug
uint32_t hmi_debug;
//

static uint8_t hmi_battery_percent(uint16_t mv)
{
	uint8_t calc;
	
	if (mv >= 4170)
		return 100;
	
	if (mv >= 3900)
	{
		calc = 0.057 * mv - 137.46;
		return (uint8_t) calc;
	}
	if (mv >= 3400)
	{
		calc = 0.16 * mv - 540.13;
		return (uint8_t) calc;
	}
	if (mv >= 3001)
	{
		calc = 0.013* mv - 39.13;
		return (uint8_t) calc;
	}
	if (mv < 3001)
	{
		return 0;
	}
	return 0;
}
/**
 * @brief LPCOMP event handler is called when LPCOMP detects voltage drop.
 *
 * This function is called from interrupt context so it is very important
 * to return quickly. Don't put busy loops or any other CPU intensive actions here.
 * It is also not allowed to call soft device functions from it (if LPCOMP IRQ
 * priority is set to APP_IRQ_PRIORITY_HIGH).
 */
static void lpcomp_event_handler(nrf_lpcomp_event_t event)
{
    if (event == NRF_LPCOMP_EVENT_DOWN)
    {
        //bsp_board_led_invert(BSP_BOARD_LED_0); // just change state of first LED
      hmi_debug=1;
			nrf_gpio_pin_set(POWER_OFF_PIN);
     //   voltage_falls_total++;
    }
		hmi_debug=2;
}
/**
 * @brief Initialize LPCOMP driver.
 */
static void lpcomp_init(void)
{
    uint32_t                err_code;

    nrf_drv_lpcomp_config_t config = NRF_DRV_LPCOMP_DEFAULT_CONFIG;
    config.input = VSENSE2_PIN_LPCOMP;
    // initialize LPCOMP driver, from this point LPCOMP will be active and provided
    // event handler will be executed when defined action is detected
    err_code = nrf_drv_lpcomp_init(&config, lpcomp_event_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_lpcomp_enable();
}

static void button_timer_handler(void * p_context)
{
  UNUSED_PARAMETER(p_context);
}
static void led_green_timer_handler(void * p_context)
{
  UNUSED_PARAMETER(p_context);
	hmi_debug++;
	hmi_led_update(&hmi_led_green);
		
}
static void led_red_timer_handler(void * p_context)
{
  UNUSED_PARAMETER(p_context);
	hmi_led_update(&hmi_led_red);
}


void hmi_led_off(hmi_led_t * led)
{
	app_timer_stop(*led->led_timer_id);
	nrf_gpio_pin_set(led->pin);
}

static void hmi_leds_off(void)
{
	hmi_led_off(&hmi_led_green);
	hmi_led_off(&hmi_led_red);
}

static void hmi_sleep(void)
{
	ret_code_t err_code;
					
	nrf_drv_gpiote_in_uninit(BUTTON1_PIN);
	nrf_drv_gpiote_in_uninit(INT9DOF_PIN_NUMBER);
	hmi_leds_off();
	
	//Configure wake-up button
	nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_LOTOHI(false);     //Configure to generate interrupt and wakeup on pin signal low. "false" means that gpiote will use the PORT event, which is low power, i.e. does not add any noticable current consumption (<<1uA). Setting this to "true" will make the gpiote module use GPIOTE->IN events which add ~8uA for nRF52 and ~1mA for nRF51.
	in_config.pull = NRF_GPIO_PIN_PULLDOWN;                                            //Configure pullup for input pin to prevent it from floting. Pin is pulled down when button is pressed on nRF5x-DK boards, see figure two in http://infocenter.nordicsemi.com/topic/com.nordic.infocenter.nrf52/dita/nrf52/development/dev_kit_v1.1.0/hw_btns_leds.html?cp=2_0_0_1_4
	err_code = nrf_drv_gpiote_in_init(BUTTON1_PIN, &in_config, NULL);             //Initialize the wake-up pin
	APP_ERROR_CHECK(err_code);                                                       //Check error code returned
	nrf_drv_gpiote_in_event_enable(BUTTON1_PIN, true);                            //Enable event and interrupt for the wakeup pin
	//Enter System-off
	
	 err_code = sd_power_system_off();		
}

/**
 * @brief WDT events handler.
 */
void wdt_event_handler(void)
{
    nrf_gpio_pin_set(POWER_OFF_PIN);
    //NOTE: The max amount of time we can spend in WDT interrupt is two cycles of 32768[Hz] clock - after that, reset occurs
}

/**@brief Function for handling the ADC interrupt.
 *
 * @details  This function will fetch the conversion result from the ADC, convert the value into
 *           percentage and send it to peer.
 */
void saadc_event_handler(nrf_drv_saadc_evt_t const * p_event)
{
	if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
  {
	
		uint16_t          batt_lvl_in_milli_volts;
		uint8_t           percentage_batt;
		uint32_t          err_code;
		led_cmd_t led_cmd ={.ton=300,.toff=500,.reps=40}; //blink
    adc_result = p_event->data.done.p_buffer[0];

    err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, 2);
		APP_ERROR_CHECK(err_code);

    battery_mv = ADC_RESULT_IN_MILLI_VOLTS(adc_result);
    percentage_batt = hmi_battery_percent(battery_mv); //ajustar para la bateria de wise
			
		if (battery_mv < BATTERY_LEVEL_LOW && battery_update_iteration > HMI_BATTERY_UPDATE_TIME_OUT-3)
			nrf_gpio_pin_set(POWER_OFF_PIN);
		
		battery_update_iteration++;
		if ((battery_update_iteration % HMI_BATTERY_UPDATE_TIME_OUT == 0) && (hmi_smachine != HMI_STATE_CONNECTED))
		{
			ble_update_name_auto();
			if (gps_get_state() == GPS_STATE_IDLE)
			{
				hmi_led_setup(&hmi_led_green,led_cmd); //blink led greeen
			}
		}

#ifdef WISE_BAS_ENABLE
       err_code = ble_bas_battery_level_update(&m_bas, percentage_batt, BLE_CONN_HANDLE_ALL);
       if ((err_code != NRF_SUCCESS) &&
            (err_code != NRF_ERROR_INVALID_STATE) &&
            (err_code != NRF_ERROR_RESOURCES) &&
            (err_code != NRF_ERROR_BUSY) &&
            (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
           )
        {
            APP_ERROR_HANDLER(err_code);
        }
#endif
    }
}

/**@brief Function for configuring ADC to do battery level conversion.
 */
static void adc_configure(void)
{
    ret_code_t err_code = nrf_drv_saadc_init(NULL, saadc_event_handler);
    APP_ERROR_CHECK(err_code);

    nrf_saadc_channel_config_t config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(VSENSE2_PIN_ADC);
		config.gain = NRF_SAADC_GAIN1_4;

    err_code = nrf_drv_saadc_channel_init(0, &config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(&adc_buf[0], 1);
    APP_ERROR_CHECK(err_code);
/*
    err_code = nrf_drv_saadc_buffer_convert(&adc_buf[1], 1);
    APP_ERROR_CHECK(err_code);*/
}

static void adc_uninit(void)
{
	nrf_drv_saadc_channel_uninit(0);
	nrf_drv_saadc_uninit();
	nrf_gpio_input_disconnect(VSENSE2_PIN_NUMBER);
}

void hmi_led_init(hmi_led_t * led,uint8_t pin_number, app_timer_id_t const * timer_id, app_timer_timeout_handler_t timer_handler)
{
	led->pin = pin_number;
	led->led_timer_id = timer_id;
	
	led->ton = 0;
	led->toff = 0;
	
	uint32_t err_code = app_timer_create(timer_id, APP_TIMER_MODE_SINGLE_SHOT, timer_handler);
	APP_ERROR_CHECK(err_code);
}
/**
  * @brief  Config a device's LED behavior
  * @param  led to be configurated
	* @param  command to the configurator. Setup a blink, on, off  state.
  * @retval None
  */

void hmi_led_setup(hmi_led_t * led, led_cmd_t command)
{
	led->toff = command.toff;
	led->ton = command.ton;
	led->reps = command.reps;
	
	if (led->ton == 0) //si ton es 0 el led est� siempre apagado
	{
		nrf_gpio_pin_set(led->pin);
		app_timer_stop(*led->led_timer_id);
	}
	else if (led->toff == 0) //si toff es 0 el led est� siempre encendido
	{
		nrf_gpio_pin_clear(led->pin);
		app_timer_stop(*led->led_timer_id);
	}
	else
	{
		nrf_gpio_pin_clear(led->pin); //ARRANCO CON EL LED ENCENDIDO
		app_timer_start(*led->led_timer_id,APP_TIMER_TICKS(led->ton),NULL);
	}
}


/**
  * @brief  Update LED status according to its config
  * @param  led to be updated
  * @retval None
  */
void hmi_led_update(hmi_led_t * led)
{
	if (0 == nrf_gpio_pin_out_read(led->pin))//si esta encendido
	{
		nrf_gpio_pin_set(led->pin); //apago
		app_timer_start(*led->led_timer_id,APP_TIMER_TICKS(led->toff),NULL); //inicio timer toff
	}
	else
	{
		nrf_gpio_pin_clear(led->pin); //enciendo
		app_timer_start(*led->led_timer_id,APP_TIMER_TICKS(led->ton),NULL); //inicio timer ton
	}
	
	if (led->reps > 0) //se acabaron las repeticiones, apago;
	{
		led->reps--;
		if (led->reps == 0)
			hmi_led_off(led);
	}
}

/**
  * @brief  Event handler for button 1 - GPS Wake up and memory save.
  * @param  pin and action params used for nrf gpiote lib.
  * @retval None
  */

static void button_handler(uint8_t button, uint8_t action)
{
	uint32_t err_code;
	hmi_command_t command_to_hmi;
	led_cmd_t led_r_cmd = {.ton = 300, .toff = 300};
	uint8_t cmd;
#ifdef WISE_WDT_ENABLE
	nrf_drv_wdt_channel_feed(m_channel_id);
#endif

  switch(action) 
	{
    case APP_BUTTON_PUSH:
        app_timer_start(hmi_button_tick_timer, 65535, NULL);
        button_start_ticks = app_timer_cnt_get();
				button_pressing = true;
        break;
    case APP_BUTTON_RELEASE:
        button_tick_release = app_timer_cnt_get();
        err_code = app_timer_stop(hmi_button_tick_timer);
        APP_ERROR_CHECK(err_code);

        button_ticks = app_timer_cnt_diff_compute(button_tick_release, button_start_ticks);
				button_pressing = false;
				if (button_ticks > BUTTON_DETECTION_RELEASE && button_ticks < BUTTON_DETECTION_HOLD)
				{
#ifdef WISE_MEM_ENABLE					
					cmd = MEM_COMMAND_SAVE_START;
					xQueueSendToBack( xMEM_Commands_Queue, &cmd, 10 );
#endif

#ifdef WISE_GPS_ENABLE
					cmd = GPS_COMMAND_START;
					xQueueSendToBack( xGPS_Commands_Queue, &cmd, 10);
					vTaskResume(xGPS_task_handle);
#endif
				}
				else if (button_ticks >= BUTTON_DETECTION_HOLD)
				{
					command_to_hmi.type = HMI_COMMAND_SLEEP;
					//led_r_cmd.ton=50;
					//led_r_cmd.toff = 200;
					//hmi_led_setup(&hmi_led_red,led_r_cmd);
					xQueueSendToBack( xHMI_Commands_Queue, &command_to_hmi, 10 );
				//	nrf_gpio_pin_set(POWER_OFF_PIN);
				}
				else if (hmi_smachine == HMI_STATE_IDLE)
				{
					led_r_cmd.ton=300;
					led_r_cmd.toff = 500;
					led_r_cmd.reps = 20;
					hmi_led_setup(&hmi_led_green,led_r_cmd);
				}
    break;
  }
}

static void hmi_gpio_init()
{
	nrf_gpio_cfg_output(LED_RED_PIN_NUMBER);
	nrf_gpio_cfg_output(LED_GREEN_PIN_NUMBER);
	nrf_gpio_cfg_output(POWER_OFF_PIN);
	nrf_gpio_pin_clear(POWER_OFF_PIN);
}

static void hmi_wdt_init()
{
    uint32_t err_code;
		nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
		config.reload_value = 20000;
    err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
    nrf_drv_wdt_enable();
}

void buttons_init(void)
{

// Note: Array must be static because a pointer to it will be saved in the Button handler
//       module.
	uint32_t err_code = 0;

	static app_button_cfg_t buttons[] = {
        {BUTTON1_PIN, APP_BUTTON_ACTIVE_HIGH, NRF_GPIO_PIN_PULLDOWN, button_handler}
    };
    
	err_code = app_button_init((app_button_cfg_t *)buttons,
                               sizeof(buttons) / sizeof(buttons[0]),
                               BUTTON_DETECTION_DELAY);
  APP_ERROR_CHECK(err_code);
    
	err_code = app_button_enable();
  APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_create(&hmi_button_tick_timer, APP_TIMER_MODE_REPEATED, button_timer_handler);
	APP_ERROR_CHECK(err_code);

}

/**
  * @brief  HMI Task function called from the scheduler
  * @param  pvParameters definition for FreeRTOS not used
  * @retval None
  */
void xHMI_task(void *pvParameters)
{
	const TickType_t xDelay100ms = pdMS_TO_TICKS(100);
	BaseType_t xStatus;
	ret_code_t err_code;
	 
	hmi_smachine = HMI_STATE_IDLE;
	hmi_time_out = HMI_SLEEP_TIME_OUT;
	
	led_cmd_t led_cmd ={.ton=300,.toff=500,.reps=10}; //blink
	
	hmi_gpio_init();
	buttons_init();
//	lpcomp_init();
  adc_configure();
	
	hmi_led_init(&hmi_led_green,LED_GREEN_PIN_NUMBER,&hmi_led_green_timer,led_green_timer_handler);
	hmi_led_init(&hmi_led_red,LED_RED_PIN_NUMBER,&hmi_led_red_timer,led_red_timer_handler);
	
	hmi_led_setup(&hmi_led_green,led_cmd); //blink led greeen
	led_cmd.ton = 0;
	hmi_led_setup(&hmi_led_red,led_cmd); //led red off
	
	battery_update_iteration = HMI_BATTERY_UPDATE_TIME_OUT-5;
	battery_mv = 3500;
	  //Configure WDT.
#ifdef WISE_WDT_ENABLE
	hmi_wdt_init();
#endif
	
#ifdef WISE_POWERTEST_ENABLE	
	current_prom = 0;
	current_prom_index = 0;
#endif

	for(;;)
	{
		err_code = nrf_drv_saadc_sample(); //medir bateria
#ifdef WISE_WDT_ENABLE
 		nrf_drv_wdt_channel_feed(m_channel_id);
#endif		
		if ((button_pressing) && (app_timer_cnt_diff_compute(app_timer_cnt_get(), button_start_ticks) > BUTTON_DETECTION_HOLD))
		{
			led_cmd.ton = 0;
			led_cmd.toff = 1;
			hmi_led_setup(&hmi_led_red,led_cmd);
			nrf_gpio_pin_clear(LED_RED_PIN_NUMBER); //FUERZO LED ROJO
		}
		

		xStatus = xQueueReceive( xHMI_Commands_Queue, &hmi_cmd_rcv,xDelay100ms);
		if( xStatus == pdPASS )
		{
		 /* Data was successfully received from the queue*/
			switch(hmi_cmd_rcv.type)
			{
				case HMI_COMMAND_LED_GREEN:
					memcpy(&led_cmd,hmi_cmd_rcv.value,sizeof(led_cmd));
					hmi_led_setup(&hmi_led_green,led_cmd);
					break;
				case HMI_COMMAND_LED_RED:
					memcpy(&led_cmd,hmi_cmd_rcv.value,sizeof(led_cmd));
					hmi_led_setup(&hmi_led_red,led_cmd);
					break;
				case HMI_COMMAND_SLEEP:
					led_cmd.ton=50;
					led_cmd.toff = 200;
					hmi_led_setup(&hmi_led_red,led_cmd);
					hmi_smachine = HMI_STATE_SLEEP;
					break;
				case HMI_COMMAND_BLE_CONNECTED:
					led_cmd.ton = 1;
					led_cmd.toff = 0;
					hmi_led_setup(&hmi_led_green,led_cmd);
					hmi_time_out = HMI_SLEEP_TIME_OUT;
					hmi_smachine = HMI_STATE_CONNECTED;
					break;
				case HMI_COMMAND_BLE_DISCONNECTED:
					led_cmd.ton = 50;
					led_cmd.toff = 200;
					led_cmd.reps = 10;
					hmi_led_setup(&hmi_led_green,led_cmd);
					hmi_time_out = HMI_SLEEP_TIME_OUT;
					hmi_smachine = HMI_STATE_IDLE;
					break;
				default:
					xStatus = pdFAIL;
					break;
			}
		}
		
		if (gps_get_state() != GPS_STATE_FIXED && battery_mv < BATTERY_LEVEL_MID)
			hmi_time_out--;
		else if (gps_get_state() == GPS_STATE_IDLE && battery_mv < BATTERY_LEVEL_HIGH)
			hmi_time_out--;
		else
			hmi_time_out = HMI_SLEEP_TIME_OUT;
		
		if ((hmi_time_out < HMI_SLEEP_TIME_OUT /4) && (hmi_time_out % 10 == 0)) //BLINKEO PARA AVISAR QUE SE VA A DORMIR
		{
			led_cmd.ton = 200;
			led_cmd.toff = 20;
			led_cmd.reps = 2;
			hmi_led_setup(&hmi_led_green,led_cmd);
		}

		switch ((uint8_t) hmi_smachine)
		{
			case HMI_STATE_IDLE:
				if ((battery_mv < BATTERY_LEVEL_MID && hmi_time_out < HMI_SLEEP_TIME_OUT-10) || (hmi_time_out < 60)) //retraso el apagado por el startup de la fuente
				{
					led_cmd.ton=50;
					led_cmd.toff = 200;
					hmi_led_setup(&hmi_led_red,led_cmd);
					hmi_smachine = HMI_STATE_SLEEP; //state transition --> SLEEP
					
				}
				break;
			case HMI_STATE_CONNECTED:
				break;
			case HMI_STATE_SLEEP:
			{
#ifdef WISE_MEM_ENABLE										
					mem_command_t mem_cmd =MEM_COMMAND_SAVE_STOP;
					xQueueSendToBack( xMEM_Commands_Queue, &mem_cmd, xDelay100ms );
#endif				
#ifdef WISE_GPS_ENABLE
				gps_command_t gps_cmd = GPS_COMMAND_STOP;
				xStatus = xQueueSendToBack( xGPS_Commands_Queue,&gps_cmd, xDelay100ms );
#endif
#ifdef WISE_MPU_ENABLE
				mpu_command_t mpu_cmd = MPU_COMMAND_OFF;
				xStatus = xQueueSendToBack( xMPU_Commands_Queue,&mpu_cmd, xDelay100ms );
#endif	
				vTaskDelay(xDelay100ms*20);
#ifdef WISE_SLEEP_MODE
				adc_uninit();
				if (battery_mv > BATTERY_LEVEL_MID)
					hmi_sleep();
				else
					nrf_gpio_pin_set(POWER_OFF_PIN);
#else
			nrf_gpio_pin_set(POWER_OFF_PIN);
			vTaskDelay(xDelay100ms);
#endif			

				break; 
			}
			default:
				break;
		}
		
#ifdef WISE_POWERTEST_ENABLE		
		current_prom += HAL_ADC_Sample_GetLast(0)/50;
		current_prom_index++;
		
		if (current_prom_index > POWER_SAMPLE_TIME)
		{
		/*
			char aux[20];
			memset(aux,0,20);
			sprintf(aux,"%d,%d,%d",HAL_ADC_Sample_GetLast(1)*2,xGPS_get_state(),xMEM_get_packages());
			ble_update_name(aux,15);
			*/
			current_prom /= (current_prom_index);
			power_register.type = POWER_SAMPLE_ID;
			power_register.time = APP_TIMER_MS(app_timer_cnt_get());
			power_register.current = current_prom;
			power_register.mode.bit_gps = (bool) xGPS_get_state();
			power_register.mode.bit_mpu =  xMPU_get_power();
			power_register.mode.bit_dwt =  xMPU_get_dwt_state();
			power_register.dtw_times = xMPU_get_dwt_times();
			
			xMEM_add_to_buffer((uint8_t*) &power_register,sizeof(power_register));
			
			current_prom = 0;
			current_prom_index = 0;
		}
#endif
		vTaskDelay(xDelay100ms*5);
	}
}

uint16_t hmi_battery_level(bool percent)
{
 if (percent)
	return hmi_battery_percent(battery_mv);
 else
	 return battery_mv;
}


