#include "ble_task.h"
#include "app_error.h"
#include "nrf_log.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "wise_board.h"
#include "hmi_Task.h"

#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_conn_state.h"
#include "ble_bas.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "ble_dfu.h"

#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_freertos.h"

#ifdef WISE_GPS_ENABLE
#include "gps_task.h"
#endif
#ifdef WISE_MEM_ENABLE
#include "mem_task.h"
#endif
#ifdef WISE_MPU_ENABLE
#include "mpu_task.h"
#endif

#ifdef WISE_STATISTICS_ENABLE
#include "mem_statistics.h"
#endif
/**
  * @brief  External ble commands definitions - Commands received throught ble nus.
*/
#define EXT_COMMAND_SIMBOL 					'@'
#define EXT_COMMAND_DATAIN					'S' //RECIBE DATOS 20 BYTES MAX
#define EXT_COMMAND_POWER_OFF				'0' //APAGA LA PLACA
#define EXT_COMMAND_GPS_START				'1'
#define EXT_COMMAND_GPS_RATE_1HZ		'a' //1a
#define EXT_COMMAND_GPS_RATE_4HZ		'b' //1b
#define EXT_COMMAND_GPS_RATE_10HZ		'c' //1c
#define EXT_COMMAND_GPS_STOP				'2'
#define EXT_COMMAND_MONITORMODE			'3'
#define EXT_COMMAND_TRANSFERMODE 		'4'
#define EXT_COMMAND_SAVE_START			'5'
#define EXT_COMMAND_SAVE_STOP				'6'
#define EXT_COMMAND_MEM_CLEAR				'7'

#define EXT_COMMAND_MPU_MONITOR			'8'
#define EXT_COMMAND_MPU_START				'm'
#define EXT_COMMAND_MPU_STOP				'n'
#define EXT_COMMAND_MPU_QUATERN_MONITOR 'q'

#define EXT_COMMAND_BULKERASE				'9'

#define EXT_COMMAND_GPS_STATUS			'G'
#define EXT_COMMAND_MEM_STATUS			'M'
#define EXT_COMMAND_BAT_STATUS_PERCENT			'B'
#define EXT_COMMAND_BAT_STATUS			'b'
#define EXT_COMMAND_MEM_DEBUG				'D'
#define EXT_COMMAND_SW_VERSION 			'V'
#define EXT_COMMAND_ID							'I'

#define EXT_COMMAND_UART_ENABLE			'U'
#define EXT_COMMAND_UART_DISABLE		'u'

#define EXT_COMMAND_STATISTICS_CALC 'e'
#define EXT_COMMAND_STATISTICS_GET 'f'

#define EXT_COMMAND_HW_DATA_SET '>'
#define EXT_COMMAND_HW_DATA_GET 'H'

#define BLE_TX_BUFFER_SIZE 128
#define BLE_RX_BUFFER_SIZE 64

const char COMPILATION_DATE[] =  __DATE__;
static ble_uuid_t m_adv_uuids[] = {{BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}, {BLE_UUID_BATTERY_SERVICE, BLE_UUID_TYPE_BLE}};
ble_uuid_t m_adv_uuids_scan[] = {{BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}};


BLE_BAS_DEF(m_bas);                                     														/**< Battery service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                           /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                             /**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising);                                                 /**< Advertising module instance. */

uint8_t ble_rx_data[BLE_RX_BUFFER_SIZE];
uint8_t ble_tx_data[BLE_TX_BUFFER_SIZE];
uint8_t ble_tx_data_index;
uint8_t ble_rx_data_index;

uint8_t device_name[] = "xxxx,1234,5,6\0";
BLE_NUS_DEF(m_nus, NRF_SDH_BLE_TOTAL_LINK_COUNT);                                   /**< BLE NUS service instance. */
uint16_t   m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3;            /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */	

uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;                            /**< Handle of the current connection. */

TaskHandle_t ble_cmd_task_handle;

uint8_t command_buffer[40];
uint16_t command_parameters[20];
uint8_t command_parameters_number;
uint8_t command_len = 0;


 
static uint8_t number_to_ascii(uint8_t in)
{
	uint8_t aux = in;
	uint8_t timeout = 0;
	while ((aux > '9') || (aux < '0'))
	{
		if (aux > '9')
			aux /=2;
		else if (aux < '0')
		{
			aux-=timeout/2;
			aux *= 2;
		}
		
		timeout++;
		if (timeout > 250)
			return '1';
	}
	return aux;
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;
		ble_gap_addr_t address;
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
	
		sd_ble_gap_addr_get(&address);
		device_name[0] = number_to_ascii(address.addr[BLE_GAP_ADDR_LEN-1]);
		device_name[1] = number_to_ascii(address.addr[BLE_GAP_ADDR_LEN-2]);
		device_name[2] = number_to_ascii(address.addr[0]);
		device_name[3] = number_to_ascii(address.addr[2]);
    
	err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)device_name,
                                          strlen((char*)device_name));
    APP_ERROR_CHECK(err_code);

    /* YOUR_JOB: Use an appearance value matching the application's use case.
       err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_);
       APP_ERROR_CHECK(err_code); */

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

static uint8_t * get_int(uint8_t * data_buff, uint16_t * val)
{
	uint16_t res = 0;

	while (*data_buff >= '0' && *data_buff <= '9')
	{
		res = res * 10 + *data_buff - '0';
		data_buff++;
	}
	*val = res;
	return data_buff;
}

static bool parse_command(uint8_t * buffer, uint8_t len)
{
	uint8_t i = 0;
	command_parameters_number = 0;
	while(*buffer!=',')
	{
		buffer++;
		i++;
		if (i >= len)
			return false;
	}
	
	while ((*buffer == ',') && (command_parameters_number < sizeof(command_parameters)))
	{
		buffer++;
		buffer = get_int(buffer,&command_parameters[command_parameters_number]);
		command_parameters_number++;
	}
	
	return true;
	
}


/**@snippet [Handling the data received over BLE] */
/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_evt       Nordic UART Service event.
 */
/**@snippet [Handling the data received over BLE] */
static void nus_data_handler(ble_nus_evt_t * p_evt)
{

	if (p_evt->type == BLE_NUS_EVT_RX_DATA)
	{
		uint32_t err_code;

		NRF_LOG_DEBUG("Received data from BLE NUS. Writing data on UART.");
		NRF_LOG_HEXDUMP_DEBUG(p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);
		if (p_evt->params.rx_data.length < BLE_RX_BUFFER_SIZE-1)
		{
			memcpy(ble_rx_data,p_evt->params.rx_data.p_data,p_evt->params.rx_data.length);
			ble_rx_data_index = p_evt->params.rx_data.length;
			ble_rx_data[ble_rx_data_index+1] = '\0';
		}
		else
		{
			memcpy(ble_rx_data,p_evt->params.rx_data.p_data,BLE_RX_BUFFER_SIZE);
			ble_rx_data[BLE_RX_BUFFER_SIZE-1] = '\0';
			ble_rx_data_index = BLE_RX_BUFFER_SIZE;
		}
	
	
	vTaskResume(ble_cmd_task_handle);
   //UNUSED_VARIABLE(xSemaphoreGiveFromISR(ble_semaphore_rx, &yield_req));
   //portYIELD_FROM_ISR(yield_req);	
	}

}

/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    pm_handler_on_pm_evt(p_evt);
    pm_handler_flash_clean(p_evt);
}

/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

void advertising_config_get(ble_adv_modes_config_t * p_config)
{
    memset(p_config, 0, sizeof(ble_adv_modes_config_t));

    p_config->ble_adv_fast_enabled  = true;
    p_config->ble_adv_fast_interval = APP_ADV_INTERVAL;
    p_config->ble_adv_fast_timeout  = APP_ADV_DURATION;
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
//            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
           // APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_IDLE:
          //  sleep_mode_enter();
            break;

        default:
            break;
    }
}

/**@brief Function for initializing the Advertising functionality.
 */
void advertising_init(void)
{
    uint32_t               err_code;
    ble_advertising_init_t init;
	
		memset(&init, 0, sizeof(init));
		
    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = false;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
   
		init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;
		
		
		init.srdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids_scan) / sizeof(m_adv_uuids_scan[0]);
		init.srdata.uuids_complete.p_uuids  = m_adv_uuids_scan;

//    advertising_config_get(&init.config);
		init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;
    init.evt_handler = on_adv_evt;


    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
		
}


/** @brief Clear bonding information from persistent storage.
 */
static void delete_bonds(void)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}
///**@brief Function for starting advertising. */
 void advertising_start(void * p_erase_bonds)
{
    bool erase_bonds = *(bool*)p_erase_bonds;

   /* if (erase_bonds)
    {
        delete_bonds();
        // Advertising is started by PM_EVT_PEERS_DELETE_SUCCEEDED event.
    }
    else
    {
        ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
        APP_ERROR_CHECK(err_code);
    }*/
	
	ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
	APP_ERROR_CHECK(err_code);
}
/**@brief Function for starting advertising.
 */

void nus_service_init()
{
	uint32_t                  err_code;	
	ble_nus_init_t     nus_init;
		// Initialize NUS.
    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = nus_data_handler;

    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);

}

static BaseType_t command_receive (uint8_t * command)
{
	const TickType_t xDelay100ms = pdMS_TO_TICKS( 100UL );
	BaseType_t xStatus = pdPASS;
	uint8_t cmd;
	uint8_t cmd_in	= command[1];
	uint8_t cmd_in_b	= command[2];
	
	switch (cmd_in) 
	{
		case EXT_COMMAND_POWER_OFF:
				cmd = HMI_COMMAND_SLEEP;
				xStatus = xQueueSendToBack( xHMI_Commands_Queue,&cmd, xDelay100ms*5 );
				break;
#ifdef WISE_MPU_ENABLE
		case EXT_COMMAND_MPU_START:
				switch(cmd_in_b)
				{
					case '0':
						cmd = MPU_COMMAND_ON;
						break;
					case '1':
						cmd = MPU_COMMAND_GESTURE_ON;
						break;
					case '2':
						cmd = MPU_COMMAND_ROT_ON;
						break;
					case '3':
						cmd = MPU_COMMAND_EULER_ON;
						break;
					case '8':
						cmd = MPU_COMMAND_DTW_ON;
						break;
					case '9':
						cmd = MPU_COMMAND_DEBUG_ON;
						break;
					default:
						break;
				}
				xStatus = xQueueSendToBack( xMPU_Commands_Queue,&cmd, xDelay100ms );
				vTaskResume(xMPU_task_handle);
				break;
		case EXT_COMMAND_MPU_STOP:
				switch(cmd_in_b)
					{
						case '0':
							cmd = MPU_COMMAND_OFF;
							break;
						case '1':
							cmd = MPU_COMMAND_GESTURE_OFF;
							break;
						case '2':
							cmd = MPU_COMMAND_ROT_OFF;
							break;
						case '3':
						cmd = MPU_COMMAND_EULER_OFF;
						case '8':
							cmd = MPU_COMMAND_DTW_OFF;
							break;
						case '9':
							cmd = MPU_COMMAND_DEBUG_OFF;
							break;
						default:
							break;
					}
				xStatus = xQueueSendToBack( xMPU_Commands_Queue,&cmd, xDelay100ms );
		  	break;
		case EXT_COMMAND_MPU_MONITOR:
				cmd = MPU_COMMAND_RAW;
				xStatus = xQueueSendToBack( xMPU_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_MPU_QUATERN_MONITOR:
				cmd = MPU_COMMAND_QUATERN;
				xStatus = xQueueSendToBack( xMPU_Commands_Queue,&cmd, xDelay100ms );
				break;
#endif
		case EXT_COMMAND_ID:
		{
			ble_gap_addr_t address;
    	sd_ble_gap_addr_get(&address);
			ble_tx_data_index = 0;
			memset(ble_tx_data,0,sizeof(ble_tx_data));
			for (uint8_t i = 0; i < BLE_GAP_ADDR_LEN;i++)
			{
				ble_tx_data_index += sprintf((char*)ble_tx_data+ble_tx_data_index,"%02x-",address.addr[i]);
			}
			ble_tx_data_index--;
			ble_tx_data[ble_tx_data_index] = '\r';
			ble_tx_data_index++;
			ble_tx_data[ble_tx_data_index] = '\n';
			ble_string_send(ble_tx_data, ble_tx_data_index);
		}			break;
		case EXT_COMMAND_SW_VERSION:
			ble_string_send((uint8_t *)WISE_SORFTWARE_VERSION, strlen(WISE_SORFTWARE_VERSION));
	//		ble_string_send((uint8_t *)COMPILATION_DATE, strlen(COMPILATION_DATE));
			break;
#ifdef WISE_MEM_ENABLE
		case EXT_COMMAND_MEM_CLEAR:
				cmd = MEM_COMMAND_ERASE;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_SAVE_START:
				cmd=  MEM_COMMAND_SAVE_START;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_SAVE_STOP:
				cmd = MEM_COMMAND_SAVE_STOP;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_MEM_STATUS:
				cmd = MEM_COMMAND_GET_STATE;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
		  	break;
		case EXT_COMMAND_MEM_DEBUG:
				cmd = MEM_COMMAND_READ_DEBUG;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_TRANSFERMODE:
				cmd = MEM_COMMAND_READ_START;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
#endif
#ifdef WISE_STATISTICS_ENABLE
		case EXT_COMMAND_STATISTICS_CALC:
		{
				statistics_time_t tstr, tstp;
				
				if (command_parameters_number < 6)
				{
					tstr.hour = 0;
					tstr.minute =  0;
					tstr.second = 0;
			
					tstp.hour = 0;
					tstp.minute =  0;
					tstp.second = 0;
				}
				else
				{
					tstr.hour = command_parameters[0];
					tstr.minute =  command_parameters[1];
					tstr.second = command_parameters[2];
			
					tstp.hour = command_parameters[3];
					tstp.minute =  command_parameters[4];
					tstp.second = command_parameters[5];
				}
		
				statistics_calc_start(tstr,tstp,command_parameters[6],command_parameters[7]);
				cmd = MEM_COMMAND_STATISTICS_CALC;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
		}	break;
		case EXT_COMMAND_STATISTICS_GET:
				cmd = MEM_COMMAND_STATISTICS_GET;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
#endif
#ifdef WISE_GPS_ENABLE
		case EXT_COMMAND_GPS_START:			
				cmd = GPS_COMMAND_START;
				xStatus = xQueueSendToBack( xGPS_Commands_Queue,&cmd, xDelay100ms );
				switch (cmd_in_b)
				{
					case EXT_COMMAND_GPS_RATE_1HZ:
						cmd = GPS_COMMAND_RATE_1HZ;
						break;
					case EXT_COMMAND_GPS_RATE_4HZ:
						cmd = GPS_COMMAND_RATE_4HZ;
						break;
					case EXT_COMMAND_GPS_RATE_10HZ:
						cmd = GPS_COMMAND_RATE_10HZ;
						break;
					default:
						cmd = GPS_COMMAND_NULL;
						break;
				}
				xStatus = xQueueSendToBack( xGPS_Commands_Queue,&cmd, xDelay100ms );
				cmd=  MEM_COMMAND_SAVE_START; //entra en modo grabacion
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				vTaskResume(xGPS_task_handle);
		    break;
		case EXT_COMMAND_GPS_STOP:
				cmd = GPS_COMMAND_STOP;
				xStatus = xQueueSendToBack( xGPS_Commands_Queue,&cmd, xDelay100ms );
				cmd=  MEM_COMMAND_SAVE_STOP; //tambien sale del modo grabacion
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_MONITORMODE:
				cmd = GPS_COMMAND_MONITOR;
				xStatus = xQueueSendToBack( xGPS_Commands_Queue,&cmd, xDelay100ms );
		    break;
#endif
		case EXT_COMMAND_BAT_STATUS:
				memset(ble_tx_data,0,20);
				ble_tx_data_index = sprintf((char*)ble_tx_data,"%d mV",hmi_battery_level(false));
				ble_string_send(ble_tx_data,ble_tx_data_index);
		  	break;
		case EXT_COMMAND_BAT_STATUS_PERCENT:
				memset(ble_tx_data,0,20);
				ble_tx_data_index = sprintf((char*)ble_tx_data,"%d %%",hmi_battery_level(true));
				ble_string_send(ble_tx_data,ble_tx_data_index);
		  	break;
		case EXT_COMMAND_HW_DATA_GET:
			ble_tx_data_index=mem_read_manufacturing_data(ble_tx_data);
			ble_string_send(ble_tx_data,ble_tx_data_index);
			break;
		case EXT_COMMAND_HW_DATA_SET:
			if (command_parameters_number < 3)
				break;
			
			ble_tx_data_index = sprintf((char*) ble_tx_data,"REDIMEC S.R.L - HW version: %2d.%2d - MPU CODE: %d	- QUALITY PASS DATE: %2d/%2d/%2d\r\n",
																											command_parameters[0]/10,command_parameters[0]%10,command_parameters[1],
																											command_parameters[2],command_parameters[3],command_parameters[4]);
			mem_write_manufacturing_data(ble_tx_data,ble_tx_data_index);
			ble_string_send((uint8_t*)"OK",2);
			break;
		case EXT_COMMAND_UART_ENABLE:
//				cmd = BLE_COMMAND_UART_ENABLE;
				break;
		case EXT_COMMAND_UART_DISABLE:
	//			cmd= BLE_COMMAND_UART_DISABLE;
				break;
		default:
			xStatus = pdFAIL;
				break;

	}
	memset(command_parameters,0,sizeof(command_parameters));
	command_parameters_number = 0;
	return xStatus;
}


void ble_cmd_task(void *pvParameters)
{
	uint8_t *datain = (uint8_t*) pvParameters;
	const TickType_t xDelay500ms = pdMS_TO_TICKS( 500UL );
	bool cmd_ok = false;
	for(;;)
	{
		//memcpy(command,pvParameters,3);
		command_len = strlen((char*)pvParameters);
		memcpy(command_buffer,pvParameters,command_len);
		if (command_len > 3)
		{
			parse_command(command_buffer,command_len);
		}
		
		if (pdFAIL == command_receive(command_buffer))
		{
			ble_string_send((uint8_t *) "@@", 2); // rejected command
		}
		else
		{
			ble_string_send(command_buffer, 2); // Accepted command
			vTaskDelay(xDelay500ms);
			//ble_update_name_auto();
		}
		
		vTaskSuspend(ble_cmd_task_handle);
	}
}

uint32_t ble_string_send(uint8_t *data, uint16_t len)
{
	uint32_t err_code;
	uint16_t time_out = 10;
	uint16_t m_len = 0;
	
	if (len == 0)
		return 0;
	
	if (len > m_ble_nus_max_data_len)
		m_len = m_ble_nus_max_data_len;
	else
		m_len = len;
	do
	{
		err_code = ble_nus_data_send(&m_nus, data, &m_len, m_conn_handle);
		time_out--;
	} while ((err_code == NRF_ERROR_BUSY || err_code == NRF_ERROR_RESOURCES)  && time_out);

	if (err_code == NRF_SUCCESS)
		return m_len;
	else
		return 0;
}

bool ble_block_send(uint8_t *data, uint16_t len)
{
//implementar para enviar paquetes mas grandes

}

void ble_cmd_init(void)
{

if(pdPASS != xTaskCreate(ble_cmd_task, "CMD", 256, (void *) ble_rx_data, 3, &ble_cmd_task_handle))
	{
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
}

/**@brief Function for initializing services that will be used by the application.
 */
void services_init(void)
{
    uint32_t                  err_code;
    nrf_ble_qwr_init_t        qwr_init  = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

		nus_service_init();
		
#ifdef WISE_BAS_ENABLE
    ble_bas_init_t bas_init_obj;

    memset(&bas_init_obj, 0, sizeof(bas_init_obj));

    bas_init_obj.evt_handler          = NULL;
    bas_init_obj.support_notification = true;
    bas_init_obj.p_report_ref         = NULL;
    bas_init_obj.initial_batt_level   = 100;

    bas_init_obj.bl_rd_sec        = SEC_OPEN;
    bas_init_obj.bl_cccd_wr_sec   = SEC_OPEN;
    bas_init_obj.bl_report_rd_sec = SEC_OPEN;

    err_code = ble_bas_init(&m_bas, &bas_init_obj);
    APP_ERROR_CHECK(err_code);
#endif

    /* YOUR_JOB: Add code to initialize the services used by the application.
       uint32_t                           err_code;
       ble_xxs_init_t                     xxs_init;
       ble_yys_init_t                     yys_init;

       // Initialize XXX Service.
       memset(&xxs_init, 0, sizeof(xxs_init));

       xxs_init.evt_handler                = NULL;
       xxs_init.is_xxx_notify_supported    = true;
       xxs_init.ble_xx_initial_value.level = 100;

       err_code = ble_bas_init(&m_xxs, &xxs_init);
       APP_ERROR_CHECK(err_code);

       // Initialize YYY Service.
       memset(&yys_init, 0, sizeof(yys_init));
       yys_init.evt_handler                  = on_yys_evt;
       yys_init.ble_yy_initial_value.counter = 0;

       err_code = ble_yy_service_init(&yys_init, &yy_init);
       APP_ERROR_CHECK(err_code);
     */
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}



/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}
/**@brief Function for initializing the Connection Parameters module.
 */
void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code = NRF_SUCCESS;
		hmi_command_t command_to_hmi;
		command_to_hmi.type = HMI_COMMAND_BLE_CONNECTED;
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
						
						command_to_hmi.type = HMI_COMMAND_BLE_CONNECTED;
            break;
				 case BLE_GAP_EVT_DISCONNECTED:
						command_to_hmi.type = HMI_COMMAND_BLE_DISCONNECTED;
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
            break;
        }

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
						command_to_hmi.type = HMI_COMMAND_BLE_DISCONNECTED;
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
						command_to_hmi.type = HMI_COMMAND_BLE_DISCONNECTED;
            break;

        default:
            // No implementation needed.
            break;
    }
		xQueueSendToBack( xHMI_Commands_Queue, &command_to_hmi, 100 );

}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for the Peer Manager initialization.
 */
void peer_manager_init()
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}



/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED))
    {
        m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        NRF_LOG_INFO("Data len is set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
    NRF_LOG_DEBUG("ATT MTU exchange completed. central 0x%x peripheral 0x%x",
                  p_gatt->att_mtu_desired_central,
                  p_gatt->att_mtu_desired_periph);
}

/**@brief   Function for initializing the GATT module.
 * @details The GATT module handles ATT_MTU and Data Length update procedures automatically.
 */
void gatt_init(void)
{
     ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}


bool ble_update_name(char * buffer,uint8_t len)
{
	uint32_t err_code;
	if (len > 15 || len == 0)
		return false;

	memset(device_name+4,0,15);
	device_name[4] = ',';
	strcat((char*) device_name, buffer);
	
	sd_ble_gap_adv_stop(m_advertising.adv_handle);
	
	ble_gap_conn_sec_mode_t sec_mode;
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
	sd_ble_gap_device_name_set(&sec_mode,(const uint8_t *) device_name,strlen((char*) device_name));
	
	advertising_init();
	ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
	
//	ble_advertising_advdata_update(&m_advertising,&m_advertising.adv_data,true);
	return true;
}

bool ble_update_name_auto(void)
{
	char aux[20];
	memset(aux,0,20);
	sprintf(aux,"%d,%d,%d",hmi_battery_level(true),xMEM_get_state(),xMEM_get_packages()); //actualizo el nombre del sensor
	
	ble_update_name(aux,strlen(aux));
}