/**
  * @file    gps_task.c
  * @author  Mariano G. Scasso
  * @version V1.0
  * @date    May 1st, 2017
  * @brief   	This file implements the functions of the gps task. A gps smachine with on, off and other states is defined. 
	*						The states transition is achived throught a commands queue.
	* 	      
	*
	*	@copyright COPYRIGHT(c) 2017 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/


#include "gps_task.h"
#include "wise_board.h"

#include "semphr.h"

#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"
#include "app_uart.h"
#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

#include "ble_task.h"
#include "hmi_task.h"
#ifdef WISE_MEM_ENABLE
#include "mem_task.h"
#endif

#include "minmea.h" 											/*!< parsing functions */
#include "mpu_task.h"
#include "app_timer.h"

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */

#define GPS_RESET_TIME_OUT 7000 // un poco mas de 5 minutos?? <-- modificabo bug?
#define GPS_PERIOD_MS 100
#define GPS_PDOP_THREHOLD 10
//#define GPS_TEST

_APP_TIMER_DEF(app_timer_gps);

uint16_t gps_reset_timeout;
SemaphoreHandle_t uart_semaphore_nmea; //uart nmea receive syncro


uint8_t uart_nmea_buffer[128];
uint8_t uart_nmea_buffer_index;
static uint8_t in_buffer[128];
static uint8_t in_buffer_index;

TaskHandle_t xGPS_task_handle;
QueueHandle_t xGPS_Commands_Queue; 				/*!< gps commands queue */
gps_smachine_t smachine_gps;									/*!< gps smachine */

minmea_frame_t gps_nmea_parsed;						/*!< parsed gps data struct */
minmea_sentence_optimized_t to_save; /*!< memory optimized gps struct*/

uint32_t t_ms;
volatile uint8_t fix_timeout; /*!< Time since last gps ppt*/
volatile uint16_t reset_timeout;
volatile bool gps_binary_mode = false;

session_t session;

static bool new_session;
static bool gps_monitor_mode;
static bool gps_bin_new;
gps_binary_t gps_bin;
gps_bin_save_t gps_bin_save;

uint16_t gps_freq = 1000;
uint32_t gps_last_ms = 0;
static void uart_init(void);

static void gps_bin_save_cpy(void)
{
	gps_bin_save.type = GPS_BIN_TYPE;
	gps_bin_save.t_ms = gps_bin.t_ms;
	gps_bin_save.aAcc = gps_bin.aAcc;
	gps_bin_save.fixType = gps_bin.fixType;
	gps_bin_save.gSpeed = gps_bin.gSpeed;
	gps_bin_save.height = gps_bin.height;
	gps_bin_save.hMSL = gps_bin.hMSL;
	gps_bin_save.lat = gps_bin.lat;
	gps_bin_save.lon = gps_bin.lon;
	gps_bin_save.vAcc = gps_bin.vAcc;
	gps_bin_save.valid = gps_bin.valid;
	gps_bin_save.velD = gps_bin.velD;
	gps_bin_save.velE = gps_bin.velE;
	gps_bin_save.velN = gps_bin.velN;
}


static void uart_string_send(uint8_t * msg, uint8_t len)
{
	for (uint8_t i = 0; i<len; i++)
	{
		app_uart_put(msg[i]);
		
	}
}


static void gps_msg_config()
{
const TickType_t xDelayms = pdMS_TO_TICKS( 100UL );
uint8_t msg1 [] = {0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00, 0x80, 0x25};
uint8_t msg1_b[] = {0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9A, 0x79};
uint8_t msg2[] = {0xB5, 0x62, 0x06, 0x00, 0x01, 0x00, 0x01, 0x08, 0x22};
uint8_t msg3[] = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0x01, 0x07, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x18, 0xE1};
uint8_t msg4[] = {0xB5, 0x62, 0x06, 0x01, 0x02, 0x00, 0x01, 0x07,	0x11, 0x3A};

uart_string_send(msg1,sizeof(msg1));
uart_string_send(msg1_b,sizeof(msg1));
vTaskDelay(xDelayms);

uart_string_send(msg2,sizeof(msg2));
vTaskDelay(xDelayms);

uart_string_send(msg3,sizeof(msg3));
vTaskDelay(xDelayms);

uart_string_send(msg4,sizeof(msg4));
vTaskDelay(xDelayms);

gps_binary_mode=true;

}

static void gps_rate10hz_config()
{
	const TickType_t xDelayms = pdMS_TO_TICKS( 100UL );

	uint8_t gps_msg_1[] = {0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0x64, 0x00, 0x01, 0x00, 0x01, 0x00, 0x7A, 0x12};
	uint8_t gps_msg_2[] = {0xB5, 0x62, 0x06, 0x08, 0x00, 0x00, 0x0E, 0x30};
	
	filter_set_gps_dt(0.1);
	
	uart_string_send(gps_msg_1,sizeof(gps_msg_1));
	vTaskDelay(xDelayms);
	uart_string_send(gps_msg_2,sizeof(gps_msg_2));
	
}

static void gps_rate4hz_config()
{
	const TickType_t xDelayms = pdMS_TO_TICKS( 100UL );

	uint8_t gps_msg_1[] = {0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0xFA, 0x00, 0x01, 0x00, 0x01, 0x00, 0x10, 0x96};//{0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0x64, 0x00, 0x01, 0x00, 0x01, 0x00, 0x7A, 0x12};
	uint8_t gps_msg_2[] = {0xB5, 0x62, 0x06, 0x08, 0x00, 0x00, 0x0E, 0x30};
	
	filter_set_gps_dt(0.25);
	
	uart_string_send(gps_msg_1,sizeof(gps_msg_1));
	vTaskDelay(xDelayms);
	uart_string_send(gps_msg_2,sizeof(gps_msg_2));
	
}

static void gps_rate1hz_config()
{
	const TickType_t xDelayms = pdMS_TO_TICKS( 100UL );

	uint8_t gps_msg_1[] = {0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0xE8, 0x03, 0x01, 0x00, 0x01, 0x00, 0x01, 0x39};
	uint8_t gps_msg_2[] = {0xB5, 0x62, 0x06, 0x08, 0x00, 0x00, 0x0E, 0x30};
	
	filter_set_gps_dt(1.0);
		
	uart_string_send(gps_msg_1,sizeof(gps_msg_1));
	vTaskDelay(xDelayms);
	uart_string_send(gps_msg_2,sizeof(gps_msg_2));
}

void gps_PPT_event_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	fix_timeout = 100;
}

void gps_timer_handler()
{
 t_ms++;
}

uint16_t gps_binary_to_string(char * buffer,const gps_binary_t * gps)
{
	uint16_t len = 0;
	len = sprintf(buffer,"T:%d,flag:%02X, FIX:%d;POS: %f,%f;VEL: %d,%d;ACC:%d,%d,%d;PDOP: %d;SAT: %d\r\n",gps->t_ms,gps->flags,
																										gps->fixType,(float) gps->lat * 1e-7f,(float) gps->lon * 1e-7f,gps->velE,gps->velN,
																											gps->aAcc,gps->vAcc,gps->sAcc, gps->pDOP, gps->numSV);
	return len;
}
static void gps_gpio_init()
{
	ret_code_t err_code;
	
	nrf_gpio_cfg_output(ANTON_PIN_NUMBER);
	nrf_gpio_cfg_output(RST_GPS_PIN_NUMBER);
	nrf_gpio_cfg_output(GPSON_PIN_NUMBER);
	
	nrf_gpio_pin_set(RST_GPS_PIN_NUMBER);
	nrf_gpio_pin_clear(ANTON_PIN_NUMBER);
	nrf_gpio_pin_clear(GPSON_PIN_NUMBER);
	
	if (!nrf_drv_gpiote_is_init())
	{
		err_code = nrf_drv_gpiote_init();	
		APP_ERROR_CHECK(err_code);
	}
	
	nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
	in_config.pull = NRF_GPIO_PIN_NOPULL;
  err_code = nrf_drv_gpiote_in_init(PPS_PIN_NUMBER, &in_config,  gps_PPT_event_handler);
  nrf_drv_gpiote_in_event_enable(PPS_PIN_NUMBER, true);

	nrf_gpio_cfg_input(TX_PIN_NUMBER, NRF_GPIO_PIN_NOPULL);
	nrf_gpio_cfg_input(RX_PIN_NUMBER, NRF_GPIO_PIN_NOPULL);
}

/**
  * @brief  Function to send serial start/stop commands to the gps module
  * @param  state: true = on / false = off
  * @retval None
  */
void gps_power(bool state)
{
	if (state)
	{
		nrf_gpio_pin_set(ANTON_PIN_NUMBER);
		nrf_gpio_pin_set(GPSON_PIN_NUMBER);
	}
	else
	{
		nrf_gpio_pin_clear(ANTON_PIN_NUMBER);
		nrf_gpio_pin_clear(GPSON_PIN_NUMBER);

	}
}

static void gps_reset()
{
	const TickType_t xDelay100ms = pdMS_TO_TICKS( 100UL );
	nrf_gpio_pin_clear(RST_GPS_PIN_NUMBER);
	vTaskDelay(xDelay100ms);
	nrf_gpio_pin_set(RST_GPS_PIN_NUMBER);
	gps_reset_timeout = GPS_RESET_TIME_OUT;
}

static void uart_reset()
{
	nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_TXDRDY);
  nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_RXTO);
  nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_ERROR);
  app_uart_close();
	uart_init();
}

void gps_uart_event_handle(app_uart_evt_t * p_event)
{
	BaseType_t yield_req = pdFALSE;
	switch (p_event->evt_type)
  {
		case APP_UART_DATA_READY:
		{
			if (in_buffer_index >= sizeof(in_buffer))
				in_buffer_index = 0;
			
      UNUSED_VARIABLE(app_uart_get(&in_buffer[in_buffer_index]));
			if (in_buffer[0] != 0xb5)
			{
				in_buffer_index =0;
				return;
			}
			else if (in_buffer_index > 1 && in_buffer[1] != 0x62)
			{
				in_buffer_index =0;
				return;
			}
			
			in_buffer_index++;
			if (in_buffer_index >= sizeof(gps_bin) + 6)
			{
				memcpy(&gps_bin,&in_buffer[6],sizeof(gps_bin));
				in_buffer_index = 0;
				UNUSED_VARIABLE(xSemaphoreGiveFromISR(uart_semaphore_nmea, &yield_req));
				portYIELD_FROM_ISR(yield_req);
			}
			break;
		}
			
		default:
			uart_reset();
			break;
  }
}

/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */
static void uart_init(void)
{
    uint32_t  err_code;
    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = UART_PIN_DISCONNECTED,
        .cts_pin_no   = UART_PIN_DISCONNECTED,
        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
        .use_parity   = false,
#if defined (UART_PRESENT)
        .baud_rate    = NRF_UART_BAUDRATE_9600
#else
        .baud_rate    = NRF_UART_BAUDRATE_9600
#endif
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       gps_uart_event_handle,
                       APP_IRQ_PRIORITY_LOW,
                       err_code);
    APP_ERROR_CHECK(err_code);
}
/**@snippet [UART Initialization] */


/**
  * @brief  GPS task function, called from the scheduler
  * @param  FreeRTOS task parameter, non used.
  * @retval None
  */

void xGPS_task(void *pvParameters)
{
	const TickType_t xDelay100ms = pdMS_TO_TICKS( 100UL );
	hmi_command_t command_to_hmi;
	BaseType_t xStatus;
	smachine_gps.state = GPS_STATE_IDLE;
	session.number = 0;
	fix_timeout = 0;
	session.type = GPS_SESSION_TYPE;
	new_session = true;
	gps_monitor_mode = false;
	gps_reset_timeout = GPS_RESET_TIME_OUT;
	gps_gpio_init();
	uart_init();
	ret_code_t err_code = app_timer_create(&app_timer_gps, APP_TIMER_MODE_REPEATED, gps_timer_handler);
	app_timer_start(app_timer_gps,APP_TIMER_TICKS(GPS_PERIOD_MS), NULL);
for(;;)
	{
		xStatus = xQueueReceive( xGPS_Commands_Queue, &smachine_gps.command, 0);
		if (pdPASS == xStatus)
			switch(smachine_gps.command)
				{
					case GPS_COMMAND_START:
						smachine_gps.state = GPS_STATE_START;
						command_to_hmi.type = HMI_COMMAND_LED_RED;
						command_to_hmi.value[0] = 100;
						command_to_hmi.value[1] = 500;
						command_to_hmi.value[2] = 0;
						xQueueSendToBack( xHMI_Commands_Queue, &command_to_hmi, 10 );
						fix_timeout = 0;
						gps_power(true);
						vTaskDelay(xDelay100ms*10);
						gps_msg_config();
						filter_set_gps_dt(1.0);
						gps_rate4hz_config(); //fuerzo 4hz
						break;
					case GPS_COMMAND_STOP:
						gps_power(false);
						session.number = 0;
						command_to_hmi.type = HMI_COMMAND_LED_RED;
						command_to_hmi.value[0] = 0;
						command_to_hmi.value[1] = 500;
						command_to_hmi.value[2] = 0;
						
						smachine_gps.state = GPS_STATE_IDLE;
						xQueueSendToBack( xHMI_Commands_Queue, &command_to_hmi, xDelay100ms * 10 );
						vTaskSuspend(xGPS_task_handle);
						break;
					case GPS_COMMAND_MONITOR:
						gps_monitor_mode=!gps_monitor_mode;
						break;
					case GPS_COMMAND_RATE_1HZ:
						gps_rate1hz_config();
						smachine_gps.state = GPS_STATE_FIXED;
						break;
					case GPS_COMMAND_RATE_4HZ:
						gps_rate4hz_config();
						break;
					case GPS_COMMAND_RATE_10HZ:
						gps_rate10hz_config();
						break;
					default:
						break;
				}
				
		if(xSemaphoreTake(uart_semaphore_nmea, xDelay100ms) == pdPASS)	/*!< nmea sentence received from uart*/
		{
			gps_bin_new = true;
			if (gps_monitor_mode) /*!< Avaible raw transference at xBLE_task*/
			{
					uart_nmea_buffer_index = gps_binary_to_string((char*) uart_nmea_buffer,&gps_bin);
					ble_string_send(uart_nmea_buffer,uart_nmea_buffer_index);
			}
		}
		
		if(fix_timeout >0) 
					fix_timeout--;
		
		switch ((uint8_t) smachine_gps.state)
		{
			case GPS_STATE_START: /*!< Send "on" command and go to idle*/
			{
				command_to_hmi.type = HMI_COMMAND_LED_RED;
				command_to_hmi.value[0] = 100;
				command_to_hmi.value[1] = 100;
				command_to_hmi.value[2] = 20;

				if (gps_reset_timeout-- == 0)
					gps_reset();
				
				if (!gps_bin_new)
					break;
				
				if(gps_bin.fixType > 2 && (gps_bin.flags & 0x01) && gps_bin.year > 0 && gps_bin.day > 0 && gps_bin.pDOP*0.01 < GPS_PDOP_THREHOLD)
				{
					session.day = gps_bin.day;
					session.year = gps_bin.year;
					session.month = gps_bin.month;
					session.hour = gps_bin.hour;
					session.minute = gps_bin.min;
					session.second = gps_bin.sec;
					
#ifdef WISE_MEM_ENABLE
					xMEM_add_to_buffer((uint8_t *)&session, sizeof(session));
#endif
					new_session = false;
					session.number++;
					smachine_gps.state = GPS_STATE_FIXED; /*<--- State trnsition*/
					xQueueSendToBack( xHMI_Commands_Queue, &command_to_hmi, 10 );
				}
				break;
			}
			case GPS_STATE_FIXED:
			{
				gps_reset_timeout = GPS_RESET_TIME_OUT;
				if (!gps_bin_new)
					break;
				
				if (gps_bin.fixType == 0 || gps_bin.fixType == 5 || gps_bin.pDOP*0.01 >GPS_PDOP_THREHOLD) //vuelvo a buscar se�al
				{
					smachine_gps.state = GPS_STATE_START;
					command_to_hmi.type = HMI_COMMAND_LED_RED;
					command_to_hmi.value[0] = 100;
					command_to_hmi.value[1] = 100;
					command_to_hmi.value[2] = 0;
					xQueueSendToBack( xHMI_Commands_Queue, &command_to_hmi, 10 );
					break;
				}
				if (gps_bin.pDOP*0.01 < 10) //filtro por calidad
				{					
					gps_buffer_add(gps_bin.t_ms,(double) gps_bin.lat * 1e-7 ,(double) gps_bin.lon * 1e-7, (float) gps_bin.height / 1000,(float)gps_bin.velN/ 1000,
																																		(float)gps_bin.velE / 1000, (float)gps_bin.velD/ 1000,
																																		(float) gps_bin.aAcc/1e8, (float) gps_bin.vAcc/1e8, (float) gps_bin.sAcc/10000); //(float) 0, (float) 0, (float) gps_bin.sAcc/10000);	//
					if (gps_bin.gSpeed > 1500)
						gps_freq = 250;
					else
						gps_freq = 1000;
				}
				if (gps_bin.t_ms - gps_last_ms >= gps_freq-10)
				{
					gps_last_ms = gps_bin.t_ms;
					while (!filter_save())
					{
						vTaskDelay(xDelay100ms/10);
					}
				}
#ifdef GPS_TEST			
				gps_bin_save_cpy();
				xMEM_add_to_buffer((uint8_t *) &gps_bin_save, sizeof(gps_bin_save)); /*!< Add to mem buffer,at xMEM_task*/
#endif	/*				
					command_to_hmi.type = HMI_COMMAND_LED_RED;
					command_to_hmi.value[0] = 40;
					command_to_hmi.value[1] = 60;
					command_to_hmi.value[2] = 2;
					xQueueSendToBack( xHMI_Commands_Queue, &command_to_hmi, 10 );*/
				break;	
			}

			case GPS_STATE_IDLE:

						vTaskDelay(xDelay100ms*5);
						break;
			default:
					break;
			}
		smachine_gps.command = GPS_COMMAND_NULL;
		gps_bin_new = false;
		vTaskDelay(xDelay100ms/10);
	}
}


void gps_task_init(void)
{
	uart_semaphore_nmea = xSemaphoreCreateBinary(); // use for nmea synchro and parsing
	
  if(NULL == uart_semaphore_nmea)
  {
       APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }

	xGPS_Commands_Queue = xQueueCreate( 2,sizeof(gps_command_t));

	if (xGPS_Commands_Queue != NULL)
	{
		if(pdPASS != xTaskCreate(xGPS_task, "GPS", 256, NULL, 2, &xGPS_task_handle))
		{
      APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
		}
	}
}
/**
  * @brief  get the date obtained from RMC frame
  * @param  year, month and day pointer to the destination variable.
  * @retval None
  */

bool gps_get_date(uint16_t * year,uint8_t * month, uint8_t * day)
{/*
	if (session_date.year < 18) //chequeo rapido para evitar fechas en 0
		return false;
	
	*day = session_date.day;
	*month =session_date.month;
	*year = session_date.year;
	*/
	return true;
}
void gps_get_time(uint8_t * hour,uint8_t * minute, uint8_t * second)
{/*
	*hour		= session_time.hours;
	*minute = session_time.minutes;
	*second = session_time.seconds;*/
}

uint32_t gps_get_ms()
{
	return gps_bin.t_ms;
}
	
uint8_t gps_get_state()
{
	return smachine_gps.state;
}

void gps_fill_session_aux(uint8_t*buffer,uint8_t len)
{
	if (len >=sizeof(session.aux))
		return;
	
	memcpy(session.aux,buffer,len);
	xMEM_add_to_buffer((uint8_t *)&session, sizeof(session));
}