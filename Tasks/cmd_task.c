/**
  * @file    hmi.c
  * @author  Mariano G. Scasso
  * @version V1.0
  * @date    May 1st, 2017
  * @brief   	This file implements the functions of the hmi task. A command received from BLE is 
  *						processed here as well one from a button. Also the task is able to receive a command from
	*						other task. The commands are enqueued on the xHMI_Commands_Queue.
	* 	      
	*
	*	@copyright COPYRIGHT(c) 2016 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/



#include "cmd_task.h"
#include "mem_task.h"
#include "nrf51xxx_hal_ble.h"
#include "ble_task.h"
#include "mpu_task.h"
#include "mem_task.h"
#include "gps_task.h"
#include "nordic_ble_functions.h"
#include "nrf52xxx_hal_adc.h"

#include "mem_statistics.h"

TaskHandle_t xCMD_task_handle;
//uint8_t debug_cmd[10];
uint8_t command_buffer[20];
uint8_t command_parameters[20];
uint8_t command_parameters_number;
uint8_t command_len = 0;
/**
  * @brief  Used to parse and enqueue a received command
  * @param  command, received from ble
	* @param  hmi_command, Point to hmi command queue.
  * @retval pdPass / pdFail
  */

static uint8_t * get_int(uint8_t * data_buff, uint8_t * val)
{
	uint16_t res = 0;

	while (*data_buff >= '0' && *data_buff <= '9')
	{
		res = res * 10 + *data_buff - '0';
		data_buff++;
	}
	*val = res;
	return data_buff;
}

static bool parse_command(uint8_t * buffer, uint8_t len)
{
	uint8_t i = 0;
	command_parameters_number = 0;
	while(*buffer!=',')
	{
		buffer++;
		i++;
		if (i >= len)
			return false;
	}
	
	while ((*buffer == ',') && (command_parameters_number < sizeof(command_parameters)))
	{
		buffer++;
		buffer = get_int(buffer,&command_parameters[command_parameters_number]);
		command_parameters_number++;
	}
	
	return true;
	
}

static BaseType_t command_receive (uint8_t * command)
{
	const TickType_t xDelay100ms = pdMS_TO_TICKS( 100UL );
	BaseType_t xStatus;
	uint8_t cmd;
	uint8_t cmd_in	= command[1];
	uint8_t cmd_in_b	= command[2];
	switch (cmd_in) 
	{
		case EXT_COMMAND_STOP:
				cmd = BLE_COMMAND_STOP;
				xStatus = xQueueSendToBack( xBLE_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_MONITORMODE:
				cmd = BLE_COMMAND_TRANSFER_RAW;
				xStatus = xQueueSendToBack( xBLE_Commands_Queue,&cmd, xDelay100ms );
		    break;
		case EXT_COMMAND_MPU_START:
				switch(cmd_in_b)
				{
					case '0':
						cmd = MPU_COMMAND_ON;
						break;
					case '1':
						cmd = MPU_COMMAND_GESTURE_ON;
						break;
					case '2':
						cmd = MPU_COMMAND_ROT_ON;
						break;
					case '3':
						cmd = MPU_COMMAND_EULER_ON;
						break;
					case '8':
						cmd = MPU_COMMAND_DTW_ON;
						break;
					case '9':
						cmd = MPU_COMMAND_DEBUG_ON;
						break;
					default:
						break;
				}
				xStatus = xQueueSendToBack( xMPU_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_MPU_STOP:
				switch(cmd_in_b)
					{
						case '0':
							cmd = MPU_COMMAND_OFF;
							break;
						case '1':
							cmd = MPU_COMMAND_GESTURE_OFF;
							break;
						case '2':
							cmd = MPU_COMMAND_ROT_OFF;
							break;
						case '3':
						cmd = MPU_COMMAND_EULER_OFF;
						case '8':
							cmd = MPU_COMMAND_DTW_OFF;
							break;
						case '9':
							cmd = MPU_COMMAND_DEBUG_OFF;
							break;
						default:
							break;
					}
				xStatus = xQueueSendToBack( xMPU_Commands_Queue,&cmd, xDelay100ms );
				cmd = BLE_COMMAND_STOP;
				xStatus = xQueueSendToBack( xBLE_Commands_Queue,&cmd, xDelay100ms );
		  	break;
		case EXT_COMMAND_MPU_MONITOR:
				cmd = MPU_COMMAND_RAW;
				xStatus = xQueueSendToBack( xMPU_Commands_Queue,&cmd, xDelay100ms );
				cmd = BLE_COMMAND_TRANSFER_RAW;
				xStatus = xQueueSendToBack( xBLE_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_MPU_QUATERN_MONITOR:
				cmd = MPU_COMMAND_QUATERN;
				xStatus = xQueueSendToBack( xMPU_Commands_Queue,&cmd, xDelay100ms );
				//cmd = BLE_COMMAND_TRANSFER_RAW;
				//xStatus = xQueueSendToBack( xBLE_Commands_Queue,&cmd, xDelay100ms );
				break;
		
		case EXT_COMMAND_ID:
					ble_tx_data_index = ble_get_device_name(ble_tx_data);
					ble_string_send(ble_tx_data, strlen((char *)ble_tx_data));
				break;
		case EXT_COMMAND_SW_VERSION:
						ble_string_send((uint8_t *)SOFTWARE_VERSION, strlen(SOFTWARE_VERSION));
				break;
		case EXT_COMMAND_MEM_CLEAR:
				cmd = MEM_COMMAND_ERASE;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_SAVE_START:
				cmd=  MEM_COMMAND_SAVE_START;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_SAVE_STOP:
				cmd = MEM_COMMAND_SAVE_STOP;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_MEM_STATUS:
				cmd = MEM_COMMAND_GET_STATE;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
		  	break;
		case EXT_COMMAND_MEM_DEBUG:
				cmd = MEM_COMMAND_READ_DEBUG;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				cmd = BLE_COMMAND_TRANSFER_RAW;
				xStatus = xQueueSendToBack( xBLE_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_TRANSFERMODE:
				cmd = MEM_COMMAND_READ_START;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				cmd = BLE_COMMAND_TRANSFER_RAW;
				xStatus = xQueueSendToBack( xBLE_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_STATISTICS_CALC:
		{
				statistics_time_t tstr, tstp;
				
				if (command_parameters_number < 6)
				{
					tstr.hour = 0;
					tstr.minute =  0;
					tstr.second = 0;
			
					tstp.hour = 0;
					tstp.minute =  0;
					tstp.second = 0;
				}
				else
				{
					tstr.hour = command_parameters[0];
					tstr.minute =  command_parameters[1];
					tstr.second = command_parameters[2];
			
					tstp.hour = command_parameters[3];
					tstp.minute =  command_parameters[4];
					tstp.second = command_parameters[5];
				}
		
				statistics_calc_start(tstr,tstp,command_parameters[6],command_parameters[7]);
				cmd = MEM_COMMAND_STATISTICS_CALC;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
		}	break;
		case EXT_COMMAND_STATISTICS_GET:
				cmd = MEM_COMMAND_STATISTICS_GET;
				xStatus = xQueueSendToBack( xMEM_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_GPS_START:			
				cmd = GPS_COMMAND_START;
				xStatus = xQueueSendToBack( xGPS_Commands_Queue,&cmd, xDelay100ms );
				vTaskResume(xGPS_task_handle);
		    break;
		case EXT_COMMAND_GPS_STOP:
				cmd = GPS_COMMAND_STOP;
				xStatus = xQueueSendToBack( xGPS_Commands_Queue,&cmd, xDelay100ms );
				break;
		case EXT_COMMAND_BAT_STATUS:
				memset(ble_tx_data,0,20);
				ble_tx_data_index = sprintf((char*)ble_tx_data,"%d mA,%dmV\n",HAL_ADC_Sample_GetLast(0)/50,HAL_ADC_Sample_GetLast(1)*2);
				ble_string_send(ble_tx_data,ble_tx_data_index);
		  	break;
		case EXT_COMMAND_UART_ENABLE:
//				cmd = BLE_COMMAND_UART_ENABLE;
				break;
		case EXT_COMMAND_UART_DISABLE:
	//			cmd= BLE_COMMAND_UART_DISABLE;
				break;
		default:
			xStatus = pdFAIL;
				break;

	}
	memset(command_parameters,0,sizeof(command_parameters));
	command_parameters_number = 0;
	return xStatus;
}


void xCMD_task(void *pvParameters)
{
	uint8_t *datain = (uint8_t*) pvParameters;
	const TickType_t xDelay10ms = pdMS_TO_TICKS( 10UL );
	
	for(;;)
	{
		//memcpy(command,pvParameters,3);
		command_len = strlen((char*)pvParameters);
		memcpy(command_buffer,pvParameters,command_len);
		if (command_len > 3)
		{
			parse_command(command_buffer,command_len);
		}
		
		if (pdFAIL == command_receive(command_buffer))
		{
			uint8_t cmd = HMI_COMMAND_CMD_REJECT;
			xQueueSendToBack( xHMI_Commands_Queue,&cmd, 0 );
			HAL_BLE_StringSend((uint8_t *) "@@", 2); // Accepted command
		}
		else
		{
			ble_string_send(command_buffer, 2); // Accepted command
		}
		
		vTaskDelay(xDelay10ms);
		vTaskSuspend(xCMD_task_handle);
	}
}
