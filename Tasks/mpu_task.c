/**
  * @file    mem_task.c
  * @author  Mariano G. Scasso
  * @version V1.0
  * @date    May 1st, 2017
  * @brief   	This file implements the functions of the mpu task. A mpu smachine with init, quaternion, raw and other states is defined. 
	*						The states transition is achived throught a commands queue.
	* 	      
	*
	*	@copyright COPYRIGHT(c) 2017 Redimec SRL
  *						Redistribution and use in source and binary forms, with or without modification,
  *						are permitted provided that the following conditions are met:
	*						1.Redistributions of source code must retain the above copyright notice,
  *     				this list of conditions and the following disclaimer.
  *   				2.Redistributions in binary form must reproduce the above copyright notice,
  *      				this list of conditions and the following disclaimer in the documentation and/or other materials 
	*							provided with the distribution.
  *   				3. Neither the name of STMicroelectronics nor the names of its contributors
  *      				may be used to endorse or promote products derived from this software
  *      				without specific prior written permission.
  *
  * 					THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * 					AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * 					IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * 					DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * 					FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * 					DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * 					SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * 					CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * 					OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * 					OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*  
	*********************************************************************************************************/



#include "mpu_task.h"
#include "hmi_task.h"
#include "ble_task.h"
#include "mem_task.h"
#include "gps_task.h"

#include "wise_board.h"
#include "nrf_gpio.h"
#include "app_timer.h"


#ifdef WISE_MPU_DWT_ENABLE
#include "dtw_test.h"
static bool dtw_on = false;
volatile bool dtw_do = false;
static uint16_t dtw_window = 10;
uint16_t dtw_exec_count  = 0;
#endif

#
#define FIXED_POINT_INTEGER_BITS 6     // Number of bits used for integer part of raw data.


#define IMU_MODE
uint8_t debug_filtro = 0;
TaskHandle_t xMPU_task_handle;
QueueHandle_t xMPU_Commands_Queue;	/*!< mpu commands queue */
uint32_t mpu_debug;
_APP_TIMER_DEF(mpu_timer);
static mpu_smachine_t smachine_MPU;

///


static uint8_t ble_tx_data[128];
static uint8_t ble_tx_data_index;


///testing
#ifdef IMU_MODE
#include "calibrarGPScoder.h"
#include "calibrarIMUcoder.h"
#include "calibrarMagnetometro.h"
#include "filtroCoder.h"
#include "mem_task.h"
#include "relativePos2Elliptic.h"
#include  "quaternion2Eul.h"

#define IMU_BUFFER_SIZE 10
#define MAG_BUFFER_SIZE 10 

#define IMU_PERIOD_MS 100


  c_fusion_internal_coder_insfilt filter;
  
	double ubicacionReferencia[3];
  float posAnterior[3];
  bool evaluarGPS;
	bool evaluarIMU;
	bool grabarFiltro;
	bool mag_calib_ok;
	bool gps_init;
	struct3_T salida;
	float gps_dt = 1.0;	
	
	struct1_T TIMU;
  float accel[IMU_BUFFER_SIZE * 3];
  float gyro[IMU_BUFFER_SIZE * 3];
  float mag[IMU_BUFFER_SIZE * 3];
//	float mag_ajustado[IMU_BUFFER_SIZE * 3];
	
	float mag_calib[MAG_BUFFER_SIZE * 3];
	uint8_t mag_calib_index=0;
  float A[9] = {1,0,0,0,1,0,0,0,1};//{1,1,1,1,1,1,1,1,1};//
  float b[3] = {0,0,0};
	
	struct0_T TGPS;
	uint32_t gps_time_ms;
  double lat[GPS_BUFFER_SIZE];
  double lon[GPS_BUFFER_SIZE];
  double alt[GPS_BUFFER_SIZE];
  float vN[GPS_BUFFER_SIZE];
	float vE[GPS_BUFFER_SIZE];
	float vD[GPS_BUFFER_SIZE];
	float covarianza[3];
	
	static bool ref_position_isset;
	double lla[3];
	float VGPS[3];
	uint8_t lla_index;
	filter_data_t to_mem; /*!< memory optimized gps struct*/
	uint32_t imu_time;
	
	#define FILTER_FRACTIONAL_BITS 11 
	// Convert 16-bit fixed-point to double
float fixed16_to_double(int16_t input, uint8_t fractional_bits)
{
    return ((float)input / (float)(1 << fractional_bits));
}

 int16_t float_to_fixed(float input, uint8_t fractional_bits)
{
    return (int16_t)(input * (1 <<  fractional_bits));
}
	
static void lla_fill(void)
{
	if (!ref_position_isset) //seteo posicion de referencia
	{
		ubicacionReferencia[0] = lat[lla_index];
		ubicacionReferencia[1] = lon[lla_index];
		ubicacionReferencia[2] = alt[lla_index];
		ref_position_isset = true;
		imu_time = 0;
		gps_fill_session_aux((uint8_t*)ubicacionReferencia,sizeof(double)*2);

	}
	
	if (lla_index >= GPS_BUFFER_SIZE)
		return;
		
	lla[0] = lat[lla_index];
	lla[1] = lon[lla_index];
	lla[2] = alt[lla_index]; 
	VGPS[0] = vN[lla_index];
	VGPS[1] = vE[lla_index];
	VGPS[2] = vD[lla_index];
	lla_index++;
}
bool gps_buffer_add(uint32_t time, double lat, double lon, float alt,float vN, float vE, float vD, float aAcc, float vAcc, float sAcc )
{
	if(gps_init)
		evaluarGPS = true;
	
	gps_time_ms = time;
	covarianza[0] = aAcc * aAcc /3;
	covarianza[1] = vAcc * vAcc /3;
	covarianza[2] = sAcc * sAcc /3;
	
	if (TGPS.index < GPS_BUFFER_SIZE)
	{
		TGPS.Latitud[TGPS.index] = lat;
		TGPS.Longitud[TGPS.index] = lon;
		TGPS.Altitud[TGPS.index] = alt;
		TGPS.velN[TGPS.index] = vN;
		TGPS.velE[TGPS.index] = vE;
		TGPS.velD[TGPS.index] = vD;
		TGPS.index++;
		return true;
	}
	else
	{
		
		return false;
	}
}
static bool imu_buffer_add(mpu_data_t * in)
{
	if (TIMU.index < IMU_BUFFER_SIZE)
	{
		TIMU.ax[TIMU.index] = (double) in->accel.x / (1<<15) * 2 * 9.8;
		TIMU.ay[TIMU.index] = (double) in->accel.y / (1<<15) * 2 * 9.8;
		TIMU.az[TIMU.index] = (double) in->accel.z / (1<<15) * 2 * 9.8;
		TIMU.gx[TIMU.index] = (double) in->gyro.x / (1<<15) * 250;
		TIMU.gy[TIMU.index] = (double) in->gyro.y / (1<<15) * 250;
		TIMU.gz[TIMU.index] = (double) in->gyro.z / (1<<15) * 250;
		TIMU.mx[TIMU.index] = (double) in->magn.x / (1<<15) * 4800;
		TIMU.my[TIMU.index] = (double) in->magn.y / (1<<15) * 4800;
		TIMU.mz[TIMU.index] = (double) in->magn.z / (1<<15) * 4800;
		
		
		TIMU.index++;
		return true;
	}
	else
		return false;
}

static bool mag_buffer_add()
{
	if (mag_calib_index >=	MAG_BUFFER_SIZE)
		return false;
	
	//memcpy(&mag_calib[mag_calib_index],m_in,sizeof(float) * IMU_BUFFER_SIZE);
	mag_calib[mag_calib_index] = TIMU.mx[0];
	
	mag_calib[mag_calib_index+MAG_BUFFER_SIZE] = TIMU.my[0];
	
	mag_calib[mag_calib_index+MAG_BUFFER_SIZE*2] = TIMU.mz[0];
	
	mag_calib_index++;
		return true;

}

static void filter_debug_ble()
{
		ble_tx_data_index = sprintf((char*) ble_tx_data,"[%f,%f,%f] [%f,%f,%f] [%.2f,%.2f,%.2f,%.2f] [%.2f,%.2f,%.2f]", ubicacionReferencia[0],ubicacionReferencia[1],ubicacionReferencia[2],
																																																salida.p[0],salida.p[1],salida.p[2],salida.cuaternion.a.data[0],salida.cuaternion.b.data[0],
																																																salida.cuaternion.c.data[0],salida.cuaternion.d.data[0],salida.velocidad[0],salida.velocidad[1],salida.velocidad[2]);
			ble_string_send(ble_tx_data,ble_tx_data_index);
}

static void filter_init(void)
{
	ref_position_isset = false;
	memset(accel,0,IMU_BUFFER_SIZE);
	memset(gyro,0,IMU_BUFFER_SIZE);
	memset(mag,0,IMU_BUFFER_SIZE);
	memset(&filter,0,sizeof(filter));
	
	for (int i = 0; i < 28; i++) {
	  filter.StateCovariance[29*i] = 0.001;
  }
	
	filter.pState[0] = 1;
	filter.pState[22] = 27.550;
	filter.pState[23] = -2.4169;
	filter.pState[24] = -16.0849;
	
	filter.QuaternionNoise[0] = 1.0E-6;
 filter.QuaternionNoise[1] = 1.0E-6;
 filter.QuaternionNoise[2] = 1.0E-6;
 filter.QuaternionNoise[3] = 1.0E-6;
 filter.PositionNoise[0] = 1.0E-6;
 filter.VelocityNoise[0] = 1.0E-6;

 filter.GyroscopeBiasNoise[0] = 1.6965748727670871E-5;
 filter.AccelerometerBiasNoise[0] = 0.082911938428878784;
 filter.AngularVelocityNoise[0] = 0.0010436082957312465;
 filter.AccelerationNoise[0] = 0.26262769103050232;
 filter.MagnetometerBiasNoise[0] = 0.18115557730197906;
 filter.GeomagneticVectorNoise[0] = 9.9999999747524271E-7;
 filter.PositionNoise[1] = 1.0E-6;
 filter.VelocityNoise[1] = 1.0E-6;
 
 filter.GyroscopeBiasNoise[1] = 2.8926244340254925E-5;
 filter.AccelerometerBiasNoise[1] = 0.054855264723300934;
 filter.AngularVelocityNoise[1] = 0.00098851346410810947;
 filter.AccelerationNoise[1] = 0.018492855131626129;
 filter.MagnetometerBiasNoise[1] = 0.079999998211860657;
 filter.GeomagneticVectorNoise[1] = 9.9999999747524271E-7;
 filter.PositionNoise[2] = 1.0E-6;
 filter.VelocityNoise[2] = 1.0E-6;
 
 filter.GyroscopeBiasNoise[2] = 4.1841463826131076E-5;
 filter.AccelerometerBiasNoise[2] = 0.045979466289281845;
 filter.AngularVelocityNoise[2] = 0.0010617680381983519;
 filter.AccelerationNoise[2] = 0.028735747560858727;
 filter.MagnetometerBiasNoise[2] = 0.14000000059604645;
 filter.GeomagneticVectorNoise[2] = 9.9999999747524271E-7;

	evaluarGPS=false;
	gps_init = false;
	gps_dt = 1.0;	
}


#endif
///
mpu_data_t data_to_send;

static bool m_debug_mode;


void mpu_debug_mode(bool mode)
{
	m_debug_mode = mode;
}

#define FPU_EXCEPTION_MASK 0x0000009F

void FPU_IRQHandler(void)
{
    uint32_t *fpscr = (uint32_t *)(FPU->FPCAR+0x40);
    (void)__get_FPSCR();

    *fpscr = *fpscr & ~(FPU_EXCEPTION_MASK);
}

void mpu_init(void)
{
    ret_code_t ret_code;
    // Initiate MPU driver
    ret_code = app_mpu_init();
 //   APP_ERROR_CHECK(ret_code); // Check for errors in return value
    
    // Setup and configure the MPU with intial values
    app_mpu_config_t p_mpu_config = MPU_DEFAULT_CONFIG(); // Load default values
    p_mpu_config.smplrt_div = 19;   // Change sampelrate. Sample Rate = Gyroscope Output Rate / (1 + SMPLRT_DIV). 19 gives a sample rate of 50Hz
    p_mpu_config.accel_config.afs_sel = AFS_2G; // Set accelerometer full scale range to 2G
		p_mpu_config.gyro_config.fs_sel = GFS_250DPS;
    ret_code = app_mpu_config(&p_mpu_config); // Configure the MPU with above values
   // APP_ERROR_CHECK(ret_code); // Check for errors in return value 
    

	// Enable magnetometer
		app_mpu_magn_config_t magnetometer_config;
		magnetometer_config.mode = CONTINUOUS_MEASUREMENT_100Hz_MODE;
    ret_code = app_mpu_magnetometer_init(&magnetometer_config);
   // APP_ERROR_CHECK(ret_code); // Check for errors in return value
}

void xMPU_task(void *pvParameters)
{
	const TickType_t xDelayIMU = pdMS_TO_TICKS( IMU_PERIOD_MS );
	uint8_t mag_calib_time = 1;	
	BaseType_t xStatus;
	hmi_command_t command_to_hmi;
	uint8_t twi_result = 0;
	mpu_init();
	smachine_MPU.state = MPU_STATE_INIT;
	m_debug_mode=false;
	filter_init();
	/*
	data_to_send.accel.x=1000;
	data_to_send.accel.y=1000;
	data_to_send.accel.z=1000;
	data_to_send.magn.x=1000;
	data_to_send.magn.y=1000;
	data_to_send.magn.z=1000;
	data_to_send.gyro.x=1000;
	data_to_send.gyro.y=1000;
	data_to_send.gyro.z=1000;*/
	
	for(;;)
	{

		uint32_t err_code = app_mpu_read_accel(&data_to_send.accel);
		err_code = app_mpu_read_gyro(&data_to_send.gyro);
		err_code = app_mpu_read_magnetometer(&data_to_send.magn,NULL);
		mag_calib_time++;
		
		xStatus = xQueueReceive( xMPU_Commands_Queue, &smachine_MPU.command, 0); /*!< commands receive */
		if (pdPASS == xStatus)
			switch(smachine_MPU.command)
			{
				case MPU_COMMAND_RAW:
				{
					if(m_debug_mode)
						m_debug_mode = false;
					else
						m_debug_mode = true;
					break;
				}
				default:
					break;
			}
		
		if (mag_calib_time % 10 == 0)
		{
			if (!mag_buffer_add())
			{
				//	calibrarMagnetometro(mag_calib,A,b);
					mag_calib_index = 0;
					mag_calib_ok = true; //espero a que se calibre el magnetometro
			}
				mag_calib_time = 1;
				
		}
	

		if(!imu_buffer_add(&data_to_send))
		{
			if(mag_calib_ok)
			{
				calibrarIMUcoder(&TIMU, IMU_BUFFER_SIZE/3,A,b, accel, gyro, mag);
				evaluarIMU = true;
			}
				TIMU.index = 0; //HABILITO EL BUFFER PARA SEGUIR
		}

		if (TGPS.index == GPS_BUFFER_SIZE)
		{
			calibrarGPScoder(&TGPS, GPS_BUFFER_SIZE/3,lat, lon, alt, vN, vE, vD);
			gps_init = true;
			TGPS.index = 0;
			lla_index = 0; //reseteo indice para pasar la posicion
		}
		
		if(evaluarGPS)
		{
			lla_fill();	
		}
		
			filtroCoder(&filter,accel, gyro, mag, lla, VGPS, ubicacionReferencia,covarianza, evaluarGPS, evaluarIMU,gps_dt,&salida);
		
			to_mem.pos[0] = salida.p[0];
			to_mem.pos[1] = salida.p[1];
			//	to_mem.pos[2] = salida.p[2];
			to_mem.vel[0] = float_to_fixed(salida.velocidad[0],FILTER_FRACTIONAL_BITS);
			to_mem.vel[1] = float_to_fixed(salida.velocidad[1],FILTER_FRACTIONAL_BITS);
			to_mem.vel[2] = float_to_fixed(salida.velocidad[2],FILTER_FRACTIONAL_BITS);
			to_mem.acc[0] = salida.aceleracion[0] * 10;
			to_mem.acc[1] = salida.aceleracion[1] * 10;
			to_mem.acc[2] = salida.aceleracion[2] * 10;
			to_mem.quat[0] = float_to_fixed(salida.cuaternion.a.data[0],FILTER_FRACTIONAL_BITS);
			to_mem.quat[1] = float_to_fixed(salida.cuaternion.b.data[0],FILTER_FRACTIONAL_BITS);
			to_mem.quat[2] = float_to_fixed(salida.cuaternion.c.data[0],FILTER_FRACTIONAL_BITS);
			to_mem.quat[3] = float_to_fixed(salida.cuaternion.d.data[0],FILTER_FRACTIONAL_BITS);
			to_mem.type = 50;
			to_mem.time = gps_get_ms();
		//	xMEM_add_to_buffer((uint8_t*)&to_mem,sizeof(to_mem));
			grabarFiltro = false;
			
			
			if (m_debug_mode)
				filter_debug_ble();
				
			evaluarGPS = false;
		
			imu_time++;
			vTaskDelay(xDelayIMU);
	}
}


uint8_t xMPU_get_state()
{
	return smachine_MPU.state;
}

bool xMPU_get_power()
{
	return true; //ojo
}
#ifdef WISE_MPU_DWT_ENABLE

bool xMPU_get_dwt_state()
{
	return dtw_on;
}

uint16_t xMPU_get_dwt_times()
{
	uint16_t times = dtw_exec_count;
	dtw_exec_count = 0;
	return times;
}
#endif
void mpu_task_init(void)
{
	
	xMPU_Commands_Queue = xQueueCreate( 3,sizeof(mpu_command_t));

	if (xMPU_Commands_Queue != NULL)
	{
		if(pdPASS != xTaskCreate(xMPU_task, "MPU", 2048, NULL, 1, &xMPU_task_handle))
		{
      APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
		}
	}
	
}

void filter_set_gps_dt(float dt)
{
	gps_dt = dt;
}

bool filter_save(void)
{
	to_mem.time = gps_get_ms();
	if (gps_init)
	{
		return xMEM_add_to_buffer((uint8_t*)&to_mem,sizeof(to_mem));
	}
	else
		return true;
}